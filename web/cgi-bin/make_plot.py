#!/home/devel/anaconda3/bin/python
# -*- coding: utf-8 -*-
#
#filename : make_plot.py
#author : Hirotaka Yuzurihara, Koyama
#Date : 2021/04/09 ~
#

import cgi
import cgitb
import sys
import os # to read query

import util_func
import plot_finesse

# funcsの代わりに、GUI関連のパッケージを読み込む
#import funcs as funcs
#import slackweb

#
# initialize cgi-bin
#
cgitb.enable()
form = cgi.FieldStorage()
print('Content-Type: text/html; charset=UTF-8\n')

#print(os.environ)
#print(cgi.parse_qs(os.environ['QUERY_STRING']))
print("form = %s" % form)
print("<br>")

#form_take_over=os.environ['QUERY_STRING'] # date_beg=2020-04-07&hour_beg=5&min_beg=0&sec_....
#print(os.environ['QUERY_STRING'])

# ラジオボックスの場合はこれでOK
pd_type = form.getvalue("pd_type", "power_detector")

# PDのようにチェックボックスの場合は、それをfor文でループさせるとかしないといけないかも
# /home/yuzu/git_local/work/past_data_viewer/cgi-bin/make_plot_sngl.py
# の書き方を参考に
# htmlは
# /home/yuzu/git_local/work/past_data_viewer/index_sngl_channel.html
# 

#
# Error check
#
# if gps_beg < gps_min or gps_end < gps_min:
#     error_message="gps_beg or gps_end should be larger than %d" % gps_min
#     print(error_message)
#     exit()



if form.getvalue("text_dump") is not None:
     val_text_dump=True

#
# Add options for x/y-ranges in dictionary
#
# dic_range = {} #dic["key"] = "value"

# if 'xmin_TimeSeries' in form:
#     dic_range["xmin_TimeSeries"] = float(form.getfirst("xmin_TimeSeries"))
# else:
#     dic_range["xmin_TimeSeries"] = float(gps_beg)
# if 'xmax_TimeSeries' in form:
#     dic_range["xmax_TimeSeries"] = float(form.getfirst("xmax_TimeSeries"))
# else:
#     dic_range["xmax_TimeSeries"] = float(gps_end)
# if 'ymin_TimeSeries' in form:
#     dic_range["ymin_TimeSeries"] = float(form.getfirst("ymin_TimeSeries"))
# else:
#     dic_range["ymin_TimeSeries"] = "none"    
# if 'ymax_TimeSeries' in form:
#     dic_range["ymax_TimeSeries"] = float(form.getfirst("ymax_TimeSeries"))
# else:
#     dic_range["ymax_TimeSeries"] = "none"    

# if 'xmin_WhitenedTimeSeries' in form:
#     dic_range["xmin_WhitenedTimeSeries"] = float(form.getfirst("xmin_WhitenedTimeSeries"))
# else:
#     dic_range["xmin_WhitenedTimeSeries"] = float(gps_beg)
# if 'xmax_WhitenedTimeSeries' in form:
#     dic_range["xmax_WhitenedTimeSeries"] = float(form.getfirst("xmax_WhitenedTimeSeries"))
# else:
#     dic_range["xmax_WhitenedTimeSeries"] = float(gps_end)
# if 'ymin_WhitenedTimeSeries' in form:
#     dic_range["ymin_WhitenedTimeSeries"] = float(form.getfirst("ymin_WhitenedTimeSeries"))
# else:
#     dic_range["ymin_WhitenedTimeSeries"] = "none"    
# if 'ymax_WhitenedTimeSeries' in form:
#     dic_range["ymax_WhitenedTimeSeries"] = float(form.getfirst("ymax_WhitenedTimeSeries"))
# else:
#     dic_range["ymax_WhitenedTimeSeries"] = "none"    
    

     
#
# set scale of x, y, z-axis
#        
if 'scale' in form:
    flag_scale = form.getvalue("scale", 'none')
else:
    flag_scale='none'
    
#print(flag_scale)
#print(filttype)    
#
# make plot by using funcs.py
#
# funcs.make_plot(plottype, cache_file, channel, gps_beg, gps_end,
#                 flag_scale, flag_text_dump=val_text_dump,
#                 nproc=nproc, filttype=filttype,
#                 fftlengths=fftlength, n_average=n_average, fmin=fmin, fmax=fmax,
#                 xmin=xmin, xmax=xmax, ymin=ymin, ymax=ymax, cbmin=cbmin, cbmax=cbmax)

# if type(plottype) is str:
#     plottype=[plottype] # if plottype is char, convert to list

print(pd_type)
    
    
# funcs.make_plot(flag_scale, dic_range, channel_state, bits_dq=bits_dq, flag_text_dump=val_text_dump,
#                 nproc=nproc, filttype=filttype,
#                 fftlengths=fftlength, n_average=n_average, method_average=method_average, fmin=fmin, fmax=fmax,
#                 cbmin=cbmin, cbmax=cbmax)

#cgi.print_form(form)
#print("a")


############################

sim_conf = {}

interferometer = "kagra"
base_name      = "DRFPMI"
type_of_pd_signal = "sw_power"
sw_dmod1_plotnum = 6
tf_dmod2_plotnum = 6
an_misal_plotnum = 6
# optical parameter
#   laser
sim_conf["k_inf_c_laser_power"] = "1"
#   eom
sim_conf["k_inf_c_f1_mod_frequency"] = "1"
sim_conf["k_inf_c_f1_mod_index"]     = "1"
sim_conf["k_inf_c_f2_mod_frequency"] = "1"
sim_conf["k_inf_c_f2_mod_index"]     = "1"
sim_conf["k_inf_c_num_of_sidebands"] = "1"
#   hom
sim_conf["k_inf_c_laser_use_hom"] = False
sim_conf["k_inf_c_num_of_hom_order"] = "1"
#   mirror
#       mibs
sim_conf["k_inf_c_mibs_mirror_transmittance"]         = "1"
sim_conf["k_inf_c_mibs_mirror_loss"]                  = "0"
sim_conf["k_inf_c_mirror_sec_isload_mirror_map_bs"]   = False
#       itmx
sim_conf["k_inf_c_itmx_mirror_transmittance"]         = "1"
sim_conf["k_inf_c_itmx_mirror_loss"]                  = "0"
sim_conf["k_inf_c_mirror_sec_isload_mirror_map_itmx"] = False
#       itmy
sim_conf["k_inf_c_itmy_mirror_transmittance"]         = "1"
sim_conf["k_inf_c_itmy_mirror_loss"]                  = "0"
sim_conf["k_inf_c_mirror_sec_isload_mirror_map_itmy"] = False
#       etmx
sim_conf["k_inf_c_etmx_mirror_transmittance"]         = "1"
sim_conf["k_inf_c_etmx_mirror_loss"]                  = "0"
sim_conf["k_inf_c_mirror_sec_isload_mirror_map_etmx"] = False
#       etmy
sim_conf["k_inf_c_etmy_mirror_transmittance"]         = "1"
sim_conf["k_inf_c_etmy_mirror_loss"]                  = "0"
sim_conf["k_inf_c_mirror_sec_isload_mirror_map_etmy"] = False
#       PRM
sim_conf["k_inf_c_prm_mirror_transmittance"]          = "0"
sim_conf["k_inf_c_prm_mirror_loss"]                   = "0"
sim_conf["k_inf_c_mirror_sec_isload_mirror_map_prm"]  = False
#       PR2
sim_conf["k_inf_c_pr2_mirror_transmittance"]          = "1"
sim_conf["k_inf_c_pr2_mirror_loss"]                   = "0"
sim_conf["k_inf_c_mirror_sec_isload_mirror_map_pr2"]  = False
#       PR3
sim_conf["k_inf_c_pr3_mirror_transmittance"]          = "1"
sim_conf["k_inf_c_pr3_mirror_loss"]                   = "0"
sim_conf["k_inf_c_mirror_sec_isload_mirror_map_pr3"]  = False
#       SRM
sim_conf["k_inf_c_srm_mirror_transmittance"]          = "1"
sim_conf["k_inf_c_srm_mirror_loss"]                   = "0"
sim_conf["k_inf_c_mirror_sec_isload_mirror_map_srm"]  = False
#       SR2
sim_conf["k_inf_c_sr2_mirror_transmittance"]          = "1"
sim_conf["k_inf_c_sr2_mirror_loss"]                   = "0"
sim_conf["k_inf_c_mirror_sec_isload_mirror_map_sr2"]  = False
#       SR3
sim_conf["k_inf_c_sr3_mirror_transmittance"]          = "1"
sim_conf["k_inf_c_sr3_mirror_loss"]                   = "0"
sim_conf["k_inf_c_mirror_sec_isload_mirror_map_sr3"]  = False
# length
sim_conf["k_inf_c_length_prm_pr2"]  = "1"
sim_conf["k_inf_c_length_pr2_pr3"]  = "1"
sim_conf["k_inf_c_length_pr3_bs"]  = "1"
sim_conf["k_inf_c_length_bs_itmx"]  = "1"
sim_conf["k_inf_c_length_itmx_etmx"]  = "1"
sim_conf["k_inf_c_length_bs_itmy"]  = "1"
sim_conf["k_inf_c_length_itmy_etmy"]  = "1"
sim_conf["k_inf_c_length_srm_sr2"]  = "1"
sim_conf["k_inf_c_length_sr2_sr3"]  = "1"
sim_conf["k_inf_c_length_sr3_bs"]  = "1"
# plot_conf
sim_conf["k_inf_c_samplingnum"]      = "1000"
sim_conf["k_inf_c_xaxis_lin"]       = True
sim_conf["k_inf_c_xaxis_log"]       = False
sim_conf["k_inf_c_yaxis_lin"]       = True
sim_conf["k_inf_c_yaxis_log"]       = False
sim_conf["k_inf_c_xaxis_range_beg"] = "-180"
sim_conf["k_inf_c_xaxis_range_end"] = "180"
# port
port_list = util_func.get_all_port_list()
# dof
sim_conf["kifo_dof_selection"] = True
sim_conf["kifo_dof"] = "DARM"
# 
for port in port_list:
    sim_conf["kifo_sw_power_%s"%port] = False
    # test
    sim_conf["kifo_sw_power_REFL"] = True
    sim_conf["kifo_sw_power_AS"] = True
# run finesse
inteferometer_config_list = [interferometer, type_of_pd_signal]
sim_conf["kgui_sw_dmod1_plotnum"] = sw_dmod1_plotnum
sim_conf["kgui_tf_dmod2_plotnum"] = tf_dmod2_plotnum
sim_conf["kgui_an_misal_plotnum"] = an_misal_plotnum

print(sim_conf)

model = util_func.get_model(sim_conf, inteferometer_config_list)
out = model.run()
print(model)

# plot
plot_title="test plot"
plot_finesse.plot_run(model, out, sim_conf, type_of_pd_signal, base_name, flag_web=True)

# output
#output_finesse.output(sim_conf, out, model, type_of_pd_signal, plot_title)