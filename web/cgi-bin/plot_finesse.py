import matplotlib.pyplot as plt
import matplotlib.cm as cm
import numpy as np
import math
import util_func


def plot_run(model, out, sim_conf, type_of_pd_signal, base_name, flag_web=False):
    ############################
    # plot
    x_plotscale = "linear"
    if sim_conf["k_inf_c_xaxis_log"]:
        x_plotscale = "log"   
    y_plotscale = "linear"
    if sim_conf["k_inf_c_yaxis_log"]:
        y_plotscale = "log"   
    ############################
    detectors = list(model.detectors.keys())
    
    if type_of_pd_signal=="sw_power":
        fig1      = plt.figure(figsize=(12.80, 7.20))
        plotnum   = len(detectors)
        v_plotnum = math.ceil(math.sqrt(plotnum))
        h_plotnum = v_plotnum
        fontsize   = util_func.calculate_plot_fontsize(plotnum, v_plotnum, h_plotnum)
        i = 1
        for pdname in detectors:
            s = model.detectors.get(pdname)
            print(s.getFinesseText())
            plt.subplot(v_plotnum,h_plotnum,i)
            pdname = "%s"%(pdname)
            #model.detectors.get(pdname).node
            axes_title = model.detectors.get(pdname).node
            plt.plot(out.x, (out['%s' % pdname])**2)
            plt.xscale(x_plotscale)
            plt.yscale(y_plotscale)
            plt.ylabel("Power[W]", fontsize=fontsize)
            plt.xlabel(out.xlabel.split()[0]+out.xlabel.split()[1], fontsize=fontsize)
            plt.title('%s' % axes_title, fontsize=fontsize)
            plt.tick_params(labelsize=fontsize)
            # 凡例の表示
            #plt.legend(fontsize=fontsize)
            # gridの表示
            plt.grid()
            # loop
            i += 1
        plt.tight_layout()
        figure_title = base_name+" "+sim_conf["kifo_dof"]+" "+"Power"
        fig1.suptitle(figure_title, fontsize=20)
        plt.subplots_adjust(top=0.9)
        plt.show(block=False)

        if flag_web:
            # save as png
            plt.savefig('/home/devel/finesse-gui/web/fig/hoge.png') # -----(2)

    if type_of_pd_signal=="sw_amptd":
        fig1      = plt.figure(figsize=(12.80, 7.20))
        portnum = 0
        if sim_conf['kifo_put_car_sw_amptd_flag']:# or dic_selected_setting_from_gui['put_car_tf_amptd_flag']:
            portnum=portnum+1
        if sim_conf['kifo_put_f1u_sw_amptd_flag']:# or dic_selected_setting_from_gui['put_car_tf_amptd_flag']:
            portnum=portnum+1
        if sim_conf['kifo_put_f1l_sw_amptd_flag']:# or dic_selected_setting_from_gui['put_car_tf_amptd_flag']:
            portnum=portnum+1
        if sim_conf['kifo_put_f2u_sw_amptd_flag']:# or dic_selected_setting_from_gui['put_car_tf_amptd_flag']:
            portnum=portnum+1
        if sim_conf['kifo_put_f2l_sw_amptd_flag']:# or dic_selected_setting_from_gui['put_car_tf_amptd_flag']:
            portnum=portnum+1
        if portnum==0:
            pass
        plotnum   = len(detectors)/portnum
        v_plotnum = math.ceil(math.sqrt(plotnum))
        h_plotnum = v_plotnum
        fontsize   = util_func.calculate_plot_fontsize(plotnum, v_plotnum, h_plotnum)
        i = 1
        k=0
        while k<len(detectors):
            plt.subplot(v_plotnum,h_plotnum,i)
            if sim_conf['kifo_put_car_sw_amptd_flag']:# or dic_selected_setting_from_gui['put_car_tf_amptd_flag']:
                print(k)
                label  = "carrier"
                plt.plot(out.x, out[detectors[k]], label=label)
                k=k+1
            if sim_conf['kifo_put_f1u_sw_amptd_flag']:# or dic_selected_setting_from_gui['put_car_tf_amptd_flag']:
                print(k)
                label  = "fsb1 upper"
                plt.plot(out.x, out[detectors[k]], label=label)
                k=k+1
            if sim_conf['kifo_put_f1l_sw_amptd_flag']:# or dic_selected_setting_from_gui['put_car_tf_amptd_flag']:
                print(k)
                label  = "fsb1 lower"
                plt.plot(out.x, out[detectors[k]], label=label)
                k=k+1
            if sim_conf['kifo_put_f2u_sw_amptd_flag']:# or dic_selected_setting_from_gui['put_car_tf_amptd_flag']:
                print(k)
                label  = "fsb2 upper"
                plt.plot(out.x, out[detectors[k]], label=label)
                k=k+1
            if sim_conf['kifo_put_f2l_sw_amptd_flag']:# or dic_selected_setting_from_gui['put_car_tf_amptd_flag']:
                print(k)
                label  = "fsb2 lower"
                plt.plot(out.x, out[detectors[k]], label=label)
                k=k+1
            detecternum = k-1
            axes_title = str(model.detectors.get(detectors[detecternum]).node) + " " + "mode"
            plt.xscale(x_plotscale)
            plt.yscale(y_plotscale)
            plt.ylabel("amplitude", fontsize=fontsize)
            plt.xlabel(out.xlabel.split()[0]+out.xlabel.split()[1], fontsize=fontsize)
            plt.title('%s' % axes_title, fontsize=fontsize)
            # 凡例の表示
            plt.legend(bbox_to_anchor=(1, 1), loc='upper right', borderaxespad=0, fontsize=fontsize)
            #plt.legend(fontsize=fontsize)
            # gridの表示
            plt.grid()
            # loop
            i += 1
        plt.tight_layout()
        figure_title = base_name+" "+sim_conf["kifo_dof"]+" "+"Amplitude"
        fig1.suptitle(figure_title, fontsize=20)
        plt.subplots_adjust(top=0.9)
        plt.show(block=False)

    if type_of_pd_signal=="sw_dmod1":
        fig1      = plt.figure(figsize=(12.80, 7.20))
        plotnum   = len(detectors)
        v_plotnum = math.ceil(math.sqrt(plotnum))
        h_plotnum = v_plotnum
        fontsize   = util_func.calculate_plot_fontsize(plotnum, v_plotnum, h_plotnum)
        i = 1
        for pdname in detectors:
            pd = model.detectors.get(pdname)
            plt.subplot(v_plotnum,h_plotnum,i)
            pdname = "%s"%(pdname)
            axes_title = str(model.detectors.get(pdname).node) + " " + "demodulated by " + str(model.detectors.get(pdname).phase1) + "[deg] " + str(model.detectors.get(pdname).f1) + "[Hz]"
            plt.plot(out.x, (out['%s' % pdname]))
            plt.xscale(x_plotscale)
            plt.yscale(y_plotscale)
            plt.ylabel("Power[W]", fontsize=fontsize)
            plt.xlabel(out.xlabel.split()[0]+out.xlabel.split()[1], fontsize=fontsize)
            plt.title('%s' % axes_title, fontsize=fontsize)
            plt.tick_params(labelsize=fontsize)
            # gridの表示
            plt.grid()
            # loop
            i += 1
        plt.tight_layout()
        figure_title = base_name+" "+sim_conf["kifo_dof"]+" "+"Demodulated signal"
        fig1.suptitle(figure_title, fontsize=20)
        plt.subplots_adjust(top=0.9)
        plt.show(block=False)
        
    if type_of_pd_signal=="tf_dmod2":# すべてバラバラに表示する
        fig1      = plt.figure(figsize=(12.8, 21.6))
        plotnum    = len(detectors)
        h_plotnum  = math.ceil(len(detectors)/2)
        v_plotnum  = 4
        fontsize   = util_func.calculate_plot_fontsize(plotnum, v_plotnum, h_plotnum)
        for i in range(len(detectors)):
            # プロットするpd
            pdname = detectors[i]
            # gainのsubplotの表示位置
            k1 = math.floor((i/h_plotnum))*2*h_plotnum + i%h_plotnum+1
            # phaseのsubplotの表示位置
            k2 = math.floor((i/h_plotnum))*2*h_plotnum + i%h_plotnum+1+h_plotnum
            print("check1001")
            #abs
            plt.subplot(v_plotnum,h_plotnum,k1)
            plt.plot(out.x, np.abs(out[detectors[i]]), color="red")
            plt.xscale(x_plotscale)
            plt.yscale(y_plotscale)
            plt.ylabel('Magnitude', fontsize=fontsize)
            plt.xlabel(out.xlabel.split()[0]+out.xlabel.split()[1], fontsize=fontsize)
            axes_title = str(model.detectors.get(pdname).node) + " \n" + "demodulated by " + str(model.detectors.get(pdname).phase1) + "[deg] " + str(model.detectors.get(pdname).f1) + "[Hz]"
            plt.title("magnitude "+axes_title, fontsize=fontsize)
            plt.tick_params(labelsize=fontsize)
            # gridの表示
            plt.grid()
            #phase
            plt.subplot(v_plotnum,h_plotnum,k2)
            plt.plot(out.x, np.angle(out[detectors[i]]), color="blue")
            plt.xscale(x_plotscale)
            plt.yscale(y_plotscale)
            plt.ylabel('phase', fontsize=fontsize)
            plt.xlabel(out.xlabel.split()[0]+out.xlabel.split()[1], fontsize=fontsize)
            axes_title = str(model.detectors.get(pdname).node) + " \n" + "demodulated by " + str(model.detectors.get(pdname).phase1) + "[deg] " + str(model.detectors.get(pdname).f1) + "[Hz]"
            plt.title("phase "+axes_title, fontsize=fontsize)
            plt.tick_params(labelsize=fontsize)
            # gridの表示
            plt.grid()
            i+=1
                
        plt.tight_layout()
        figure_title = base_name+" "+sim_conf["kifo_dof"]+" "+"transfer function"
        fig1.suptitle(figure_title, fontsize=20)
        plt.subplots_adjust(top=0.935)
        plt.show(block=False)

    if type_of_pd_signal=="st_type1":
        fig1      = plt.figure(figsize=(12.80, 7.20))
        plotnum   = len(detectors)
        v_plotnum = math.ceil(math.sqrt(plotnum))
        h_plotnum = v_plotnum
        fontsize   = util_func.calculate_plot_fontsize(plotnum, v_plotnum, h_plotnum)
        i = 1
        for pdname in detectors:
            s = model.detectors.get(pdname)
            print(s.getFinesseText())
            plt.subplot(v_plotnum,h_plotnum,i)
            pdname = "%s"%(pdname)
            # bugあり
            axes_title = str(model.detectors.get(pdname).node) + " " + "demodulated by " + str(model.detectors.get(pdname).phase1) + "[deg] " + str(model.detectors.get(pdname).f1) + "[Hz]"
            plt.plot(out.x, out['%s' % pdname])
            plt.xscale(x_plotscale)
            plt.yscale(y_plotscale)
            plt.ylabel("m/sqrt(Hz)", fontsize=fontsize)
            plt.xlabel(out.xlabel.split()[0]+out.xlabel.split()[1], fontsize=fontsize)
            plt.title('%s' % axes_title, fontsize=fontsize)
            plt.tick_params(labelsize=fontsize)
            # gridの表示
            plt.grid()
            # loop
            i += 1
        plt.tight_layout()
        figure_title = base_name+" "+sim_conf["kifo_dof"]+" "+"sensitivity shotnoise"
        fig1.suptitle(figure_title, fontsize=20)
        plt.subplots_adjust(top=0.9)
        plt.show(block=False)

    if type_of_pd_signal=="an_misal":
        fig1      = plt.figure(figsize=(12.80, 7.20))
        plotnum   = len(detectors)
        v_plotnum = math.ceil(math.sqrt(plotnum))
        h_plotnum = v_plotnum
        fontsize   = util_func.calculate_plot_fontsize(plotnum, v_plotnum, h_plotnum)
        i = 1
        for pdname in detectors:
            s = model.detectors.get(pdname)
            print(s.getFinesseText())
            plt.subplot(v_plotnum,h_plotnum,i)
            pdname = "%s"%(pdname)
            #model.detectors.get(pdname).node
            axes_title = model.detectors.get(pdname).node
            plt.plot(out.x, (out['%s' % pdname]))
            plt.xscale(x_plotscale)
            plt.yscale(y_plotscale)
            plt.ylabel("label[]", fontsize=fontsize)
            plt.xlabel(out.xlabel.split()[0]+out.xlabel.split()[1], fontsize=fontsize)
            plt.title('%s' % axes_title, fontsize=fontsize)
            plt.tick_params(labelsize=fontsize)
            # 凡例の表示
            #plt.legend(fontsize=fontsize)
            # gridの表示
            plt.grid()
            # loop
            i += 1
        plt.tight_layout()
        figure_title = base_name+" "+sim_conf["kifo_dof"]+" "+"Power"
        fig1.suptitle(figure_title, fontsize=20)
        plt.subplots_adjust(top=0.9)
        plt.show(block=False)


