# finesse\_GUI.py

# Features

finesse\_GUI is a graphical user interface for finesse simulation tool.
`finesse_GUI.py` uses `pykat` and `pysimplegui`.

[pykat][1] is a Python wrapper package to use finesse.
[pysimplegui][2] is a Python package to develop gui. 

This tool is developed for improving work efficiency in KAGRA commissioning.
In this tool, DRFPMI configuration has already build.
Therefore, we can omit interferometer model building.
The simulation is performed by selecting the conditions displayed on the screen.
We can run the simulation by selecting the simulation conditions displayed on the screen.
By changing parameters and selecting additional options, we can make the simulation closer to the actual experimental conditions.

# Requirement

* Python 3.8.2
* pysimplegui 4.46.0 
* matplotlib 3.4.2
* pykat 1.2.1 

Installation under [Anaconda][3] environment  is recommended.

```
$ conda create --name igwn-py38 python=3.8.2
```

# Installation

Install `pysimplegui` and `pykat` in the conda environment

```
$ conda activate igwn-py38
$ conda install -c gwoptics pykat
$ conda install -c conda-forge pysimplegui=4.46.0 
$ conda install -c conda-forge matplotlib=3.4.2 
```

# Usage

Please Run `run_gui.py`

```
$ conda activate igwn-py38
$ cd gui/bin
$ python run_gui.py
```

# Note


# developer
* Naoki Koyama
* Chiaki Hirose
* Hirotaka yuzurihara
* Keiko Kokeyama
* Osamu Miyakawa

# Author
* Naoki Koyama
* mail: f22l004a\*mail.cc.niigata-u.ac.jp


[1]:	https://www.gwoptics.org/finesse/
[2]:	https://pysimplegui.readthedocs.io/en/latest/
[3]:	https://computing.docs.ligo.org/conda/environments/igwn-py38/
