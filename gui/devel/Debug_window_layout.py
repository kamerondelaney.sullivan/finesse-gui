import PySimpleGUI as sg
import util_func
import util_func_pysimplegui
import My_simplified_optics

# util_func_pysimplegui.frame_layout(layouts,frame_keys)
# util_func_pysimplegui.column_layout(layouts,column_keys)
# util_func_pysimplegui.tabgroup_layout(layouts,tab_keys)
# util_func_pysimplegui.Debug_window(window)

def make_window():
    #####
    layout = [
        [sg.Sizer(5,0),sg.Multiline(default_text="", size=(60, 60),key="model_edit_box")],
        [sg.Sizer(0,0),sg.Text("Please push button to update the model.",key="run_check_model_caution")],
        [sg.Sizer(0,0),sg.Button("check model",key="run_check_model")],
    ]
    layouts    = [layout]
    frame_keys = ["left_frame"]
    display_texts = [""]
    left_frame = [util_func_pysimplegui.frame_layout(layouts,frame_keys,display_texts)] 
    ######

    ######

    right_layout_bottom_1= [
        [sg.Text('related line viewer')],
        [sg.Combo(values=[], key = "related_line_command_box",   enable_events=True,size=(30,1))],
        [sg.Combo(values=[], key = "related_line_object_box",    enable_events=True, size=(30,1))],
        [sg.Combo(values=[], key = "related_line_pd_box",        enable_events=True, size=(30,1))],
        [sg.Combo(values=[], key = "related_line_remaining_box", enable_events=True, size=(30,1))],
        [sg.Multiline(default_text='', key="related_line_box",   enable_events=True, size=(30, 23))]
    ]
    right_layout_bottom_2 = [
        [sg.Text('Optical parameter viewer')],
        [sg.Combo(values=[],key="optical_parameter_combo_component", enable_events=True, size=(30,1))],
        [sg.Listbox(values=[],  key="optical_parameter_combo_parameter", enable_events=True, size=(30,15))],
        [sg.Multiline(default_text='', key="optical_parameter_box", size=(30, 5))],
        [sg.Button("fix",key="Optical_parameter_viewr_fix"),sg.Button("cansel",key="Optical_parameter_viewr_cansel")]
    ]
    right_layout_bottom_3 = [
        [sg.Text('Cavity parameter viewer')],
        [sg.Combo(values=[],key="cavity_parameter_combo_cavity", enable_events=True, size=(30,1))],
        [sg.Listbox(values=[],key="cavity_parameter_combo_para", enable_events=True, size=(30,15))],
        [sg.Multiline(default_text='', size=(30, 5),key="cavity_parameter_box")]
    ]
    right_layout_bottom_4 = [
        [sg.Text('beam patameter viewer'),sg.Button("Run",key="right_layout_bottom_4_Run_Button")],
        [sg.Combo(values=[],key="beam_parameter_combo_port", enable_events=True, size=(30,1))],
        [sg.Listbox(values=[],key="beam_parameter_combo_para", enable_events=True, size=(30,15))],
        [sg.Multiline(default_text='', size=(30, 5),key="beam_parameter_box")]
    ]

    layouts    = [right_layout_bottom_1,right_layout_bottom_2,right_layout_bottom_3,right_layout_bottom_4]
    frame_keys = ["right_layout_bottom_1","right_layout_bottom_2","right_layout_bottom_3","right_layout_bottom_4"]
    display_texts =["","","",""]
    right_frame_bottom_child_1 = [util_func_pysimplegui.frame_layout(layouts,frame_keys,display_texts)]
    right_frame_bottom_child_2 = [
        [sg.Multiline(default_text='', size=(140, 25),key="right_frame_bottom_child_2_pythoncodepanel")],
        [sg.Button("Run this code", key='right_frame_bottom_child_2_Run_code'),sg.Button("Clear dialog", key='right_frame_bottom_child_2_cleardialogbutton')],
        ]


    right_frame_bottom_child_3 = [
        [sg.Text('Listbox with search')],
        [sg.Input(size=(140, 1), enable_events=True, key='right_frame_bottom_child_3_input')],
        [sg.Listbox([""], size=(140, 20), enable_events=True, key='right_frame_bottom_child_3_list')]
    ]
    layouts = [right_frame_bottom_child_1,right_frame_bottom_child_2,right_frame_bottom_child_3]
    tab_keys = ["Tab1","Tab2","Tab3"]
    right_frame_bottom_child_tabgroup = [util_func_pysimplegui.tabgroup_layout(layouts,tab_keys)]

    right_frame_bottom = util_func_pysimplegui.frame_layout([right_frame_bottom_child_tabgroup],["right_frame_bottom_key"],[""])
    ######

    ######
    right_layout_top_1_child_1 = [[sg.Multiline(default_text='', size=(140, 30),key="right_layout_top_1_child_stdoutpanel",background_color = "black",text_color = "yellow")]]
    right_layout_top_1_child_2 = [[sg.Button("GUI_components_tree",key="right_layout_top_1_child_GUI_components_tree"),sg.Button("Pysimplegui Debug window",key="right_layout_top_1_child_ysimplegui_Debug_window")]]
    right_layout_top_1_child_3 = [[sg.Check("printing parameter values in console",key="right_layout_top_1_child_3_printinconsole")]]
    layouts = [right_layout_top_1_child_1,right_layout_top_1_child_2,right_layout_top_1_child_3]
    tab_keys = ["Tab1","Tab2","Tab3"]
    right_layout_top_1 = [util_func_pysimplegui.tabgroup_layout(layouts,tab_keys)]

    layouts = [right_layout_top_1]
    frame_keys = ["right_layout_top_1"]
    display_texts = [""]
    right_frame_top = util_func_pysimplegui.frame_layout(layouts,frame_keys,display_texts)
    ######

    right_frame = [
        right_frame_top,
        right_frame_bottom
    ]

    Debug_tab_layout = [util_func_pysimplegui.frame_layout([left_frame,right_frame],["left","right"],["",""])]

    return Debug_tab_layout
    """
    def get_debug_window():
        return Debug_window_layout
    """