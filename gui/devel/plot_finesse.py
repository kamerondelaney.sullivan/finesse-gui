import matplotlib
import matplotlib.pyplot as plt
import matplotlib.cm as cm
import numpy as np
import math
import util_func

def get_figure(detectors, out, sim_conf,figure_title,axes_titles,x_labels,y_labels):

    ############################
    # plot
    x_plotscale = "linear"
    if sim_conf["k_inf_c_xaxis_log"]:
        x_plotscale = "log"   
    y_plotscale = "linear"
    if sim_conf["k_inf_c_yaxis_log"]:
        y_plotscale = "log"   
    ############################
    #detectors = list(model.detectors.keys())
    
    fig      = plt.figure(figsize=(7, 4),dpi=100)
    fontsize   = util_func.calculate_plot_fontsize(1, 1, 1)
    plt.subplot(1,1,1)
    i = 0
    for pdname in detectors:
        plt.plot(out.x, (out[pdname]),label=pdname)

    plt.xscale(x_plotscale)
    plt.yscale(y_plotscale)

    plt.ylabel(y_labels[0], fontsize=fontsize)
    plt.xlabel(out.xlabel.split()[0]+out.xlabel.split()[1], fontsize=fontsize)

    #plt.title('%s' % axes_title, fontsize=fontsize)
    plt.tick_params(labelsize=fontsize)
    # 凡例の表示
    plt.legend(fontsize=fontsize)
    # gridの表示
    plt.grid()

    plt.tight_layout()

    fig.suptitle(figure_title, fontsize=20)
    plt.subplots_adjust(top=0.9)
    #plt.show(block=False)
    print("running plot_finesse.get_figure")
    return fig

def get_figure_title(type_of_pd_signal,sim_conf,base_name):
    if type_of_pd_signal == "import_kat_style":
        figure_title = "import kat script"
    elif type_of_pd_signal=="sw_power":
        figure_title = base_name+" "+sim_conf["kifo_dof"]+" "+"Power"
    elif type_of_pd_signal=="sw_amptd":
        figure_title = base_name+" "+sim_conf["kifo_dof"]+" "+"Amplitude"
    elif type_of_pd_signal=="sw_dmod1":
        figure_title = base_name+" "+sim_conf["kifo_dof"]+" "+"Demodulated signal"
    elif type_of_pd_signal=="sw_dmodp":
        figure_title = base_name+" "+sim_conf["kifo_dof"]+" "+"plotting optimal demodulation phase"
    elif type_of_pd_signal=="tf_dmod2":
        figure_title = base_name+" "+sim_conf["kifo_dof"]+" "+"transfer function"
    elif type_of_pd_signal=="st_type1":
        figure_title = base_name+" "+sim_conf["kifo_dof"]+" "+"sensitivity shotnoise"
    elif type_of_pd_signal=="st_type2":
        figure_title = base_name+" "+sim_conf["kifo_dof"]+" "+"sensitivity"
    elif type_of_pd_signal=="swan_mirot_power":
        figure_title = base_name+" "+ "mirror rotation " +" "+"Power"
    elif type_of_pd_signal=="swan_mirot_amptd":
        figure_title = base_name+" "+ "mirror rotation " +" "+"amplitude"
    elif type_of_pd_signal=="swan_mirot_dmod1":
        figure_title = base_name+" "+ "mirror rotation " +" "+"WFS"
    elif type_of_pd_signal=="swan_gouyp_power":
        figure_title = base_name+" "+ "gouy phase sweep " +" "+"Power"
    elif type_of_pd_signal=="swan_gouyp_amptd":
        figure_title = base_name+" "+ "gouy phase sweep " +" "+"amplitude"
    elif type_of_pd_signal=="swan_gouyp_dmod1":
        figure_title = base_name+" "+ "gouy phase sweep " +" "+"WFS"
    elif type_of_pd_signal=="swan_dmodp_dmod1":
        figure_title = base_name+" "+ "demodulation phase sweep " +" "
    elif type_of_pd_signal=="tfan_dmod2_mirot":
        figure_title = base_name+" "+ "rotation transfer function " +" "
    elif type_of_pd_signal=="st_type1":
        figure_title = base_name+" "+sim_conf["kifo_dof"]+" "+"sensitivity shotnoise"
    elif type_of_pd_signal=="st_type1":
        figure_title = base_name+" "+sim_conf["kifo_dof"]+" "+"sensitivity shotnoise"
    
    return figure_title

def plot_run(model, out, sim_conf, type_of_pd_signal, base_name,show_detectors):
    ############################
    # plot
    x_plotscale = "linear"
    if sim_conf["k_inf_c_xaxis_log"]:
        x_plotscale = "log"   
    y_plotscale = "linear"
    if sim_conf["k_inf_c_yaxis_log"]:
        y_plotscale = "log"   
    ############################
    if show_detectors==[]:
        detectors = list(model.detectors.keys())
    else:
        detectors = show_detectors

    if type_of_pd_signal == "import_kat_style":
        fig1      = plt.figure(figsize=(7, 4),dpi=100)
        plotnum   = len(detectors)
        v_plotnum = math.ceil(math.sqrt(plotnum))
        h_plotnum = v_plotnum
        fontsize   = util_func.calculate_plot_fontsize(plotnum, v_plotnum, h_plotnum)
        i = 1
        for pdname in detectors:
            s = model.detectors.get(pdname)
            #print(s.getFinesseText())
            ax = fig1.add_subplot(v_plotnum,h_plotnum,i)
            pdname = "%s"%(pdname)
            #model.detectors.get(pdname).node
            axes_title = model.detectors.get(pdname).node
            ax.plot(out.x, (out[pdname]), label=pdname)
            ax.set_xscale("linear")
            ax.set_yscale("linear")
            ax.set_ylabel("A.U.", fontsize=fontsize)
            ax.set_xlabel(out.xlabel.split()[0]+out.xlabel.split()[1], fontsize=fontsize)
            ax.set_title('%s' % axes_title, fontsize=fontsize)
            ax.tick_params(labelsize=fontsize)
            # 凡例の表示
            ax.legend(fontsize=fontsize)
            # gridの表示
            ax.grid(True)
            # loop
            i += 1
        plt.tight_layout()
        fig1.suptitle("あとで代入する", fontsize=20)
        plt.subplots_adjust(top=0.9)
        plt.show(block=False)
        #fig = plt.gcf()
        if sim_conf["kgui_plot_in_gui"]:
            #fig1.set_size_inches(9.5, 5.063)
            #fig1 = plt.gcf()
            return fig1
        pass
    elif type_of_pd_signal=="sw_power":
        #fig1      = plt.figure(figsize=(12.80, 7.20))
        fig1      = plt.figure(figsize=(7, 4),dpi=100)
        plotnum   = len(detectors)
        v_plotnum = math.ceil(math.sqrt(plotnum))
        h_plotnum = v_plotnum
        fontsize   = util_func.calculate_plot_fontsize(plotnum, v_plotnum, h_plotnum)
        i = 1
        for pdname in detectors:
            s = model.detectors.get(pdname)
            #print(s.getFinesseText())
            ax = fig1.add_subplot(v_plotnum,h_plotnum,i)
            pdname = "%s"%(pdname)
            #model.detectors.get(pdname).node
            axes_title = model.detectors.get(pdname).node
            ax.plot(out.x, (out[pdname]), label=pdname)
            ax.set_xscale(x_plotscale)
            ax.set_yscale(y_plotscale)
            ax.set_ylabel("Power[W]", fontsize=fontsize)
            ax.set_xlabel(out.xlabel.split()[0]+out.xlabel.split()[1], fontsize=fontsize)
            ax.set_title('%s' % axes_title, fontsize=fontsize)
            ax.tick_params(labelsize=fontsize)
            # 凡例の表示
            ax.legend(fontsize=fontsize)
            # gridの表示
            ax.grid(True)
            # loop
            i += 1
        plt.tight_layout()
        figure_title = base_name+" "+sim_conf["kifo_dof"]+" "+"Power"
        fig1.suptitle(figure_title, fontsize=20)
        plt.subplots_adjust(top=0.9)
        #plt.show(block=False)
        #fig = plt.gcf()
        if sim_conf["kgui_plot_in_gui"]:
            #fig1.set_size_inches(9.5, 5.063)
            #fig1 = plt.gcf()
            return fig1
    
    elif type_of_pd_signal=="sw_amptd":
        fig1      = plt.figure(figsize=(7, 4),dpi=100)
        plotnum   = len(detectors)
        v_plotnum = math.ceil(math.sqrt(plotnum))
        h_plotnum = v_plotnum
        fontsize   = util_func.calculate_plot_fontsize(plotnum, v_plotnum, h_plotnum)
        i = 1
        for pdname in detectors:
            print(f"i={i}")
            s = model.detectors.get(pdname)
            print(s.getFinesseText())
            plt.subplot(v_plotnum,h_plotnum,i)
            pdname = "%s"%(pdname)
            #model.detectors.get(pdname).node
            axes_title = model.detectors.get(pdname).node
            plt.plot(out.x, (out['%s' % pdname]), label=pdname)
            plt.xscale(x_plotscale)
            plt.yscale(y_plotscale)
            plt.ylabel("amplitude[sqrt(Watt)]", fontsize=fontsize)
            plt.xlabel(out.xlabel.split()[0]+out.xlabel.split()[1], fontsize=fontsize)
            plt.title('%s' % axes_title, fontsize=fontsize)
            plt.tick_params(labelsize=fontsize)
            # 凡例の表示
            plt.legend(fontsize=fontsize)
            # gridの表示
            plt.grid()
            # loop
            i += 1
        plt.tight_layout()
        figure_title = base_name+" "+sim_conf["kifo_dof"]+" "+"Amplitude"
        fig1.suptitle(figure_title, fontsize=20)
        plt.subplots_adjust(top=0.9)
        plt.show(block=False)
        if sim_conf["kgui_plot_in_gui"]:
            #fig1.set_size_inches(9.5, 5.063)
            #fig1 = plt.gcf()
            return fig1

    elif type_of_pd_signal=="sw_dmod1":
        fig1      = plt.figure(figsize=(7, 4),dpi=100)
        plotnum   = len(detectors)
        v_plotnum = math.ceil(math.sqrt(plotnum))
        h_plotnum = v_plotnum
        fontsize   = util_func.calculate_plot_fontsize(plotnum, v_plotnum, h_plotnum)
        i = 1
        for pdname in detectors:
            pd = model.detectors.get(pdname)
            plt.subplot(v_plotnum,h_plotnum,i)
            pdname = "%s"%(pdname)
            axes_title = str(model.detectors.get(pdname).node) + " " + "demodulated by " + str(model.detectors.get(pdname).phase1) + "[deg] " + str(model.detectors.get(pdname).f1) + "[Hz]"
            plt.plot(out.x, (out['%s' % pdname]))
            plt.xscale(x_plotscale)
            plt.yscale(y_plotscale)
            plt.ylabel("[W]", fontsize=fontsize)
            plt.xlabel(out.xlabel.split()[0]+out.xlabel.split()[1], fontsize=fontsize)
            plt.title('%s' % axes_title, fontsize=fontsize)
            plt.tick_params(labelsize=fontsize)
            # gridの表示
            plt.grid()
            # loop
            i += 1
        plt.tight_layout()
        figure_title = base_name+" "+sim_conf["kifo_dof"]+" "+"Demodulated signal"
        fig1.suptitle(figure_title, fontsize=20)
        plt.subplots_adjust(top=0.9)
        plt.show(block=False)
        if sim_conf["kgui_plot_in_gui"]:
            #fig1.set_size_inches(9.5, 5.063)
            #fig1 = plt.gcf()
            return fig1
    
    elif type_of_pd_signal=="sw_dmodp":
        fig1      = plt.figure(figsize=(7, 4),dpi=100)
        plotnum   = len(detectors)
        v_plotnum = math.ceil(math.sqrt(plotnum))
        h_plotnum = v_plotnum
        fontsize   = util_func.calculate_plot_fontsize(plotnum, v_plotnum, h_plotnum)
        i = 1
        for pdname in detectors:
            pd = model.detectors.get(pdname)
            plt.subplot(v_plotnum,h_plotnum,i)
            pdname = "%s"%(pdname)
            axes_title = str(model.detectors.get(pdname).node) + " " + str(model.detectors.get(pdname).f1) + "[Hz]"
            plt.plot(out.x, (out[pdname]))
            max = out.x[out[pdname].argmax()]
            s = "Optimal phase=%s[deg]"%max
            plt.text(max, out[pdname][out[pdname].argmax()], s)
            plt.scatter(max, out[pdname][out[pdname].argmax()], marker='.',c="black") 
            plt.xscale(x_plotscale)
            plt.yscale(y_plotscale)
            #plt.ylabel("Power[W]", fontsize=fontsize)
            #plt.xlabel(out.xlabel.split()[0]+out.xlabel.split()[1], fontsize=fontsize)
            plt.xlabel("demodulation phase[deg]")
            plt.ylabel("error signal[A.U.]") 
            plt.title('%s' % axes_title, fontsize=fontsize)
            plt.tick_params(labelsize=fontsize)
            # gridの表示
            plt.grid()
            # loop
            i += 1
        plt.tight_layout()
        figure_title = base_name+" "+sim_conf["kifo_dof"]+" "+"plotting optimal demodulation phase"
        fig1.suptitle(figure_title, fontsize=20)
        plt.subplots_adjust(top=0.9)
        plt.show(block=False)
        if sim_conf["kgui_plot_in_gui"]:
            #fig1.set_size_inches(9.5, 5.063)
            #fig1 = plt.gcf()
            return fig1
    
    elif type_of_pd_signal=="tf_dmod2":# すべてバラバラに表示する
        if sim_conf["kifo_tf_dmod2_matrix"]:

            fig1   = plt.figure(figsize=(7, 4),dpi=100)

            column_labels=["DC gain"]
            row_labels=[]
            data = []

            column_labels=[""]#この列はmagnitudeとphaseを縦に並べる列
            for i in range(len(model.detectors.keys())):
                column_labels.append(detectors[i])
            
            data_row_1=["magnitude"]
            for pdname in model.detectors.keys():
                data_row_1.append(abs(out[pdname][0]))
            data_row_2=["phase"]
            for pdname in model.detectors.keys():
                data_row_2.append(np.angle(out[pdname][0]))
            data = [data_row_1,
                    data_row_2
            ]

            plt.axis('tight')
            plt.axis('off')

            the_table = plt.table(cellText=data,colLabels=column_labels,loc="center")
            the_table.auto_set_font_size(False)
            the_table.set_fontsize(9)
        else:

            #fig1      = plt.figure(figsize=(12.8, 21.6))
            if len(detectors)==1:
                fig1   = plt.figure(figsize=(7, 4),dpi=100)
            else:
                #fig1   = plt.figure(figsize=(7, 10),dpi=100)
                fig1   = plt.figure(figsize=(7, 4),dpi=100)
            plotnum    = len(detectors)
            if len(detectors)==1 or len(detectors)==2:
                v_plotnum = 2
            else:
                v_plotnum  = 4
            #h_plotnum  = math.ceil(len(detectors)/2)
            h_plotnum  = math.ceil(2*len(detectors)/v_plotnum)
            fontsize   = util_func.calculate_plot_fontsize(plotnum, v_plotnum, h_plotnum)   

            for i in range(len(detectors)):
                # プロットするpd
                pdname = detectors[i]
                # gainのsubplotの表示位置
                k1 = math.floor((i/h_plotnum))*2*h_plotnum + i%h_plotnum+1
                # phaseのsubplotの表示位置
                k2 = math.floor((i/h_plotnum))*2*h_plotnum + i%h_plotnum+1+h_plotnum
                print("check1001")
                #abs
                plt.subplot(v_plotnum,h_plotnum,k1)
                plt.plot(out.x, np.abs(out[detectors[i]]), color="red")
                plt.xscale(x_plotscale)
                plt.yscale(y_plotscale)
                plt.ylabel('Magnitude[W]', fontsize=fontsize)
                plt.xlabel(out.xlabel.split()[0]+out.xlabel.split()[1], fontsize=fontsize)
                axes_title = str(model.detectors.get(pdname).node) + " \n" + "demodulated by " + str(model.detectors.get(pdname).phase1) + "[deg] " + str(model.detectors.get(pdname).f1) + "[Hz]"
                plt.title("magnitude "+axes_title, fontsize=fontsize)
                plt.tick_params(labelsize=fontsize)
                # gridの表示
                plt.grid()
                #phase
                plt.subplot(v_plotnum,h_plotnum,k2)
                plt.plot(out.x, np.angle(out[detectors[i]]), color="blue")
                plt.xscale(x_plotscale)
                plt.yscale("linear")
                #plt.yscale(y_plotscale)
                plt.ylabel('phase [rad]', fontsize=fontsize)
                plt.xlabel(out.xlabel.split()[0]+out.xlabel.split()[1], fontsize=fontsize)
                axes_title = str(model.detectors.get(pdname).node) + " \n" + "demodulated by " + str(model.detectors.get(pdname).phase1) + "[deg] " + str(model.detectors.get(pdname).f1) + "[Hz]"
                plt.title("phase "+axes_title, fontsize=fontsize)
                plt.tick_params(labelsize=fontsize)
                # gridの表示
                plt.grid()
                i+=1

        plt.tight_layout()
        figure_title = base_name+" "+sim_conf["kifo_dof"]+" "+"transfer function"
        fig1.suptitle(figure_title, fontsize=20)
        plt.subplots_adjust(top=0.935)
        plt.show(block=False)
        if sim_conf["kgui_plot_in_gui"]:
            #fig1.set_size_inches(9.5, 5.063)
            #fig1 = plt.gcf()
            return fig1

    elif type_of_pd_signal=="st_type1":
        fig1      = plt.figure(figsize=(7, 4),dpi=100)
        plotnum   = len(detectors)
        v_plotnum = math.ceil(math.sqrt(plotnum))
        h_plotnum = v_plotnum
        fontsize   = util_func.calculate_plot_fontsize(plotnum, v_plotnum, h_plotnum)
        i = 1
        for pdname in detectors:
            s = model.detectors.get(pdname)
            print(s.getFinesseText())
            plt.subplot(v_plotnum,h_plotnum,i)
            pdname = "%s"%(pdname)
            # bugあり
            axes_title = str(model.detectors.get(pdname).node) + " " + "demodulated by " + str(model.detectors.get(pdname).phase1) + "[deg] " + str(model.detectors.get(pdname).f1) + "[Hz]"
            plt.plot(out.x, out['%s' % pdname])
            plt.xscale(x_plotscale)
            plt.yscale(y_plotscale)
            plt.ylabel("m/sqrt(Hz)", fontsize=fontsize)
            plt.xlabel(out.xlabel.split()[0]+out.xlabel.split()[1], fontsize=fontsize)
            plt.title('%s' % axes_title, fontsize=fontsize)
            plt.tick_params(labelsize=fontsize)
            # gridの表示
            plt.grid()
            # loop
            i += 1
        plt.tight_layout()
        figure_title = base_name+" "+sim_conf["kifo_dof"]+" "+"sensitivity shotnoise"
        fig1.suptitle(figure_title, fontsize=20)
        plt.subplots_adjust(top=0.9)
        plt.show(block=False)
        if sim_conf["kgui_plot_in_gui"]:
            #fig1.set_size_inches(9.5, 5.063)
            #fig1 = plt.gcf()
            return fig1

    elif type_of_pd_signal=="st_type2": # ここを作る
        fig1      = plt.figure(figsize=(7, 4),dpi=100)
        plotnum   = len(detectors)
        v_plotnum = math.ceil(math.sqrt(plotnum))
        h_plotnum = v_plotnum
        fontsize   = util_func.calculate_plot_fontsize(plotnum, v_plotnum, h_plotnum)
        i = 1
        for pdname in detectors:
            s = model.detectors.get(pdname)
            print(s.getFinesseText())
            plt.subplot(v_plotnum,h_plotnum,i)
            pdname = "%s"%(pdname)
            # bugあり
            axes_title = str(model.detectors.get(pdname).node) + " " + "demodulated by " + str(model.detectors.get(pdname).phase1) + "[deg] " + str(model.detectors.get(pdname).f1) + "[Hz]"
            plt.plot(out.x, out['%s' % pdname])
            plt.xscale(x_plotscale)
            plt.yscale(y_plotscale)
            plt.ylabel("W/sqrt(Hz)", fontsize=fontsize)
            plt.xlabel(out.xlabel.split()[0]+out.xlabel.split()[1], fontsize=fontsize)
            plt.title('%s' % axes_title, fontsize=fontsize)
            plt.tick_params(labelsize=fontsize)
            # gridの表示
            plt.grid()
            # loop
            i += 1
        plt.tight_layout()
        figure_title = base_name+" "+sim_conf["kifo_dof"]+" "+"sensitivity"
        fig1.suptitle(figure_title, fontsize=20)
        plt.subplots_adjust(top=0.9)
        plt.show(block=False)
        if sim_conf["kgui_plot_in_gui"]:
            #fig1.set_size_inches(9.5, 5.063)
            #fig1 = plt.gcf()
            return fig1

    elif type_of_pd_signal=="swan_mirot_power":
        fig1      = plt.figure(figsize=(7, 4),dpi=100)
        plotnum   = len(detectors)
        v_plotnum = math.ceil(math.sqrt(plotnum))
        h_plotnum = v_plotnum
        fontsize   = util_func.calculate_plot_fontsize(plotnum, v_plotnum, h_plotnum)
        i = 1
        for pdname in detectors:
            s = model.detectors.get(pdname)
            print(s.getFinesseText())
            plt.subplot(v_plotnum,h_plotnum,i)
            pdname = "%s"%(pdname)
            #model.detectors.get(pdname).node
            if pdname.split('_')[-1] == "y-split":
                qpd_divide_direction = "yaw"
            elif pdname.split('_')[-1] == "x-split":
                qpd_divide_direction = "pitch"
            axes_title = str(model.detectors.get(pdname).node) +" "+qpd_divide_direction
            plt.plot(out.x, (out['%s' % pdname]), label=pdname)
            plt.xscale(x_plotscale)
            plt.yscale(y_plotscale)
            plt.ylabel("[W]", fontsize=fontsize)
            plt.xlabel(out.xlabel.split()[0]+out.xlabel.split()[1], fontsize=fontsize)
            plt.title('%s' % axes_title, fontsize=fontsize)
            plt.tick_params(labelsize=fontsize)
            # 凡例の表示
            #plt.legend(fontsize=fontsize)
            # gridの表示
            plt.grid()
            # loop
            i += 1
        plt.tight_layout()
        figure_title = base_name+" "+sim_conf["kifo_dof"]+" "+"Power"
        fig1.suptitle(figure_title, fontsize=10)
        plt.subplots_adjust(top=0.9)
        plt.show(block=False)
        if sim_conf["kgui_plot_in_gui"]:
            #fig1.set_size_inches(9.5, 5.063)
            #fig1 = plt.gcf()
            return fig1
    
    elif type_of_pd_signal=="swan_mirot_amptd":
        fig1      = plt.figure(figsize=(7, 4),dpi=100)
        plotnum   = len(detectors)
        v_plotnum = math.ceil(math.sqrt(plotnum))
        h_plotnum = v_plotnum
        fontsize   = util_func.calculate_plot_fontsize(plotnum, v_plotnum, h_plotnum)
        i = 1
        for pdname in detectors:
            print(pdname)
            s = model.detectors.get(pdname)
            print(s)
            print(s.getFinesseText())
            plt.subplot(v_plotnum,h_plotnum,i)
            pdname = "%s"%(pdname)
            #model.detectors.get(pdname).node
            if pdname.split('_')[-1] == "y-split":
                qpd_divide_direction = "yaw"
            elif pdname.split('_')[-1] == "x-split":
                qpd_divide_direction = "pitch"
            axes_title = str(model.detectors.get(pdname).node) +" "+qpd_divide_direction
            label      = pdname
            plt.plot(out.x, (out['%s' % pdname]),label=label)
            plt.xscale(x_plotscale)
            plt.yscale(y_plotscale)
            plt.ylabel("label[]", fontsize=fontsize)
            plt.xlabel(out.xlabel.split()[0]+out.xlabel.split()[1], fontsize=fontsize)
            plt.title('%s' % axes_title, fontsize=fontsize)
            plt.tick_params(labelsize=fontsize)
            # 凡例の表示
            plt.legend(fontsize=fontsize)
            # gridの表示
            plt.grid()
            # loop
            i += 1
        plt.tight_layout()
        figure_title = base_name+" "+sim_conf["kifo_dof"]+" "+"Power"
        fig1.suptitle(figure_title, fontsize=20)
        plt.subplots_adjust(top=0.9)
        plt.show(block=False)
        if sim_conf["kgui_plot_in_gui"]:
            #fig1.set_size_inches(9.5, 5.063)
            #fig1 = plt.gcf()
            return fig1

    elif type_of_pd_signal=="swan_mirot_dmod1":
        fig1      = plt.figure(figsize=(7, 4),dpi=100)
        plotnum   = len(detectors)
        v_plotnum = math.ceil(math.sqrt(plotnum))
        h_plotnum = v_plotnum
        fontsize   = util_func.calculate_plot_fontsize(plotnum, v_plotnum, h_plotnum)
        i = 1
        for pdname in detectors:
            s = model.detectors.get(pdname)
            print(s.getFinesseText())
            plt.subplot(v_plotnum,h_plotnum,i)
            pdname = "%s"%(pdname)
            #model.detectors.get(pdname).node
            if pdname.split('_')[-1] == "y-split":
                qpd_divide_direction = "yaw"
            elif pdname.split('_')[-1] == "x-split":
                qpd_divide_direction = "pitch"
            axes_title = str(model.detectors.get(pdname).node) +" "+qpd_divide_direction
            label      = pdname
            plt.plot(out.x, (out['%s' % pdname]),label=label)
            plt.xscale(x_plotscale)
            plt.yscale(y_plotscale)
            plt.ylabel("label[]", fontsize=fontsize)
            plt.xlabel(out.xlabel.split()[0]+out.xlabel.split()[1], fontsize=fontsize)
            plt.title('%s' % axes_title, fontsize=fontsize)
            plt.tick_params(labelsize=fontsize)
            # 凡例の表示
            plt.legend(fontsize=fontsize)
            # gridの表示
            plt.grid()
            # loop
            i += 1
        plt.tight_layout()
        figure_title = base_name+" "+sim_conf["kifo_dof"]+" "+"Power"
        fig1.suptitle(figure_title, fontsize=20)
        plt.subplots_adjust(top=0.9)
        plt.show(block=False)
        if sim_conf["kgui_plot_in_gui"]:
            #fig1.set_size_inches(9.5, 5.063)
            #fig1 = plt.gcf()
            return fig1

    elif type_of_pd_signal=="swan_gouyp_power":
        fig1      = plt.figure(figsize=(7, 4),dpi=100)
        plotnum   = len(detectors)
        v_plotnum = math.ceil(math.sqrt(plotnum))
        h_plotnum = v_plotnum
        fontsize   = util_func.calculate_plot_fontsize(plotnum, v_plotnum, h_plotnum)
        i = 1
        for pdname in detectors:
            s = model.detectors.get(pdname)
            print(s.getFinesseText())
            plt.subplot(v_plotnum,h_plotnum,i)
            pdname = "%s"%(pdname)
            #model.detectors.get(pdname).node
            if pdname.split('_')[-1] == "y-split":
                qpd_divide_direction = "yaw"
            elif pdname.split('_')[-1] == "x-split":
                qpd_divide_direction = "pitch"
            axes_title = str(model.detectors.get(pdname).node) +" "+qpd_divide_direction
            plt.plot(out.x, (out['%s' % pdname]))
            plt.xscale(x_plotscale)
            plt.yscale(y_plotscale)
            plt.ylabel("[W]", fontsize=fontsize)
            plt.xlabel("gouy phase[deg]", fontsize=fontsize)
            #plt.xlabel(out.xlabel.split()[0]+out.xlabel.split()[1], fontsize=fontsize)
            print(out.xlabel)
            plt.title('%s' % axes_title, fontsize=fontsize)
            plt.tick_params(labelsize=fontsize)
            # 凡例の表示
            #plt.legend(fontsize=fontsize)
            # gridの表示
            plt.grid()
            # loop
            i += 1
        plt.tight_layout()
        figure_title = base_name+" "+sim_conf["kifo_dof"]+" "+"Power"
        fig1.suptitle(figure_title, fontsize=20)
        plt.subplots_adjust(top=0.9)
        plt.show(block=False)
        if sim_conf["kgui_plot_in_gui"]:
            #fig1.set_size_inches(9.5, 5.063)
            #fig1 = plt.gcf()
            return fig1

    elif type_of_pd_signal=="swan_gouyp_amptd":
        fig1      = plt.figure(figsize=(7, 4),dpi=100)
        plotnum   = len(detectors)
        v_plotnum = math.ceil(math.sqrt(plotnum))
        h_plotnum = v_plotnum
        fontsize   = util_func.calculate_plot_fontsize(plotnum, v_plotnum, h_plotnum)
        i = 1
        for pdname in detectors:
            s = model.detectors.get(pdname)
            print(s.getFinesseText())
            plt.subplot(v_plotnum,h_plotnum,i)
            pdname = "%s"%(pdname)
            #model.detectors.get(pdname).node
            if pdname.split('_')[-1] == "y-split":
                qpd_divide_direction = "yaw"
            elif pdname.split('_')[-1] == "x-split":
                qpd_divide_direction = "pitch"
            axes_title = str(model.detectors.get(pdname).node) +" "+qpd_divide_direction
            label      = pdname
            plt.plot(out.x, (out['%s' % pdname]), label=label)
            plt.xscale(x_plotscale)
            plt.yscale(y_plotscale)
            plt.ylabel("[W]", fontsize=fontsize)
            plt.xlabel("gouy phase[deg]", fontsize=fontsize)
            #plt.xlabel(out.xlabel.split()[0]+out.xlabel.split()[1], fontsize=fontsize)
            print(out.xlabel)
            plt.title('%s' % axes_title, fontsize=fontsize)
            plt.tick_params(labelsize=fontsize)
            # 凡例の表示
            #plt.legend(fontsize=fontsize)
            # gridの表示
            plt.grid()
            # loop
            i += 1
        plt.tight_layout()
        figure_title = base_name+" "+sim_conf["kifo_dof"]+" "+"Power"
        fig1.suptitle(figure_title, fontsize=20)
        plt.subplots_adjust(top=0.9)
        plt.show(block=False)
        if sim_conf["kgui_plot_in_gui"]:
            #fig1.set_size_inches(9.5, 5.063)
            #fig1 = plt.gcf()
            return fig1

    elif type_of_pd_signal=="swan_gouyp_dmod1":
        fig1      = plt.figure(figsize=(7, 4),dpi=100)
        plotnum   = len(detectors)
        v_plotnum = math.ceil(math.sqrt(plotnum))
        h_plotnum = v_plotnum
        fontsize   = util_func.calculate_plot_fontsize(plotnum, v_plotnum, h_plotnum)
        i = 1
        for pdname in detectors:
            s = model.detectors.get(pdname)
            print(s.getFinesseText())
            plt.subplot(v_plotnum,h_plotnum,i)
            pdname = "%s"%(pdname)
            #model.detectors.get(pdname).node
            if pdname.split('_')[-1] == "y-split":
                qpd_divide_direction = "yaw"
            elif pdname.split('_')[-1] == "x-split":
                qpd_divide_direction = "pitch"
            axes_title = str(model.detectors.get(pdname).node) +" "+qpd_divide_direction
            label      = pdname
            plt.plot(out.x, (out['%s' % pdname]), label=label)
            plt.xscale(x_plotscale)
            plt.yscale(y_plotscale)
            plt.ylabel("[W]", fontsize=fontsize)
            plt.xlabel("gouy phase[deg]", fontsize=fontsize)
            #plt.xlabel(out.xlabel.split()[0]+out.xlabel.split()[1], fontsize=fontsize)
            print(out.xlabel)
            plt.title('%s' % axes_title, fontsize=fontsize)
            plt.tick_params(labelsize=fontsize)
            # 凡例の表示
            plt.legend(fontsize=fontsize)
            # gridの表示
            plt.grid()
            # loop
            i += 1
        plt.tight_layout()
        figure_title = base_name+" "+sim_conf["kifo_dof"]+" "+"Power"
        fig1.suptitle(figure_title, fontsize=20)
        plt.subplots_adjust(top=0.9)
        plt.show(block=False)
        if sim_conf["kgui_plot_in_gui"]:
            #fig1.set_size_inches(9.5, 5.063)
            #fig1 = plt.gcf()
            return fig1

    elif type_of_pd_signal=="swan_dmodp_dmod1":
        fig1      = plt.figure(figsize=(7, 4),dpi=100)
        plotnum   = len(detectors)
        v_plotnum = math.ceil(math.sqrt(plotnum))
        h_plotnum = v_plotnum
        fontsize   = util_func.calculate_plot_fontsize(plotnum, v_plotnum, h_plotnum)
        i = 1
        for pdname in detectors:
            s = model.detectors.get(pdname)
            print(s.getFinesseText())
            plt.subplot(v_plotnum,h_plotnum,i)
            pdname = "%s"%(pdname)
            #model.detectors.get(pdname).node
            axes_title = str(model.detectors.get(pdname).node)
            label      = pdname
            plt.plot(out.x, (out['%s' % pdname]), label=label)
            plt.xscale(x_plotscale)
            plt.yscale(y_plotscale)
            plt.ylabel("[W]", fontsize=fontsize)
            plt.xlabel("demodulation phase[deg]", fontsize=fontsize)
            #plt.xlabel(out.xlabel.split()[0]+out.xlabel.split()[1], fontsize=fontsize)
            print(out.xlabel)
            plt.title('%s' % axes_title, fontsize=fontsize)
            plt.tick_params(labelsize=fontsize)
            # 凡例の表示
            plt.legend(fontsize=fontsize)
            # gridの表示
            plt.grid()
            # loop
            i += 1
        plt.tight_layout()
        figure_title = base_name+" "+sim_conf["kifo_dof"]+" "+"error signal"
        fig1.suptitle(figure_title, fontsize=20)
        plt.subplots_adjust(top=0.9)
        plt.show(block=False)
        if sim_conf["kgui_plot_in_gui"]:
            #fig1.set_size_inches(9.5, 5.063)
            #fig1 = plt.gcf()
            return fig1

    elif type_of_pd_signal=="tfan_dmod2_mirot":
        
        if sim_conf["Select_tfan_dmod2_pd_matrix_checkbox"]:

            fig1   = plt.figure(figsize=(7, 4),dpi=100)

            column_labels=["DC gain"]
            row_labels=[]
            data = []

            column_labels=[""]#この列はmagnitudeとphaseを縦に並べる列
            for i in range(len(model.detectors.keys())):
                column_labels.append(detectors[i])
            
            data_row_1=["magnitude"]
            for pdname in model.detectors.keys():
                data_row_1.append(abs(out[pdname][0]))
            data_row_2=["phase"]
            for pdname in model.detectors.keys():
                data_row_2.append(np.angle(out[pdname][0]))
            data = [data_row_1,
                    data_row_2
            ]

            plt.axis('tight')
            plt.axis('off')

            the_table = plt.table(cellText=data,colLabels=column_labels,loc="center")
            the_table.auto_set_font_size(False)
            the_table.set_fontsize(9)
        else:

            #fig1      = plt.figure(figsize=(12.8, 21.6))
            if len(detectors)==1:
                fig1   = plt.figure(figsize=(7, 4),dpi=100)
            else:
                #fig1   = plt.figure(figsize=(7, 10),dpi=100)
                fig1   = plt.figure(figsize=(7, 4),dpi=100)
            plotnum    = len(detectors)
            if len(detectors)==1 or len(detectors)==2:
                v_plotnum = 2
            else:
                v_plotnum  = 4
            #h_plotnum  = math.ceil(len(detectors)/2)
            h_plotnum  = math.ceil(2*len(detectors)/v_plotnum)
            fontsize   = util_func.calculate_plot_fontsize(plotnum, v_plotnum, h_plotnum)   

            for i in range(len(detectors)):
                # プロットするpd
                pdname = detectors[i]
                # gainのsubplotの表示位置
                k1 = math.floor((i/h_plotnum))*2*h_plotnum + i%h_plotnum+1
                # phaseのsubplotの表示位置
                k2 = math.floor((i/h_plotnum))*2*h_plotnum + i%h_plotnum+1+h_plotnum
                print("check1001")
                #abs
                plt.subplot(v_plotnum,h_plotnum,k1)
                plt.plot(out.x, np.abs(out[detectors[i]]), color="red",label=pdname)
                plt.xscale(x_plotscale)
                plt.yscale(y_plotscale)
                plt.ylabel('Magnitude[W]', fontsize=fontsize)
                plt.xlabel(out.xlabel.split()[0]+out.xlabel.split()[1], fontsize=fontsize)
                axes_title = str(model.detectors.get(pdname).node) + " \n" + "demodulated by " + str(model.detectors.get(pdname).phase1) + "[deg] " + str(model.detectors.get(pdname).f1) + "[Hz]"
                plt.title("magnitude "+axes_title, fontsize=fontsize)
                plt.tick_params(labelsize=fontsize)
                # gridの表示
                plt.grid()
                #phase
                plt.subplot(v_plotnum,h_plotnum,k2)
                plt.plot(out.x, np.angle(out[detectors[i]]), color="blue",label=pdname)
                plt.xscale(x_plotscale)
                plt.yscale("linear")
                #plt.yscale(y_plotscale)
                plt.ylabel('phase [rad]', fontsize=fontsize)
                plt.xlabel(out.xlabel.split()[0]+out.xlabel.split()[1], fontsize=fontsize)
                axes_title = str(model.detectors.get(pdname).node) + " \n" + "demodulated by " + str(model.detectors.get(pdname).phase1) + "[deg] " + str(model.detectors.get(pdname).f1) + "[Hz]"
                plt.title("phase "+axes_title, fontsize=fontsize)
                plt.tick_params(labelsize=fontsize)
                # gridの表示
                plt.grid()
                i+=1

        plt.tight_layout()
        #plt.legend(fontsize=fontsize)
        plt.legend(fontsize=5)
        figure_title = base_name+" "+"mirror rotation+"+"transfer function"
        fig1.suptitle(figure_title, fontsize=20)
        plt.subplots_adjust(top=0.935)
        plt.show(block=False)
        if sim_conf["kgui_plot_in_gui"]:
            #fig1.set_size_inches(9.5, 5.063)
            #fig1 = plt.gcf()
            return fig1

def plot_in_gui(out,matplotlib_code):
    #%matplotlib notebook
    ####################
    #1つ目のウィンドウ
    ####################
    fig = plt.figure(figsize=(7, 4),dpi=100)

    ################################
    #1つ目のウィンドウ 1つ目のグラフ
    ################################
    exec(matplotlib_code)


    #fig  = plt.figure(figsize=(7, 4),dpi=100)
    #exec(matplot_code)
    #print("matplot_code =\n")
    #print(matplot_code)
    #print("out =\n")
    #print(out)
    return fig