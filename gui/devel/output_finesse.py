import PySimpleGUI as sg
import datetime
import util_func
import sys
import numpy as np

def output(sim_conf, out, model, type_of_pd_signal, plot_title):
    if sim_conf['k_inf_c_output_kat'] == True:
        dt_now = str(datetime.datetime.now())
        tmp_list = dt_now.split(":")
        del tmp_list[2]
        tmp_list = ''.join(tmp_list)
        tmp_list = util_func.date_to_num(tmp_list)

        #header_file   = "# This is kat file created at %s \n" % dt_now
        filename_head = plot_title
        filename_tail = util_func.date_to_num(dt_now)
        filename  = "%s_%s"%(filename_head, filename_tail)
        filename  = filename.replace("dmod", "dm")
        filename  = filename.replace("phase", "")
        #kat       = header_file
        kat       = str(model)
        fname_kat = sg.popup_get_file('Select the output name for kat file?', save_as=True, default_path="../export/kat/%s.kat"%(filename), file_types=(('ALL Files', '*.kat'),))
        try:
            f = open(fname_kat, 'x')
            f.writelines(kat)
            f.close()
        except FileExistsError:
            sg.popup_ok('Error : there is a file %s' % fname_kat)
        except Exception:
            sg.popup_ok('Unexpected error:', sys.exc_info()[0])

    if sim_conf['k_inf_c_output_plotdata'] == True:
        dt_now   = str(datetime.datetime.now())
        tmp_list = dt_now.split((":"))
        del tmp_list[2]
        tmp_list = ''.join(tmp_list)
        dt_now   = util_func.date_to_num(tmp_list)
        filename_head = plot_title
        filename_tail = util_func.date_to_num(dt_now)
        filename    = "%s_%s"%(filename_head, filename_tail)
        header_file = "# This is finesse result simulated at %s \n" % dt_now \
                    + "%s\n"                                        %filename\
                    + "# x " 
        filename = filename.replace("dmod", "dm")
        filename = filename.replace("phase", "")
        fname_plotdata = sg.popup_get_file('Select the output name for plot data?', save_as=True, default_path="../export/plotdata/%s.txt"%(filename),file_types=(("ALL Files", "*.txt"),))
        try:
            f       = open(fname_plotdata, 'x')
            arr         = np.empty((0, out.x.size), float)
            arr         = np.append(arr, [out.x], axis=0)
            pdnames = list(model.detectors.keys())
            for pdname in pdnames:
                if(type_of_pd_signal=='tf_dmod2'):#phaseも書き込む必要があるため追加した
                    arr          = np.append(arr, [np.abs(out[pdname])], axis=0)
                    header_file += pdname+"_abs "#　半角スペースが必要なので注意する　CSVにするならカンマに置き換える
                    arr          = np.append(arr, [np.angle(out[pdname])], axis=0)
                    header_file += pdname+"_phase "
                else:
                    arr          = np.append(arr, [out[pdname]], axis=0)
                    header_file += pdname+" "
            L        = np.asarray(arr).T
            x        = L.tolist()
            plotdata = [" ".join(map(str, i)) for i in x]
            f.writelines(header_file + '\n')
            for i in plotdata:
                f.writelines(i)
                f.write('\n')
            f.close()
        except FileExistsError:
            sg.popup_ok("Error : there is a file %s" % fname_plotdata)
        except Exception:
            sg.popup_ok("Unexpected error:", sys.exc_info()[0])


def output_kat_2(sim_conf, out, model, plot_title):
    if sim_conf['k_inf_c_output_kat'] == True:
        dt_now = str(datetime.datetime.now())
        tmp_list = dt_now.split(":")
        del tmp_list[2]
        tmp_list = ''.join(tmp_list)
        tmp_list = util_func.date_to_num(tmp_list)

        #header_file   = "# This is kat file created at %s \n" % dt_now
        filename_head = plot_title
        filename_tail = util_func.date_to_num(dt_now)
        filename  = "%s_%s"%(filename_head, filename_tail)
        filename  = filename.replace("dmod", "dm")
        filename  = filename.replace("phase", "")
        #kat       = header_file
        kat       = str(model)
        fname_kat = sg.popup_get_file('Select the output name for kat file?', save_as=True, default_path="../export/kat/%s.kat"%(filename), file_types=(('ALL Files', '*.kat'),))
        try:
            f = open(fname_kat, 'x')
            f.writelines(kat)
            f.close()
        except FileExistsError:
            sg.popup_ok('Error : there is a file %s' % fname_kat)
        except Exception:
            sg.popup_ok('Unexpected error:', sys.exc_info()[0])

def output_plotdata_2(sim_conf, out, model, plot_title):
    if sim_conf['k_inf_c_output_plotdata'] == True:
        dt_now   = str(datetime.datetime.now())
        tmp_list = dt_now.split((":"))
        del tmp_list[2]
        tmp_list = ''.join(tmp_list)
        dt_now   = util_func.date_to_num(tmp_list)
        filename_head = plot_title
        filename_tail = util_func.date_to_num(dt_now)
        filename    = "%s_%s"%(filename_head, filename_tail)
        header_file = "# This is finesse result simulated at %s \n" % dt_now \
                    + "%s\n"                                        %filename\
                    + "# x " 
        filename = filename.replace("dmod", "dm")
        filename = filename.replace("phase", "")
        fname_plotdata = sg.popup_get_file('Select the output name for plot data?', save_as=True, default_path="../export/plotdata/%s.txt"%(filename),file_types=(("ALL Files", "*.txt"),))
        try:
            f       = open(fname_plotdata, 'x')
            arr         = np.empty((0, out.x.size), float)
            arr         = np.append(arr, [out.x], axis=0)
            print(out.x)
            pdnames = list(model.detectors.keys())
            print(pdnames)
            for pdname in pdnames:
                if("deg" in out.yaxis):#phaseも書き込む必要があるため追加した
                    arr          = np.append(arr, [np.abs(out[pdname])], axis=0)
                    header_file += pdname+"_abs "#　半角スペースが必要なので注意する　CSVにするならカンマに置き換える
                    arr          = np.append(arr, [np.angle(out[pdname])], axis=0)
                    header_file += pdname+"_phase "
                else:
                    arr          = np.append(arr, [out[pdname]], axis=0)
                    header_file += pdname+" "
                    print("1")
                    print(pdname)
            L        = np.asarray(arr).T
            x        = L.tolist()
            plotdata = [" ".join(map(str, i)) for i in x]
            f.writelines(header_file + '\n')
            for i in plotdata:
                f.writelines(i)
                f.write('\n')
            f.close()
        except FileExistsError:
            sg.popup_ok("Error : there is a file %s" % fname_plotdata)
        except Exception:
            sg.popup_ok("Unexpected error:", sys.exc_info()[0])