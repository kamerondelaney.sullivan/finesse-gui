import matplotlib
import matplotlib.pyplot as plt
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg
import PySimpleGUI as sg
import sys
import os

import matplotlib.pyplot as plt
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg, NavigationToolbar2Tk

# utility

def Popup_notification_window(note):
    Answer = False
    layout = [
        [sg.Text(note), sg.Text(size=(15,3), key='-OUTPUT-')],
        [sg.Button('Ok'), sg.Button('Exit')]
    ]
    window = sg.Window('Pattern 2B', layout)
    while True:  # Event Loop
        event, values = window.read()
        print(event, values)
        if event == sg.WIN_CLOSED or event == 'Exit':
            Answer = False
            return Answer
        if event == 'Ok':
            Answer = True
            window.close()
            return Answer

def frame_layout(layouts,frame_keys,display_texts):
    """
        

        Parameter
        ----------
        layouts    : [ [[element,element],[element,element]] , [[element,element],[element,element]]]
        frame_keys : ["",""]
        display_texts : ["",""]

        Returns
        -------
        False         : 
        framed_layout : List[sg.Frame]
    """
    if len(layouts)!=len(frame_keys):
        return [sg.Text("error function(frame_layout)")]

    framed_layout = []
    for i in range(len(layouts)):
        framed_layout.append(sg.Frame(display_texts[i],layouts[i],key=frame_keys[i]))

    return framed_layout

def column_layout(layouts,column_keys):

    if len(layouts)!=len(column_keys):
        return [sg.Text("error function(column_layout)")]

    column_layout = []
    for i in range(len(layouts)):
        column_layout.append(sg.Column(layouts[i],key=column_keys[i],scrollable=True))

    return column_layout

def tabgroup_layout(layouts,tab_keys):

    if len(layouts)!=len(tab_keys):
        return [sg.Text("error function(tabgroup_layout)")]

    tabgroup_layout = []
    for i in range(len(layouts)):
        tabgroup_layout.append(sg.Tab(tab_keys[i], layouts[i]))

    return [sg.TabGroup([tabgroup_layout])]


def Debug_window(window):
    """
        Debug用のwindowを新しく作成する。

        Parameter
        ----------
        window : デバッグするpysimpleguiのwindow


        Returns
        -------
    """

    # Base64 versions of images of a folder and a file. PNG files (may not work with PySimpleGUI27, swap with GIFs)

    folder_icon = b'iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAACXBIWXMAAAsSAAALEgHS3X78AAABnUlEQVQ4y8WSv2rUQRSFv7vZgJFFsQg2EkWb4AvEJ8hqKVilSmFn3iNvIAp21oIW9haihBRKiqwElMVsIJjNrprsOr/5dyzml3UhEQIWHhjmcpn7zblw4B9lJ8Xag9mlmQb3AJzX3tOX8Tngzg349q7t5xcfzpKGhOFHnjx+9qLTzW8wsmFTL2Gzk7Y2O/k9kCbtwUZbV+Zvo8Md3PALrjoiqsKSR9ljpAJpwOsNtlfXfRvoNU8Arr/NsVo0ry5z4dZN5hoGqEzYDChBOoKwS/vSq0XW3y5NAI/uN1cvLqzQur4MCpBGEEd1PQDfQ74HYR+LfeQOAOYAmgAmbly+dgfid5CHPIKqC74L8RDyGPIYy7+QQjFWa7ICsQ8SpB/IfcJSDVMAJUwJkYDMNOEPIBxA/gnuMyYPijXAI3lMse7FGnIKsIuqrxgRSeXOoYZUCI8pIKW/OHA7kD2YYcpAKgM5ABXk4qSsdJaDOMCsgTIYAlL5TQFTyUIZDmev0N/bnwqnylEBQS45UKnHx/lUlFvA3fo+jwR8ALb47/oNma38cuqiJ9AAAAAASUVORK5CYII='
    file_icon = b'iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAACXBIWXMAAAsSAAALEgHS3X78AAABU0lEQVQ4y52TzStEURiHn/ecc6XG54JSdlMkNhYWsiILS0lsJaUsLW2Mv8CfIDtr2VtbY4GUEvmIZnKbZsY977Uwt2HcyW1+dTZvt6fn9557BGB+aaNQKBR2ifkbgWR+cX13ubO1svz++niVTA1ArDHDg91UahHFsMxbKWycYsjze4muTsP64vT43v7hSf/A0FgdjQPQWAmco68nB+T+SFSqNUQgcIbN1bn8Z3RwvL22MAvcu8TACFgrpMVZ4aUYcn77BMDkxGgemAGOHIBXxRjBWZMKoCPA2h6qEUSRR2MF6GxUUMUaIUgBCNTnAcm3H2G5YQfgvccYIXAtDH7FoKq/AaqKlbrBj2trFVXfBPAea4SOIIsBeN9kkCwxsNkAqRWy7+B7Z00G3xVc2wZeMSI4S7sVYkSk5Z/4PyBWROqvox3A28PN2cjUwinQC9QyckKALxj4kv2auK0xAAAAAElFTkSuQmCC'

    #starting_path = sg.popup_get_folder('Folder to display')
    def add_components_in_container(parent, treedata, window_rows):
        components = window_rows
        for component in components:
            #print(fullname)
            for i in component:
                #print("\n")
                #print(i)
                #print(type(i).__name__)
                #print(i.Key)
                fullname = i.Key
                if isinstance(i,sg.Frame): # if it's a container, add folder and recurse
                    print("frame")
                    if fullname==None:
                        fullname = "(no key defined)"
                        treedata.Insert(parent, fullname, fullname, values=[i], icon=folder_icon)
                        #print(component[0].Rows)
                        #print("\n")
                        add_components_in_container(fullname, treedata, i.Rows)
                    else:
                        treedata.Insert(parent, fullname, fullname, values=[i], icon=folder_icon)
                        #print(component[0].Rows)
                        #print("\n")
                        add_components_in_container(fullname, treedata, i.Rows)
                elif isinstance(i,sg.Column): # if it's a container, add folder and recurse
                    if fullname==None:
                        #fullname = i.Rows[0][0].Key
                        #treedata.Insert(parent, fullname, fullname, values=[fullname], icon=folder_icon)
                        print("column")
                        print(i.ParentContainer)
                        if isinstance(i.ParentContainer,sg.Window):
                            add_components_in_container("", treedata, i.Rows)
                        else:
                            add_components_in_container(i.ParentContainer.Key, treedata, i.Rows)
                    else:
                        print(f"parent= {parent}")
                        #print(f"parent key= {parent.Key}")
                        print(f"fullname= {fullname}")
                        treedata.Insert(parent, fullname, fullname, values=[i], icon=folder_icon)
                        #print(component[0].Rows)
                        #print("\n")
                        add_components_in_container(fullname, treedata, i.Rows)
                elif isinstance(i,sg.Tab): # if it's a container, add folder and recurse
                    treedata.Insert(parent, fullname, fullname, values=[i], icon=folder_icon)
                    #print(component[0].Rows)
                    #print("\n")
                    add_components_in_container(fullname, treedata, i.Rows)
                elif isinstance(i,sg.TabGroup): # if it's a container, add folder and recurse
                    treedata.Insert(parent, fullname, fullname, values=[i], icon=folder_icon)
                    #print(component[0].Rows)
                    #print("\n")
                    add_components_in_container(fullname, treedata, i.Rows)
                elif isinstance(i,sg.Canvas):
                    break
                else:
                    print("\n")
                    print(type(i).__name__)
                    print(i.Key)
                    if isinstance(parent,sg.Window):
                        treedata.Insert("", fullname, fullname, values=[i], icon=file_icon)
                    else:
                        print(f"parent= {parent}")
                        #print(f"parent key= {parent.Key}")
                        print(f"fullname= {fullname}")
                        treedata.Insert(parent, fullname, fullname, values=[i], icon=file_icon)

    def make_components_tree(target_window):
        treedata = sg.TreeData()
        add_components_in_container("", treedata, target_window.Rows)
        layout_child1 = [
            [sg.Text('GUI tree test')],
            [sg.Tree(data=treedata,
                    headings=['class',"Values"],
                    auto_size_columns=True,
                    select_mode=sg.TABLE_SELECT_MODE_EXTENDED,
                    num_rows=20,
                    col0_width=80,
                    key='-TREE-',
                    show_expanded=False,
                    enable_events=True,
                    ),],
            [sg.Button('Ok'), sg.Button('Cancel')]
            ]
        layout = [
            [sg.Frame("",layout_child1),sg.Multiline("",key="multiline_test",size=[80,40])]

        ]
        window = sg.Window('Tree Element Test', layout, resizable=True, finalize=True)
        return window
    
    window3 = make_components_tree(window)
    window3["multiline_test"].reroute_stdout_to_here()

    while True:
        ev3, vals3 = window3.Read()
        if ev3 == sg.WIN_CLOSED or ev3 == 'Exit':
            window3["multiline_test"].restore_stdout()
            window3.Close()
            win3_active = False
            window.UnHide()
            break
        if ev3 == "-TREE-":
            window3["multiline_test"].update("")
            #print(vals3["multiline_test"])
            print(vals3["-TREE-"])
            if vals3["-TREE-"][0]!="(pin)":
                print(sg.obj_to_string_single_obj(window[vals3["-TREE-"][0]]))
            elif vals3["-TREE-"][0]!="(no key defined)":
                pass

"""
    pysimpleguiのsg.Column when `scrollable=True` or `size not (None, None)`となっている要素のサイズをセットする。

    Parameter
    ----------
    element : pysimpleguiのelement

    size    : list[int,int]
    [width,height]

    Returns
    -------
    value : type
    doc
"""
def set_size(element, size):
    """
        pysimpleguiのsg.Column when `scrollable=True` or `size not (None, None)`となっている要素のサイズをセットする。

        Parameter
        ----------
        element : pysimpleguiのelement

        size : list[int,int]
        [width,height]

        Returns
        -------
        None
    """
    # Only work for sg.Column when `scrollable=True` or `size not (None, None)`
    options = {'width':size[0], 'height':size[1]}
    if element.Scrollable or element.Size!=(None, None):
        element.Widget.canvas.configure(**options)
    else:
        element.Widget.pack_propagate(0)
        element.set_size(size)

def event_k_inf_c_laser_use_hom(window,values,flag):
    """
    Higher order modeに関係する設定項目をGUIで設定可能に切り替える。

    Parameter
    ----------
    window : dictionary
    pysimpleguiのwindow

    values : dictionary
    pysimpleguiのvalues
    
    flag   : bool
    on/off

    Returns
    -------
    None
    """
    if flag==True:
        window["k_inf_c_num_of_hom_order"].update(disabled=False)
        #window["select_amptd_n_mode_key"].update(disabled=False)
        #window["kifo_sw_amptd_ad_hom_n_order"].update(disabled=False)
        #window["select_amptd_m_mode_key"].update(disabled=False)
        #window["kifo_sw_amptd_ad_hom_m_order"].update(disabled=False)
        num = int(values["k_inf_c_num_of_hom_order"])
        new_hom_order_selection_list = []
        #values["kifo_sw_amptd_ad_hom_n_order"] = "0"
        #values["kifo_sw_amptd_ad_hom_m_order"] = "0"
        for i in range(num+1):
            new_hom_order_selection_list.append(str(i))
        #window["kifo_sw_amptd_ad_hom_n_order"].update(values=new_hom_order_selection_list)
        #window["kifo_sw_amptd_ad_hom_m_order"].update(values=new_hom_order_selection_list)
    else:
        window["k_inf_c_num_of_hom_order"].update(disabled=True)
        #window["select_amptd_n_mode_key"].update(disabled=True)
        #window["kifo_sw_amptd_ad_hom_n_order"].update(disabled=True)
        #window["select_amptd_m_mode_key"].update(disabled=True)
        #window["kifo_sw_amptd_ad_hom_m_order"].update(disabled=True)

def get_all_dof_list(interferometer):
    """
        選択したinterferometerの構成でのDoFを返す

        Parameter
        ----------
        interferometer : str
        DRFPMI,PRFPMI,FPMI,MI...

        Returns
        -------
        dof_list : list[str]
        DoFのlist[str]
    """
    dof_list =[]
    if interferometer=="all_dof":
        dof_list = ["DARM", "CARM", "BS", "PRCL","SRCL"]
    if interferometer=="DRFPMI":
        dof_list = ["DARM", "CARM", "BS", "PRCL","SRCL"]
    if interferometer=="PRFPMI":
        dof_list = ["DARM", "CARM", "BS", "PRCL"]
    if interferometer=="FPMI":
        dof_list = ["DARM", "CARM", "BS"]
    if interferometer=="MI":
        dof_list = ["BS"]
    return dof_list

def get_all_parameter_names():
    """
        pysimpleguiのparameterのtabにある要素のkeyをlist[str]で返す

        Parameter
        ----------
        None

        Returns
        -------
        all_parameter_key_names : list[str]
        pysimpleguiのparameterのtabにある要素のkeyのlist
    """
    all_parameter_key_names = []
    # laser
    key = "k_inf_c_laser_power"
    all_parameter_key_names.append(key)
    # mirror
    mirror_names = ["mibs", "itmx", "itmy", "etmx", "etmy", "prm", "srm", "pr2", "pr3", "sr2", "sr3"]
    mirror_paras = ["transmittance", "loss"]
    for mirror in mirror_names:
        for param in mirror_paras:
            key = "k_inf_c_%s_mirror_%s"%(mirror, param)
            all_parameter_key_names.append(key)
    # length
    length_names = ["prm_pr2", "pr2_pr3", "pr3_bs",
                    "srm_sr2", "sr2_sr3", "sr3_bs",
                    "bs_itmx", "bs_itmy", "itmx_etmx", "itmy_etmy"]
    for length in length_names:
        key = "k_inf_c_length_%s"%length
        all_parameter_key_names.append(key)
    # eom
    eom_names = ["f1", "f2"]
    eom_params = ["mod_frequency", "mod_index"]
    for eom in eom_names:
        for param in eom_params:
            key = "k_inf_c_%s_%s"%(eom, param)
            all_parameter_key_names.append(key)
    return all_parameter_key_names

def get_all_param_sec():
    """
        GUIのparameterのtabにあるsectionの名前をkeyにしたdictionaryを返す。
        windowの初期化に使う

        Parameter
        ----------
        None

        Returns
        -------
        param_sec : dictionary{str:bool}
        doc

        Example Use
        -----------
        window[key].update( get_all_param_sec()[key] )
    """
    param_sec ={
        "laser" :False,
        "mirror":False,
        "eom"   :False,
        "length":False,
        "hom"   :False
    }

    return param_sec

class Toolbar(NavigationToolbar2Tk):
    """
        pysimpleguiのcanvasにmatplotlibのinteractiveを表示するためのclass
        これがあるとmatplotlibに「拡大」「移動」「保存」などのボタンが表示される
        よくわからない

        Parameter
        ----------
        NavigationToolbar2Tk : ?
    """
    def __init__(self, *args, **kwargs):
        super(Toolbar, self).__init__(*args, **kwargs)

def draw_figure_w_toolbar(canvas, fig, canvas_toolbar):
    """
        pysimpleguiのcanvasにmatplotlibのfigを表示する。
        interactiveあり

        Parameter
        ----------
        canvas         : pysimpleguiのcanvas

        fig            : matplotlibのfigure fig = plt.figure(1)
        
        canvas_toolbar :定義したclass Toolbar

        Returns
        -------
        figure_canvas_agg : backend_tkagg.FigureCanvasTkAgg
        よくわからない
    """
    if canvas.children:
        for child in canvas.winfo_children():
            child.destroy()
    if canvas_toolbar.children:
        for child in canvas_toolbar.winfo_children():
            child.destroy()
    figure_canvas_agg = FigureCanvasTkAgg(fig, master=canvas)
    figure_canvas_agg.draw()
    toolbar = Toolbar(figure_canvas_agg, canvas_toolbar)
    toolbar.update()
    figure_canvas_agg.get_tk_widget().pack(side='right', fill='both', expand=1)
    return figure_canvas_agg

def draw_figure(canvas, figure):
    """
        pysimpleguiのcanvasにmatplotlibのfigを表示する。
        interactiveなし

        Parameter
        ----------
        canvas         : pysimpleguiのcanvas

        figure            : matplotlibのfigure fig = plt.figure(1)
        
        canvas_toolbar :定義したclass Toolbar

        Returns
        -------
        figure_canvas_agg : backend_tkagg.FigureCanvasTkAgg
        よくわからない
    """
    # ------------------------------- Beginning of Matplotlib helper code -----------------------
    figure_canvas_agg = matplotlib.backends.backend_tkagg.FigureCanvasTkAgg(figure, canvas)
    figure_canvas_agg.draw()
    figure_canvas_agg.get_tk_widget().pack(side='top', fill='both', expand=1)
    return figure_canvas_agg

def delete_fig_agg(fig_agg):
    """
        pysimpleguiのcanvasにセットされているfigを取り除く。
        Tkinterの機能なのでよくわからない。

        Parameter
        ----------
        名前 : fig_agg
            matplotlibの何らかのclass?
            class matplotlib.backends.backend_tkagg.FigureCanvasTkAgg

        Returns
        -------
    """
    fig_agg.get_tk_widget().forget()
    plt.close('all')


def add_files_in_folder(parent, dirname,treedata):
    """
        pysimpleguiのtreedata要素を受け取り、dirnameにあるファイルをtreedataに格納して表示できるようにする。

        Parameter
        ----------
        parent : 

        dirname    : str
        ディレクトリのパス

        treedata    : pysimpleguiのtreedata

        Returns
        -------
        None
    """
    # Base64 versions of images of a folder and a file. PNG files (may not work with PySimpleGUI27, swap with GIFs)

    folder_icon = b'iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAACXBIWXMAAAsSAAALEgHS3X78AAABnUlEQVQ4y8WSv2rUQRSFv7vZgJFFsQg2EkWb4AvEJ8hqKVilSmFn3iNvIAp21oIW9haihBRKiqwElMVsIJjNrprsOr/5dyzml3UhEQIWHhjmcpn7zblw4B9lJ8Xag9mlmQb3AJzX3tOX8Tngzg349q7t5xcfzpKGhOFHnjx+9qLTzW8wsmFTL2Gzk7Y2O/k9kCbtwUZbV+Zvo8Md3PALrjoiqsKSR9ljpAJpwOsNtlfXfRvoNU8Arr/NsVo0ry5z4dZN5hoGqEzYDChBOoKwS/vSq0XW3y5NAI/uN1cvLqzQur4MCpBGEEd1PQDfQ74HYR+LfeQOAOYAmgAmbly+dgfid5CHPIKqC74L8RDyGPIYy7+QQjFWa7ICsQ8SpB/IfcJSDVMAJUwJkYDMNOEPIBxA/gnuMyYPijXAI3lMse7FGnIKsIuqrxgRSeXOoYZUCI8pIKW/OHA7kD2YYcpAKgM5ABXk4qSsdJaDOMCsgTIYAlL5TQFTyUIZDmev0N/bnwqnylEBQS45UKnHx/lUlFvA3fo+jwR8ALb47/oNma38cuqiJ9AAAAAASUVORK5CYII='
    file_icon = b'iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAACXBIWXMAAAsSAAALEgHS3X78AAABU0lEQVQ4y52TzStEURiHn/ecc6XG54JSdlMkNhYWsiILS0lsJaUsLW2Mv8CfIDtr2VtbY4GUEvmIZnKbZsY977Uwt2HcyW1+dTZvt6fn9557BGB+aaNQKBR2ifkbgWR+cX13ubO1svz++niVTA1ArDHDg91UahHFsMxbKWycYsjze4muTsP64vT43v7hSf/A0FgdjQPQWAmco68nB+T+SFSqNUQgcIbN1bn8Z3RwvL22MAvcu8TACFgrpMVZ4aUYcn77BMDkxGgemAGOHIBXxRjBWZMKoCPA2h6qEUSRR2MF6GxUUMUaIUgBCNTnAcm3H2G5YQfgvccYIXAtDH7FoKq/AaqKlbrBj2trFVXfBPAea4SOIIsBeN9kkCwxsNkAqRWy7+B7Z00G3xVc2wZeMSI4S7sVYkSk5Z/4PyBWROqvox3A28PN2cjUwinQC9QyckKALxj4kv2auK0xAAAAAElFTkSuQmCC'

    files = os.listdir(dirname)
    for f in files:
        fullname = os.path.join(dirname, f)
        if os.path.isdir(fullname):            # if it's a folder, add folder and recurse
            treedata.Insert(parent, fullname, f, values=[], icon=folder_icon)
            add_files_in_folder(fullname, fullname,treedata)
        else:

            treedata.Insert(parent, fullname, f, values=[
                            os.stat(fullname).st_size], icon=file_icon)

# window
def disable_FP_param(window, sim_conf):
    """
        XarmとYarmに関する部分を消去する

        Parameter
        ----------
        window : dictionary
        pysimpleguiのwindow

        sim_conf    : dictionary

        Returns
        -------
        None
    """
    window["k_inf_c_itmx_mirror_transmittance"].update(value="1")
    window["k_inf_c_itmx_mirror_loss"].update(value="0")
    window["k_inf_c_itmy_mirror_transmittance"].update(value="1")
    window["k_inf_c_itmy_mirror_loss"].update(value="0")
    window["k_inf_c_length_itmx_etmx"].update(value="0")
    window["k_inf_c_length_itmy_etmy"].update(value="0")

def disable_PRC_param(window, sim_conf):
    """
        PRCに関する部分を消去する

        Parameter
        ----------
        window : dictionary
        pysimpleguiのwindow

        sim_conf    : dictionary

        Returns
        -------
        None
    """
    window["k_inf_c_prm_mirror_transmittance"].update(value="1")
    window["k_inf_c_prm_mirror_loss"].update(value="0")
    window["k_inf_c_pr2_mirror_transmittance"].update(value="0")
    window["k_inf_c_pr2_mirror_loss"].update(value="0")
    window["k_inf_c_pr3_mirror_transmittance"].update(value="0")
    window["k_inf_c_pr3_mirror_loss"].update(value="0")
    window["k_inf_c_length_prm_pr2"].update(value="0")
    window["k_inf_c_length_pr2_pr3"].update(value="0")
    window["k_inf_c_length_pr3_bs"].update(value="0")

def disable_SRC_param(window, sim_conf):
    """
        SRCに関する部分を消去する

        Parameter
        ----------
        window : dictionary
        pysimpleguiのwindow

        sim_conf    : dictionary

        Returns
        -------
        None
    """
    window["k_inf_c_srm_mirror_transmittance"].update(value="1")
    window["k_inf_c_srm_mirror_loss"].update(value="0")
    window["k_inf_c_sr2_mirror_transmittance"].update(value="0")
    window["k_inf_c_sr2_mirror_loss"].update(value="0")
    window["k_inf_c_sr3_mirror_transmittance"].update(value="0")
    window["k_inf_c_sr3_mirror_loss"].update(value="0")
    window["k_inf_c_length_srm_sr2"].update(value="0")
    window["k_inf_c_length_sr2_sr3"].update(value="0")
    window["k_inf_c_length_sr3_bs"].update(value="0")


def change_interferometer(window, sim_conf, interferometer):
    """

        Parameter
        ----------
        window         : dictionary
        pysimpleguiのwindow

        sim_conf       : dictionary

        interferometer : str
        DRFPMI,PRFPMI,FPMI,MI...

        Returns
        -------
        None
    """
    if interferometer=="MI":
        disable_SRC_param(window, sim_conf)
        disable_PRC_param(window, sim_conf)
        disable_FP_param(window,  sim_conf)
    elif interferometer=="FPMI":
        disable_SRC_param(window, sim_conf)
        disable_PRC_param(window, sim_conf)
    elif interferometer=="PRFPMI":
        disable_SRC_param(window, sim_conf)
    elif interferometer=="DRFPMI":
        pass

def default_optical_parameter(window, sim_conf, name):
    """
        干渉計のデフォルトの値にセットする。
        Parameter
        ----------
        window   : dictionary
        pysimpleguiのwindow

        sim_conf : dictionary

        name     : str
        kagra,ligo,virgo...

        Returns
        -------
        None
    """
    def set_parameter(window, default_dict):
        keys = list(default_dict.keys())
        for key in keys:
            window[key].update(value=str(default_dict[key]))
        for paranamekey in get_all_parameter_names():
            window[paranamekey].update(background_color="#d3d7dd")
    # kagra parameter
    if name == "kagra":
        default_value = {
        # laser
        "k_inf_c_laser_power"               : 1,
        # mirror
            # transmittance
        "k_inf_c_mibs_mirror_transmittance" : 0.5,
        "k_inf_c_itmx_mirror_transmittance" : 0.004,
        "k_inf_c_itmy_mirror_transmittance" : 0.004,
        "k_inf_c_etmx_mirror_transmittance" : 5e-6,
        "k_inf_c_etmy_mirror_transmittance" : 5e-6,
        "k_inf_c_prm_mirror_transmittance"  : 0.1,
        "k_inf_c_pr2_mirror_transmittance"  : 500e-6,
        "k_inf_c_pr3_mirror_transmittance"  : 50e-6,
        "k_inf_c_srm_mirror_transmittance"  : 0.1536,
        "k_inf_c_sr2_mirror_transmittance"  : 500e-6,
        "k_inf_c_sr3_mirror_transmittance"  : 50e-6,
            # loss
        "k_inf_c_mibs_mirror_loss"          : 0,
        "k_inf_c_itmx_mirror_loss"          : 45e-6,
        "k_inf_c_itmy_mirror_loss"          : 45e-6,
        "k_inf_c_etmx_mirror_loss"          : 45e-6,
        "k_inf_c_etmy_mirror_loss"          : 45e-6,
        "k_inf_c_prm_mirror_loss"           : 45e-6,
        "k_inf_c_pr2_mirror_loss"           : 45e-6,
        "k_inf_c_pr3_mirror_loss"           : 45e-6,
        "k_inf_c_srm_mirror_loss"           : 45e-6,
        "k_inf_c_sr2_mirror_loss"           : 45e-6,
        "k_inf_c_sr3_mirror_loss"           : 45e-6,
        # length
        "k_inf_c_length_prm_pr2"            : 14.7615,#slpr1
        "k_inf_c_length_pr2_pr3"            : 11.0661,#slpr2
        "k_inf_c_length_pr3_bs"             : 15.7638,#slpr3
        "k_inf_c_length_bs_itmx"            : 26.4018,#lx
        "k_inf_c_length_itmx_etmx"          : 3000,#sx1
        "k_inf_c_length_bs_itmy"            : 23.072,#ly
        "k_inf_c_length_itmy_etmy"          : 3000,#sy1
        "k_inf_c_length_srm_sr2"            : 14.7412,#slsr1
        "k_inf_c_length_sr2_sr3"            : 11.1115,#slsr2
        "k_inf_c_length_sr3_bs"             : 15.7386,#slsr3
        # EOM
        "k_inf_c_f1_mod_frequency"          : 16.881e6,
        "k_inf_c_f1_mod_index"              : 0.3,
        "k_inf_c_f2_mod_frequency"          : 45.0159e6,
        "k_inf_c_f2_mod_index"              : 0.3,
        "k_inf_c_num_of_sidebands"          : 3
        }
        set_parameter(window, default_value)

def layout_drawing_change(window, path):
    """
        干渉計の画像を変更する

        Parameter
        ----------
        window : dictionary
        pysimpleguiのwindow

        path    : str

        Returns
        -------
        None
    """
    window["layout_drawing_key"].update
    window["layout_drawing_key"].update(filename=path)
    window["para_drawing_key"].update
    window["para_drawing_key"].update(filename=path)


def all_sw_dmod1_box_close(dmod1_plotnum_max, window):
    """
        sw_dmod1の項目にあるプロットするPDの数を変更するボックスを全て開く

        Parameter
        ----------
        dmod1_plotnum_max : int
        ボックスの最大数

        window            : dictionary
        pysimpleguiのwindow

        Returns
        -------
        None
    """
    for num in range(dmod1_plotnum_max):
        i = num+1
        window["kifo_sw_dmod1_plot_section%s"%str(i)].update(False)
    window["kifo_sw_dmod1_plot_section1"].update(True)
def all_tf_dmod2_box_close(dmod2_plotnum_max, window):
    """
        tf_dmod2の項目にあるプロットするPDの数を変更するボックスを全て開く

        Parameter
        ----------
        dmod1_plotnum_max : int
        ボックスの最大数

        window            : dictionary
        pysimpleguiのwindow

        Returns
        -------
        None
    """
    for num in range(dmod2_plotnum_max):
        i = num+1
        window["kifo_tf_dmod2_plot_section%s"%str(i)].update(False)
    window["kifo_tf_dmod2_plot_section1"].update(True)
def all_an_misal_box_close(an_misal_plotnum_max, window):
    """
        an_misalの項目にあるミスアラインするミラーの数を変更するボックスを全て開く

        Parameter
        ----------
        an_misal_plotnum_max : int
        ボックスの最大数

        window               : dictionary
        pysimpleguiのwindow

        Returns
        -------
        None
    """
    for num in range(an_misal_plotnum_max):
        i = num+1
        window["kifo_an_misal_plot_section%s"%str(i)].update(False)
    window["kifo_an_misal_plot_section1"].update(True)
def all_swan_gouyp_box_close(an_misal_plotnum_max, window):
    """
        swan_gouypの項目にあるミスアラインするミラーの数を変更するボックスを全て開く

        Parameter
        ----------
        an_misal_plotnum_max : int
        ボックスの最大数

        window               : dictionary
        pysimpleguiのwindow

        Returns
        -------
        None
    """
    for num in range(an_misal_plotnum_max):
        i = num+1
        #i = num
        window["kifo_swan_gouyp_plot_section%s_a"%str(i)].update(False)
    window["kifo_swan_gouyp_plot_section1_a"].update(True)
def all_an_misal_mirt_box_close(an_misal_mirtnum_max, window):
    """
        an_misalの項目にあるプロットするPDの数を変更するボックスを全て開く

        Parameter
        ----------
        an_misal_mirtnum_max : int
        ボックスの最大数

        window               : dictionary
        pysimpleguiのwindow

        Returns
        -------
        None
    """
    for num in range(an_misal_mirtnum_max):
        i = num+1
        window["kifo_an_misal_mirt_section%s"%str(i)].update(False)
    window["kifo_an_misal_mirt_section1"].update(True)
def all_swan_gouyp_mirt_box_close(an_misal_mirtnum_max, window):
    """
        swan_gouypの項目にあるプロットするPDの数を変更するボックスを全て開く

        Parameter
        ----------
        an_misal_mirtnum_max : int
        ボックスの最大数

        window               : dictionary
        pysimpleguiのwindow

        Returns
        -------
        None
    """
    for num in range(an_misal_mirtnum_max):
        i = num+1
        window["kifo_swan_gouyp_section%s"%str(i)].update(False)
    window["kifo_swan_gouyp_section1"].update(True)

def disable_all_section(window):
    """
        GUIのシミュレーションのtabのすべてのセクションを閉じる

        Parameter
        ----------
        window : dictionary
        pysimpleguiのwindow

        Returns
        -------
        None
    """
    tmp = "ifo"
    all_section = [
        # classification
        "k%s_classification_length"%tmp,
        "k%s_classification_alignment"%tmp,
        #section
        "k%s_sec_sw_setting"%tmp,
        "k%s_sec_tf_setting"%tmp,
        "k%s_sec_st_setting"%tmp,
        "k%s_sec_swan_setting"%tmp,
        "k%s_sec_tfan_setting"%tmp,
        #"k%s_sec_tfan_setting"%tmp,
        "k%s_sec_sw_power_setting"%tmp,
        "k%s_sec_sw_amptd_setting"%tmp,
        #"k%s_sw_amptd_ad_hom_order_list"%tmp,
        "k%s_sec_sw_dmodp_setting"%tmp,
        "k%s_sec_sw_dmod1_setting"%tmp,
        "k%s_sec_tf_power_setting"%tmp,
        #"k%s_sec_tf_amptd_setting"%tmp,
        "k%s_sec_tf_dmod2_setting"%tmp,
        "k%s_sec_st_type1_setting"%tmp,

        #"k%s_sec_an_misal_setting"%tmp,
        "k%s_sec_swan_mirot_setting"%tmp,
        "k%s_sec_swan_mirot_power_setting"%tmp,
        "k%s_sec_swan_mirot_amptd_setting"%tmp,
        "k%s_sec_swan_mirot_dmod1_setting"%tmp,
        "k%s_sec_swan_gouyp_setting"%tmp,
        "k%s_sec_swan_gouyp_power_setting"%tmp,
        "k%s_sec_swan_gouyp_amptd_setting"%tmp,
        "k%s_sec_swan_gouyp_dmod1_setting"%tmp,
        "k%s_sec_swan_dmodp_setting"%tmp,
        "k%s_sec_tfan_mirot_setting"%tmp,
        
                  ]
    for key in all_section:
        window[key].update(False)

def all_invisible_box(currentnum, num_max, keyword_head, window):
    """
        window["keyword_head+%s"%数]のボックスのvisibleをすべてFalseにして表示しないようにさせる
        window自体をFalseにできないものなどに使う

        Parameter
        ----------
        currentnum   : int
        開く個数
        num_max      : int
        ボックスの最大数
        keyword_head : str
        windowのkeyの上のところ
        window : dictionary
        pysimpleguiのwindow

        Returns
        -------
        None

        Example Use
        -----------
    """
    for num in range(num_max):
        i = num+1
        window[keyword_head+"%s"%str(i)].update(visible=False)

def repeat_visible_box(currentnum, num_max, keyword_head, window):
    """
        window["keyword_head+%s"%数]のボックスをすべて閉じてcurrentnumの数だけ開く

        Parameter
        ----------
        currentnum   : int
        開く個数
        num_max      : int
        ボックスの最大数
        keyword_head : str
        windowのkeyの上のところ
        window : dictionary
        pysimpleguiのwindow

        Returns
        -------
        None

        Example Use
        -----------
    """

    all_invisible_box(currentnum, num_max, keyword_head, window)

    currentnum += 1
    if currentnum>=num_max:
        currentnum=num_max
        #sg.pop()
    for i in range(currentnum):
        num = i+1
        window[keyword_head+"%s"%str(num)].update(visible=True)

def all_close_box(currentnum, num_max, keyword_head, window):
    """
        window["keyword_head+%s"%数]のボックスをすべて閉じる

        Parameter
        ----------
        currentnum   : int
        開く個数
        num_max      : int
        ボックスの最大数
        keyword_head : str
        windowのkeyの上のところ
        window : dictionary
        pysimpleguiのwindow

        Returns
        -------
        None

        Example Use
        -----------
    """
    for num in range(num_max):
        i = num+1
        window[keyword_head+"%s"%str(i)].update(False)

def mirror_sec_condition(condition, window, mirrorname="mirror_param_sec_itmx", close_all=False):
    """
        GUIのparameterのtabにあるミラーのセクションを開いたり閉じたりする

        Parameter
        ----------
        condition  : bool
        Trueで開く,Falseで閉じる

        window     : dictionary
        pysimpleguiのwindow

        mirrorname : str
        開くミラーのセクションのkey

        close_all  : bool
        Trueの時すべてのセクションを閉じる

        Returns
        -------
        None

        Example Use
        -----------
    """
    if close_all==True:
        mirrorname_list = [
            "mibs", 
            "itmx", "itmy", "etmx", "etmy", 
            "prm", "pr2", "pr3", 
            "srm", "sr2", "sr3"
        ]
        mirrorname_list = [
            "mirror_param_sec_mibs", 
            "mirror_param_sec_itmx", "mirror_param_sec_itmy", "mirror_param_sec_etmx", "mirror_param_sec_etmy", 
            "mirror_param_sec_prm", "mirror_param_sec_pr2", "mirror_param_sec_pr3", 
            "mirror_param_sec_srm", "mirror_param_sec_sr2", "mirror_param_sec_sr3"
        ]
        for mirrorname in mirrorname_list:
            window[mirrorname].update(False)
    else:
        window[mirrorname].update(condition)

def change_default_plotscale(scale_name, window, beg, end):
    """
        GUI上のx軸の値とスケールを変更する

        Parameter
        ----------
        scale_name : str
        スケールの名前 lin,log

        window     : dictionary
        pysimpleguiのwindow

        beg        : int
        x軸の開始の値

        end        : int
        x軸の終了の値

        Returns
        -------
        None

        Example Use
        -----------
    """
    if scale_name=="lin":
        window["k_inf_c_xaxis_lin"].update(True)
        window["k_inf_c_xaxis_log"].update(False)
        window["k_inf_c_xaxis_range_beg"].update(value=str(beg))
        window["k_inf_c_xaxis_range_end"].update(value=str(end))
    elif scale_name=="log":
        window["k_inf_c_xaxis_lin"].update(False)
        window["k_inf_c_xaxis_log"].update(True)
        window["k_inf_c_xaxis_range_beg"].update(value=str(beg))
        window["k_inf_c_xaxis_range_end"].update(value=str(end))

def change_xaxis_object(what_to_move, window):
    """
        GUI上の表示で、x軸を動かす部分のセクションを切り替える

        Parameter
        ----------
        what_to_move : str
        DoF,misalign_mirror...

        window     : dictionary
        pysimpleguiのwindow

        Returns
        -------
        None

        Example Use
        -----------
    """
    window["kifo_dof_selection"].update(visible=False)
    window["kifo_misal_mirror_selection"].update(visible=False)
    if what_to_move=="dof":
        window["kifo_dof_selection"].update(visible=True)
        window["kifo_dof_selection"].update(True)
        window["dof_selection_menu"].update(True)
        window["misalign_mirror_selection_menu"].update(False)
        window["tfan_dof_selection_menu"].update(False)
    elif what_to_move=="misalign_mirror":
        window["kifo_misal_mirror_selection"].update(visible=True)
        window["kifo_misal_mirror_selection"].update(True)
        window["dof_selection_menu"].update(False)
        window["misalign_mirror_selection_menu"].update(True)
        window["tfan_dof_selection_menu"].update(False)
    elif what_to_move=="tfan_mirror":
        window["kifo_misal_mirror_selection"].update(visible=False)
        window["kifo_misal_mirror_selection"].update(False)
        window["dof_selection_menu"].update(False)
        window["misalign_mirror_selection_menu"].update(False)
        window["tfan_dof_selection_menu"].update(True)
    return


def classification_selector(classification_name, indig_num, indig_num_max, window, initilize=True):
    """
        GUI上のシミュレーションのtabでlengthを押した時とalignmentを押した時に表示するセクションを開く

        Parameter
        ----------
        classification_name : str
        length, alignment...

        indig_num : int
        an_misal_mirtnum, swan_gouyp_mirtnum...

        indig_num_max : int
        an_misal_mirtnum_max, swan_gouyp_mirtnum_max...

        window     : dictionary
        pysimpleguiのwindow

        initilize : bool

        Returns
        -------
        None

        Example Use
        -----------
    """
    if classification_name=="length":
        disable_all_section(window)
        window["kifo_classification_length"].update(True)
        window["dof_selection_menu"].update(True)
        window["misalign_mirror_selection_menu"].update(False)
        window["kifo_selection2_text"].update(value='2. Select which DoF to move')
        if initilize:
            window["kifo_sec_sw_setting"].update(True)
            window["kifo_sec_sw_power_setting"].update(True)
            change_default_plotscale("lin", window,-180,180)
            change_xaxis_object("dof", window)
            change_default_plotscale("lin", window, -180,180)
    elif classification_name=="alignment":
        disable_all_section(window)
        window["kifo_classification_alignment"].update(True)
        window["dof_selection_menu"].update(False)
        window["misalign_mirror_selection_menu"].update(True)
        window["kifo_selection2_text"].update(value='2. Select which mirror to misalign')
        if initilize:
            alignment_swan_obj_selector("mirror misalign", indig_num, indig_num_max, window)
    else:
        pass


# def length_selector()
#     pass
def alignment_swan_obj_selector(select_name, indig_num, indig_num_max, window):
    """
        GUI上のシミュレーションのtabでlengthを押した時とalignmentを押した時に表示するセクションを開く

        Parameter
        ----------
        select_name : str
        mirror misalign, gouy phase...

        indig_num : int
        an_misal_mirtnum, swan_gouyp_mirtnum...

        indig_num_max : int
        an_misal_mirtnum_max, swan_gouyp_mirtnum_max...

        window     : dictionary
        pysimpleguiのwindow

        Returns
        -------
        None

        Example Use
        -----------
    """
    classification_selector("alignment", indig_num, indig_num_max, window, False)
    window["kifo_sec_swan_setting"].update(True)
    if select_name=="mirror misalign":
        alignment_swan_mirot_obj_selector("power", indig_num, indig_num_max, window)
        window["kifo_sec_swan_mirot_setting"].update(True)
        dof_selector_alignment_swan_mirot_templates(window)
    elif select_name=="gouy phase":
        alignment_swan_gouyp_obj_selector("power", indig_num, indig_num_max, window)
        window["kifo_sec_swan_gouyp_setting"].update(True)
        dof_selector_alignment_swan_gouyp_templates(window)
    elif select_name=="dmod phase":
        window["kifo_sec_swan_dmodp_setting"].update(True)
        dof_selector_alignment_swan_dmodp_templates(window)
    else:
        pass


def dof_selector_length_all_close(window):
    """
        GUI上のシミュレーションのtabでlengthのsectionを全て閉じる

        Parameter
        ----------
        window     : dictionary
        pysimpleguiのwindow

        Returns
        -------
        None

        Example Use
        -----------
    """
    window["kifo_dof_selection"].update(False)
def dof_selector_alignment_all_close(window):
    """
        GUI上のシミュレーションのtabでalignmentのsectionを全て閉じる

        Parameter
        ----------
        window     : dictionary
        pysimpleguiのwindow

        Returns
        -------
        None

        Example Use
        -----------
    """
    window["misalign_mirror_dof_radiobox1"].update(visible=False)
    window["misalign_mirror_dof_radiobox2"].update(visible=False)
    window["misalign_dof_box_1"].update(False)
    window["kifo_an_misal_use_dof_sc_1"].update(False)
    window["misalign_dof_box_2"].update(False)
    window["kifo_an_misal_use_dof_sc_2"].update(False)
    window["misalign_dof_box_1_gouyp"].update(False)
    window["kifo_swan_gouy_use_dof_sc_1"].update(False)
    window["misalign_dof_box_2_gouyp"].update(False)
    window["kifo_swan_gouy_use_dof_sc_2"].update(False)
    window["swan_dof_dmodp_mirot_radiobox"].update(visible=False)
    window["swan_dof_dmodp_mirot_dc_template"].update(False)
    window["swan_dof_dmodp_mirot_radiobox_key_1"].update(False)
    window["swan_dof_dmodp_mirot_dc_optional"].update(False)
    window["swan_dof_dmodp_mirot_radiobox_key_2"].update(False)
    window["swan_dof_dmodp_mirot_fsig_template"].update(False)
    window["swan_dof_dmodp_mirot_fsig_radiobox_key_1"].update(False)
    window["swan_dof_dmodp_mirot_fsig_optional"].update(False)
    window["swan_dof_dmodp_mirot_fsig_radiobox_key_2"].update(False)
    window["tfan_dof_dmod2_mirot_fsig_template"].update(False)
    window["tfan_dof_dmod2_mirot_fsig_optional"].update(False)
    


def dof_selector_alignment_swan_mirot_templates(window):
    """
        GUI上のシミュレーションのtabでalignment-swanを押した時にsweepさせるミラーを選択するtemplateのセクションを開く
        GUIのxaxisのスケールなどをデフォルトの値に変更する

        Parameter
        ----------
        window     : dictionary
        pysimpleguiのwindow

        Returns
        -------
        None

        Example Use
        -----------
    """
    window["misalign_mirror_dof_radiobox1"].update(visible=True)
    window["misalign_mirror_dof_radiobox2"].update(visible=False)
    window["misalign_dof_box_1"].update(True)
    window["kifo_an_misal_use_dof_sc_1"].update(True)
    window["misalign_dof_box_2"].update(False)
    window["kifo_an_misal_use_dof_sc_2"].update(False)
    window["misalign_dof_box_1_gouyp"].update(False)
    window["kifo_swan_gouy_use_dof_sc_1"].update(False)
    window["misalign_dof_box_2_gouyp"].update(False)
    window["kifo_swan_gouy_use_dof_sc_2"].update(False)
    
    window["swan_dof_dmodp_mirot_radiobox"].update(visible=False)
    window["swan_dof_dmodp_mirot_dc_template"].update(False)
    window["swan_dof_dmodp_mirot_radiobox_key_1"].update(False)
    window["swan_dof_dmodp_mirot_dc_optional"].update(False)
    window["swan_dof_dmodp_mirot_radiobox_key_2"].update(False)
    window["swan_dof_dmodp_mirot_fsig_template"].update(False)
    window["swan_dof_dmodp_mirot_fsig_radiobox_key_1"].update(False)
    window["swan_dof_dmodp_mirot_fsig_optional"].update(False)
    window["swan_dof_dmodp_mirot_fsig_radiobox_key_2"].update(False)
    change_default_plotscale("lin", window,-0.000001,0.000001)
def dof_selector_alignment_swan_mirot_optional(window):
    """
        GUI上のシミュレーションのtabでalignment-swanを押した時にsweepさせるミラーを選択するoptionalのセクションを開く
        GUIのxaxisのスケールなどをデフォルトの値に変更する

        Parameter
        ----------
        window     : dictionary
        pysimpleguiのwindow

        Returns
        -------
        None

        Example Use
        -----------
    """
    window["misalign_mirror_dof_radiobox1"].update(visible=True)
    window["misalign_mirror_dof_radiobox2"].update(visible=False)
    window["misalign_dof_box_1"].update(False)
    window["kifo_an_misal_use_dof_sc_1"].update(False)
    window["misalign_dof_box_2"].update(True)
    window["kifo_an_misal_use_dof_sc_2"].update(True)
    window["misalign_dof_box_1_gouyp"].update(False)
    window["kifo_swan_gouy_use_dof_sc_1"].update(False)
    window["misalign_dof_box_2_gouyp"].update(False)
    window["kifo_swan_gouy_use_dof_sc_2"].update(False)
    window["swan_dof_dmodp_mirot_radiobox"].update(visible=False)
    window["swan_dof_dmodp_mirot_dc_template"].update(False)
    window["swan_dof_dmodp_mirot_radiobox_key_1"].update(False)
    window["swan_dof_dmodp_mirot_dc_optional"].update(False)
    window["swan_dof_dmodp_mirot_radiobox_key_2"].update(False)
    window["swan_dof_dmodp_mirot_fsig_template"].update(False)
    window["swan_dof_dmodp_mirot_fsig_radiobox_key_1"].update(False)
    window["swan_dof_dmodp_mirot_fsig_optional"].update(False)
    window["swan_dof_dmodp_mirot_fsig_radiobox_key_2"].update(False)
    change_default_plotscale("lin", window,-0.000001,0.000001)
def dof_selector_alignment_swan_gouyp_templates(window):
    """
        GUI上のシミュレーションのtabでalignment-swanを押した時にsweepさせるgouy phaseを選択するtemplateのセクションを開く
        GUIのxaxisのスケールなどをデフォルトの値に変更する

        Parameter
        ----------
        window     : dictionary
        pysimpleguiのwindow

        Returns
        -------
        None

        Example Use
        -----------
    """
    window["misalign_mirror_dof_radiobox1"].update(visible=False)
    window["misalign_mirror_dof_radiobox2"].update(visible=True)
    window["misalign_dof_box_1"].update(False)
    window["kifo_an_misal_use_dof_sc_1"].update(False)
    window["misalign_dof_box_2"].update(False)
    window["kifo_an_misal_use_dof_sc_2"].update(False)
    window["misalign_dof_box_1_gouyp"].update(True)
    window["kifo_swan_gouy_use_dof_sc_1"].update(True)
    window["misalign_dof_box_2_gouyp"].update(False)
    window["kifo_swan_gouy_use_dof_sc_2"].update(False)

    window["swan_dof_dmodp_mirot_radiobox"].update(visible=False)
    window["swan_dof_dmodp_mirot_dc_template"].update(False)
    window["swan_dof_dmodp_mirot_radiobox_key_1"].update(False)
    window["swan_dof_dmodp_mirot_dc_optional"].update(False)
    window["swan_dof_dmodp_mirot_radiobox_key_2"].update(False)
    window["swan_dof_dmodp_mirot_fsig_template"].update(False)
    window["swan_dof_dmodp_mirot_fsig_radiobox_key_1"].update(False)
    window["swan_dof_dmodp_mirot_fsig_optional"].update(False)
    window["swan_dof_dmodp_mirot_fsig_radiobox_key_2"].update(False)

    change_default_plotscale("lin", window,-180,180)
def dof_selector_alignment_swan_gouyp_optional(window):
    """
        GUI上のシミュレーションのtabでalignment-swanを押した時にsweepさせるgouy phaseを選択するoptionalのセクションを開く
        GUIのxaxisのスケールなどをデフォルトの値に変更する

        Parameter
        ----------
        window     : dictionary
        pysimpleguiのwindow

        Returns
        -------
        None

        Example Use
        -----------
    """
    window["misalign_mirror_dof_radiobox1"].update(visible=False)
    window["misalign_mirror_dof_radiobox2"].update(visible=True)
    window["misalign_dof_box_1"].update(False)
    window["kifo_an_misal_use_dof_sc_1"].update(False)
    window["misalign_dof_box_2"].update(False)
    window["kifo_an_misal_use_dof_sc_2"].update(False)
    window["misalign_dof_box_1_gouyp"].update(False)
    window["kifo_swan_gouy_use_dof_sc_1"].update(False)
    window["misalign_dof_box_2_gouyp"].update(True)
    window["kifo_swan_gouy_use_dof_sc_2"].update(True)
    window["swan_dof_dmodp_mirot_radiobox"].update(visible=False)
    window["swan_dof_dmodp_mirot_dc_template"].update(False)
    window["swan_dof_dmodp_mirot_radiobox_key_1"].update(True)
    window["swan_dof_dmodp_mirot_dc_optional"].update(False)
    window["swan_dof_dmodp_mirot_radiobox_key_2"].update(False)
    window["swan_dof_dmodp_mirot_fsig_template"].update(False)
    window["swan_dof_dmodp_mirot_fsig_radiobox_key_1"].update(False)
    window["swan_dof_dmodp_mirot_fsig_optional"].update(False)
    window["swan_dof_dmodp_mirot_fsig_radiobox_key_2"].update(False)
    change_default_plotscale("lin", window,-0.000001,0.000001)
def dof_selector_alignment_swan_dmodp_templates(window):
    """
        GUI上のシミュレーションのtabでalignment-swanを押した時にsweepさせるgouy phaseを選択するoptionalのセクションを開く
        GUIのxaxisのスケールなどをデフォルトの値に変更する

        Parameter
        ----------
        window     : dictionary
        pysimpleguiのwindow

        Returns
        -------
        None

        Example Use
        -----------
    """
    window["misalign_mirror_dof_radiobox1"].update(visible=False)
    window["misalign_dof_box_1"].update(False)
    window["kifo_an_misal_use_dof_sc_1"].update(False)
    window["misalign_dof_box_2"].update(False)
    window["kifo_an_misal_use_dof_sc_2"].update(False)

    window["misalign_mirror_dof_radiobox2"].update(visible=False)
    window["misalign_dof_box_1_gouyp"].update(False)
    window["kifo_swan_gouy_use_dof_sc_1"].update(False)
    window["misalign_dof_box_2_gouyp"].update(False)
    window["kifo_swan_gouy_use_dof_sc_2"].update(False)

    window["swan_dof_dmodp_mirot_radiobox"].update(visible=True)
    window["swan_dof_dmodp_mirot_dc_template"].update(True)
    window["swan_dof_dmodp_mirot_radiobox_key_1"].update(True)
    window["swan_dof_dmodp_mirot_dc_optional"].update(False)
    window["swan_dof_dmodp_mirot_radiobox_key_2"].update(False)
    window["swan_dof_dmodp_mirot_fsig_template"].update(False)
    window["swan_dof_dmodp_mirot_fsig_radiobox_key_1"].update(False)
    window["swan_dof_dmodp_mirot_fsig_optional"].update(False)
    window["swan_dof_dmodp_mirot_fsig_radiobox_key_2"].update(False)

    change_default_plotscale("lin", window,-0.000001,0.000001)
def dof_selector_alignment_swan_dmodp_optional(window):
    """
        GUI上のシミュレーションのtabでalignment-swanを押した時にsweepさせるgouy phaseを選択するoptionalのセクションを開く
        GUIのxaxisのスケールなどをデフォルトの値に変更する

        Parameter
        ----------
        window     : dictionary
        pysimpleguiのwindow

        Returns
        -------
        None

        Example Use
        -----------
    """
    window["misalign_mirror_dof_radiobox1"].update(visible=False)
    window["misalign_dof_box_1"].update(False)
    window["kifo_an_misal_use_dof_sc_1"].update(False)
    window["misalign_dof_box_2"].update(False)
    window["kifo_an_misal_use_dof_sc_2"].update(False)

    window["misalign_mirror_dof_radiobox2"].update(visible=False)
    window["misalign_dof_box_1_gouyp"].update(False)
    window["kifo_swan_gouy_use_dof_sc_1"].update(False)
    window["misalign_dof_box_2_gouyp"].update(False)
    window["kifo_swan_gouy_use_dof_sc_2"].update(False)

    window["swan_dof_dmodp_mirot_radiobox"].update(visible=True)

    window["swan_dof_dmodp_mirot_dc_template"].update(False)
    window["swan_dof_dmodp_mirot_radiobox_key_1"].update(False)
    window["swan_dof_dmodp_mirot_dc_optional"].update(True)
    window["swan_dof_dmodp_mirot_radiobox_key_2"].update(True)
    window["swan_dof_dmodp_mirot_fsig_template"].update(False)
    window["swan_dof_dmodp_mirot_fsig_radiobox_key_1"].update(False)
    window["swan_dof_dmodp_mirot_fsig_optional"].update(False)
    window["swan_dof_dmodp_mirot_fsig_radiobox_key_2"].update(False)

    change_default_plotscale("lin", window,-0.000001,0.000001)
def dof_selector_alignment_swan_dmodp_fsig_templates(window):
    """
        GUI上のシミュレーションのtabでalignment-swanを押した時にsweepさせるgouy phaseを選択するoptionalのセクションを開く
        GUIのxaxisのスケールなどをデフォルトの値に変更する

        Parameter
        ----------
        window     : dictionary
        pysimpleguiのwindow

        Returns
        -------
        None

        Example Use
        -----------
    """
    window["misalign_mirror_dof_radiobox1"].update(visible=False)
    window["misalign_dof_box_1"].update(False)
    window["kifo_an_misal_use_dof_sc_1"].update(False)
    window["misalign_dof_box_2"].update(False)
    window["kifo_an_misal_use_dof_sc_2"].update(False)

    window["misalign_mirror_dof_radiobox2"].update(visible=False)
    window["misalign_dof_box_1_gouyp"].update(False)
    window["kifo_swan_gouy_use_dof_sc_1"].update(False)
    window["misalign_dof_box_2_gouyp"].update(False)
    window["kifo_swan_gouy_use_dof_sc_2"].update(False)

    window["swan_dof_dmodp_mirot_radiobox"].update(visible=True)

    window["swan_dof_dmodp_mirot_dc_template"].update(False)
    window["swan_dof_dmodp_mirot_radiobox_key_1"].update(False)
    window["swan_dof_dmodp_mirot_dc_optional"].update(False)
    window["swan_dof_dmodp_mirot_radiobox_key_2"].update(False)
    window["swan_dof_dmodp_mirot_fsig_template"].update(True)
    window["swan_dof_dmodp_mirot_fsig_radiobox_key_1"].update(True)
    window["swan_dof_dmodp_mirot_fsig_optional"].update(False)
    window["swan_dof_dmodp_mirot_fsig_radiobox_key_2"].update(False)

    change_default_plotscale("lin", window,-0.000001,0.000001)
def dof_selector_alignment_swan_dmodp_fsig_optional(window):
    """
        GUI上のシミュレーションのtabでalignment-swanを押した時にsweepさせるgouy phaseを選択するoptionalのセクションを開く
        GUIのxaxisのスケールなどをデフォルトの値に変更する

        Parameter
        ----------
        window     : dictionary
        pysimpleguiのwindow

        Returns
        -------
        None

        Example Use
        -----------
    """
    window["misalign_mirror_dof_radiobox1"].update(visible=False)
    window["misalign_dof_box_1"].update(False)
    window["kifo_an_misal_use_dof_sc_1"].update(False)
    window["misalign_dof_box_2"].update(False)
    window["kifo_an_misal_use_dof_sc_2"].update(False)

    window["misalign_mirror_dof_radiobox2"].update(visible=False)
    window["misalign_dof_box_1_gouyp"].update(False)
    window["kifo_swan_gouy_use_dof_sc_1"].update(False)
    window["misalign_dof_box_2_gouyp"].update(False)
    window["kifo_swan_gouy_use_dof_sc_2"].update(False)

    window["swan_dof_dmodp_mirot_radiobox"].update(visible=True)

    window["swan_dof_dmodp_mirot_dc_template"].update(False)
    window["swan_dof_dmodp_mirot_radiobox_key_1"].update(False)
    window["swan_dof_dmodp_mirot_dc_optional"].update(False)
    window["swan_dof_dmodp_mirot_radiobox_key_2"].update(False)
    window["swan_dof_dmodp_mirot_fsig_template"].update(False)
    window["swan_dof_dmodp_mirot_fsig_radiobox_key_1"].update(False)
    window["swan_dof_dmodp_mirot_fsig_optional"].update(True)
    window["swan_dof_dmodp_mirot_fsig_radiobox_key_2"].update(True)

    change_default_plotscale("lin", window,-0.000001,0.000001)


def dof_selector_alignment_tfan_dmod2_template(window):
    """
        GUI上のシミュレーションのtabでalignment-swanを押した時にsweepさせるgouy phaseを選択するoptionalのセクションを開く
        GUIのxaxisのスケールなどをデフォルトの値に変更する

        Parameter
        ----------
        window     : dictionary
        pysimpleguiのwindow

        Returns
        -------
        None

        Example Use
        -----------
    """

    window["tfan_dof_dmod2_mirot_fsig_template"].update(True)
    window["tfan_dof_dmod2_fsig_mirot_radiobox_key_1"].update(True)
    window["tfan_dof_dmod2_mirot_fsig_optional"].update(False)
    window["tfan_dof_dmod2_fsig_mirot_radiobox_key_2"].update(False)
    change_default_plotscale("lin", window,-0.000001,0.000001)
def dof_selector_alignment_tfan_dmod2_optional(window):
    """
        GUI上のシミュレーションのtabでalignment-swanを押した時にsweepさせるgouy phaseを選択するoptionalのセクションを開く
        GUIのxaxisのスケールなどをデフォルトの値に変更する

        Parameter
        ----------
        window     : dictionary
        pysimpleguiのwindow

        Returns
        -------
        None

        Example Use
        -----------
    """

    window["tfan_dof_dmod2_mirot_fsig_template"].update(False)
    window["tfan_dof_dmod2_fsig_mirot_radiobox_key_1"].update(False)
    window["tfan_dof_dmod2_mirot_fsig_optional"].update(True)
    window["tfan_dof_dmod2_fsig_mirot_radiobox_key_2"].update(True)
    change_default_plotscale("lin", window,-0.000001,0.000001)
# ここから
def alignment_swan_mirot_obj_selector(select_name, an_misal_mirtnum, an_misal_mirtnum_max, window):
    """
        GUI上のシミュレーションのtabでalignment-swanを押した時に表示するセクションを開く
        GUIのxaxisのスケールなどをデフォルトの値に変更する

        Parameter
        ----------
        select_name : str
        power,amptd,dmod1...

        indig_num : int
        an_misal_mirtnum, swan_gouyp_mirtnum...

        indig_num_max : int
        an_misal_mirtnum_max, swan_gouyp_mirtnum_max...

        window     : dictionary
        pysimpleguiのwindow

        Returns
        -------
        None

        Example Use
        -----------
    """
    window["kifo_sec_swan_mirot_power_setting"].update(False)
    window["kifo_sec_swan_mirot_amptd_setting"].update(False)
    window["kifo_sec_swan_mirot_dmod1_setting"].update(False)
    if select_name=="power":
        window["kifo_sec_swan_mirot_power_setting"].update(True)
        all_invisible_box(an_misal_mirtnum, an_misal_mirtnum_max, "amptd_kifo_an_misal_mirt_section", window)
        all_invisible_box(an_misal_mirtnum, an_misal_mirtnum_max, "dmod1_kifo_an_misal_mirt_section", window)
        change_default_plotscale("lin", window,-0.000001,0.000001)
        change_xaxis_object("misalign_mirror", window)
    elif select_name=="amptd":
        window["kifo_sec_swan_mirot_amptd_setting"].update(True)
        all_invisible_box(an_misal_mirtnum, an_misal_mirtnum_max, "dmod1_kifo_an_misal_mirt_section", window)
        repeat_visible_box(an_misal_mirtnum, an_misal_mirtnum_max, "amptd_kifo_an_misal_mirt_section", window)
        change_default_plotscale("lin", window,-0.000001,0.000001)
        change_xaxis_object("misalign_mirror", window)
    elif select_name=="dmod1":
        window["kifo_sec_swan_mirot_dmod1_setting"].update(True)
        repeat_visible_box(an_misal_mirtnum, an_misal_mirtnum_max, "dmod1_kifo_an_misal_mirt_section", window)
        all_invisible_box(an_misal_mirtnum, an_misal_mirtnum_max, "amptd_kifo_an_misal_mirt_section", window)
        change_default_plotscale("lin", window,-0.000001,0.000001)
        change_xaxis_object("misalign_mirror", window)
    else:
        pass
def alignment_swan_gouyp_obj_selector(select_name, swan_gouyp_mirtnum, swan_gouyp_mirtnum_max, window):
    """
        GUI上のシミュレーションのtabでalignment-gouypを押した時に表示するセクションを開く
        GUIのxaxisのスケールなどをデフォルトの値に変更する

        Parameter
        ----------
        select_name : str
        power,amptd,dmod1...

        indig_num : int
        an_misal_mirtnum, swan_gouyp_mirtnum...

        indig_num_max : int
        an_misal_mirtnum_max, swan_gouyp_mirtnum_max...

        window     : dictionary
        pysimpleguiのwindow

        Returns
        -------
        None

        Example Use
        -----------
    """
    window["kifo_sec_swan_gouyp_power_setting"].update(False)
    window["kifo_sec_swan_gouyp_amptd_setting"].update(False)
    window["kifo_sec_swan_gouyp_dmod1_setting"].update(False)
    if select_name=="power":
        window["kifo_sec_swan_gouyp_power_setting"].update(True)
        all_invisible_box(  swan_gouyp_mirtnum, swan_gouyp_mirtnum_max, "amptd_kifo_swan_gouyp_section", window)
        all_invisible_box(  swan_gouyp_mirtnum, swan_gouyp_mirtnum_max, "dmod1_kifo_swan_gouyp_section", window)
        change_default_plotscale("lin", window,-180,180)
        change_xaxis_object("misalign_mirror", window)
    elif select_name=="amptd":
        window["kifo_sec_swan_gouyp_amptd_setting"].update(True)
        repeat_visible_box(swan_gouyp_mirtnum, swan_gouyp_mirtnum_max, "amptd_kifo_swan_gouyp_section", window)
        all_invisible_box(swan_gouyp_mirtnum, swan_gouyp_mirtnum_max, "dmod1_kifo_swan_gouyp_section", window)
        change_default_plotscale("lin", window,-180,180)
        change_xaxis_object("misalign_mirror", window)
    elif select_name=="dmod1":
        window["kifo_sec_swan_gouyp_dmod1_setting"].update(True)
        repeat_visible_box(swan_gouyp_mirtnum, swan_gouyp_mirtnum_max, "dmod1_kifo_swan_gouyp_section", window)
        all_invisible_box(  swan_gouyp_mirtnum, swan_gouyp_mirtnum_max, "amptd_kifo_swan_gouyp_section", window)
        change_default_plotscale("lin", window,-180,180)
        change_xaxis_object("misalign_mirror", window)
    else:
        pass

def alignment_tfan_obj_selector(select_name, window):#(select_name, swan_gouyp_mirtnum, swan_gouyp_mirtnum_max, window):
    """
        GUI上のシミュレーションのtabでalignment-gouypを押した時に表示するセクションを開く
        GUIのxaxisのスケールなどをデフォルトの値に変更する

        Parameter
        ----------
        select_name : str
        power,amptd,dmod1...

        indig_num : int
        an_misal_mirtnum, swan_gouyp_mirtnum...

        indig_num_max : int
        an_misal_mirtnum_max, swan_gouyp_mirtnum_max...

        window     : dictionary
        pysimpleguiのwindow

        Returns
        -------
        None

        Example Use
        -----------
    """
    if select_name=="dmod2":
        #repeat_visible_box(swan_gouyp_mirtnum, swan_gouyp_mirtnum_max, "dmod1_kifo_swan_gouyp_section", window)
        #all_invisible_box( swan_gouyp_mirtnum, swan_gouyp_mirtnum_max, "amptd_kifo_swan_gouyp_section", window)
        change_default_plotscale("lin", window,-180,180)
        change_xaxis_object("tfan_mirror", window)
        window["kifo_istfan_mirot"].update(True)
        dof_selector_alignment_tfan_dmod2_template(window)
        #window["tfan_dof_dmod2_mirot_fsig_template"].update(True)
        #window["tfan_dof_dmod2_mirot_fsig_optional"].update(False)
