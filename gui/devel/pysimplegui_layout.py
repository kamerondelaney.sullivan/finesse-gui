########################
# 実行時にFalseにすること
debugflag = False

def debug_text(debugflag, warntext):
    if debugflag:
        return [sg.Text(warntext)]
    else:
        return [sg.Text("", visible=False)]
########################

import PySimpleGUI as sg
import util_func_pysimplegui
import util_func
import matplotlib
import matplotlib.pyplot as plt
import sys
import os
import Debug_window_layout

sg.theme('LightGreen')

def collapse(layout, key):
    """
    Helper function that creates a Column that can be later made hidden, thus appearing "collapsed"
    :param layout: The layout for the section
    :param key: Key used to make this seciton visible / invisible
    :return: A pinned column that can be placed directly into your layout
    :rtype: sg.pin
    """
    return sg.pin(sg.Column(layout, key=key))


# layout
def make_layout_drawing(path,keyname):
    drawing = [
            [sg.Image(path, key=keyname,  size=(400,300),background_color="White")]#size=(800,600)
    ]
    return drawing

def make_kifo_layout():
    # fig
    # kifo
    kifo_kagra_drawing_mi_normalsize = [
                    [sg.Image('../fig/DRFPMI_picture_normal.png', key='kifo_imageContainer1',  size=(400,300),background_color="White")]#size=(800,600)
    ]
    kifo_kagra_drawing_mi_largesize = [
                    [sg.Image('../fig/DRFPMI_picture_large.png',  key='kifo_imageContainer2', size=(800,600),background_color="White")]#size=(800,600)
    ]
    kifo_kagra_drawing_fpmi_normalsize = [
                    [sg.Image('../fig/DRFPMI_picture_normal.png', key='kifo_imageContainer3',  size=(400,300),background_color="White")]#size=(800,600)
    ]
    kifo_kagra_drawing_fpmi_largesize = [
                    [sg.Image('../fig/DRFPMI_picture_large.png',  key='kifo_imageContainer4', size=(800,600),background_color="White")]#size=(800,600)
    ]
    kifo_kagra_drawing_prfpmi_normalsize = [
                    [sg.Image('../fig/DRFPMI_picture_normal.png', key='kifo_imageContainer5',  size=(400,300),background_color="White")]#size=(800,600)
    ]
    kifo_kagra_drawing_prfpmi_largesize = [
                    [sg.Image('../fig/DRFPMI_picture_large.png',  key='kifo_imageContainer6', size=(800,600),background_color="White")]#size=(800,600)
    ]
    kifo_kagra_drawing_drfpmi_normalsize = [
                    [sg.Image('../fig/DRFPMI_picture_normal.png', key='kifo_imageContainer7',  size=(400,300),background_color="White")]#size=(800,600)
    ]
    kifo_kagra_drawing_drfpmi_largesize = [
                    [sg.Image('../fig/DRFPMI_picture_large.png',  key='kifo_imageContainer8', size=(800,600),background_color="White")]#size=(800,600)
    ]
    # kifo
    kifo_drawing_size_buttons =[
                    [sg.Button('normal size', size=(10,1), font=(10), key='kifo_normalize_drawing_size'),
                     sg.Button('Large size',  size=(10,1), font=(10), key='kifo_expand_drawing_size')],
    ]
    #sw_power
    kifo_sec_sw_power_setting = [
                [sg.Text('no advanced settings')],
                ## PDs checkbox
                [sg.Checkbox('REFL', default=True, key='kifo_sw_power_REFL'), sg.Checkbox('AS',   default=True, key='kifo_sw_power_AS'),
                 sg.Checkbox('POP',  default=True, key='kifo_sw_power_POP'),
                 sg.Checkbox('TMSY',default=True, key='kifo_sw_power_TMSY'),  sg.Checkbox('TMSX',default=True, key='kifo_sw_power_TMSX'),
                 sg.Checkbox('POS',  default=True, key='kifo_sw_power_POS')],
                [sg.Checkbox('n0',    key='kifo_sw_power_n0'),
                 sg.Checkbox('n_eo1', key='kifo_sw_power_n_eo1'),sg.Checkbox('n_eo2', key='kifo_sw_power_n_eo2'),
                 sg.Checkbox('n_eo3', key='kifo_sw_power_n_eo3'),sg.Checkbox('n_eo4', key='kifo_sw_power_n_eo4'),],
                [sg.Checkbox('npr1',  key='kifo_sw_power_npr1'), sg.Checkbox('npr2',  key='kifo_sw_power_npr2'),# npr
                 sg.Checkbox('npr3',  key='kifo_sw_power_npr3'), sg.Checkbox('npr4',  key='kifo_sw_power_npr4'),
                 sg.Checkbox('npr5',  key='kifo_sw_power_npr5'), sg.Checkbox('npr6',  key='kifo_sw_power_npr6')],
                [sg.Checkbox('nsr1',  key='kifo_sw_power_nsr1'), sg.Checkbox('nsr2',  key='kifo_sw_power_nsr2'),# nsr
                 sg.Checkbox('nsr3',  key='kifo_sw_power_nsr3'), sg.Checkbox('nsr4',  key='kifo_sw_power_nsr4'),
                 sg.Checkbox('nsr5',  key='kifo_sw_power_nsr5')],
                [sg.Checkbox('n2',    key='kifo_sw_power_n2'),   sg.Checkbox('n3',    key='kifo_sw_power_n3'),# n
                 sg.Checkbox('ny1',   key='kifo_sw_power_ny1'),  sg.Checkbox('nx1',   key='kifo_sw_power_nx1'),
                 sg.Checkbox('ny2',   key='kifo_sw_power_ny2'),  sg.Checkbox('nx2',   key='kifo_sw_power_nx2'),
                 sg.Checkbox('ny3',   key='kifo_sw_power_ny3'),  sg.Checkbox('nx3',   key='kifo_sw_power_nx3')],
                ]
    #tf_power
    kifo_sec_tf_power_setting = [
                [sg.Text('no advanced settings')]
                ]
    #sw_amptd
    amptdlist = ["CR_field", "f1_SB_field_upper", "f1_SB_field_lower", "f2_SB_field_upper", "f2_SB_field_lower"]
    sw_amptd_hom_n_order_list = ["0", "1", "2", "3", "4", "5"]
    sw_amptd_hom_m_order_list = ["0", "1", "2", "3", "4", "5"]
    kifo_sw_amptd_ad_hom_order_list = [
            [sg.Text('Select n mode: ', key="select_amptd_n_mode_key"), sg.Combo((sw_amptd_hom_n_order_list) , size=(20,1), default_value=sw_amptd_hom_n_order_list[0] , key='kifo_sw_amptd_ad_hom_n_order', disabled=False)],
            [sg.Text('Select m mode: ', key="select_amptd_m_mode_key"), sg.Combo((sw_amptd_hom_m_order_list) , size=(20,1), default_value=sw_amptd_hom_m_order_list[0] , key='kifo_sw_amptd_ad_hom_m_order', disabled=False)]
            ]
    #util_func_pysimplegui.frame_layout(layouts,frame_keys,display_texts)

    def get_amptd_field_select_box(num):
        amptd_field_select   =  [
            [sg.Checkbox("Use this detector",key = f"kifo_sw_amptd_field_select_box_field_checkbox_{num}", enable_events=True)],
            [sg.Text("Field",key = f"kifo_sw_amptd_field_select_box_field_text_{num}"),sg.Combo(["carrier","f1_upper"],key = f"kifo_sw_amptd_field_select_box_field_combo_{num}", enable_events=True, default_value="carrier")
            ,sg.Text("TEM_n",key = f"kifo_sw_amptd_field_select_box_TEM_n_text_{num}"),sg.Combo(["0","1"],             key = f"kifo_sw_amptd_field_select_box_TEM_n_combo_{num}", enable_events=True, default_value="0")
            ,sg.Text("TEM_m",key = f"kifo_sw_amptd_field_select_box_TEM_m_text_{num}"),sg.Combo(["0","1"],             key = f"kifo_sw_amptd_field_select_box_TEM_m_combo_{num}", enable_events=True, default_value="0")
            ,sg.Text("port", key = f"kifo_sw_amptd_field_select_box_port_text_{num}") ,sg.Combo(["REFL","AS"],         key = f"kifo_sw_amptd_field_select_box_port_combo_{num}",  enable_events=True, default_value="REFL")]
        ]
        return amptd_field_select

    layouts = []
    for num in range(10):
        layouts.append(
            util_func_pysimplegui.frame_layout([get_amptd_field_select_box(f"{num}")],[f"amptd_field_select_box_detector_{num}"],[f"detector_{num}"])
        )
    kifo_sec_sw_amptd_setting = [
        util_func_pysimplegui.frame_layout([layouts],["Select_sideband_field_key"],["Select sideband field"])
        ]

        
    #sw_dmod1
    dmod_freqlist = ["","DC", "fsb1", "2fsb1", "3fsb1", "fsb2", "2fsb2", "3fsb2"]
    dmod_phaselist = ["", "Iphase", "Qphase", "optimal"]

    sw_dmod1_box = []
    ######
    num = 0
    phase_tooltip = "I:0[deg]\n"+"Q:90[deg]\n"+"optimal:calculate arg(I+iQ) and demodulate signal by using this phase\n"
    for i in range(20):
        num = i+1
        layout_name = "kifo_sw_dmod1_plot_section%s"%str(num)
        exec(
'''
%s =[
[sg.Text("%02d")
,sg.Text('port = ')
,sg.Combo((util_func.get_all_port_list())      , size=(20,1), default_value=util_func.get_all_port_list()[0]      , key='kifo_sw_dmod1_port_combo%s', readonly=True)
,sg.Text('demod freq = ')
,sg.Combo((dmod_freqlist) , size=(20,1), default_value=dmod_freqlist[2] , key='kifo_sw_dmod1_freq_combo%s')
,sg.Text('demod phase = ')
,sg.Combo((dmod_phaselist), size=(20,1), default_value=dmod_phaselist[1], key='kifo_sw_dmod1_phase_combo%s', tooltip=phase_tooltip)]
]
'''%(layout_name, num, str(num), str(num), str(num)))
        
        exec('sw_dmod1_box.append([collapse(kifo_sw_dmod1_plot_section%s, "kifo_sw_dmod1_plot_section%s")])'%(str(num), str(num)))
        #exec('sw_dmod1_box.append([sg.Column(kifo_sw_dmod1_plot_section%s, key="kifo_sw_dmod1_plot_section%s")])'%(str(num), str(num)))
    #####
    kifo_sec_sw_dmod1_setting = [
            #[sg.Text('if select "plot separately", pd results displayed all separately.')],
            #[sg.Radio('overplot selected port output', 'RADIO_sw_dmod1_plot', default=False, key='kDRFPMI_sw_dmod1_overplot', enable_events=True),
             #sg.Radio('plot separately',        'RADIO_sw_dmod1_plot', default=True,  key='kDRFPMI_sw_dmod1_sepaplot', enable_events=True)],
            #[sg.Text('Which phase to plot?')],
            [sg.Text('select "port", "demodulation frequency", "demodulation phase"'), sg.Button('-', button_color=('white', 'black'), key='kifo_sw_dmod1_plotminus'), sg.Button('+', button_color=('white', 'black'), key='kifo_sw_dmod1_plotplus')],
            [collapse(sw_dmod1_box, 'sw_dmod1_box')],
            [sg.Text('select option from list or input number directly')],
            ]

    # sw_dmodp
    dmod_freqlist = ["", "fsb1", "2fsb1", "3fsb1", "fsb2", "2fsb2", "3fsb2"]
    dmod_phaselist = ["", "Iphase", "Qphase", "optimal"]
    phase_tooltip = "I:0[deg]\n"+"Q:90[deg]\n"+"optimal:calculate arg(I+iQ) and demodulate signal by using this phase\n"
    sw_dmodp_box_01 = [
        [sg.Text("01"),
        sg.Text('port = '),
        sg.Combo((util_func.get_all_port_list()), size=(20,1), default_value=util_func.get_all_port_list()[0]      , key='kifo_sw_dmodp_port_combo1', readonly=True),
        sg.Text('demod freq = '),
        sg.Combo((dmod_freqlist)                , size=(20,1), default_value=dmod_freqlist[2] , key='kifo_sw_dmodp_freq_combo1')]
        ]
    sw_dmodp_box_02 = [
        [sg.Text("02"),
        sg.Text('port = '),
        sg.Combo((util_func.get_all_port_list()), size=(20,1), default_value=util_func.get_all_port_list()[0]      , key='kifo_sw_dmodp_port_combo2', readonly=True),
        sg.Text('demod freq = '),
        sg.Combo((dmod_freqlist)                , size=(20,1), default_value=dmod_freqlist[2] , key='kifo_sw_dmodp_freq_combo2')]
        ]
    sw_dmodp_box_03 = [
        [sg.Text("03"),
        sg.Text('port = '),
        sg.Combo((util_func.get_all_port_list()), size=(20,1), default_value=util_func.get_all_port_list()[0]      , key='kifo_sw_dmodp_port_combo3', readonly=True),
        sg.Text('demod freq = '),
        sg.Combo((dmod_freqlist)                , size=(20,1), default_value=dmod_freqlist[2] , key='kifo_sw_dmodp_freq_combo3')]
        ]
    sw_dmodp_box_04 = [
        [sg.Text("04"),
        sg.Text('port = '),
        sg.Combo((util_func.get_all_port_list()), size=(20,1), default_value=util_func.get_all_port_list()[0]      , key='kifo_sw_dmodp_port_combo4', readonly=True),
        sg.Text('demod freq = '),
        sg.Combo((dmod_freqlist)                , size=(20,1), default_value=dmod_freqlist[2] , key='kifo_sw_dmodp_freq_combo4')]
        ]
    sw_dmodp_box_05 = [
        [sg.Text("05"),
        sg.Text('port = '),
        sg.Combo((util_func.get_all_port_list()), size=(20,1), default_value=util_func.get_all_port_list()[0]      , key='kifo_sw_dmodp_port_combo5', readonly=True),
        sg.Text('demod freq = '),
        sg.Combo((dmod_freqlist)                , size=(20,1), default_value=dmod_freqlist[2] , key='kifo_sw_dmodp_freq_combo5')]
        ]
    sw_dmodp_box_06 = [
        [sg.Text("06"),
        sg.Text('port = '),
        sg.Combo((util_func.get_all_port_list()), size=(20,1), default_value=util_func.get_all_port_list()[0]      , key='kifo_sw_dmodp_port_combo6', readonly=True),
        sg.Text('demod freq = '),
        sg.Combo((dmod_freqlist)                , size=(20,1), default_value=dmod_freqlist[2] , key='kifo_sw_dmodp_freq_combo6')]
        ]
    sw_dmodp_box_07 = [
        [sg.Text("07"),
        sg.Text('port = '),
        sg.Combo((util_func.get_all_port_list()), size=(20,1), default_value=util_func.get_all_port_list()[0]      , key='kifo_sw_dmodp_port_combo7', readonly=True),
        sg.Text('demod freq = '),
        sg.Combo((dmod_freqlist)                , size=(20,1), default_value=dmod_freqlist[2] , key='kifo_sw_dmodp_freq_combo7')]
        ]
    sw_dmodp_box_08 = [
        [sg.Text("08"),
        sg.Text('port = '),
        sg.Combo((util_func.get_all_port_list()), size=(20,1), default_value=util_func.get_all_port_list()[0]      , key='kifo_sw_dmodp_port_combo8', readonly=True),
        sg.Text('demod freq = '),
        sg.Combo((dmod_freqlist)                , size=(20,1), default_value=dmod_freqlist[2] , key='kifo_sw_dmodp_freq_combo8')]
        ]
    sw_dmodp_box_09 = [
        [sg.Text("09"),
        sg.Text('port = '),
        sg.Combo((util_func.get_all_port_list()), size=(20,1), default_value=util_func.get_all_port_list()[0]      , key='kifo_sw_dmodp_port_combo9', readonly=True),
        sg.Text('demod freq = '),
        sg.Combo((dmod_freqlist)                , size=(20,1), default_value=dmod_freqlist[2] , key='kifo_sw_dmodp_freq_combo9')]
        ]
    sw_dmodp_box_10 = [
        [sg.Text("10"),
        sg.Text('port = '),
        sg.Combo((util_func.get_all_port_list()), size=(20,1), default_value=util_func.get_all_port_list()[0]      , key='kifo_sw_dmodp_port_combo10', readonly=True),
        sg.Text('demod freq = '),
        sg.Combo((dmod_freqlist)                , size=(20,1), default_value=dmod_freqlist[2] , key='kifo_sw_dmodp_freq_combo10')]
        ]
    kifo_sec_sw_dmodp_setting = [
        [sg.Text("This function can be used to obtain the optimal demodulation phase.")],
        #[sg.Text('select "port", "demodulation frequency"'), sg.Button('-', button_color=('white', 'black'), key='kifo_sw_dmodp_plotminus'), sg.Button('+', button_color=('white', 'black'), key='kifo_sw_dmodp_plotplus')],
        [sg.Text('select "port", "demodulation frequency"'), sg.Combo(("1","2","3","4","5","6","7","8","9","10",), size=(2,1), default_value="1", key='kifo_sw_dmodp_box_combo', readonly=True, enable_events=True)],

        [collapse(sw_dmodp_box_01, "sw_dmodp_box_1")],
        [collapse(sw_dmodp_box_02, "sw_dmodp_box_2")],
        [collapse(sw_dmodp_box_03, "sw_dmodp_box_3")],
        [collapse(sw_dmodp_box_04, "sw_dmodp_box_4")],
        [collapse(sw_dmodp_box_05, "sw_dmodp_box_5")],
        [collapse(sw_dmodp_box_06, "sw_dmodp_box_6")],
        [collapse(sw_dmodp_box_07, "sw_dmodp_box_7")],
        [collapse(sw_dmodp_box_08, "sw_dmodp_box_8")],
        [collapse(sw_dmodp_box_09, "sw_dmodp_box_9")],
        [collapse(sw_dmodp_box_10, "sw_dmodp_box_10")]

    ]
    #tf_dmod2
    dmod_freqlist = ["", "fsb1", "2fsb1", "3fsb1", "fsb2", "2fsb2", "3fsb2"]
    dmod_phaselist = ["", "Iphase", "Qphase", "optimal"]
    tf_dmod2_box = []
    #st_type1
    dmod_freqlist = ["", "fsb1", "2fsb1", "3fsb1", "fsb2", "2fsb2", "3fsb2"]
    dmod_phaselist = ["", "Iphase", "Qphase", "optimal"]
    st_type1_box_1 = [
        [sg.Text("01"),
        sg.Text('port = '),
        sg.Combo((util_func.get_all_port_list()), size=(20,1), default_value=util_func.get_all_port_list()[0]      , key='kifo_st_type1_port_combo1', readonly=True),
        sg.Text('demod freq = '),
        sg.Combo((dmod_freqlist)                , size=(20,1) , default_value=dmod_freqlist[2]  , key='kifo_st_type1_freq_combo1'),
        sg.Text('demod phase = '),
        sg.Combo((dmod_phaselist)                , size=(20,1), default_value=dmod_phaselist[2] , key='kifo_st_type1_phase_combo1')]
        ]
    st_type1_box_2 = [
        [sg.Text("02"),
        sg.Text('port = '),
        sg.Combo((util_func.get_all_port_list()), size=(20,1), default_value=util_func.get_all_port_list()[0]      , key='kifo_st_type1_port_combo2', readonly=True),
        sg.Text('demod freq = '),
        sg.Combo((dmod_freqlist)                , size=(20,1) , default_value=dmod_freqlist[2]  , key='kifo_st_type1_freq_combo2'),
        sg.Text('demod phase = '),
        sg.Combo((dmod_phaselist)                , size=(20,1), default_value=dmod_phaselist[2] , key='kifo_st_type1_phase_combo2')]
        ]
    st_type1_box_3 = [
        [sg.Text("03"),
        sg.Text('port = '),
        sg.Combo((util_func.get_all_port_list()), size=(20,1), default_value=util_func.get_all_port_list()[0]      , key='kifo_st_type1_port_combo3', readonly=True),
        sg.Text('demod freq = '),
        sg.Combo((dmod_freqlist)                , size=(20,1) , default_value=dmod_freqlist[2]  , key='kifo_st_type1_freq_combo3'),
        sg.Text('demod phase = '),
        sg.Combo((dmod_phaselist)                , size=(20,1), default_value=dmod_phaselist[2] , key='kifo_st_type1_phase_combo3')]
        ]
    st_type1_box_4 = [
        [sg.Text("04"),
        sg.Text('port = '),
        sg.Combo((util_func.get_all_port_list()), size=(20,1), default_value=util_func.get_all_port_list()[0]      , key='kifo_st_type1_port_combo4', readonly=True),
        sg.Text('demod freq = '),
        sg.Combo((dmod_freqlist)                , size=(20,1) , default_value=dmod_freqlist[2]  , key='kifo_st_type1_freq_combo4'),
        sg.Text('demod phase = '),
        sg.Combo((dmod_phaselist)                , size=(20,1), default_value=dmod_phaselist[2] , key='kifo_st_type1_phase_combo4')]
        ]
    st_type1_box_5 = [
        [sg.Text("05"),
        sg.Text('port = '),
        sg.Combo((util_func.get_all_port_list()), size=(20,1), default_value=util_func.get_all_port_list()[0]      , key='kifo_st_type1_port_combo5', readonly=True),
        sg.Text('demod freq = '),
        sg.Combo((dmod_freqlist)                , size=(20,1) , default_value=dmod_freqlist[2]  , key='kifo_st_type1_freq_combo5'),
        sg.Text('demod phase = '),
        sg.Combo((dmod_phaselist)                , size=(20,1), default_value=dmod_phaselist[2] , key='kifo_st_type1_phase_combo5')]
        ]
    st_type1_box_6 = [
        [sg.Text("06"),
        sg.Text('port = '),
        sg.Combo((util_func.get_all_port_list()), size=(20,1), default_value=util_func.get_all_port_list()[0]      , key='kifo_st_type1_port_combo6', readonly=True),
        sg.Text('demod freq = '),
        sg.Combo((dmod_freqlist)                , size=(20,1) , default_value=dmod_freqlist[2]  , key='kifo_st_type1_freq_combo6'),
        sg.Text('demod phase = '),
        sg.Combo((dmod_phaselist)                , size=(20,1), default_value=dmod_phaselist[2] , key='kifo_st_type1_phase_combo6')]
        ]
    st_type1_box_7 = [
        [sg.Text("07"),
        sg.Text('port = '),
        sg.Combo((util_func.get_all_port_list()), size=(20,1), default_value=util_func.get_all_port_list()[0]      , key='kifo_st_type1_port_combo7', readonly=True),
        sg.Text('demod freq = '),
        sg.Combo((dmod_freqlist)                , size=(20,1) , default_value=dmod_freqlist[2]  , key='kifo_st_type1_freq_combo7'),
        sg.Text('demod phase = '),
        sg.Combo((dmod_phaselist)                , size=(20,1), default_value=dmod_phaselist[2] , key='kifo_st_type1_phase_combo7')]
        ]
    st_type1_box_8 = [
        [sg.Text("08"),
        sg.Text('port = '),
        sg.Combo((util_func.get_all_port_list()), size=(20,1), default_value=util_func.get_all_port_list()[0]      , key='kifo_st_type1_port_combo8', readonly=True),
        sg.Text('demod freq = '),
        sg.Combo((dmod_freqlist)                , size=(20,1) , default_value=dmod_freqlist[2]  , key='kifo_st_type1_freq_combo8'),
        sg.Text('demod phase = '),
        sg.Combo((dmod_phaselist)                , size=(20,1), default_value=dmod_phaselist[2] , key='kifo_st_type1_phase_combo8')]
        ]
    st_type1_box_9 = [
        [sg.Text("09"),
        sg.Text('port = '),
        sg.Combo((util_func.get_all_port_list()), size=(20,1), default_value=util_func.get_all_port_list()[0]      , key='kifo_st_type1_port_combo9', readonly=True),
        sg.Text('demod freq = '),
        sg.Combo((dmod_freqlist)                , size=(20,1) , default_value=dmod_freqlist[2]  , key='kifo_st_type1_freq_combo9'),
        sg.Text('demod phase = '),
        sg.Combo((dmod_phaselist)                , size=(20,1), default_value=dmod_phaselist[2] , key='kifo_st_type1_phase_combo9')]
        ]
    st_type1_box_10 = [
        [sg.Text("10"),
        sg.Text('port = '),
        sg.Combo((util_func.get_all_port_list()), size=(20,1), default_value=util_func.get_all_port_list()[0]      , key='kifo_st_type1_port_combo10', readonly=True),
        sg.Text('demod freq = '),
        sg.Combo((dmod_freqlist)                , size=(20,1) , default_value=dmod_freqlist[2]  , key='kifo_st_type1_freq_combo10'),
        sg.Text('demod phase = '),
        sg.Combo((dmod_phaselist)                , size=(20,1), default_value=dmod_phaselist[2] , key='kifo_st_type1_phase_combo10')]
        ]

    kifo_sec_st_type1_setting = [
        debug_text(debugflag, "kifo_sec_st_type1_setting"),
        [sg.Text('select "port", "demodulation frequency", "demodulation phase"'), sg.Combo(("1","2","3","4","5","6","7","8","9","10",), size=(2,1), default_value="1", key='kifo_st_type1_box_combo', readonly=True, enable_events=True)],
        [collapse(st_type1_box_1, "st_type1_box_1")],
        [collapse(st_type1_box_2, "st_type1_box_2")],
        [collapse(st_type1_box_3, "st_type1_box_3")],
        [collapse(st_type1_box_4, "st_type1_box_4")],
        [collapse(st_type1_box_5, "st_type1_box_5")],
        [collapse(st_type1_box_6, "st_type1_box_6")],
        [collapse(st_type1_box_7, "st_type1_box_7")],
        [collapse(st_type1_box_8, "st_type1_box_8")],
        [collapse(st_type1_box_9, "st_type1_box_9")],
        [collapse(st_type1_box_10,"st_type1_box_10")],
        [sg.Text('no advanced settings')],
        ## PDs checkbox
        [sg.Checkbox('REFL', default=True, key='kifo_st_type1_REFL'), sg.Checkbox('AS',   default=True, key='kifo_st_type1_AS'),
            sg.Checkbox('POP',  default=True, key='kifo_st_type1_POP'),
            sg.Checkbox('TMSY',default=True, key='kifo_st_type1_TMSY'),  sg.Checkbox('TMSX',default=True, key='kifo_st_type1_TMSX'),
            sg.Checkbox('POS',  default=True, key='kifo_st_type1_POS')],
        [sg.Checkbox('n0',    key='kifo_st_type1_n0'),
            sg.Checkbox('n_eo1', key='kifo_st_type1_n_eo1'),sg.Checkbox('n_eo2', key='kifo_st_type1_n_eo2'),
            sg.Checkbox('n_eo3', key='kifo_st_type1_n_eo3'),sg.Checkbox('n_eo4', key='kifo_st_type1_n_eo4'),],
        [sg.Checkbox('npr1',  key='kifo_st_type1_npr1'), sg.Checkbox('npr2',  key='kifo_st_type1_npr2'),# npr
            sg.Checkbox('npr3',  key='kifo_st_type1_npr3'), sg.Checkbox('npr4',  key='kifo_st_type1_npr4'),
            sg.Checkbox('npr5',  key='kifo_st_type1_npr5'), sg.Checkbox('npr6',  key='kifo_st_type1_npr6')],
        [sg.Checkbox('nsr1',  key='kifo_st_type1_nsr1'), sg.Checkbox('nsr2',  key='kifo_st_type1_nsr2'),# nsr
            sg.Checkbox('nsr3',  key='kifo_st_type1_nsr3'), sg.Checkbox('nsr4',  key='kifo_st_type1_nsr4'),
            sg.Checkbox('nsr5',  key='kifo_st_type1_nsr5')],
        [sg.Checkbox('n2',    key='kifo_st_type1_n2'),   sg.Checkbox('n3',    key='kifo_st_type1_n3'),# n
            sg.Checkbox('ny1',   key='kifo_st_type1_ny1'),  sg.Checkbox('nx1',   key='kifo_st_type1_nx1'),
            sg.Checkbox('ny2',   key='kifo_st_type1_ny2'),  sg.Checkbox('nx2',   key='kifo_st_type1_nx2'),
            sg.Checkbox('ny3',   key='kifo_st_type1_ny3'),  sg.Checkbox('nx3',   key='kifo_st_type1_nx3')],
    ]
    #st_type2
    dmod_freqlist = ["", "fsb1", "2fsb1", "3fsb1", "fsb2", "2fsb2", "3fsb2"]
    dmod_phaselist = ["", "Iphase", "Qphase", "optimal"]
    st_type2_box_1 = [
        [sg.Text("01"),
        sg.Text('port = '),
        sg.Combo((util_func.get_all_port_list()), size=(20,1), default_value=util_func.get_all_port_list()[0]      , key='kifo_st_type2_port_combo1', readonly=True),
        sg.Text('demod freq = '),
        sg.Combo((dmod_freqlist)                , size=(20,1) , default_value=dmod_freqlist[2]  , key='kifo_st_type2_freq_combo1'),
        sg.Text('demod phase = '),
        sg.Combo((dmod_phaselist)                , size=(20,1), default_value=dmod_phaselist[2] , key='kifo_st_type2_phase_combo1')]
        ]
    st_type2_box_2 = [
        [sg.Text("02"),
        sg.Text('port = '),
        sg.Combo((util_func.get_all_port_list()), size=(20,1), default_value=util_func.get_all_port_list()[0]      , key='kifo_st_type2_port_combo2', readonly=True),
        sg.Text('demod freq = '),
        sg.Combo((dmod_freqlist)                , size=(20,1) , default_value=dmod_freqlist[2]  , key='kifo_st_type2_freq_combo2'),
        sg.Text('demod phase = '),
        sg.Combo((dmod_phaselist)                , size=(20,1), default_value=dmod_phaselist[2] , key='kifo_st_type2_phase_combo2')]
        ]
    st_type2_box_3 = [
        [sg.Text("03"),
        sg.Text('port = '),
        sg.Combo((util_func.get_all_port_list()), size=(20,1), default_value=util_func.get_all_port_list()[0]      , key='kifo_st_type2_port_combo3', readonly=True),
        sg.Text('demod freq = '),
        sg.Combo((dmod_freqlist)                , size=(20,1) , default_value=dmod_freqlist[2]  , key='kifo_st_type2_freq_combo3'),
        sg.Text('demod phase = '),
        sg.Combo((dmod_phaselist)                , size=(20,1), default_value=dmod_phaselist[2] , key='kifo_st_type2_phase_combo3')]
        ]
    st_type2_box_4 = [
        [sg.Text("04"),
        sg.Text('port = '),
        sg.Combo((util_func.get_all_port_list()), size=(20,1), default_value=util_func.get_all_port_list()[0]      , key='kifo_st_type2_port_combo4', readonly=True),
        sg.Text('demod freq = '),
        sg.Combo((dmod_freqlist)                , size=(20,1) , default_value=dmod_freqlist[2]  , key='kifo_st_type2_freq_combo4'),
        sg.Text('demod phase = '),
        sg.Combo((dmod_phaselist)                , size=(20,1), default_value=dmod_phaselist[2] , key='kifo_st_type2_phase_combo4')]
        ]
    st_type2_box_5 = [
        [sg.Text("05"),
        sg.Text('port = '),
        sg.Combo((util_func.get_all_port_list()), size=(20,1), default_value=util_func.get_all_port_list()[0]      , key='kifo_st_type2_port_combo5', readonly=True),
        sg.Text('demod freq = '),
        sg.Combo((dmod_freqlist)                , size=(20,1) , default_value=dmod_freqlist[2]  , key='kifo_st_type2_freq_combo5'),
        sg.Text('demod phase = '),
        sg.Combo((dmod_phaselist)                , size=(20,1), default_value=dmod_phaselist[2] , key='kifo_st_type2_phase_combo5')]
        ]
    st_type2_box_6 = [
        [sg.Text("06"),
        sg.Text('port = '),
        sg.Combo((util_func.get_all_port_list()), size=(20,1), default_value=util_func.get_all_port_list()[0]      , key='kifo_st_type2_port_combo6', readonly=True),
        sg.Text('demod freq = '),
        sg.Combo((dmod_freqlist)                , size=(20,1) , default_value=dmod_freqlist[2]  , key='kifo_st_type2_freq_combo6'),
        sg.Text('demod phase = '),
        sg.Combo((dmod_phaselist)                , size=(20,1), default_value=dmod_phaselist[2] , key='kifo_st_type2_phase_combo6')]
        ]
    st_type2_box_7 = [
        [sg.Text("07"),
        sg.Text('port = '),
        sg.Combo((util_func.get_all_port_list()), size=(20,1), default_value=util_func.get_all_port_list()[0]      , key='kifo_st_type2_port_combo7', readonly=True),
        sg.Text('demod freq = '),
        sg.Combo((dmod_freqlist)                , size=(20,1) , default_value=dmod_freqlist[2]  , key='kifo_st_type2_freq_combo7'),
        sg.Text('demod phase = '),
        sg.Combo((dmod_phaselist)                , size=(20,1), default_value=dmod_phaselist[2] , key='kifo_st_type2_phase_combo7')]
        ]
    st_type2_box_8 = [
        [sg.Text("08"),
        sg.Text('port = '),
        sg.Combo((util_func.get_all_port_list()), size=(20,1), default_value=util_func.get_all_port_list()[0]      , key='kifo_st_type2_port_combo8', readonly=True),
        sg.Text('demod freq = '),
        sg.Combo((dmod_freqlist)                , size=(20,1) , default_value=dmod_freqlist[2]  , key='kifo_st_type2_freq_combo8'),
        sg.Text('demod phase = '),
        sg.Combo((dmod_phaselist)                , size=(20,1), default_value=dmod_phaselist[2] , key='kifo_st_type2_phase_combo8')]
        ]
    st_type2_box_9 = [
        [sg.Text("09"),
        sg.Text('port = '),
        sg.Combo((util_func.get_all_port_list()), size=(20,1), default_value=util_func.get_all_port_list()[0]      , key='kifo_st_type2_port_combo9', readonly=True),
        sg.Text('demod freq = '),
        sg.Combo((dmod_freqlist)                , size=(20,1) , default_value=dmod_freqlist[2]  , key='kifo_st_type2_freq_combo9'),
        sg.Text('demod phase = '),
        sg.Combo((dmod_phaselist)                , size=(20,1), default_value=dmod_phaselist[2] , key='kifo_st_type2_phase_combo9')]
        ]
    st_type2_box_10 = [
        [sg.Text("10"),
        sg.Text('port = '),
        sg.Combo((util_func.get_all_port_list()), size=(20,1), default_value=util_func.get_all_port_list()[0]      , key='kifo_st_type2_port_combo10', readonly=True),
        sg.Text('demod freq = '),
        sg.Combo((dmod_freqlist)                , size=(20,1) , default_value=dmod_freqlist[2]  , key='kifo_st_type2_freq_combo10'),
        sg.Text('demod phase = '),
        sg.Combo((dmod_phaselist)                , size=(20,1), default_value=dmod_phaselist[2] , key='kifo_st_type2_phase_combo10')]
        ]

    kifo_sec_st_type2_setting = [
        debug_text(debugflag, "kifo_sec_st_type2_setting"),
        #[sg.Text('no advanced settings')],
        [sg.Text('select "port", "demodulation frequency", "demodulation phase"'), sg.Combo(("1","2","3","4","5","6","7","8","9","10",), size=(2,1), default_value="1", key='kifo_st_type2_box_combo', readonly=True, enable_events=True)],
        [collapse(st_type2_box_1, "st_type2_box_1")],
        [collapse(st_type2_box_2, "st_type2_box_2")],
        [collapse(st_type2_box_3, "st_type2_box_3")],
        [collapse(st_type2_box_4, "st_type2_box_4")],
        [collapse(st_type2_box_5, "st_type2_box_5")],
        [collapse(st_type2_box_6, "st_type2_box_6")],
        [collapse(st_type2_box_7, "st_type2_box_7")],
        [collapse(st_type2_box_8, "st_type2_box_8")],
        [collapse(st_type2_box_9, "st_type2_box_9")],
        [collapse(st_type2_box_10,"st_type2_box_10")]

    ]
    #st_type3
    kifo_sec_st_type3_setting = [
        debug_text(debugflag, "kifo_sec_st_type3_setting"),
        [sg.Text('no advanced settings')],
        ## PDs checkbox
        [sg.Checkbox('REFL', default=True, key='kifo_st_type3_REFL'), sg.Checkbox('AS',   default=True, key='kifo_st_type3_AS'),
            sg.Checkbox('POP',  default=True, key='kifo_st_type3_POP'),
            sg.Checkbox('TMSY',default=True, key='kifo_st_type3_TMSY'),  sg.Checkbox('TMSX',default=True, key='kifo_st_type3_TMSX'),
            sg.Checkbox('POS',  default=True, key='kifo_st_type3_POS')],
        [sg.Checkbox('n0',    key='kifo_st_type3_n0'),
            sg.Checkbox('n_eo1', key='kifo_st_type3_n_eo1'),sg.Checkbox('n_eo2', key='kifo_st_type3_n_eo2'),
            sg.Checkbox('n_eo3', key='kifo_st_type3_n_eo3'),sg.Checkbox('n_eo4', key='kifo_st_type3_n_eo4'),],
        [sg.Checkbox('npr1',  key='kifo_st_type3_npr1'), sg.Checkbox('npr2',  key='kifo_st_type3_npr2'),# npr
            sg.Checkbox('npr3',  key='kifo_st_type3_npr3'), sg.Checkbox('npr4',  key='kifo_st_type3_npr4'),
            sg.Checkbox('npr5',  key='kifo_st_type3_npr5'), sg.Checkbox('npr6',  key='kifo_st_type3_npr6')],
        [sg.Checkbox('nsr1',  key='kifo_st_type3_nsr1'), sg.Checkbox('nsr2',  key='kifo_st_type3_nsr2'),# nsr
            sg.Checkbox('nsr3',  key='kifo_st_type3_nsr3'), sg.Checkbox('nsr4',  key='kifo_st_type3_nsr4'),
            sg.Checkbox('nsr5',  key='kifo_st_type3_nsr5')],
        [sg.Checkbox('n2',    key='kifo_st_type3_n2'),   sg.Checkbox('n3',    key='kifo_st_type3_n3'),# n
            sg.Checkbox('ny1',   key='kifo_st_type3_ny1'),  sg.Checkbox('nx1',   key='kifo_st_type3_nx1'),
            sg.Checkbox('ny2',   key='kifo_st_type3_ny2'),  sg.Checkbox('nx2',   key='kifo_st_type3_nx2'),
            sg.Checkbox('ny3',   key='kifo_st_type3_ny3'),  sg.Checkbox('nx3',   key='kifo_st_type3_nx3')],
    ]
    ######
    num = 0
    phase_tooltip = "I:0[deg]\n"+"Q:90[deg]\n"+"optimal:calculate arg(I+iQ) and demodulate signal by using this phase\n"
    for i in range(20):
        num = i+1
        layout_name = "kifo_tf_dmod2_plot_section%s"%str(num)
        exec(
'''
%s =[
[sg.Text("%s")
,sg.Text('port = ')
,sg.Combo((util_func.get_all_port_list())      , size=(20,1), default_value=util_func.get_all_port_list()[0]      , key='kifo_tf_dmod2_port_combo%s', readonly=True)
,sg.Text('demod freq = ')
,sg.Combo((dmod_freqlist) , size=(20,1), default_value=dmod_freqlist[1] , key='kifo_tf_dmod2_freq_combo%s')
,sg.Text('demod phase = ')
,sg.Combo((dmod_phaselist), size=(20,1), default_value=dmod_phaselist[1], key='kifo_tf_dmod2_phase_combo%s', tooltip=phase_tooltip)]
]
'''%(layout_name, str(num), str(num), str(num), str(num)))
        
        exec('tf_dmod2_box.append([collapse(kifo_tf_dmod2_plot_section%s, "kifo_tf_dmod2_plot_section%s")])'%(str(num), str(num)))
    #####
    kifo_sec_tf_dmod2_setting = [
    
            #[sg.Text('if select "plot separately", pd results displayed all separately.')],
            #[sg.Radio('overplot selected port output', 'RADIO_sw_dmod2_plot', default=False, key='kDRFPMI_sw_dmod2_overplot', enable_events=True),
             #sg.Radio('plot separately'              , 'RADIO_sw_dmod2_plot', default=True,  key='kDRFPMI_sw_dmod2_sepaplot', enable_events=True)],
            #[sg.Text('Which phase to plot?')],
            [sg.Text('select "demodulation frequency", "demodulation phase", "port"')],
            [sg.Text('select option from list or input number directly'), sg.Button('-', button_color=('white', 'black'), key='kifo_tf_dmod2_plotminus'), sg.Button('+', button_color=('white', 'black'), key='kifo_tf_dmod2_plotplus')
            ,sg.Checkbox('Matrix',   key='kifo_tf_dmod2_matrix')],
            
            [collapse(tf_dmod2_box, "tf_dmod2_box")],
            
            ]

    #### angular


    #an_misal
    ######
    num = 0
    an_misal_box = []#使ってない
    an_misal_box_1 = []#PDを置くやつ
    an_misal_box_2 = []#dof2 optional
    an_misal_box_2_a = []# dof1 template　のつもりだったが使ってない
    #an_misal_box_port = []
    #an_misal_box_mirt = []

    # kifo_an_misal_mirt_section
    # an_misal_box_1
    for i in range(20):
        num = i+1
        layout_name = "kifo_an_misal_mirt_section%s"%str(num)
        exec('''
# for amplitude detector
amptd_%s = [

[
sg.Text("  ")
,sg.Text('sideband = ',                                         key='kifo_swan_mirot_side_text_%s')
,sg.Combo(("fsb1", "fsb2"), size=(20,1), default_value="fsb1",  key='kifo_swan_mirot_side_combo%s'
,readonly=True)

,sg.Text('Gaussian mode n = ',                                  key='kifo_swan_mirot_gaun_text_%s')
,sg.Combo(("0","1","2","3"), size=(20,1), default_value="0",    key='kifo_swan_mirot_gaun_combo%s' ,readonly=True)

,sg.Text('Gaussian mode m = ',                                  key='kifo_swan_mirot_gaum_text_%s')
,sg.Combo(("0","1","2","3"), size=(20,1), default_value="0",    key='kifo_swan_mirot_gaum_combo%s' ,readonly=True)

]
]

dmod1_%s = [

# for dmod1 detector
[sg.Text("  ")
,sg.Text('demodulation frequency = ',                           key='kifo_swan_mirot_dmof_text_%s')
,sg.Combo(("fsb1","fsb2"), size=(20,1), default_value="fsb1",   key='kifo_swan_mirot_dmof_combo%s',readonly=True)

,sg.Text('demodulation phase = ',                               key='kifo_swan_mirot_dmop_text_%s')
,sg.Combo(("0","90","optimal+I","optimal+Q"), size=(20,1),      key='kifo_swan_mirot_dmop_combo%s', default_value="optimal+I",readonly=True)

]
]

%s =[

[sg.Text("%02d")
,sg.Text('detection port = ')
,sg.Combo((util_func.get_main_port_list()), size=(20,1),         key='kifo_an_misal_port_combo%s', default_value=util_func.get_main_port_list()[0],readonly=True)

,sg.Text('QPD divide direction = ')
,sg.Combo(("pitch", "yaw"), size=(20,1), default_value="pitch", key='kifo_swan_mirot_pddr_combo%s'
,readonly=True)

,sg.Text('gouy phase = ',                                       key='kifo_swan_mirot_pdgy_text_%s')
,sg.Combo(("0","90"), size=(20,1),                              key='kifo_swan_mirot_pdgy_combo%s', default_value="0")

],


[collapse(amptd_%s, 'amptd_%s')],

[collapse(dmod1_%s, 'dmod1_%s')],

]
'''%(layout_name,num,num,num,num,num,num,layout_name,num,num,num,num,layout_name,num,num,num,num,num,layout_name,layout_name,layout_name,layout_name))
        #exec('''an_misal_box.append([collapse(kifo_an_misal_mirt_section%s, "kifo_an_misal_mirt_section%s")])'''%(str(num), str(num)))
        exec('''an_misal_box_1.append([collapse(kifo_an_misal_mirt_section%s, "kifo_an_misal_mirt_section%s")])'''%(str(num), str(num)))
        #exec('''an_misal_box_port.append([collapse(kifo_an_misal_mirt_section%s, "kifo_an_misal_mirt_section%s")])'''%(str(num), str(num)))

    # kifo_an_misal_plot_section
    # an_misal_box_2
    for i in range(20):
        num = i+1
        layout_name = "kifo_an_misal_plot_section%s"%str(num)
        exec('''
%s =[

[sg.Text("%02d")
,sg.Text('mirror = ')
,sg.Combo(util_func.get_all_mirror_list("DRFPMI"), size=(20,1), default_value="ITMX", key='kifo_an_misal_mirr_combo%s'
,readonly=True)
,sg.Text('derection = ')
,sg.Combo(("+", "-"), size=(20,1), default_value="+", key='kifo_an_misal_plmi_combo%s'
,readonly=True)
,sg.Combo(("pitch", "yaw"), size=(20,1), default_value="pitch", key='kifo_an_misal_drct_combo%s'#drct direction
,readonly=True)
#,sg.Text('gouy phase = ')
#,sg.Combo(("45", "90"), size=(20,1), default_value="0", key='kifo_an_misal_a_rtan_combo%s'
#,readonly=True)

]

]
'''%(layout_name, num, num, num, num, num))
        #exec('''an_misal_box.append([collapse(kifo_an_misal_plot_section%s, "kifo_an_misal_plot_section%s")])'''%(str(num), str(num)))
        exec('''an_misal_box_2.append([collapse(kifo_an_misal_plot_section%s, "kifo_an_misal_plot_section%s")])'''%(str(num), str(num)))
        
    # boxの名前の付け方のルールを間違えたけど作ってしまったのでこのまま作成する
    swan_gouyp_box_1 = []#PDを置くやつ
    swan_gouyp_box_2 = []#dof1のつもりだったが、直接入力したらしい template
    swan_gouyp_box_1_a = []#PDを置くやつ２　使ってない
    swan_gouyp_box_2_a = []# dof2 optional
    # kifo_swan_gouyp_section
    for i in range(20):
        num = i+1
        layout_name = "kifo_swan_gouyp_section%s"%str(num)
        exec('''
# for amplitude detector
amptd_%s = [

[

sg.Text("  ")
,sg.Text('sideband = ',                                  key='kifo_swan_gouyp_side_text_%s')
,sg.Combo(("fsb1", "fsb2"), size=(20,1), default_value="fsb1",    key='kifo_swan_gouyp_side_combo%s'
,readonly=True)

,sg.Text("  ")
,sg.Text('Gaussian mode n = ',                                  key='kifo_swan_gouyp_gaun_text_%s')
,sg.Combo(("0","1","2","3"), size=(20,1), default_value="0",    key='kifo_swan_gouyp_gaun_combo%s'
,readonly=True)

,sg.Text('Gaussian mode m = ',                                  key='kifo_swan_gouyp_gaum_text_%s')
,sg.Combo(("0","1","2","3"), size=(20,1), default_value="0",    key='kifo_swan_gouyp_gaum_combo%s'
,readonly=True)
]
]

dmod1_%s = [

# for dmod1 detector
[sg.Text("  ")
,sg.Text('demodulation frequency = ',                           key='kifo_swan_gouyp_dmof_text_%s')
,sg.Combo(("fsb1","fsb2"), size=(20,1), default_value="fsb1",    key='kifo_swan_gouyp_dmof_combo%s'
,readonly=True)

,sg.Text('demodulation phase = ',                               key='kifo_swan_gouyp_dmop_text_%s')
,sg.Combo(("0","90","optimal+I","optimal+Q"), size=(20,1), default_value="optimal+I",    key='kifo_swan_gouyp_dmop_combo%s'
,readonly=True)
]
]

%s =[

[sg.Text("%02d")
,sg.Text('detection port = ')
,sg.Combo((util_func.get_all_port_list()), size=(20,1), default_value=util_func.get_all_port_list()[0], key='kifo_swan_gouyp_port_combo%s'
,readonly=True)

,sg.Text('QPD divide direction = ')
,sg.Combo(("pitch", "yaw"), size=(20,1), default_value="pitch", key='kifo_swan_gouyp_pddr_combo%s'
,readonly=True)

# dofの方に移した
#,sg.Text('mirror rotate phase = ')
#,sg.Combo(("0", "45", "90"), size=(20,1), default_value="0",    key='kifo_swan_gouyp_gyan_combo%s'
#,readonly=True)
],


[collapse(amptd_%s, 'amptd_%s')],

[collapse(dmod1_%s, 'dmod1_%s')],

]
'''%(layout_name,num,num,num,num,num,num,layout_name,num,num,num,num,layout_name,num,num,num,num,layout_name,layout_name,layout_name,layout_name))
        #exec('''an_misal_box.append([collapse(kifo_an_misal_mirt_section%s, "kifo_an_misal_mirt_section%s")])'''%(str(num), str(num)))
        exec('''swan_gouyp_box_1.append([collapse(kifo_swan_gouyp_section%s, "kifo_swan_gouyp_section%s")])'''%(str(num), str(num)))
        #exec('''an_misal_box_port.append([collapse(kifo_an_misal_mirt_section%s, "kifo_an_misal_mirt_section%s")])'''%(str(num), str(num)))

    # kifo_an_misal_plot_section
    for i in range(20):
        num = i+1
        layout_name = "kifo_swan_gouyp_plot_section%s"%str(num)
        exec('''
%s =[

[sg.Text("%02d")
,sg.Text('mirror = ')
,sg.Combo(util_func.get_all_mirror_list("DRFPMI"), size=(20,1), default_value="ITMX", key='kifo_swan_gouyp_combo%s'
,readonly=True)
,sg.Text('derection = ')
,sg.Combo(("+", "-"), size=(20,1), default_value="+",           key='kifo_swan_gouyp_plmi_combo%s'
,readonly=True)
,sg.Combo(("pitch", "yaw"), size=(20,1), default_value="pitch", key='kifo_swan_gouyp_drct_combo%s'
,readonly=True)
#,sg.Text('rotate_angle = ')
#,sg.Combo(("45", "90"), size=(20,1), default_value="1e-6",    key='kifo_swan_gouyp_a_rtan_combo%s'
#,readonly=True)

]

]
'''%(layout_name, num, num, num, num, num))
        #exec('''an_misal_box.append([collapse(kifo_an_misal_plot_section%s, "kifo_an_misal_plot_section%s")])'''%(str(num), str(num)))
        exec('''swan_gouyp_box_2.append([collapse(kifo_swan_gouyp_plot_section%s, "kifo_swan_gouyp_plot_section%s")])'''%(str(num), str(num)))
        
# an_misal_box_2_a
    for i in range(20):
        num = i+1
        layout_name = "kifo_swan_gouyp_plot_section%s_a"%str(num)
        exec('''
%s =[

[sg.Text("%02d")
,sg.Text('mirror = ')
,sg.Combo(util_func.get_all_mirror_list("DRFPMI"), size=(20,1), default_value="ITMX", key='kifo_swan_gouyp_a_mirr_combo%s'
,readonly=True)
,sg.Text('derection = ')
,sg.Combo(("+", "-"), size=(20,1), default_value="+",                                 key='kifo_swan_gouyp_a_plmi_combo%s'
,readonly=True)
,sg.Combo(("pitch", "yaw"), size=(20,1), default_value="pitch",                       key='kifo_swan_gouyp_a_drct_combo%s'
,readonly=True)
,sg.Text('rotate angle = ')
,sg.Combo(("-60", "20"), size=(20,1), default_value="1e-6",                          key='kifo_swan_gouyp_a_rtan_combo%s'
,readonly=True)

]

]
'''%(layout_name, num, num, num, num, num))
        exec('''swan_gouyp_box_2_a.append([collapse(kifo_swan_gouyp_plot_section%s_a, "kifo_swan_gouyp_plot_section%s_a")])'''% (str(num), str(num)))
       #exec('''an_misal_box_2  .append([collapse(kifo_an_misal_plot_section%s  , "kifo_an_misal_plot_section%s  ")])'''%(str(num), str(num)))
    
    # ali_swan_mirot_power
    kifo_sec_swan_mirot_power_setting= [
        debug_text(debugflag, "kifo_sec_swan_mirot_power_setting"),
    ]
    # ali_swan_mirot_amptd
    kifo_sec_swan_mirot_amptd_setting= [
        debug_text(debugflag, "kifo_sec_swan_mirot_amptd_setting"),
    ]
    # ali_swan_mirot_dmod1
    kifo_sec_swan_mirot_dmod1_setting= [
        debug_text(debugflag, "kifo_sec_swan_mirot_dmod1_setting"),
    ]

    # ali_swan_gouyp_power
    kifo_sec_swan_gouyp_power_setting= [
        debug_text(debugflag, "kifo_sec_swan_gouyp_power_setting"),
    ]
    # ali_swan_gouyp_amptd
    kifo_sec_swan_gouyp_amptd_setting= [
        debug_text(debugflag, "kifo_sec_swan_gouyp_amptd_setting"),
    ]
    # ali_swan_gouyp_dmod1
    kifo_sec_swan_gouyp_dmod1_setting= [
        debug_text(debugflag, "kifo_sec_swan_gouyp_dmod1_setting"),
    ]
    kifo_sec_swan_mirot_setting= [
        debug_text(debugflag, "kifo_sec_swan_mirot_setting"),
        [sg.Text('Select mirot')],
        [sg.Radio('Power detector [W]'       , 'kifo_swan_mirot_RADIO', default=True  , key='kifo_isswan_mirot_power', enable_events=True)
        ,sg.Radio('Amplitud detector'        , 'kifo_swan_mirot_RADIO', default=False , key='kifo_isswan_mirot_amptd', enable_events=True)
        ,sg.Radio('Demodulated signal [A.U.]', 'kifo_swan_mirot_RADIO', default=False , key='kifo_isswan_mirot_dmod1', enable_events=True)],

        [collapse(kifo_sec_swan_mirot_power_setting, 'kifo_sec_swan_mirot_power_setting')],
        [collapse(kifo_sec_swan_mirot_amptd_setting, 'kifo_sec_swan_mirot_amptd_setting')],
        [collapse(kifo_sec_swan_mirot_dmod1_setting, 'kifo_sec_swan_mirot_dmod1_setting')],

        # mirror rotate num
        [sg.Text('select "port", "QPD divided axis"'), 
        sg.Button('-', button_color=('white', 'black'), key='kifo_an_misal_mirtminus'),
        sg.Button('+', button_color=('white', 'black'), key='kifo_an_misal_mirtplus')],
        [collapse(an_misal_box_1, 'an_misal_box_1')],

    ]
    
    kifo_sec_swan_gouyp_setting= [
        debug_text(debugflag, "kifo_sec_swan_gouyp_setting"),
        [sg.Text('Select gouy')],
        [sg.Radio('Power detector [W]'       , 'kifo_swan_gouyp_RADIO', default=True  , key='kifo_isswan_gouyp_power', enable_events=True)
        ,sg.Radio('Amplitud detector'        , 'kifo_swan_gouyp_RADIO', default=False , key='kifo_isswan_gouyp_amptd', enable_events=True)
        ,sg.Radio('Demodulated signal [A.U.]', 'kifo_swan_gouyp_RADIO', default=False , key='kifo_isswan_gouyp_dmod1', enable_events=True)],

        [collapse(kifo_sec_swan_gouyp_power_setting, 'kifo_sec_swan_gouyp_power_setting')],
        [collapse(kifo_sec_swan_gouyp_amptd_setting, 'kifo_sec_swan_gouyp_amptd_setting')],
        [collapse(kifo_sec_swan_gouyp_dmod1_setting, 'kifo_sec_swan_gouyp_dmod1_setting')],

        # gouyp box num
        [sg.Text('select "port", "QPD divided axis"'), 
        sg.Button('-', button_color=('white', 'black'), key='kifo_swan_gouyp_boxnum_minus'),
        sg.Button('+', button_color=('white', 'black'), key='kifo_swan_gouyp_boxnum_plus')],
        [collapse(swan_gouyp_box_1, 'swan_gouyp_box_1')],
    ]


    def get_swan_dmodp_select_box(num):
        dmodp_detector_select   =  [
            [sg.Checkbox("Use this detector", key = f"kifo_swan_dmodp_detector_select_box_detector_checkbox_{num}", enable_events=True)],
            [sg.Text("port",                  key = f"kifo_swan_dmodp_detector_select_box_port_text_{num}"),                  sg.Combo(["REFL","AS"],  key = f"kifo_swan_dmodp_detector_select_box_port_combo_{num}",                  enable_events=True, default_value="REFL")
            ,sg.Text("split direction",       key = f"kifo_swan_dmodp_detector_select_box_split_direction_text_{num}"),       sg.Combo(["yaw","pitch"],      key = f"kifo_swan_dmodp_detector_select_box_split_direction_combo_{num}",       enable_events=True, default_value="yaw")
            ,sg.Text("demodularion frequency",key = f"kifo_swan_dmodp_detector_select_box_demodularion_frequency_text_{num}"),sg.Combo(["fsb1","fsb2"],key = f"kifo_swan_dmodp_detector_select_box_demodularion_frequency_combo_{num}",enable_events=True, default_value="fsb1")
            ,sg.Text("gouy phase",            key = f"kifo_swan_dmodp_detector_select_box_gouy_phase_text_{num}"),            sg.Combo(["0","90"],     key = f"kifo_swan_dmodp_detector_select_box_gouy_phasecombo_{num}",             enable_events=True, default_value="0")]
        ]
        return dmodp_detector_select

    layouts = []
    for num in range(10):
        layouts.append(
            util_func_pysimplegui.frame_layout([get_swan_dmodp_select_box(f"{num}")],[f"swan_dmodp_detector_select_box_detector_{num}"],[f"detector_{num}"])
        )
    kifo_sec_swan_dmodp_setting = [
        [sg.Checkbox("Open",key="Select_swan_dmodp_detector_open_checkbox",enable_events=True)],
        [collapse([util_func_pysimplegui.frame_layout([layouts],["Select_swan_dmodp_detector_key"],["Select swan dmodp"])], "Select_swan_dmodp_detector_key_pin")]
        ]
    # ali_tfan_mirot

    dmod_freqlist = ["","DC", "fsb1", "2fsb1", "3fsb1", "fsb2", "2fsb2", "3fsb2"]
    dmod_phaselist = ["", "Iphase", "Qphase", "optimal"]
    def get_tfan_dmod2_pd_select_box(num):
        tfan_dmod2_pd_select_box   =  [
            [sg.Checkbox("Use this",           key = f"kifo_tfan_dmod2_pd_select_box_checkbox_{num}", enable_events=True)],
            [sg.Text("port",                   key = f"kifo_tfan_dmod2_pd_select_box_port_text_{num}"),                   sg.Combo(util_func.get_main_port_list(), key = f"kifo_tfan_dmod2_pd_select_box_port_combo_{num}",                  enable_events=True, default_value=util_func.get_main_port_list()[0])
            ,sg.Text("demodulation_frequency", key = f"kifo_tfan_dmod2_pd_select_box_demodulation_frequency_text_{num}"), sg.Combo(dmod_freqlist,                  key = f"kifo_tfan_dmod2_pd_select_box_demodulation_frequency_combo_{num}",enable_events=True, default_value=dmod_freqlist[2])
            ,sg.Text("demodulation_phase",     key = f"kifo_tfan_dmod2_pd_select_box_demodulation_phase_text_{num}"),     sg.Combo(dmod_phaselist,                 key = f"kifo_tfan_dmod2_pd_select_box_demodulation_phase_combo_{num}",    enable_events=True, default_value="0")
            ],
            [sg.Text("gouy_phase",             key = f"kifo_tfan_dmod2_pd_select_box_gouy_phase_text_{num}"),             sg.Combo(["0","180"],                    key = f"kifo_tfan_dmod2_pd_select_box_gouy_phase_combo_{num}",            enable_events=True, default_value="0")
            ,sg.Text("split_axis",             key = f"kifo_tfan_dmod2_pd_select_box_split_axis_text_{num}"),             sg.Combo(["pitch","yaw"],                key = f"kifo_tfan_dmod2_pd_select_box_split_axis_combo_{num}",            enable_events=True, default_value="yaw")
            ]
        ]
        return tfan_dmod2_pd_select_box

    layouts = [
        [sg.Checkbox("Matrix",key="Select_tfan_dmod2_pd_matrix_checkbox",enable_events=True)]
        ]
    for num in range(10):
        layouts.append(
            util_func_pysimplegui.frame_layout([get_tfan_dmod2_pd_select_box(f"{num}")],[f"tfan_dmod2_pd_select_box{num}"],[f"detector_{num}"])
        )
    kifo_sec_tfan_mirot_setting = [
        [sg.Checkbox("Open",key="Selecttfan_dmod2_pd_open_checkbox",enable_events=True)],
        [collapse([util_func_pysimplegui.frame_layout([layouts],["Select_tfan_dmod2_pd_key"],["Select tfan_dmod2_pd"])], "Select_tfan_dmod2_pd_key_pin")]
        ]

    #section
    kifo_sec_sw_setting = [

            [sg.Text('Select PD type.')],
            [sg.Radio('Power detector [W]'       , 'kifo_sw_RADIO', default=True  , key='kifo_issw_power', enable_events=True),
             sg.Radio('Amplitud detector'        , 'kifo_sw_RADIO', default=False , key='kifo_issw_amptd', enable_events=True),
             sg.Radio('Demodulation phase sweep' , 'kifo_sw_RADIO', default=False , key='kifo_issw_dmodp', enable_events=True),
             sg.Radio('Demodulated signal [A.U.]', 'kifo_sw_RADIO', default=False , key='kifo_issw_dmod1', enable_events=True)],

            ### pd0_setting_section
            [collapse(kifo_sec_sw_power_setting, 'kifo_sec_sw_power_setting')],
            ### amplitude
            [collapse(kifo_sec_sw_amptd_setting, 'kifo_sec_sw_amptd_setting')],
            ### pd1_settnig_section
            [collapse(kifo_sec_sw_dmod1_setting, 'kifo_sec_sw_dmod1_setting')],
            ### demodulation_phase_section
            [collapse(kifo_sec_sw_dmodp_setting, 'kifo_sec_sw_dmodp_setting')]

    ]

    kifo_sec_tf_setting = [
    
            [sg.Text('Select PD type.')],
            [#sg.Radio('Power detector [W]', 'kifo_tf_RADIO', default=False,  key='kifo_istf_power', enable_events=True),
             #sg.Radio('Amplitud detector',  'kifo_tf_RADIO', default=False, key='kifo_istf_amptd', enable_events=True),
             sg.Radio('Demodulated signal',  'kifo_tf_RADIO', default=True, key='kifo_istf_dmod2', enable_events=True)],

            ### pd2_setting_section
            [collapse(kifo_sec_tf_power_setting, 'kifo_sec_tf_power_setting')],
            ### amplitude
            #[collapse(kifo_sec_tf_amptd_setting, 'kifo_sec_tf_amptd_setting')],
            ### pd2_setting_section
            [collapse(kifo_sec_tf_dmod2_setting, 'kifo_sec_tf_dmod2_setting')]
    
            ]
    kifo_sec_st_setting = [

            [sg.Text('Select PD type.')],
            [#sg.Radio('Power detector [W]'       , 'kifo_sw_RADIO', default=False  , key='kifo_issw_power', enable_events=True),
             #sg.Radio('Amplitud detector'        , 'kifo_sw_RADIO', default=False , key='kifo_issw_amptd', enable_events=True),
             sg.Radio('shot noise only', 'kifo_st_RADIO', default=True , key='kifo_isst_type1', enable_events=True),
             sg.Radio('radiation pressure and shot noise', 'kifo_st_RADIO', default=False, disabled=False, key='kifo_isst_type2', enable_events=True),
             #sg.Radio('Quantum noise (disabled)', 'kifo_st_RADIO', default=False, disabled=False, key='kifo_isst_type3', enable_events=True),
             ],

            ### pd0_setting_section
            [collapse(kifo_sec_st_type1_setting, 'kifo_sec_st_type1_setting')],
            [collapse(kifo_sec_st_type2_setting, 'kifo_sec_st_type2_setting')],
            [collapse(kifo_sec_st_type3_setting, 'kifo_sec_st_type3_setting')],
            ### amplitude
            #[collapse(kifo_sec_sw_amptd_setting, 'kifo_sec_sw_amptd_setting')],
            ### pd1_settnig_section
            #[collapse(kifo_sec_sw_dmod1_setting, 'kifo_sec_sw_dmod1_setting')]

                ]

    kifo_sec_swan_setting= [
    debug_text(debugflag, "kifo_sec_swan_setting"),
    [sg.Text('Select what to sweep')],
    [sg.Radio('Mirror rotation',        'kifo_swan_RADIO', default=True,  key='kifo_isswan_mirot', enable_events=True)
    ,sg.Radio('Gouy phase at Detector', 'kifo_swan_RADIO', default=False, key='kifo_isswan_gouyp', enable_events=True)
    ,sg.Radio('WFS Demodulation phase', 'kifo_swan_RADIO', default=False, key='kifo_isswan_dmodp', enable_events=True)
    ],

    [collapse(kifo_sec_swan_mirot_setting, 'kifo_sec_swan_mirot_setting')],
    [collapse(kifo_sec_swan_gouyp_setting, 'kifo_sec_swan_gouyp_setting')],
    [collapse(kifo_sec_swan_dmodp_setting, 'kifo_sec_swan_dmodp_setting')],
    
    
    ]

    kifo_sec_tfan_setting= [

    debug_text(debugflag, "kifo_sec_tfan_setting"),

    [sg.Radio('transfer function', 'kifo_tfan_RADIO', default=False, key='kifo_istfan_mirot', enable_events=True)],
    [collapse(kifo_sec_tfan_mirot_setting, 'kifo_sec_tfan_mirot_setting')]
    
    ]
    
    # classification

    kifo_classification_length= [
        debug_text(debugflag, "kifo_classification_length"),
        [sg.Radio('Sweep',               'kifo_sim_mode1',       default=True,  key='kifo_issw', enable_events=True),
        sg.Radio('Transfer function',    'kifo_sim_mode1',       default=False, key='kifo_istf', enable_events=True),
        sg.Radio('Sensitivity',          'kifo_sim_mode1',       default=False, key='kifo_isst', enable_events=True)],


        [collapse(kifo_sec_sw_setting,       'kifo_sec_sw_setting')],
        [collapse(kifo_sec_tf_setting,       'kifo_sec_tf_setting')],
        [collapse(kifo_sec_st_setting,       'kifo_sec_st_setting')],

        ]

    kifo_classification_alignment= [
        debug_text(debugflag, "kifo_classification_alignment"),
        [sg.Radio('Sweep',             'kifo_sim_mode2',    default=True,  key='kifo_isswan', enable_events=True)
        ,sg.Radio('Transfer function', 'kifo_sim_mode2',    default=False, key='kifo_istfan', enable_events=True)
    ],

    [collapse(kifo_sec_swan_setting,  'kifo_sec_swan_setting')],
    [collapse(kifo_sec_tfan_setting,  'kifo_sec_tfan_setting')],
        ]

    # dof_selection
    dof_selection_menu = [
        debug_text(debugflag, "dof_selection_menu"),
        [sg.Text('　 DoF'),sg.Combo(('DARM', 'CARM', 'BS', 'PRCL', 'SRCL'), size=(20,1), default_value='DARM', readonly=True, key='kifo_dof')], #sg.comboを使う時にはサイズを指定するべき
    ]
    sweep_phase_mirror_menu = [
        debug_text(debugflag, "sweep_phase_mirror_menu"),
        [sg.Text('　　mirror name'),sg.Combo(('PRM', 'SRM'), size=(20,1), default_value='SRM', readonly=True, key='kifo_phase_sweep_mirror')], #sg.comboを使う時にはサイズを指定するべき
    ]

    misalign_dof_box_1 = [
        debug_text(debugflag, "misalign_dof_box_1"),
        [sg.Text('　　X-ARM Cavity')
        ,sg.Combo(("shift +", "shift -", "tilt +", "tilt -"), size=(20,1),default_value="shift +", readonly=True, key='kifo_an_misal_resoaxis_xarm')
        ,sg.Text('　　rotate direction')
        ,sg.Combo(("pitch","yaw"), size=(20,1),default_value="pitch", readonly=True, key='kifo_an_misal_xarm_rotate_direction')
        #,sg.Text('　　ITMX gouy phase')
        #,sg.Combo(("0","45"), size=(20,1),default_value="0", readonly=True, key='kifo_an_misal_gouy_phase_xarm_i')
        #,sg.Text('　　ETMX gouy phase')
        #,sg.Combo(("0","45"), size=(20,1),default_value="0", readonly=True, key='kifo_an_misal_gouy_phase_xarm_e')
        #,sg.Button('Reload', button_color=('white', 'black'), key='kifo_an_misal_resoaxis_xarm_reload')
        ],
        [sg.Text('　　Y-ARM Cavity')
        ,sg.Combo(("shift +", "shift -", "tilt +", "tilt -"), size=(20,1),default_value="shift +", readonly=True, key='kifo_an_misal_resoaxis_yarm')
        ,sg.Text('　　rotate direction')
        ,sg.Combo(("pitch","yaw"), size=(20,1),default_value="pitch", readonly=True, key='kifo_an_misal_yarm_rotate_direction')
        #,sg.Text('　　ITMY gouy phase')
        #,sg.Combo(("0","45"), size=(20,1),default_value="0", readonly=True, key='kifo_an_misal_gouy_phase_yarm_i')
        #,sg.Text('　　ETMY gouy phase')
        #,sg.Combo(("0","45"), size=(20,1),default_value="0", readonly=True, key='kifo_an_misal_gouy_phase_yarm_e')
        #,sg.Text('　　rotate angle')
        #,sg.Combo(("0","45"), size=(20,1),default_value="0", readonly=True, key='kifo_an_misal_rotetean_yarm')
        #,sg.Button('Reload', button_color=('white', 'black'), key='kifo_an_misal_resoaxis_yarm_reload')
        ]
    ]
    misalign_dof_box_2 = [
        debug_text(debugflag, "misalign_dof_box_2"),
        # plotnum
        [sg.Text('select "port", "misalignment axis"'), 
        sg.Button('-', button_color=('white', 'black'), key='kifo_an_misal_plotminus'),
        sg.Button('+', button_color=('white', 'black'), key='kifo_an_misal_plotplus')],
        [collapse(an_misal_box_2, 'an_misal_box_2')]
        #[collapse(an_misal_box, 'an_misal_box')]
    ]
    misalign_dof_box_1_gouyp = [
        debug_text(debugflag, "misalign_dof_box_1_gouyp"),
        [sg.Text('　　X-ARM Cavity')
        ,sg.Combo(("shift +", "shift -", "tilt +", "tilt -"), size=(20,1),default_value="shift +", readonly=True, key='kifo_an_misal_gouyp_resoaxis_xarm')
        ,sg.Text('　  rotate angle')
        ,sg.Combo(("1e-6","0"), size=(20,1),default_value="1e-6", readonly=True, key='kifo_an_misal_gouyp_rotetean_xarm')
        ,sg.Text('　　rotate direction')
        ,sg.Combo(("pitch","yaw"), size=(20,1),default_value="pitch", readonly=True, key='kifo_an_misal_gouyp_xarm_rotate_direction')
        #,sg.Button('Reload', button_color=('white', 'black'), key='kifo_an_misal_a_resoaxis_xarm_reload')
        ],
        [sg.Text('　　Y-ARM Cavity')
        ,sg.Combo(("shift +", "shift -", "tilt +", "tilt -"), size=(20,1),default_value="shift +", readonly=True, key='kifo_an_misal_gouyp_resoaxis_yarm')
        ,sg.Text('　　rotate angle')
        ,sg.Combo(("1e-6","0"), size=(20,1),default_value="1e-6", readonly=True, key='kifo_an_misal_gouyp_rotetean_yarm')
        ,sg.Text('　　rotate direction')
        ,sg.Combo(("pitch","yaw"), size=(20,1),default_value="pitch", readonly=True, key='kifo_an_misal_gouyp_yarm_rotate_direction')
        #,sg.Button('Reload', button_color=('white', 'black'), key='kifo_an_misal_a_resoaxis_yarm_reload')
        ]
    ]
    misalign_dof_box_2_gouyp = [
        debug_text(debugflag, "misalign_dof_box_2_gouyp"),
        # plotnum
        [sg.Text('Angular DoF'), 
        sg.Button('-', button_color=('white', 'black'), key='kifo_an_misal_gouyp_plotminus'),
        sg.Button('+', button_color=('white', 'black'), key='kifo_an_misal_gouyp_plotplus')],
        [collapse(swan_gouyp_box_2_a, 'swan_gouyp_box_2_a')]
        #[collapse(an_misal_box, 'an_misal_box')]
    ]

    swan_dof_dmodp_mirot_dc_template = [
        [sg.Text('Mirror rotation')
        ,sg.Text('　　X-ARM Cavity')
        ,sg.Combo(["shift+","shift-"],key="swan_dof_dmodp_mirot_dc_template_x_cavity_combo_key",default_value="shift+")
        ,sg.Text('　  rotate angle')
        ,sg.Combo(["1e-6","0"],key="swan_dof_dmodp_mirot_dc_template_x_angle_combo_key",default_value="1e-6")
        ,sg.Text('　　rotate direction')
        ,sg.Combo(["pitch","yaw"],key="swan_dof_dmodp_mirot_dc_template_x_direction_combo_key",default_value="yaw")
        ],
        [sg.Text('Mirror rotation')
        ,sg.Text('　　Y-ARM Cavity')
        ,sg.Combo(["shift+","shift-"],key="swan_dof_dmodp_mirot_dc_template_y_cavity_combo_key",default_value="shift+")
        ,sg.Text('　  rotate angle')
        ,sg.Combo(["1e-6","0"],key="swan_dof_dmodp_mirot_dc_template_y_angle_combo_key",default_value="1e-6")
        ,sg.Text('　　rotate direction')
        ,sg.Combo(["pitch","yaw"],key="swan_dof_dmodp_mirot_dc_template_y_direction_combo_key",default_value="yaw")
        ]
    ]

    def get_swan_dof_dmodp_mirot_dc_mirror_select_box(num):
        swan_dof_dmodp_mirot_dc_mirror_select   =  [
            [sg.Checkbox("Use this",      key = f"kifo_swan_dof_dmodp_mirot_dc_mirror_select_box_checkbox_{num}", enable_events=True)],
            [sg.Text("Mirror",            key = f"kifo_swan_dof_dmodp_mirot_dc_mirror_select_box_Mirror_text_{num}"),            sg.Combo(["ITMX","ETMX"],key = f"kifo_swan_dof_dmodp_mirot_dc_mirror_select_box_Mirror_combo_{num}",            enable_events=True, default_value="ITMX")
            ,sg.Text("Rotation_angle",    key = f"kifo_swan_dof_dmodp_mirot_dc_mirror_select_box_Rotation_angle_text_{num}"),    sg.Combo(["1e-6","0"],             key = f"kifo_swan_dof_dmodp_mirot_dc_mirror_select_box_Rotation_angle_combo_{num}",    enable_events=True, default_value="1e-6")
            ,sg.Text("Rotation_direction",key = f"kifo_swan_dof_dmodp_mirot_dc_mirror_select_box_Rotation_direction_text_{num}"),sg.Combo(["pitch","yaw"],             key = f"kifo_swan_dof_dmodp_mirot_dc_mirror_select_box_Rotation_direction_combo_{num}",enable_events=True, default_value="yaw")
            ]
        ]
        return swan_dof_dmodp_mirot_dc_mirror_select

    layouts = []
    for num in range(10):
        layouts.append(
            util_func_pysimplegui.frame_layout([get_swan_dof_dmodp_mirot_dc_mirror_select_box(f"{num}")],[f"swan_dof_dmodp_mirot_dc_mirror{num}"],[f"rotaion_{num}"])
        )
    swan_dof_dmodp_mirot_dc_optional = [
        [sg.Checkbox("Open",key="Select_swan_dof_dmodp_mirot_dc_mirror_open_checkbox",enable_events=True)],
        [collapse([util_func_pysimplegui.frame_layout([layouts],["Select_swan_dof_dmodp_mirot_dc_mirror_key"],["Select swan_dof_dmodp_mirot_dc_mirror"])], "Select_swan_dof_dmodp_mirot_dc_mirror_key_pin")]
        ]

    swan_dof_dmodp_mirot_fsig_template = [
        [sg.Text('Mirror rotation')
        ,sg.Text('   X-ARM Cavity'),     sg.Combo(["shift+","shift-","tilt+","tilt-"], key="swan_dof_dmodp_mfsig_fsig_template_x_cavity_combo_key")
        ,sg.Text('   rotation fsig frequency'), sg.Combo(["fsb1","fsb2"],              key="swan_dof_dmodp_mfsig_fsig_template_x_frequency_combo_key")
        ,sg.Text('   rotation fsig direction'), sg.Combo(["pitch","yaw"],              key="swan_dof_dmodp_mfsig_fsig_template_x_direction_combo_key")
        ],
        [sg.Text('Mirror rotation')
        ,sg.Text('   Y-ARM Cavity'),     sg.Combo(["shift+","shift-","tilt+","tilt-"], key="swan_dof_dmodp_mfsig_fsig_template_y_cavity_combo_key")
        ,sg.Text('   rotation fsig frequency'), sg.Combo(["fsb1","fsb2"],              key="swan_dof_dmodp_mfsig_fsig_template_y_frequency_combo_key")
        ,sg.Text('   rotation direction'),      sg.Combo(["pitch","yaw"],              key="swan_dof_dmodp_mfsig_fsig_template_y_direction_combo_key")
        ]
    ]
    def get_swan_dof_dmodp_mfsig_mirror_select_box(num):
        swan_dof_dmodp_mfsig_mirror_select   =  [
            [sg.Checkbox("Use this",            key = f"kifo_swan_dof_dmodp_mfsig_mirror_select_box_checkbox_{num}", enable_events=True)],
            [sg.Text("Mirror",                  key = f"kifo_swan_dof_dmodp_mfsig_mirror_select_box_Mirror_text_{num}"),            sg.Combo(["ITMX","ETMX"], key = f"kifo_swan_dof_dmodp_mfsig_mirror_select_box_Mirror_combo_{num}",            enable_events=True, default_value="ITMX")
            ,sg.Text("Rotation_fsig_frequency", key = f"kifo_swan_dof_dmodp_mfsig_mirror_select_box_Rotation_angle_text_{num}"),    sg.Combo(["fsb1","fsb2"], key = f"kifo_swan_dof_dmodp_mfsig_mirror_select_box_Rotation_angle_combo_{num}",    enable_events=True, default_value="fsb1")
            ,sg.Text("Rotation_direction",      key = f"kifo_swan_dof_dmodp_mfsig_mirror_select_box_Rotation_direction_text_{num}"),sg.Combo(["pitch","yaw"], key = f"kifo_swan_dof_dmodp_mfsig_mirror_select_box_Rotation_direction_combo_{num}",enable_events=True, default_value="yaw")
            ,sg.Text("fsig_polarity",           key = f"kifo_swan_dof_dmodp_mfsig_mirror_select_box_Fsig_polarity_text_{num}"),     sg.Combo(["0","180"],     key = f"kifo_swan_dof_dmodp_mfsig_mirror_select_box_Fsig_polarity_combo_{num}",     enable_events=True, default_value="0")
            ]
        ]
        return swan_dof_dmodp_mfsig_mirror_select

    layouts = []
    for num in range(10):
        layouts.append(
            util_func_pysimplegui.frame_layout([get_swan_dof_dmodp_mfsig_mirror_select_box(f"{num}")],[f"swan_dof_dmodp_mfsig_mirror_select{num}"],[f"fsig_{num}"])
        )
    swan_dof_dmodp_mirot_fsig_optional = [
        [sg.Checkbox("Open",key="Select_swan_dof_dmodp_mirot_fsig_mirror_open_checkbox",enable_events=True)],
        [collapse([util_func_pysimplegui.frame_layout([layouts],["Select_swan_dof_dmodp_mirot_fsig_optional_key"],["Select swan_dof_dmodp_mirot_fsig_optional"])], "Select_swan_dof_dmodp_mirot_fsig_optional_key_pin")]
        ]

    misalign_mirror_dof_radiobox1 = [
        [sg.Radio('Templates',    'kifo_alignment_dof', default=True,   key='kifo_an_misal_use_dof_sc_1', enable_events=True)
        ,sg.Radio('Optional DoF', 'kifo_alignment_dof', default=False,  key='kifo_an_misal_use_dof_sc_2', enable_events=True)
        ]
    ]
    misalign_mirror_dof_radiobox2 = [
        [sg.Radio('Templates',    'kifo_alignment_dof2', default=True,  key='kifo_swan_gouy_use_dof_sc_1', enable_events=True)
        ,sg.Radio('Optional DoF', 'kifo_alignment_dof2', default=False, key='kifo_swan_gouy_use_dof_sc_2', enable_events=True)
        ]
    ]
    swan_dof_dmodp_mirot_radiobox = [
        [sg.Radio('Templates',         'swan_dof_dmodp_mirot_radiobox', default=True,  key='swan_dof_dmodp_mirot_radiobox_key_1', enable_events=True)
        ,sg.Radio('Optional DoF',      'swan_dof_dmodp_mirot_radiobox', default=False, key='swan_dof_dmodp_mirot_radiobox_key_2', enable_events=True)
        ,sg.Radio('Templates fsig',    'swan_dof_dmodp_mirot_radiobox', default=False, key='swan_dof_dmodp_mirot_fsig_radiobox_key_1', enable_events=True)
        ,sg.Radio('Optional DoF fsig', 'swan_dof_dmodp_mirot_radiobox', default=False, key='swan_dof_dmodp_mirot_fsig_radiobox_key_2', enable_events=True)
        ]
    ]
    misalign_mirror_selection_menu = [
        debug_text(debugflag, "misalign_mirror_selection_menu"),
        [sg.Text('select "port", "misalignment axis"')], 
        [collapse(misalign_mirror_dof_radiobox1, 'misalign_mirror_dof_radiobox1')],
        [collapse(misalign_mirror_dof_radiobox2, 'misalign_mirror_dof_radiobox2')],
        [collapse(swan_dof_dmodp_mirot_radiobox, 'swan_dof_dmodp_mirot_radiobox')],
         #sg.Radio('DOF_2_a',    'kifo_an_misal_dof', default=False, key='kifo_an_misal_use_dof_sc_2_a', enable_events=True)],
        [collapse(misalign_dof_box_1, 'misalign_dof_box_1')],
        [collapse(misalign_dof_box_2, 'misalign_dof_box_2')],
        [collapse(misalign_dof_box_1_gouyp, 'misalign_dof_box_1_gouyp')],
        [collapse(misalign_dof_box_2_gouyp, 'misalign_dof_box_2_gouyp')],
        # swan dmodp
        [collapse(swan_dof_dmodp_mirot_dc_template,   'swan_dof_dmodp_mirot_dc_template')],
        [collapse(swan_dof_dmodp_mirot_dc_optional,   'swan_dof_dmodp_mirot_dc_optional')],
        [collapse(swan_dof_dmodp_mirot_fsig_template, 'swan_dof_dmodp_mirot_fsig_template')],
        [collapse(swan_dof_dmodp_mirot_fsig_optional, 'swan_dof_dmodp_mirot_fsig_optional')],
    ]

    tfan_dof_dmod2_mirot_fsig_template = [
        [sg.Text('Mirror rotation')
        ,sg.Text('   X-ARM Cavity'),            sg.Combo(["shift+","shift-","tilt+","tilt-"], key="tfan_dof_dmod2_mfsig_template_x_cavity_combo_key"   ,default_value="shift+")
        ,sg.Text('   rotation fsig direction'), sg.Combo(["pitch","yaw"],                     key="tfan_dof_dmod2_mfsig_template_x_direction_combo_key",default_value="yaw")
        ],
        [sg.Text('Mirror rotation')
        ,sg.Text('   Y-ARM Cavity'),            sg.Combo(["shift+","shift-","tilt+","tilt-"], key="tfan_dof_dmod2_mfsig_template_y_cavity_combo_key"   ,default_value="shift+")
        ,sg.Text('   rotation fsig direction'), sg.Combo(["pitch","yaw"],                     key="tfan_dof_dmod2_mfsig_template_y_direction_combo_key",default_value="yaw")
        ]
    ]
    def get_tfan_dof_dmod2_mfsig_mirror_select_box(num):
        tfan_dof_dmod2_mfsig_mirror_select   =  [
            [sg.Checkbox("Use this",            key = f"kifo_tfan_dof_dmod2_mfsig_mirror_select_box_checkbox_{num}", enable_events=True)],
            [sg.Text("Mirror",                  key = f"kifo_tfan_dof_dmod2_mfsig_mirror_select_box_Mirror_text_{num}"),            sg.Combo(util_func.get_all_mirror_list("DRFPMI"), key = f"kifo_tfan_dof_dmod2_mfsig_mirror_select_box_Mirror_combo_{num}",            enable_events=True, default_value="ITMX")
            ,sg.Text("Rotation_direction",      key = f"kifo_tfan_dof_dmod2_mfsig_mirror_select_box_Rotation_direction_text_{num}"),sg.Combo(["pitch","yaw"], key = f"kifo_tfan_dof_dmod2_mfsig_mirror_select_box_Rotation_direction_combo_{num}",enable_events=True, default_value="yaw")
            ,sg.Text("fsig_polarity",           key = f"kifo_tfan_dof_dmod2_mfsig_mirror_select_box_Fsig_polarity_text_{num}"),     sg.Combo(["0","180"],     key = f"kifo_tfan_dof_dmod2_mfsig_mirror_select_box_Fsig_polarity_combo_{num}",     enable_events=True, default_value="0")
            ]
        ]
        return tfan_dof_dmod2_mfsig_mirror_select

    layouts = []
    for num in range(10):
        layouts.append(
            util_func_pysimplegui.frame_layout([get_tfan_dof_dmod2_mfsig_mirror_select_box(f"{num}")],[f"tfan_dof_dmod2_mfsig_mirror_select{num}"],[f"fsig_{num}"])
        )
    tfan_dof_dmod2_mirot_fsig_optional = [
        [sg.Checkbox("Open",key="Select_tfan_dof_dmod2_mirot_fsig_mirror_open_checkbox",enable_events=True)],
        [collapse([util_func_pysimplegui.frame_layout([layouts],["Select_tfan_dof_dmod2_mirot_fsig_optional_key"],["Select tfan_dof_dmod2_mirot_fsig_optional"])], "Select_tfan_dof_dmod2_mirot_fsig_optional_key_pin")]
        ]
    tfan_dof_selection_menu = [
        # tfan dmod2
        [sg.Radio('Templates',         'tfan_dof_dmod2_fsig_mirot_radiobox', default=True,  key='tfan_dof_dmod2_fsig_mirot_radiobox_key_1', enable_events=True)
        ,sg.Radio('Optional DoF',      'tfan_dof_dmod2_fsig_mirot_radiobox', default=False, key='tfan_dof_dmod2_fsig_mirot_radiobox_key_2', enable_events=True)
        ],
        [collapse(tfan_dof_dmod2_mirot_fsig_template, 'tfan_dof_dmod2_mirot_fsig_template')],
        [collapse(tfan_dof_dmod2_mirot_fsig_optional, 'tfan_dof_dmod2_mirot_fsig_optional')]

    ]
    # drawing
    layout_drawing = make_layout_drawing('../fig/DRFPMI_picture_normal.png', "layout_drawing_key")

    #screen
    kifo_layout = [
    # figure
    [collapse(layout_drawing, 'layout_drawing')],

    ### select section sw or tf / power, dmod1 or dmod2
    [sg.Text('0. Select length or alignment simulation.', font=('default',20))],
    [sg.Radio('Length',            'kifo_classification', default=True,  key='kifo_cl_len', enable_events=True),
     sg.Radio('Angular',           'kifo_classification', default=False, key='kifo_cl_ali', enable_events=True)],

    
    ### select section sw or tf / power, dmod1 or dmod2
    [sg.Text('1. Select simulation mode.', font=('default',20))],
    [collapse(kifo_classification_length,       'kifo_classification_length')],
    [collapse(kifo_classification_alignment,    'kifo_classification_alignment')],


    # 2 DoF or Mirror phase sweep
    [sg.Text('2. Select what to move', font=('default',20), key="kifo_selection2_text")],
    [sg.Radio('Length DoF',      'kifo_xaxis_command_type', default=True,  key='kifo_dof_selection',          enable_events=True),
     sg.Radio('Misalign mirror', 'kifo_xaxis_command_type', default=False, key='kifo_misal_mirror_selection', enable_events=True),
    #sg.Radio('phase sweep mirror' , 'kifo_xaxis_command_type', default=False , key='kifo_sweep_phase_mirror_selection', enable_events=True)
    ],
    # DoF selection
    [collapse(dof_selection_menu, 'dof_selection_menu')],
    # Mirror Phase Sweep Selection
    [collapse(sweep_phase_mirror_menu, 'sweep_phase_mirror_menu')],
    # Misalign mirror selection
    [collapse(misalign_mirror_selection_menu, 'misalign_mirror_selection_menu')],
    # tfan_dof_selection
    [collapse(tfan_dof_selection_menu, 'tfan_dof_selection_menu')],


    # 3 plot setting
    [sg.Text('3. Plot setting', font=('default',20), key="kifo_selection3_text")],

    # plot configuration
    [sg.Text('sampling num'), sg.Input(key='k_inf_c_samplingnum', default_text='1000', enable_events=True)],

    # xaxis range
    [sg.Radio('xaxis lin', 'x_plotscale', default=True,  key='k_inf_c_xaxis_lin', enable_events=True),
    sg.Radio('xaxis log', 'x_plotscale', default=False, key='k_inf_c_xaxis_log', enable_events=True)],
    #yaxis range
    [sg.Radio('yaxis lin', 'y_plotscale', default=True,  key='k_inf_c_yaxis_lin', enable_events=True),
    sg.Radio('yaxis log', 'y_plotscale', default=False, key='k_inf_c_yaxis_log', enable_events=True)],

    # xaxis range
    [sg.Text('xaxis range', key='k_inf_c_xaxis_range_text')],
    [sg.Input(key='k_inf_c_xaxis_range_beg', default_text='-180', enable_events=True),
     sg.Text('to'),
     sg.Input(key='k_inf_c_xaxis_range_end', default_text= '180', enable_events=True)],


    # plot button
    [sg.Button('make kat text', key='k_inf_make_kat_text')],
    # その他ボタン
    #[sg.Checkbox('overplot All PDs', key='k_inf_c_is_overplot_all_pds')]

    ]
    
    return kifo_layout

def make_extra_ifo_param_tab():

    # pole
    k_inf_c_mibs_mirror_tf_pf_box_1  = [[sg.Text('      frequency of pole 01'), sg.Combo(('0.1','1','10','100','1000','10000','100000','1000000'), key='k_inf_c_mibs_mirror_tf_pf_1', default_value='0.1', enable_events=True)]]
    k_inf_c_mibs_mirror_tf_pf_box_2  = [[sg.Text('      frequency of pole 02'), sg.Combo(('0.1','1','10','100','1000','10000','100000','1000000'), key='k_inf_c_mibs_mirror_tf_pf_2', default_value='0.1', enable_events=True)]]
    k_inf_c_mibs_mirror_tf_pf_box_3  = [[sg.Text('      frequency of pole 03'), sg.Combo(('0.1','1','10','100','1000','10000','100000','1000000'), key='k_inf_c_mibs_mirror_tf_pf_3', default_value='0.1', enable_events=True)]]
    k_inf_c_mibs_mirror_tf_pf_box_4  = [[sg.Text('      frequency of pole 04'), sg.Combo(('0.1','1','10','100','1000','10000','100000','1000000'), key='k_inf_c_mibs_mirror_tf_pf_4', default_value='0.1', enable_events=True)]]
    k_inf_c_mibs_mirror_tf_pf_box_5  = [[sg.Text('      frequency of pole 05'), sg.Combo(('0.1','1','10','100','1000','10000','100000','1000000'), key='k_inf_c_mibs_mirror_tf_pf_5', default_value='0.1', enable_events=True)]]
    k_inf_c_mibs_mirror_tf_pf_box_6  = [[sg.Text('      frequency of pole 06'), sg.Combo(('0.1','1','10','100','1000','10000','100000','1000000'), key='k_inf_c_mibs_mirror_tf_pf_6', default_value='0.1', enable_events=True)]]
    k_inf_c_mibs_mirror_tf_pf_box_7  = [[sg.Text('      frequency of pole 07'), sg.Combo(('0.1','1','10','100','1000','10000','100000','1000000'), key='k_inf_c_mibs_mirror_tf_pf_7', default_value='0.1', enable_events=True)]]
    k_inf_c_mibs_mirror_tf_pf_box_8  = [[sg.Text('      frequency of pole 08'), sg.Combo(('0.1','1','10','100','1000','10000','100000','1000000'), key='k_inf_c_mibs_mirror_tf_pf_8', default_value='0.1', enable_events=True)]]
    k_inf_c_mibs_mirror_tf_pf_box_9  = [[sg.Text('      frequency of pole 09'), sg.Combo(('0.1','1','10','100','1000','10000','100000','1000000'), key='k_inf_c_mibs_mirror_tf_pf_9', default_value='0.1', enable_events=True)]]
    k_inf_c_mibs_mirror_tf_pf_box_10 = [[sg.Text('      frequency of pole 10'), sg.Combo(('0.1','1','10','100','1000','10000','100000','1000000'), key='k_inf_c_mibs_mirror_tf_pf_10', default_value='0.1', enable_events=True)]]
        
    k_inf_c_itmx_mirror_tf_pf_box_1  = [[sg.Text('      frequency of pole 01'), sg.Combo(('0.1','1','10','100','1000','10000','100000','1000000'), key='k_inf_c_itmx_mirror_tf_pf_1', default_value='0.1', enable_events=True)]]
    k_inf_c_itmx_mirror_tf_pf_box_2  = [[sg.Text('      frequency of pole 02'), sg.Combo(('0.1','1','10','100','1000','10000','100000','1000000'), key='k_inf_c_itmx_mirror_tf_pf_2', default_value='0.1', enable_events=True)]]
    k_inf_c_itmx_mirror_tf_pf_box_3  = [[sg.Text('      frequency of pole 03'), sg.Combo(('0.1','1','10','100','1000','10000','100000','1000000'), key='k_inf_c_itmx_mirror_tf_pf_3', default_value='0.1', enable_events=True)]]
    k_inf_c_itmx_mirror_tf_pf_box_4  = [[sg.Text('      frequency of pole 04'), sg.Combo(('0.1','1','10','100','1000','10000','100000','1000000'), key='k_inf_c_itmx_mirror_tf_pf_4', default_value='0.1', enable_events=True)]]
    k_inf_c_itmx_mirror_tf_pf_box_5  = [[sg.Text('      frequency of pole 05'), sg.Combo(('0.1','1','10','100','1000','10000','100000','1000000'), key='k_inf_c_itmx_mirror_tf_pf_5', default_value='0.1', enable_events=True)]]
    k_inf_c_itmx_mirror_tf_pf_box_6  = [[sg.Text('      frequency of pole 06'), sg.Combo(('0.1','1','10','100','1000','10000','100000','1000000'), key='k_inf_c_itmx_mirror_tf_pf_6', default_value='0.1', enable_events=True)]]
    k_inf_c_itmx_mirror_tf_pf_box_7  = [[sg.Text('      frequency of pole 07'), sg.Combo(('0.1','1','10','100','1000','10000','100000','1000000'), key='k_inf_c_itmx_mirror_tf_pf_7', default_value='0.1', enable_events=True)]]
    k_inf_c_itmx_mirror_tf_pf_box_8  = [[sg.Text('      frequency of pole 08'), sg.Combo(('0.1','1','10','100','1000','10000','100000','1000000'), key='k_inf_c_itmx_mirror_tf_pf_8', default_value='0.1', enable_events=True)]]
    k_inf_c_itmx_mirror_tf_pf_box_9  = [[sg.Text('      frequency of pole 09'), sg.Combo(('0.1','1','10','100','1000','10000','100000','1000000'), key='k_inf_c_itmx_mirror_tf_pf_9', default_value='0.1', enable_events=True)]]
    k_inf_c_itmx_mirror_tf_pf_box_10 = [[sg.Text('      frequency of pole 10'), sg.Combo(('0.1','1','10','100','1000','10000','100000','1000000'), key='k_inf_c_itmx_mirror_tf_pf_10', default_value='0.1', enable_events=True)]]
        
    k_inf_c_etmx_mirror_tf_pf_box_1  = [[sg.Text('      frequency of pole 01'), sg.Combo(('0.1','1','10','100','1000','10000','100000','1000000'), key='k_inf_c_etmx_mirror_tf_pf_1', default_value='0.1', enable_events=True)]]
    k_inf_c_etmx_mirror_tf_pf_box_2  = [[sg.Text('      frequency of pole 02'), sg.Combo(('0.1','1','10','100','1000','10000','100000','1000000'), key='k_inf_c_etmx_mirror_tf_pf_2', default_value='0.1', enable_events=True)]]
    k_inf_c_etmx_mirror_tf_pf_box_3  = [[sg.Text('      frequency of pole 03'), sg.Combo(('0.1','1','10','100','1000','10000','100000','1000000'), key='k_inf_c_etmx_mirror_tf_pf_3', default_value='0.1', enable_events=True)]]
    k_inf_c_etmx_mirror_tf_pf_box_4  = [[sg.Text('      frequency of pole 04'), sg.Combo(('0.1','1','10','100','1000','10000','100000','1000000'), key='k_inf_c_etmx_mirror_tf_pf_4', default_value='0.1', enable_events=True)]]
    k_inf_c_etmx_mirror_tf_pf_box_5  = [[sg.Text('      frequency of pole 05'), sg.Combo(('0.1','1','10','100','1000','10000','100000','1000000'), key='k_inf_c_etmx_mirror_tf_pf_5', default_value='0.1', enable_events=True)]]
    k_inf_c_etmx_mirror_tf_pf_box_6  = [[sg.Text('      frequency of pole 06'), sg.Combo(('0.1','1','10','100','1000','10000','100000','1000000'), key='k_inf_c_etmx_mirror_tf_pf_6', default_value='0.1', enable_events=True)]]
    k_inf_c_etmx_mirror_tf_pf_box_7  = [[sg.Text('      frequency of pole 07'), sg.Combo(('0.1','1','10','100','1000','10000','100000','1000000'), key='k_inf_c_etmx_mirror_tf_pf_7', default_value='0.1', enable_events=True)]]
    k_inf_c_etmx_mirror_tf_pf_box_8  = [[sg.Text('      frequency of pole 08'), sg.Combo(('0.1','1','10','100','1000','10000','100000','1000000'), key='k_inf_c_etmx_mirror_tf_pf_8', default_value='0.1', enable_events=True)]]
    k_inf_c_etmx_mirror_tf_pf_box_9  = [[sg.Text('      frequency of pole 09'), sg.Combo(('0.1','1','10','100','1000','10000','100000','1000000'), key='k_inf_c_etmx_mirror_tf_pf_9', default_value='0.1', enable_events=True)]]
    k_inf_c_etmx_mirror_tf_pf_box_10 = [[sg.Text('      frequency of pole 10'), sg.Combo(('0.1','1','10','100','1000','10000','100000','1000000'), key='k_inf_c_etmx_mirror_tf_pf_10', default_value='0.1', enable_events=True)]]
        
    k_inf_c_itmy_mirror_tf_pf_box_1  = [[sg.Text('      frequency of pole 01'), sg.Combo(('0.1','1','10','100','1000','10000','100000','1000000'), key='k_inf_c_itmy_mirror_tf_pf_1', default_value='0.1', enable_events=True)]]
    k_inf_c_itmy_mirror_tf_pf_box_2  = [[sg.Text('      frequency of pole 02'), sg.Combo(('0.1','1','10','100','1000','10000','100000','1000000'), key='k_inf_c_itmy_mirror_tf_pf_2', default_value='0.1', enable_events=True)]]
    k_inf_c_itmy_mirror_tf_pf_box_3  = [[sg.Text('      frequency of pole 03'), sg.Combo(('0.1','1','10','100','1000','10000','100000','1000000'), key='k_inf_c_itmy_mirror_tf_pf_3', default_value='0.1', enable_events=True)]]
    k_inf_c_itmy_mirror_tf_pf_box_4  = [[sg.Text('      frequency of pole 04'), sg.Combo(('0.1','1','10','100','1000','10000','100000','1000000'), key='k_inf_c_itmy_mirror_tf_pf_4', default_value='0.1', enable_events=True)]]
    k_inf_c_itmy_mirror_tf_pf_box_5  = [[sg.Text('      frequency of pole 05'), sg.Combo(('0.1','1','10','100','1000','10000','100000','1000000'), key='k_inf_c_itmy_mirror_tf_pf_5', default_value='0.1', enable_events=True)]]
    k_inf_c_itmy_mirror_tf_pf_box_6  = [[sg.Text('      frequency of pole 06'), sg.Combo(('0.1','1','10','100','1000','10000','100000','1000000'), key='k_inf_c_itmy_mirror_tf_pf_6', default_value='0.1', enable_events=True)]]
    k_inf_c_itmy_mirror_tf_pf_box_7  = [[sg.Text('      frequency of pole 07'), sg.Combo(('0.1','1','10','100','1000','10000','100000','1000000'), key='k_inf_c_itmy_mirror_tf_pf_7', default_value='0.1', enable_events=True)]]
    k_inf_c_itmy_mirror_tf_pf_box_8  = [[sg.Text('      frequency of pole 08'), sg.Combo(('0.1','1','10','100','1000','10000','100000','1000000'), key='k_inf_c_itmy_mirror_tf_pf_8', default_value='0.1', enable_events=True)]]
    k_inf_c_itmy_mirror_tf_pf_box_9  = [[sg.Text('      frequency of pole 09'), sg.Combo(('0.1','1','10','100','1000','10000','100000','1000000'), key='k_inf_c_itmy_mirror_tf_pf_9', default_value='0.1', enable_events=True)]]
    k_inf_c_itmy_mirror_tf_pf_box_10 = [[sg.Text('      frequency of pole 10'), sg.Combo(('0.1','1','10','100','1000','10000','100000','1000000'), key='k_inf_c_itmy_mirror_tf_pf_10', default_value='0.1', enable_events=True)]]
        
    k_inf_c_etmy_mirror_tf_pf_box_1  = [[sg.Text('      frequency of pole 01'), sg.Combo(('0.1','1','10','100','1000','10000','100000','1000000'), key='k_inf_c_etmy_mirror_tf_pf_1', default_value='0.1', enable_events=True)]]
    k_inf_c_etmy_mirror_tf_pf_box_2  = [[sg.Text('      frequency of pole 02'), sg.Combo(('0.1','1','10','100','1000','10000','100000','1000000'), key='k_inf_c_etmy_mirror_tf_pf_2', default_value='0.1', enable_events=True)]]
    k_inf_c_etmy_mirror_tf_pf_box_3  = [[sg.Text('      frequency of pole 03'), sg.Combo(('0.1','1','10','100','1000','10000','100000','1000000'), key='k_inf_c_etmy_mirror_tf_pf_3', default_value='0.1', enable_events=True)]]
    k_inf_c_etmy_mirror_tf_pf_box_4  = [[sg.Text('      frequency of pole 04'), sg.Combo(('0.1','1','10','100','1000','10000','100000','1000000'), key='k_inf_c_etmy_mirror_tf_pf_4', default_value='0.1', enable_events=True)]]
    k_inf_c_etmy_mirror_tf_pf_box_5  = [[sg.Text('      frequency of pole 05'), sg.Combo(('0.1','1','10','100','1000','10000','100000','1000000'), key='k_inf_c_etmy_mirror_tf_pf_5', default_value='0.1', enable_events=True)]]
    k_inf_c_etmy_mirror_tf_pf_box_6  = [[sg.Text('      frequency of pole 06'), sg.Combo(('0.1','1','10','100','1000','10000','100000','1000000'), key='k_inf_c_etmy_mirror_tf_pf_6', default_value='0.1', enable_events=True)]]
    k_inf_c_etmy_mirror_tf_pf_box_7  = [[sg.Text('      frequency of pole 07'), sg.Combo(('0.1','1','10','100','1000','10000','100000','1000000'), key='k_inf_c_etmy_mirror_tf_pf_7', default_value='0.1', enable_events=True)]]
    k_inf_c_etmy_mirror_tf_pf_box_8  = [[sg.Text('      frequency of pole 08'), sg.Combo(('0.1','1','10','100','1000','10000','100000','1000000'), key='k_inf_c_etmy_mirror_tf_pf_8', default_value='0.1', enable_events=True)]]
    k_inf_c_etmy_mirror_tf_pf_box_9  = [[sg.Text('      frequency of pole 09'), sg.Combo(('0.1','1','10','100','1000','10000','100000','1000000'), key='k_inf_c_etmy_mirror_tf_pf_9', default_value='0.1', enable_events=True)]]
    k_inf_c_etmy_mirror_tf_pf_box_10 = [[sg.Text('      frequency of pole 10'), sg.Combo(('0.1','1','10','100','1000','10000','100000','1000000'), key='k_inf_c_etmy_mirror_tf_pf_10', default_value='0.1', enable_events=True)]]
        
    k_inf_c_prm_mirror_tf_pf_box_1   = [[sg.Text('      frequency of pole 01'), sg.Combo(('0.1','1','10','100','1000','10000','100000','1000000'), key='k_inf_c_prm_mirror_tf_pf_1', default_value='0.1', enable_events=True)]]
    k_inf_c_prm_mirror_tf_pf_box_2   = [[sg.Text('      frequency of pole 02'), sg.Combo(('0.1','1','10','100','1000','10000','100000','1000000'), key='k_inf_c_prm_mirror_tf_pf_2', default_value='0.1', enable_events=True)]]
    k_inf_c_prm_mirror_tf_pf_box_3   = [[sg.Text('      frequency of pole 03'), sg.Combo(('0.1','1','10','100','1000','10000','100000','1000000'), key='k_inf_c_prm_mirror_tf_pf_3', default_value='0.1', enable_events=True)]]
    k_inf_c_prm_mirror_tf_pf_box_4   = [[sg.Text('      frequency of pole 04'), sg.Combo(('0.1','1','10','100','1000','10000','100000','1000000'), key='k_inf_c_prm_mirror_tf_pf_4', default_value='0.1', enable_events=True)]]
    k_inf_c_prm_mirror_tf_pf_box_5   = [[sg.Text('      frequency of pole 05'), sg.Combo(('0.1','1','10','100','1000','10000','100000','1000000'), key='k_inf_c_prm_mirror_tf_pf_5', default_value='0.1', enable_events=True)]]
    k_inf_c_prm_mirror_tf_pf_box_6   = [[sg.Text('      frequency of pole 06'), sg.Combo(('0.1','1','10','100','1000','10000','100000','1000000'), key='k_inf_c_prm_mirror_tf_pf_6', default_value='0.1', enable_events=True)]]
    k_inf_c_prm_mirror_tf_pf_box_7   = [[sg.Text('      frequency of pole 07'), sg.Combo(('0.1','1','10','100','1000','10000','100000','1000000'), key='k_inf_c_prm_mirror_tf_pf_7', default_value='0.1', enable_events=True)]]
    k_inf_c_prm_mirror_tf_pf_box_8   = [[sg.Text('      frequency of pole 08'), sg.Combo(('0.1','1','10','100','1000','10000','100000','1000000'), key='k_inf_c_prm_mirror_tf_pf_8', default_value='0.1', enable_events=True)]]
    k_inf_c_prm_mirror_tf_pf_box_9   = [[sg.Text('      frequency of pole 09'), sg.Combo(('0.1','1','10','100','1000','10000','100000','1000000'), key='k_inf_c_prm_mirror_tf_pf_9', default_value='0.1', enable_events=True)]]
    k_inf_c_prm_mirror_tf_pf_box_10  = [[sg.Text('      frequency of pole 10'), sg.Combo(('0.1','1','10','100','1000','10000','100000','1000000'), key='k_inf_c_prm_mirror_tf_pf_10', default_value='0.1', enable_events=True)]]
        
    k_inf_c_pr2_mirror_tf_pf_box_1   = [[sg.Text('      frequency of pole 01'), sg.Combo(('0.1','1','10','100','1000','10000','100000','1000000'), key='k_inf_c_pr2_mirror_tf_pf_1', default_value='0.1', enable_events=True)]]
    k_inf_c_pr2_mirror_tf_pf_box_2   = [[sg.Text('      frequency of pole 02'), sg.Combo(('0.1','1','10','100','1000','10000','100000','1000000'), key='k_inf_c_pr2_mirror_tf_pf_2', default_value='0.1', enable_events=True)]]
    k_inf_c_pr2_mirror_tf_pf_box_3   = [[sg.Text('      frequency of pole 03'), sg.Combo(('0.1','1','10','100','1000','10000','100000','1000000'), key='k_inf_c_pr2_mirror_tf_pf_3', default_value='0.1', enable_events=True)]]
    k_inf_c_pr2_mirror_tf_pf_box_4   = [[sg.Text('      frequency of pole 04'), sg.Combo(('0.1','1','10','100','1000','10000','100000','1000000'), key='k_inf_c_pr2_mirror_tf_pf_4', default_value='0.1', enable_events=True)]]
    k_inf_c_pr2_mirror_tf_pf_box_5   = [[sg.Text('      frequency of pole 05'), sg.Combo(('0.1','1','10','100','1000','10000','100000','1000000'), key='k_inf_c_pr2_mirror_tf_pf_5', default_value='0.1', enable_events=True)]]
    k_inf_c_pr2_mirror_tf_pf_box_6   = [[sg.Text('      frequency of pole 06'), sg.Combo(('0.1','1','10','100','1000','10000','100000','1000000'), key='k_inf_c_pr2_mirror_tf_pf_6', default_value='0.1', enable_events=True)]]
    k_inf_c_pr2_mirror_tf_pf_box_7   = [[sg.Text('      frequency of pole 07'), sg.Combo(('0.1','1','10','100','1000','10000','100000','1000000'), key='k_inf_c_pr2_mirror_tf_pf_7', default_value='0.1', enable_events=True)]]
    k_inf_c_pr2_mirror_tf_pf_box_8   = [[sg.Text('      frequency of pole 08'), sg.Combo(('0.1','1','10','100','1000','10000','100000','1000000'), key='k_inf_c_pr2_mirror_tf_pf_8', default_value='0.1', enable_events=True)]]
    k_inf_c_pr2_mirror_tf_pf_box_9   = [[sg.Text('      frequency of pole 09'), sg.Combo(('0.1','1','10','100','1000','10000','100000','1000000'), key='k_inf_c_pr2_mirror_tf_pf_9', default_value='0.1', enable_events=True)]]
    k_inf_c_pr2_mirror_tf_pf_box_10  = [[sg.Text('      frequency of pole 10'), sg.Combo(('0.1','1','10','100','1000','10000','100000','1000000'), key='k_inf_c_pr2_mirror_tf_pf_10', default_value='0.1', enable_events=True)]]
        
    k_inf_c_pr3_mirror_tf_pf_box_1   = [[sg.Text('      frequency of pole 01'), sg.Combo(('0.1','1','10','100','1000','10000','100000','1000000'), key='k_inf_c_pr3_mirror_tf_pf_1', default_value='0.1', enable_events=True)]]
    k_inf_c_pr3_mirror_tf_pf_box_2   = [[sg.Text('      frequency of pole 02'), sg.Combo(('0.1','1','10','100','1000','10000','100000','1000000'), key='k_inf_c_pr3_mirror_tf_pf_2', default_value='0.1', enable_events=True)]]
    k_inf_c_pr3_mirror_tf_pf_box_3   = [[sg.Text('      frequency of pole 03'), sg.Combo(('0.1','1','10','100','1000','10000','100000','1000000'), key='k_inf_c_pr3_mirror_tf_pf_3', default_value='0.1', enable_events=True)]]
    k_inf_c_pr3_mirror_tf_pf_box_4   = [[sg.Text('      frequency of pole 04'), sg.Combo(('0.1','1','10','100','1000','10000','100000','1000000'), key='k_inf_c_pr3_mirror_tf_pf_4', default_value='0.1', enable_events=True)]]
    k_inf_c_pr3_mirror_tf_pf_box_5   = [[sg.Text('      frequency of pole 05'), sg.Combo(('0.1','1','10','100','1000','10000','100000','1000000'), key='k_inf_c_pr3_mirror_tf_pf_5', default_value='0.1', enable_events=True)]]
    k_inf_c_pr3_mirror_tf_pf_box_6   = [[sg.Text('      frequency of pole 06'), sg.Combo(('0.1','1','10','100','1000','10000','100000','1000000'), key='k_inf_c_pr3_mirror_tf_pf_6', default_value='0.1', enable_events=True)]]
    k_inf_c_pr3_mirror_tf_pf_box_7   = [[sg.Text('      frequency of pole 07'), sg.Combo(('0.1','1','10','100','1000','10000','100000','1000000'), key='k_inf_c_pr3_mirror_tf_pf_7', default_value='0.1', enable_events=True)]]
    k_inf_c_pr3_mirror_tf_pf_box_8   = [[sg.Text('      frequency of pole 08'), sg.Combo(('0.1','1','10','100','1000','10000','100000','1000000'), key='k_inf_c_pr3_mirror_tf_pf_8', default_value='0.1', enable_events=True)]]
    k_inf_c_pr3_mirror_tf_pf_box_9   = [[sg.Text('      frequency of pole 09'), sg.Combo(('0.1','1','10','100','1000','10000','100000','1000000'), key='k_inf_c_pr3_mirror_tf_pf_9', default_value='0.1', enable_events=True)]]
    k_inf_c_pr3_mirror_tf_pf_box_10  = [[sg.Text('      frequency of pole 10'), sg.Combo(('0.1','1','10','100','1000','10000','100000','1000000'), key='k_inf_c_pr3_mirror_tf_pf_10', default_value='0.1', enable_events=True)]]
         
    k_inf_c_srm_mirror_tf_pf_box_1   = [[sg.Text('      frequency of pole 01'), sg.Combo(('0.1','1','10','100','1000','10000','100000','1000000'), key='k_inf_c_srm_mirror_tf_pf_1', default_value='0.1', enable_events=True)]]
    k_inf_c_srm_mirror_tf_pf_box_2   = [[sg.Text('      frequency of pole 02'), sg.Combo(('0.1','1','10','100','1000','10000','100000','1000000'), key='k_inf_c_srm_mirror_tf_pf_2', default_value='0.1', enable_events=True)]]
    k_inf_c_srm_mirror_tf_pf_box_3   = [[sg.Text('      frequency of pole 03'), sg.Combo(('0.1','1','10','100','1000','10000','100000','1000000'), key='k_inf_c_srm_mirror_tf_pf_3', default_value='0.1', enable_events=True)]]
    k_inf_c_srm_mirror_tf_pf_box_4   = [[sg.Text('      frequency of pole 04'), sg.Combo(('0.1','1','10','100','1000','10000','100000','1000000'), key='k_inf_c_srm_mirror_tf_pf_4', default_value='0.1', enable_events=True)]]
    k_inf_c_srm_mirror_tf_pf_box_5   = [[sg.Text('      frequency of pole 05'), sg.Combo(('0.1','1','10','100','1000','10000','100000','1000000'), key='k_inf_c_srm_mirror_tf_pf_5', default_value='0.1', enable_events=True)]]
    k_inf_c_srm_mirror_tf_pf_box_6   = [[sg.Text('      frequency of pole 06'), sg.Combo(('0.1','1','10','100','1000','10000','100000','1000000'), key='k_inf_c_srm_mirror_tf_pf_6', default_value='0.1', enable_events=True)]]
    k_inf_c_srm_mirror_tf_pf_box_7   = [[sg.Text('      frequency of pole 07'), sg.Combo(('0.1','1','10','100','1000','10000','100000','1000000'), key='k_inf_c_srm_mirror_tf_pf_7', default_value='0.1', enable_events=True)]]
    k_inf_c_srm_mirror_tf_pf_box_8   = [[sg.Text('      frequency of pole 08'), sg.Combo(('0.1','1','10','100','1000','10000','100000','1000000'), key='k_inf_c_srm_mirror_tf_pf_8', default_value='0.1', enable_events=True)]]
    k_inf_c_srm_mirror_tf_pf_box_9   = [[sg.Text('      frequency of pole 09'), sg.Combo(('0.1','1','10','100','1000','10000','100000','1000000'), key='k_inf_c_srm_mirror_tf_pf_9', default_value='0.1', enable_events=True)]]
    k_inf_c_srm_mirror_tf_pf_box_10  = [[sg.Text('      frequency of pole 10'), sg.Combo(('0.1','1','10','100','1000','10000','100000','1000000'), key='k_inf_c_srm_mirror_tf_pf_10', default_value='0.1', enable_events=True)]]
         
    k_inf_c_sr2_mirror_tf_pf_box_1   = [[sg.Text('      frequency of pole 01'), sg.Combo(('0.1','1','10','100','1000','10000','100000','1000000'), key='k_inf_c_sr2_mirror_tf_pf_1', default_value='0.1', enable_events=True)]]
    k_inf_c_sr2_mirror_tf_pf_box_2   = [[sg.Text('      frequency of pole 02'), sg.Combo(('0.1','1','10','100','1000','10000','100000','1000000'), key='k_inf_c_sr2_mirror_tf_pf_2', default_value='0.1', enable_events=True)]]
    k_inf_c_sr2_mirror_tf_pf_box_3   = [[sg.Text('      frequency of pole 03'), sg.Combo(('0.1','1','10','100','1000','10000','100000','1000000'), key='k_inf_c_sr2_mirror_tf_pf_3', default_value='0.1', enable_events=True)]]
    k_inf_c_sr2_mirror_tf_pf_box_4   = [[sg.Text('      frequency of pole 04'), sg.Combo(('0.1','1','10','100','1000','10000','100000','1000000'), key='k_inf_c_sr2_mirror_tf_pf_4', default_value='0.1', enable_events=True)]]
    k_inf_c_sr2_mirror_tf_pf_box_5   = [[sg.Text('      frequency of pole 05'), sg.Combo(('0.1','1','10','100','1000','10000','100000','1000000'), key='k_inf_c_sr2_mirror_tf_pf_5', default_value='0.1', enable_events=True)]]
    k_inf_c_sr2_mirror_tf_pf_box_6   = [[sg.Text('      frequency of pole 06'), sg.Combo(('0.1','1','10','100','1000','10000','100000','1000000'), key='k_inf_c_sr2_mirror_tf_pf_6', default_value='0.1', enable_events=True)]]
    k_inf_c_sr2_mirror_tf_pf_box_7   = [[sg.Text('      frequency of pole 07'), sg.Combo(('0.1','1','10','100','1000','10000','100000','1000000'), key='k_inf_c_sr2_mirror_tf_pf_7', default_value='0.1', enable_events=True)]]
    k_inf_c_sr2_mirror_tf_pf_box_8   = [[sg.Text('      frequency of pole 08'), sg.Combo(('0.1','1','10','100','1000','10000','100000','1000000'), key='k_inf_c_sr2_mirror_tf_pf_8', default_value='0.1', enable_events=True)]]
    k_inf_c_sr2_mirror_tf_pf_box_9   = [[sg.Text('      frequency of pole 09'), sg.Combo(('0.1','1','10','100','1000','10000','100000','1000000'), key='k_inf_c_sr2_mirror_tf_pf_9', default_value='0.1', enable_events=True)]]
    k_inf_c_sr2_mirror_tf_pf_box_10  = [[sg.Text('      frequency of pole 10'), sg.Combo(('0.1','1','10','100','1000','10000','100000','1000000'), key='k_inf_c_sr2_mirror_tf_pf_10', default_value='0.1', enable_events=True)]]
        
    k_inf_c_sr3_mirror_tf_pf_box_1   = [[sg.Text('      frequency of pole 01'), sg.Combo(('0.1','1','10','100','1000','10000','100000','1000000'), key='k_inf_c_sr3_mirror_tf_pf_1', default_value='0.1', enable_events=True)]]
    k_inf_c_sr3_mirror_tf_pf_box_2   = [[sg.Text('      frequency of pole 02'), sg.Combo(('0.1','1','10','100','1000','10000','100000','1000000'), key='k_inf_c_sr3_mirror_tf_pf_2', default_value='0.1', enable_events=True)]]
    k_inf_c_sr3_mirror_tf_pf_box_3   = [[sg.Text('      frequency of pole 03'), sg.Combo(('0.1','1','10','100','1000','10000','100000','1000000'), key='k_inf_c_sr3_mirror_tf_pf_3', default_value='0.1', enable_events=True)]]
    k_inf_c_sr3_mirror_tf_pf_box_4   = [[sg.Text('      frequency of pole 04'), sg.Combo(('0.1','1','10','100','1000','10000','100000','1000000'), key='k_inf_c_sr3_mirror_tf_pf_4', default_value='0.1', enable_events=True)]]
    k_inf_c_sr3_mirror_tf_pf_box_5   = [[sg.Text('      frequency of pole 05'), sg.Combo(('0.1','1','10','100','1000','10000','100000','1000000'), key='k_inf_c_sr3_mirror_tf_pf_5', default_value='0.1', enable_events=True)]]
    k_inf_c_sr3_mirror_tf_pf_box_6   = [[sg.Text('      frequency of pole 06'), sg.Combo(('0.1','1','10','100','1000','10000','100000','1000000'), key='k_inf_c_sr3_mirror_tf_pf_6', default_value='0.1', enable_events=True)]]
    k_inf_c_sr3_mirror_tf_pf_box_7   = [[sg.Text('      frequency of pole 07'), sg.Combo(('0.1','1','10','100','1000','10000','100000','1000000'), key='k_inf_c_sr3_mirror_tf_pf_7', default_value='0.1', enable_events=True)]]
    k_inf_c_sr3_mirror_tf_pf_box_8   = [[sg.Text('      frequency of pole 08'), sg.Combo(('0.1','1','10','100','1000','10000','100000','1000000'), key='k_inf_c_sr3_mirror_tf_pf_8', default_value='0.1', enable_events=True)]]
    k_inf_c_sr3_mirror_tf_pf_box_9   = [[sg.Text('      frequency of pole 09'), sg.Combo(('0.1','1','10','100','1000','10000','100000','1000000'), key='k_inf_c_sr3_mirror_tf_pf_9', default_value='0.1', enable_events=True)]]
    k_inf_c_sr3_mirror_tf_pf_box_10  = [[sg.Text('      frequency of pole 10'), sg.Combo(('0.1','1','10','100','1000','10000','100000','1000000'), key='k_inf_c_sr3_mirror_tf_pf_10', default_value='0.1', enable_events=True)]]
        
    # zeros    
    k_inf_c_mibs_mirror_tf_zf_box_1  = [[sg.Text('      frequency of zero 01'), sg.Combo(('0.1','1','10','100','1000','10000','100000','1000000'), key='k_inf_c_mibs_mirror_tf_zf_1', default_value='0.1', enable_events=True)]]
    k_inf_c_mibs_mirror_tf_zf_box_2  = [[sg.Text('      frequency of zero 02'), sg.Combo(('0.1','1','10','100','1000','10000','100000','1000000'), key='k_inf_c_mibs_mirror_tf_zf_2', default_value='0.1', enable_events=True)]]
    k_inf_c_mibs_mirror_tf_zf_box_3  = [[sg.Text('      frequency of zero 03'), sg.Combo(('0.1','1','10','100','1000','10000','100000','1000000'), key='k_inf_c_mibs_mirror_tf_zf_3', default_value='0.1', enable_events=True)]]
    k_inf_c_mibs_mirror_tf_zf_box_4  = [[sg.Text('      frequency of zero 04'), sg.Combo(('0.1','1','10','100','1000','10000','100000','1000000'), key='k_inf_c_mibs_mirror_tf_zf_4', default_value='0.1', enable_events=True)]]
    k_inf_c_mibs_mirror_tf_zf_box_5  = [[sg.Text('      frequency of zero 05'), sg.Combo(('0.1','1','10','100','1000','10000','100000','1000000'), key='k_inf_c_mibs_mirror_tf_zf_5', default_value='0.1', enable_events=True)]]
    k_inf_c_mibs_mirror_tf_zf_box_6  = [[sg.Text('      frequency of zero 06'), sg.Combo(('0.1','1','10','100','1000','10000','100000','1000000'), key='k_inf_c_mibs_mirror_tf_zf_6', default_value='0.1', enable_events=True)]]
    k_inf_c_mibs_mirror_tf_zf_box_7  = [[sg.Text('      frequency of zero 07'), sg.Combo(('0.1','1','10','100','1000','10000','100000','1000000'), key='k_inf_c_mibs_mirror_tf_zf_7', default_value='0.1', enable_events=True)]]
    k_inf_c_mibs_mirror_tf_zf_box_8  = [[sg.Text('      frequency of zero 08'), sg.Combo(('0.1','1','10','100','1000','10000','100000','1000000'), key='k_inf_c_mibs_mirror_tf_zf_8', default_value='0.1', enable_events=True)]]
    k_inf_c_mibs_mirror_tf_zf_box_9  = [[sg.Text('      frequency of zero 09'), sg.Combo(('0.1','1','10','100','1000','10000','100000','1000000'), key='k_inf_c_mibs_mirror_tf_zf_9', default_value='0.1', enable_events=True)]]
    k_inf_c_mibs_mirror_tf_zf_box_10 = [[sg.Text('      frequency of zero 10'), sg.Combo(('0.1','1','10','100','1000','10000','100000','1000000'), key='k_inf_c_mibs_mirror_tf_zf_10', default_value='0.1', enable_events=True)]]
        
    k_inf_c_itmx_mirror_tf_zf_box_1  = [[sg.Text('      frequency of zero 01'), sg.Combo(('0.1','1','10','100','1000','10000','100000','1000000'), key='k_inf_c_itmx_mirror_tf_zf_1', default_value='0.1', enable_events=True)]]
    k_inf_c_itmx_mirror_tf_zf_box_2  = [[sg.Text('      frequency of zero 02'), sg.Combo(('0.1','1','10','100','1000','10000','100000','1000000'), key='k_inf_c_itmx_mirror_tf_zf_2', default_value='0.1', enable_events=True)]]
    k_inf_c_itmx_mirror_tf_zf_box_3  = [[sg.Text('      frequency of zero 03'), sg.Combo(('0.1','1','10','100','1000','10000','100000','1000000'), key='k_inf_c_itmx_mirror_tf_zf_3', default_value='0.1', enable_events=True)]]
    k_inf_c_itmx_mirror_tf_zf_box_4  = [[sg.Text('      frequency of zero 04'), sg.Combo(('0.1','1','10','100','1000','10000','100000','1000000'), key='k_inf_c_itmx_mirror_tf_zf_4', default_value='0.1', enable_events=True)]]
    k_inf_c_itmx_mirror_tf_zf_box_5  = [[sg.Text('      frequency of zero 05'), sg.Combo(('0.1','1','10','100','1000','10000','100000','1000000'), key='k_inf_c_itmx_mirror_tf_zf_5', default_value='0.1', enable_events=True)]]
    k_inf_c_itmx_mirror_tf_zf_box_6  = [[sg.Text('      frequency of zero 06'), sg.Combo(('0.1','1','10','100','1000','10000','100000','1000000'), key='k_inf_c_itmx_mirror_tf_zf_6', default_value='0.1', enable_events=True)]]
    k_inf_c_itmx_mirror_tf_zf_box_7  = [[sg.Text('      frequency of zero 07'), sg.Combo(('0.1','1','10','100','1000','10000','100000','1000000'), key='k_inf_c_itmx_mirror_tf_zf_7', default_value='0.1', enable_events=True)]]
    k_inf_c_itmx_mirror_tf_zf_box_8  = [[sg.Text('      frequency of zero 08'), sg.Combo(('0.1','1','10','100','1000','10000','100000','1000000'), key='k_inf_c_itmx_mirror_tf_zf_8', default_value='0.1', enable_events=True)]]
    k_inf_c_itmx_mirror_tf_zf_box_9  = [[sg.Text('      frequency of zero 09'), sg.Combo(('0.1','1','10','100','1000','10000','100000','1000000'), key='k_inf_c_itmx_mirror_tf_zf_9', default_value='0.1', enable_events=True)]]
    k_inf_c_itmx_mirror_tf_zf_box_10 = [[sg.Text('      frequency of zero 10'), sg.Combo(('0.1','1','10','100','1000','10000','100000','1000000'), key='k_inf_c_itmx_mirror_tf_zf_10', default_value='0.1', enable_events=True)]]
        
    k_inf_c_etmx_mirror_tf_zf_box_1  = [[sg.Text('      frequency of zero 01'), sg.Combo(('0.1','1','10','100','1000','10000','100000','1000000'), key='k_inf_c_etmx_mirror_tf_zf_1', default_value='0.1', enable_events=True)]]
    k_inf_c_etmx_mirror_tf_zf_box_2  = [[sg.Text('      frequency of zero 02'), sg.Combo(('0.1','1','10','100','1000','10000','100000','1000000'), key='k_inf_c_etmx_mirror_tf_zf_2', default_value='0.1', enable_events=True)]]
    k_inf_c_etmx_mirror_tf_zf_box_3  = [[sg.Text('      frequency of zero 03'), sg.Combo(('0.1','1','10','100','1000','10000','100000','1000000'), key='k_inf_c_etmx_mirror_tf_zf_3', default_value='0.1', enable_events=True)]]
    k_inf_c_etmx_mirror_tf_zf_box_4  = [[sg.Text('      frequency of zero 04'), sg.Combo(('0.1','1','10','100','1000','10000','100000','1000000'), key='k_inf_c_etmx_mirror_tf_zf_4', default_value='0.1', enable_events=True)]]
    k_inf_c_etmx_mirror_tf_zf_box_5  = [[sg.Text('      frequency of zero 05'), sg.Combo(('0.1','1','10','100','1000','10000','100000','1000000'), key='k_inf_c_etmx_mirror_tf_zf_5', default_value='0.1', enable_events=True)]]
    k_inf_c_etmx_mirror_tf_zf_box_6  = [[sg.Text('      frequency of zero 06'), sg.Combo(('0.1','1','10','100','1000','10000','100000','1000000'), key='k_inf_c_etmx_mirror_tf_zf_6', default_value='0.1', enable_events=True)]]
    k_inf_c_etmx_mirror_tf_zf_box_7  = [[sg.Text('      frequency of zero 07'), sg.Combo(('0.1','1','10','100','1000','10000','100000','1000000'), key='k_inf_c_etmx_mirror_tf_zf_7', default_value='0.1', enable_events=True)]]
    k_inf_c_etmx_mirror_tf_zf_box_8  = [[sg.Text('      frequency of zero 08'), sg.Combo(('0.1','1','10','100','1000','10000','100000','1000000'), key='k_inf_c_etmx_mirror_tf_zf_8', default_value='0.1', enable_events=True)]]
    k_inf_c_etmx_mirror_tf_zf_box_9  = [[sg.Text('      frequency of zero 09'), sg.Combo(('0.1','1','10','100','1000','10000','100000','1000000'), key='k_inf_c_etmx_mirror_tf_zf_9', default_value='0.1', enable_events=True)]]
    k_inf_c_etmx_mirror_tf_zf_box_10 = [[sg.Text('      frequency of zero 10'), sg.Combo(('0.1','1','10','100','1000','10000','100000','1000000'), key='k_inf_c_etmx_mirror_tf_zf_10', default_value='0.1', enable_events=True)]]
        
    k_inf_c_itmy_mirror_tf_zf_box_1  = [[sg.Text('      frequency of zero 01'), sg.Combo(('0.1','1','10','100','1000','10000','100000','1000000'), key='k_inf_c_itmy_mirror_tf_zf_1', default_value='0.1', enable_events=True)]]
    k_inf_c_itmy_mirror_tf_zf_box_2  = [[sg.Text('      frequency of zero 02'), sg.Combo(('0.1','1','10','100','1000','10000','100000','1000000'), key='k_inf_c_itmy_mirror_tf_zf_2', default_value='0.1', enable_events=True)]]
    k_inf_c_itmy_mirror_tf_zf_box_3  = [[sg.Text('      frequency of zero 03'), sg.Combo(('0.1','1','10','100','1000','10000','100000','1000000'), key='k_inf_c_itmy_mirror_tf_zf_3', default_value='0.1', enable_events=True)]]
    k_inf_c_itmy_mirror_tf_zf_box_4  = [[sg.Text('      frequency of zero 04'), sg.Combo(('0.1','1','10','100','1000','10000','100000','1000000'), key='k_inf_c_itmy_mirror_tf_zf_4', default_value='0.1', enable_events=True)]]
    k_inf_c_itmy_mirror_tf_zf_box_5  = [[sg.Text('      frequency of zero 05'), sg.Combo(('0.1','1','10','100','1000','10000','100000','1000000'), key='k_inf_c_itmy_mirror_tf_zf_5', default_value='0.1', enable_events=True)]]
    k_inf_c_itmy_mirror_tf_zf_box_6  = [[sg.Text('      frequency of zero 06'), sg.Combo(('0.1','1','10','100','1000','10000','100000','1000000'), key='k_inf_c_itmy_mirror_tf_zf_6', default_value='0.1', enable_events=True)]]
    k_inf_c_itmy_mirror_tf_zf_box_7  = [[sg.Text('      frequency of zero 07'), sg.Combo(('0.1','1','10','100','1000','10000','100000','1000000'), key='k_inf_c_itmy_mirror_tf_zf_7', default_value='0.1', enable_events=True)]]
    k_inf_c_itmy_mirror_tf_zf_box_8  = [[sg.Text('      frequency of zero 08'), sg.Combo(('0.1','1','10','100','1000','10000','100000','1000000'), key='k_inf_c_itmy_mirror_tf_zf_8', default_value='0.1', enable_events=True)]]
    k_inf_c_itmy_mirror_tf_zf_box_9  = [[sg.Text('      frequency of zero 09'), sg.Combo(('0.1','1','10','100','1000','10000','100000','1000000'), key='k_inf_c_itmy_mirror_tf_zf_9', default_value='0.1', enable_events=True)]]
    k_inf_c_itmy_mirror_tf_zf_box_10 = [[sg.Text('      frequency of zero 10'), sg.Combo(('0.1','1','10','100','1000','10000','100000','1000000'), key='k_inf_c_itmy_mirror_tf_zf_10', default_value='0.1', enable_events=True)]]
        
    k_inf_c_etmy_mirror_tf_zf_box_1  = [[sg.Text('      frequency of zero 01'), sg.Combo(('0.1','1','10','100','1000','10000','100000','1000000'), key='k_inf_c_etmy_mirror_tf_zf_1', default_value='0.1', enable_events=True)]]
    k_inf_c_etmy_mirror_tf_zf_box_2  = [[sg.Text('      frequency of zero 02'), sg.Combo(('0.1','1','10','100','1000','10000','100000','1000000'), key='k_inf_c_etmy_mirror_tf_zf_2', default_value='0.1', enable_events=True)]]
    k_inf_c_etmy_mirror_tf_zf_box_3  = [[sg.Text('      frequency of zero 03'), sg.Combo(('0.1','1','10','100','1000','10000','100000','1000000'), key='k_inf_c_etmy_mirror_tf_zf_3', default_value='0.1', enable_events=True)]]
    k_inf_c_etmy_mirror_tf_zf_box_4  = [[sg.Text('      frequency of zero 04'), sg.Combo(('0.1','1','10','100','1000','10000','100000','1000000'), key='k_inf_c_etmy_mirror_tf_zf_4', default_value='0.1', enable_events=True)]]
    k_inf_c_etmy_mirror_tf_zf_box_5  = [[sg.Text('      frequency of zero 05'), sg.Combo(('0.1','1','10','100','1000','10000','100000','1000000'), key='k_inf_c_etmy_mirror_tf_zf_5', default_value='0.1', enable_events=True)]]
    k_inf_c_etmy_mirror_tf_zf_box_6  = [[sg.Text('      frequency of zero 06'), sg.Combo(('0.1','1','10','100','1000','10000','100000','1000000'), key='k_inf_c_etmy_mirror_tf_zf_6', default_value='0.1', enable_events=True)]]
    k_inf_c_etmy_mirror_tf_zf_box_7  = [[sg.Text('      frequency of zero 07'), sg.Combo(('0.1','1','10','100','1000','10000','100000','1000000'), key='k_inf_c_etmy_mirror_tf_zf_7', default_value='0.1', enable_events=True)]]
    k_inf_c_etmy_mirror_tf_zf_box_8  = [[sg.Text('      frequency of zero 08'), sg.Combo(('0.1','1','10','100','1000','10000','100000','1000000'), key='k_inf_c_etmy_mirror_tf_zf_8', default_value='0.1', enable_events=True)]]
    k_inf_c_etmy_mirror_tf_zf_box_9  = [[sg.Text('      frequency of zero 09'), sg.Combo(('0.1','1','10','100','1000','10000','100000','1000000'), key='k_inf_c_etmy_mirror_tf_zf_9', default_value='0.1', enable_events=True)]]
    k_inf_c_etmy_mirror_tf_zf_box_10 = [[sg.Text('      frequency of zero 10'), sg.Combo(('0.1','1','10','100','1000','10000','100000','1000000'), key='k_inf_c_etmy_mirror_tf_zf_10', default_value='0.1', enable_events=True)]]
        
    k_inf_c_prm_mirror_tf_zf_box_1   = [[sg.Text('      frequency of zero 01'), sg.Combo(('0.1','1','10','100','1000','10000','100000','1000000'), key='k_inf_c_prm_mirror_tf_zf_1', default_value='0.1', enable_events=True)]]
    k_inf_c_prm_mirror_tf_zf_box_2   = [[sg.Text('      frequency of zero 02'), sg.Combo(('0.1','1','10','100','1000','10000','100000','1000000'), key='k_inf_c_prm_mirror_tf_zf_2', default_value='0.1', enable_events=True)]]
    k_inf_c_prm_mirror_tf_zf_box_3   = [[sg.Text('      frequency of zero 03'), sg.Combo(('0.1','1','10','100','1000','10000','100000','1000000'), key='k_inf_c_prm_mirror_tf_zf_3', default_value='0.1', enable_events=True)]]
    k_inf_c_prm_mirror_tf_zf_box_4   = [[sg.Text('      frequency of zero 04'), sg.Combo(('0.1','1','10','100','1000','10000','100000','1000000'), key='k_inf_c_prm_mirror_tf_zf_4', default_value='0.1', enable_events=True)]]
    k_inf_c_prm_mirror_tf_zf_box_5   = [[sg.Text('      frequency of zero 05'), sg.Combo(('0.1','1','10','100','1000','10000','100000','1000000'), key='k_inf_c_prm_mirror_tf_zf_5', default_value='0.1', enable_events=True)]]
    k_inf_c_prm_mirror_tf_zf_box_6   = [[sg.Text('      frequency of zero 06'), sg.Combo(('0.1','1','10','100','1000','10000','100000','1000000'), key='k_inf_c_prm_mirror_tf_zf_6', default_value='0.1', enable_events=True)]]
    k_inf_c_prm_mirror_tf_zf_box_7   = [[sg.Text('      frequency of zero 07'), sg.Combo(('0.1','1','10','100','1000','10000','100000','1000000'), key='k_inf_c_prm_mirror_tf_zf_7', default_value='0.1', enable_events=True)]]
    k_inf_c_prm_mirror_tf_zf_box_8   = [[sg.Text('      frequency of zero 08'), sg.Combo(('0.1','1','10','100','1000','10000','100000','1000000'), key='k_inf_c_prm_mirror_tf_zf_8', default_value='0.1', enable_events=True)]]
    k_inf_c_prm_mirror_tf_zf_box_9   = [[sg.Text('      frequency of zero 09'), sg.Combo(('0.1','1','10','100','1000','10000','100000','1000000'), key='k_inf_c_prm_mirror_tf_zf_9', default_value='0.1', enable_events=True)]]
    k_inf_c_prm_mirror_tf_zf_box_10  = [[sg.Text('      frequency of zero 10'), sg.Combo(('0.1','1','10','100','1000','10000','100000','1000000'), key='k_inf_c_prm_mirror_tf_zf_10', default_value='0.1', enable_events=True)]]
        
    k_inf_c_pr2_mirror_tf_zf_box_1   = [[sg.Text('      frequency of zero 01'), sg.Combo(('0.1','1','10','100','1000','10000','100000','1000000'), key='k_inf_c_pr2_mirror_tf_zf_1', default_value='0.1', enable_events=True)]]
    k_inf_c_pr2_mirror_tf_zf_box_2   = [[sg.Text('      frequency of zero 02'), sg.Combo(('0.1','1','10','100','1000','10000','100000','1000000'), key='k_inf_c_pr2_mirror_tf_zf_2', default_value='0.1', enable_events=True)]]
    k_inf_c_pr2_mirror_tf_zf_box_3   = [[sg.Text('      frequency of zero 03'), sg.Combo(('0.1','1','10','100','1000','10000','100000','1000000'), key='k_inf_c_pr2_mirror_tf_zf_3', default_value='0.1', enable_events=True)]]
    k_inf_c_pr2_mirror_tf_zf_box_4   = [[sg.Text('      frequency of zero 04'), sg.Combo(('0.1','1','10','100','1000','10000','100000','1000000'), key='k_inf_c_pr2_mirror_tf_zf_4', default_value='0.1', enable_events=True)]]
    k_inf_c_pr2_mirror_tf_zf_box_5   = [[sg.Text('      frequency of zero 05'), sg.Combo(('0.1','1','10','100','1000','10000','100000','1000000'), key='k_inf_c_pr2_mirror_tf_zf_5', default_value='0.1', enable_events=True)]]
    k_inf_c_pr2_mirror_tf_zf_box_6   = [[sg.Text('      frequency of zero 06'), sg.Combo(('0.1','1','10','100','1000','10000','100000','1000000'), key='k_inf_c_pr2_mirror_tf_zf_6', default_value='0.1', enable_events=True)]]
    k_inf_c_pr2_mirror_tf_zf_box_7   = [[sg.Text('      frequency of zero 07'), sg.Combo(('0.1','1','10','100','1000','10000','100000','1000000'), key='k_inf_c_pr2_mirror_tf_zf_7', default_value='0.1', enable_events=True)]]
    k_inf_c_pr2_mirror_tf_zf_box_8   = [[sg.Text('      frequency of zero 08'), sg.Combo(('0.1','1','10','100','1000','10000','100000','1000000'), key='k_inf_c_pr2_mirror_tf_zf_8', default_value='0.1', enable_events=True)]]
    k_inf_c_pr2_mirror_tf_zf_box_9   = [[sg.Text('      frequency of zero 09'), sg.Combo(('0.1','1','10','100','1000','10000','100000','1000000'), key='k_inf_c_pr2_mirror_tf_zf_9', default_value='0.1', enable_events=True)]]
    k_inf_c_pr2_mirror_tf_zf_box_10  = [[sg.Text('      frequency of zero 10'), sg.Combo(('0.1','1','10','100','1000','10000','100000','1000000'), key='k_inf_c_pr2_mirror_tf_zf_10', default_value='0.1', enable_events=True)]]
        
    k_inf_c_pr3_mirror_tf_zf_box_1   = [[sg.Text('      frequency of zero 01'), sg.Combo(('0.1','1','10','100','1000','10000','100000','1000000'), key='k_inf_c_pr3_mirror_tf_zf_1', default_value='0.1', enable_events=True)]]
    k_inf_c_pr3_mirror_tf_zf_box_2   = [[sg.Text('      frequency of zero 02'), sg.Combo(('0.1','1','10','100','1000','10000','100000','1000000'), key='k_inf_c_pr3_mirror_tf_zf_2', default_value='0.1', enable_events=True)]]
    k_inf_c_pr3_mirror_tf_zf_box_3   = [[sg.Text('      frequency of zero 03'), sg.Combo(('0.1','1','10','100','1000','10000','100000','1000000'), key='k_inf_c_pr3_mirror_tf_zf_3', default_value='0.1', enable_events=True)]]
    k_inf_c_pr3_mirror_tf_zf_box_4   = [[sg.Text('      frequency of zero 04'), sg.Combo(('0.1','1','10','100','1000','10000','100000','1000000'), key='k_inf_c_pr3_mirror_tf_zf_4', default_value='0.1', enable_events=True)]]
    k_inf_c_pr3_mirror_tf_zf_box_5   = [[sg.Text('      frequency of zero 05'), sg.Combo(('0.1','1','10','100','1000','10000','100000','1000000'), key='k_inf_c_pr3_mirror_tf_zf_5', default_value='0.1', enable_events=True)]]
    k_inf_c_pr3_mirror_tf_zf_box_6   = [[sg.Text('      frequency of zero 06'), sg.Combo(('0.1','1','10','100','1000','10000','100000','1000000'), key='k_inf_c_pr3_mirror_tf_zf_6', default_value='0.1', enable_events=True)]]
    k_inf_c_pr3_mirror_tf_zf_box_7   = [[sg.Text('      frequency of zero 07'), sg.Combo(('0.1','1','10','100','1000','10000','100000','1000000'), key='k_inf_c_pr3_mirror_tf_zf_7', default_value='0.1', enable_events=True)]]
    k_inf_c_pr3_mirror_tf_zf_box_8   = [[sg.Text('      frequency of zero 08'), sg.Combo(('0.1','1','10','100','1000','10000','100000','1000000'), key='k_inf_c_pr3_mirror_tf_zf_8', default_value='0.1', enable_events=True)]]
    k_inf_c_pr3_mirror_tf_zf_box_9   = [[sg.Text('      frequency of zero 09'), sg.Combo(('0.1','1','10','100','1000','10000','100000','1000000'), key='k_inf_c_pr3_mirror_tf_zf_9', default_value='0.1', enable_events=True)]]
    k_inf_c_pr3_mirror_tf_zf_box_10  = [[sg.Text('      frequency of zero 10'), sg.Combo(('0.1','1','10','100','1000','10000','100000','1000000'), key='k_inf_c_pr3_mirror_tf_zf_10', default_value='0.1', enable_events=True)]]
        
    k_inf_c_srm_mirror_tf_zf_box_1   = [[sg.Text('      frequency of zero 01'), sg.Combo(('0.1','1','10','100','1000','10000','100000','1000000'), key='k_inf_c_srm_mirror_tf_zf_1', default_value='0.1', enable_events=True)]]
    k_inf_c_srm_mirror_tf_zf_box_2   = [[sg.Text('      frequency of zero 02'), sg.Combo(('0.1','1','10','100','1000','10000','100000','1000000'), key='k_inf_c_srm_mirror_tf_zf_2', default_value='0.1', enable_events=True)]]
    k_inf_c_srm_mirror_tf_zf_box_3   = [[sg.Text('      frequency of zero 03'), sg.Combo(('0.1','1','10','100','1000','10000','100000','1000000'), key='k_inf_c_srm_mirror_tf_zf_3', default_value='0.1', enable_events=True)]]
    k_inf_c_srm_mirror_tf_zf_box_4   = [[sg.Text('      frequency of zero 04'), sg.Combo(('0.1','1','10','100','1000','10000','100000','1000000'), key='k_inf_c_srm_mirror_tf_zf_4', default_value='0.1', enable_events=True)]]
    k_inf_c_srm_mirror_tf_zf_box_5   = [[sg.Text('      frequency of zero 05'), sg.Combo(('0.1','1','10','100','1000','10000','100000','1000000'), key='k_inf_c_srm_mirror_tf_zf_5', default_value='0.1', enable_events=True)]]
    k_inf_c_srm_mirror_tf_zf_box_6   = [[sg.Text('      frequency of zero 06'), sg.Combo(('0.1','1','10','100','1000','10000','100000','1000000'), key='k_inf_c_srm_mirror_tf_zf_6', default_value='0.1', enable_events=True)]]
    k_inf_c_srm_mirror_tf_zf_box_7   = [[sg.Text('      frequency of zero 07'), sg.Combo(('0.1','1','10','100','1000','10000','100000','1000000'), key='k_inf_c_srm_mirror_tf_zf_7', default_value='0.1', enable_events=True)]]
    k_inf_c_srm_mirror_tf_zf_box_8   = [[sg.Text('      frequency of zero 08'), sg.Combo(('0.1','1','10','100','1000','10000','100000','1000000'), key='k_inf_c_srm_mirror_tf_zf_8', default_value='0.1', enable_events=True)]]
    k_inf_c_srm_mirror_tf_zf_box_9   = [[sg.Text('      frequency of zero 09'), sg.Combo(('0.1','1','10','100','1000','10000','100000','1000000'), key='k_inf_c_srm_mirror_tf_zf_9', default_value='0.1', enable_events=True)]]
    k_inf_c_srm_mirror_tf_zf_box_10  = [[sg.Text('      frequency of zero 10'), sg.Combo(('0.1','1','10','100','1000','10000','100000','1000000'), key='k_inf_c_srm_mirror_tf_zf_10', default_value='0.1', enable_events=True)]]
        
    k_inf_c_sr2_mirror_tf_zf_box_1   = [[sg.Text('      frequency of zero 01'), sg.Combo(('0.1','1','10','100','1000','10000','100000','1000000'), key='k_inf_c_sr2_mirror_tf_zf_1', default_value='0.1', enable_events=True)]]
    k_inf_c_sr2_mirror_tf_zf_box_2   = [[sg.Text('      frequency of zero 02'), sg.Combo(('0.1','1','10','100','1000','10000','100000','1000000'), key='k_inf_c_sr2_mirror_tf_zf_2', default_value='0.1', enable_events=True)]]
    k_inf_c_sr2_mirror_tf_zf_box_3   = [[sg.Text('      frequency of zero 03'), sg.Combo(('0.1','1','10','100','1000','10000','100000','1000000'), key='k_inf_c_sr2_mirror_tf_zf_3', default_value='0.1', enable_events=True)]]
    k_inf_c_sr2_mirror_tf_zf_box_4   = [[sg.Text('      frequency of zero 04'), sg.Combo(('0.1','1','10','100','1000','10000','100000','1000000'), key='k_inf_c_sr2_mirror_tf_zf_4', default_value='0.1', enable_events=True)]]
    k_inf_c_sr2_mirror_tf_zf_box_5   = [[sg.Text('      frequency of zero 05'), sg.Combo(('0.1','1','10','100','1000','10000','100000','1000000'), key='k_inf_c_sr2_mirror_tf_zf_5', default_value='0.1', enable_events=True)]]
    k_inf_c_sr2_mirror_tf_zf_box_6   = [[sg.Text('      frequency of zero 06'), sg.Combo(('0.1','1','10','100','1000','10000','100000','1000000'), key='k_inf_c_sr2_mirror_tf_zf_6', default_value='0.1', enable_events=True)]]
    k_inf_c_sr2_mirror_tf_zf_box_7   = [[sg.Text('      frequency of zero 07'), sg.Combo(('0.1','1','10','100','1000','10000','100000','1000000'), key='k_inf_c_sr2_mirror_tf_zf_7', default_value='0.1', enable_events=True)]]
    k_inf_c_sr2_mirror_tf_zf_box_8   = [[sg.Text('      frequency of zero 08'), sg.Combo(('0.1','1','10','100','1000','10000','100000','1000000'), key='k_inf_c_sr2_mirror_tf_zf_8', default_value='0.1', enable_events=True)]]
    k_inf_c_sr2_mirror_tf_zf_box_9   = [[sg.Text('      frequency of zero 09'), sg.Combo(('0.1','1','10','100','1000','10000','100000','1000000'), key='k_inf_c_sr2_mirror_tf_zf_9', default_value='0.1', enable_events=True)]]
    k_inf_c_sr2_mirror_tf_zf_box_10  = [[sg.Text('      frequency of zero 10'), sg.Combo(('0.1','1','10','100','1000','10000','100000','1000000'), key='k_inf_c_sr2_mirror_tf_zf_10', default_value='0.1', enable_events=True)]]
        
    k_inf_c_sr3_mirror_tf_zf_box_1   = [[sg.Text('      frequency of zero 01'), sg.Combo(('0.1','1','10','100','1000','10000','100000','1000000'), key='k_inf_c_sr3_mirror_tf_zf_1', default_value='0.1', enable_events=True)]]
    k_inf_c_sr3_mirror_tf_zf_box_2   = [[sg.Text('      frequency of zero 02'), sg.Combo(('0.1','1','10','100','1000','10000','100000','1000000'), key='k_inf_c_sr3_mirror_tf_zf_2', default_value='0.1', enable_events=True)]]
    k_inf_c_sr3_mirror_tf_zf_box_3   = [[sg.Text('      frequency of zero 03'), sg.Combo(('0.1','1','10','100','1000','10000','100000','1000000'), key='k_inf_c_sr3_mirror_tf_zf_3', default_value='0.1', enable_events=True)]]
    k_inf_c_sr3_mirror_tf_zf_box_4   = [[sg.Text('      frequency of zero 04'), sg.Combo(('0.1','1','10','100','1000','10000','100000','1000000'), key='k_inf_c_sr3_mirror_tf_zf_4', default_value='0.1', enable_events=True)]]
    k_inf_c_sr3_mirror_tf_zf_box_5   = [[sg.Text('      frequency of zero 05'), sg.Combo(('0.1','1','10','100','1000','10000','100000','1000000'), key='k_inf_c_sr3_mirror_tf_zf_5', default_value='0.1', enable_events=True)]]
    k_inf_c_sr3_mirror_tf_zf_box_6   = [[sg.Text('      frequency of zero 06'), sg.Combo(('0.1','1','10','100','1000','10000','100000','1000000'), key='k_inf_c_sr3_mirror_tf_zf_6', default_value='0.1', enable_events=True)]]
    k_inf_c_sr3_mirror_tf_zf_box_7   = [[sg.Text('      frequency of zero 07'), sg.Combo(('0.1','1','10','100','1000','10000','100000','1000000'), key='k_inf_c_sr3_mirror_tf_zf_7', default_value='0.1', enable_events=True)]]
    k_inf_c_sr3_mirror_tf_zf_box_8   = [[sg.Text('      frequency of zero 08'), sg.Combo(('0.1','1','10','100','1000','10000','100000','1000000'), key='k_inf_c_sr3_mirror_tf_zf_8', default_value='0.1', enable_events=True)]]
    k_inf_c_sr3_mirror_tf_zf_box_9   = [[sg.Text('      frequency of zero 09'), sg.Combo(('0.1','1','10','100','1000','10000','100000','1000000'), key='k_inf_c_sr3_mirror_tf_zf_9', default_value='0.1', enable_events=True)]]
    k_inf_c_sr3_mirror_tf_zf_box_10  = [[sg.Text('      frequency of zero 10'), sg.Combo(('0.1','1','10','100','1000','10000','100000','1000000'), key='k_inf_c_sr3_mirror_tf_zf_10', default_value='0.1', enable_events=True)]]
    


    # Quality factor
    ##################################################################################################################################################################################################
    k_inf_c_mibs_mirror_tf_re_box_1  = [[sg.Text('      quality factor value 01'), sg.Combo(('0.1','1','10','100','1000','10000','100000','1000000'),  key='k_inf_c_mibs_mirror_tf_rq_1', default_value='1000000', enable_events=True),sg.Text('resonance frequency 01'), sg.Combo(('0.1','1','10','100','1000','10000','100000','1000000'),  key='k_inf_c_mibs_mirror_tf_rf_1',  default_value='0.1', enable_events=True), sg.Text('pole or zero'), sg.Combo(('p','z'),  key='k_inf_c_mibs_mirror_tf_zp_1',  default_value='p', enable_events=True, readonly=True)]]
    k_inf_c_mibs_mirror_tf_re_box_2  = [[sg.Text('      quality factor value 02'), sg.Combo(('0.1','1','10','100','1000','10000','100000','1000000'),  key='k_inf_c_mibs_mirror_tf_rq_2', default_value='1000000', enable_events=True),sg.Text('resonance frequency 02'), sg.Combo(('0.1','1','10','100','1000','10000','100000','1000000'),  key='k_inf_c_mibs_mirror_tf_rf_2',  default_value='0.1', enable_events=True), sg.Text('pole or zero'), sg.Combo(('p','z'),  key='k_inf_c_mibs_mirror_tf_zp_2',  default_value='p', enable_events=True, readonly=True)]]
    k_inf_c_mibs_mirror_tf_re_box_3  = [[sg.Text('      quality factor value 03'), sg.Combo(('0.1','1','10','100','1000','10000','100000','1000000'),  key='k_inf_c_mibs_mirror_tf_rq_3', default_value='1000000', enable_events=True),sg.Text('resonance frequency 03'), sg.Combo(('0.1','1','10','100','1000','10000','100000','1000000'),  key='k_inf_c_mibs_mirror_tf_rf_3',  default_value='0.1', enable_events=True), sg.Text('pole or zero'), sg.Combo(('p','z'),  key='k_inf_c_mibs_mirror_tf_zp_3',  default_value='p', enable_events=True, readonly=True)]]
    k_inf_c_mibs_mirror_tf_re_box_4  = [[sg.Text('      quality factor value 04'), sg.Combo(('0.1','1','10','100','1000','10000','100000','1000000'),  key='k_inf_c_mibs_mirror_tf_rq_4', default_value='1000000', enable_events=True),sg.Text('resonance frequency 04'), sg.Combo(('0.1','1','10','100','1000','10000','100000','1000000'),  key='k_inf_c_mibs_mirror_tf_rf_4',  default_value='0.1', enable_events=True), sg.Text('pole or zero'), sg.Combo(('p','z'),  key='k_inf_c_mibs_mirror_tf_zp_4',  default_value='p', enable_events=True, readonly=True)]]
    k_inf_c_mibs_mirror_tf_re_box_5  = [[sg.Text('      quality factor value 05'), sg.Combo(('0.1','1','10','100','1000','10000','100000','1000000'),  key='k_inf_c_mibs_mirror_tf_rq_5', default_value='1000000', enable_events=True),sg.Text('resonance frequency 05'), sg.Combo(('0.1','1','10','100','1000','10000','100000','1000000'),  key='k_inf_c_mibs_mirror_tf_rf_5',  default_value='0.1', enable_events=True), sg.Text('pole or zero'), sg.Combo(('p','z'),  key='k_inf_c_mibs_mirror_tf_zp_5',  default_value='p', enable_events=True, readonly=True)]]
    k_inf_c_mibs_mirror_tf_re_box_6  = [[sg.Text('      quality factor value 06'), sg.Combo(('0.1','1','10','100','1000','10000','100000','1000000'),  key='k_inf_c_mibs_mirror_tf_rq_6', default_value='1000000', enable_events=True),sg.Text('resonance frequency 06'), sg.Combo(('0.1','1','10','100','1000','10000','100000','1000000'),  key='k_inf_c_mibs_mirror_tf_rf_6',  default_value='0.1', enable_events=True), sg.Text('pole or zero'), sg.Combo(('p','z'),  key='k_inf_c_mibs_mirror_tf_zp_6',  default_value='p', enable_events=True, readonly=True)]]
    k_inf_c_mibs_mirror_tf_re_box_7  = [[sg.Text('      quality factor value 07'), sg.Combo(('0.1','1','10','100','1000','10000','100000','1000000'),  key='k_inf_c_mibs_mirror_tf_rq_7', default_value='1000000', enable_events=True),sg.Text('resonance frequency 07'), sg.Combo(('0.1','1','10','100','1000','10000','100000','1000000'),  key='k_inf_c_mibs_mirror_tf_rf_7',  default_value='0.1', enable_events=True), sg.Text('pole or zero'), sg.Combo(('p','z'),  key='k_inf_c_mibs_mirror_tf_zp_7',  default_value='p', enable_events=True, readonly=True)]]
    k_inf_c_mibs_mirror_tf_re_box_8  = [[sg.Text('      quality factor value 08'), sg.Combo(('0.1','1','10','100','1000','10000','100000','1000000'),  key='k_inf_c_mibs_mirror_tf_rq_8', default_value='1000000', enable_events=True),sg.Text('resonance frequency 08'), sg.Combo(('0.1','1','10','100','1000','10000','100000','1000000'),  key='k_inf_c_mibs_mirror_tf_rf_8',  default_value='0.1', enable_events=True), sg.Text('pole or zero'), sg.Combo(('p','z'),  key='k_inf_c_mibs_mirror_tf_zp_8',  default_value='p', enable_events=True, readonly=True)]]
    k_inf_c_mibs_mirror_tf_re_box_9  = [[sg.Text('      quality factor value 09'), sg.Combo(('0.1','1','10','100','1000','10000','100000','1000000'),  key='k_inf_c_mibs_mirror_tf_rq_9', default_value='1000000', enable_events=True),sg.Text('resonance frequency 09'), sg.Combo(('0.1','1','10','100','1000','10000','100000','1000000'),  key='k_inf_c_mibs_mirror_tf_rf_9',  default_value='0.1', enable_events=True), sg.Text('pole or zero'), sg.Combo(('p','z'),  key='k_inf_c_mibs_mirror_tf_zp_9',  default_value='p', enable_events=True, readonly=True)]]
    k_inf_c_mibs_mirror_tf_re_box_10 = [[sg.Text('      quality factor value 10'), sg.Combo(('0.1','1','10','100','1000','10000','100000','1000000'), key='k_inf_c_mibs_mirror_tf_rq_10', default_value='1000000', enable_events=True),sg.Text('resonance frequency 10'), sg.Combo(('0.1','1','10','100','1000','10000','100000','1000000'),  key='k_inf_c_mibs_mirror_tf_rf_10', default_value='0.1', enable_events=True), sg.Text('pole or zero'), sg.Combo(('p','z'),  key='k_inf_c_mibs_mirror_tf_zp_10', default_value='p', enable_events=True, readonly=True)]]

    ##################################################################################################################################################################################################
    k_inf_c_itmx_mirror_tf_re_box_1  = [[sg.Text('      quality factor value 01'), sg.Combo(('0.1','1','10','100','1000','10000','100000','1000000'),  key='k_inf_c_itmx_mirror_tf_rq_1', default_value='1000000', enable_events=True),sg.Text('resonance frequency 01'), sg.Combo(('0.1','1','10','100','1000','10000','100000','1000000'),  key='k_inf_c_itmx_mirror_tf_rf_1',  default_value='0.1', enable_events=True), sg.Text('pole or zero'), sg.Combo(('p','z'),  key='k_inf_c_itmx_mirror_tf_zp_1',  default_value='p', enable_events=True, readonly=True)]]
    k_inf_c_itmx_mirror_tf_re_box_2  = [[sg.Text('      quality factor value 02'), sg.Combo(('0.1','1','10','100','1000','10000','100000','1000000'),  key='k_inf_c_itmx_mirror_tf_rq_2', default_value='1000000', enable_events=True),sg.Text('resonance frequency 02'), sg.Combo(('0.1','1','10','100','1000','10000','100000','1000000'),  key='k_inf_c_itmx_mirror_tf_rf_2',  default_value='0.1', enable_events=True), sg.Text('pole or zero'), sg.Combo(('p','z'),  key='k_inf_c_itmx_mirror_tf_zp_2',  default_value='p', enable_events=True, readonly=True)]]
    k_inf_c_itmx_mirror_tf_re_box_3  = [[sg.Text('      quality factor value 03'), sg.Combo(('0.1','1','10','100','1000','10000','100000','1000000'),  key='k_inf_c_itmx_mirror_tf_rq_3', default_value='1000000', enable_events=True),sg.Text('resonance frequency 03'), sg.Combo(('0.1','1','10','100','1000','10000','100000','1000000'),  key='k_inf_c_itmx_mirror_tf_rf_3',  default_value='0.1', enable_events=True), sg.Text('pole or zero'), sg.Combo(('p','z'),  key='k_inf_c_itmx_mirror_tf_zp_3',  default_value='p', enable_events=True, readonly=True)]]
    k_inf_c_itmx_mirror_tf_re_box_4  = [[sg.Text('      quality factor value 04'), sg.Combo(('0.1','1','10','100','1000','10000','100000','1000000'),  key='k_inf_c_itmx_mirror_tf_rq_4', default_value='1000000', enable_events=True),sg.Text('resonance frequency 04'), sg.Combo(('0.1','1','10','100','1000','10000','100000','1000000'),  key='k_inf_c_itmx_mirror_tf_rf_4',  default_value='0.1', enable_events=True), sg.Text('pole or zero'), sg.Combo(('p','z'),  key='k_inf_c_itmx_mirror_tf_zp_4',  default_value='p', enable_events=True, readonly=True)]]
    k_inf_c_itmx_mirror_tf_re_box_5  = [[sg.Text('      quality factor value 05'), sg.Combo(('0.1','1','10','100','1000','10000','100000','1000000'),  key='k_inf_c_itmx_mirror_tf_rq_5', default_value='1000000', enable_events=True),sg.Text('resonance frequency 05'), sg.Combo(('0.1','1','10','100','1000','10000','100000','1000000'),  key='k_inf_c_itmx_mirror_tf_rf_5',  default_value='0.1', enable_events=True), sg.Text('pole or zero'), sg.Combo(('p','z'),  key='k_inf_c_itmx_mirror_tf_zp_5',  default_value='p', enable_events=True, readonly=True)]]
    k_inf_c_itmx_mirror_tf_re_box_6  = [[sg.Text('      quality factor value 06'), sg.Combo(('0.1','1','10','100','1000','10000','100000','1000000'),  key='k_inf_c_itmx_mirror_tf_rq_6', default_value='1000000', enable_events=True),sg.Text('resonance frequency 06'), sg.Combo(('0.1','1','10','100','1000','10000','100000','1000000'),  key='k_inf_c_itmx_mirror_tf_rf_6',  default_value='0.1', enable_events=True), sg.Text('pole or zero'), sg.Combo(('p','z'),  key='k_inf_c_itmx_mirror_tf_zp_6',  default_value='p', enable_events=True, readonly=True)]]
    k_inf_c_itmx_mirror_tf_re_box_7  = [[sg.Text('      quality factor value 07'), sg.Combo(('0.1','1','10','100','1000','10000','100000','1000000'),  key='k_inf_c_itmx_mirror_tf_rq_7', default_value='1000000', enable_events=True),sg.Text('resonance frequency 07'), sg.Combo(('0.1','1','10','100','1000','10000','100000','1000000'),  key='k_inf_c_itmx_mirror_tf_rf_7',  default_value='0.1', enable_events=True), sg.Text('pole or zero'), sg.Combo(('p','z'),  key='k_inf_c_itmx_mirror_tf_zp_7',  default_value='p', enable_events=True, readonly=True)]]
    k_inf_c_itmx_mirror_tf_re_box_8  = [[sg.Text('      quality factor value 08'), sg.Combo(('0.1','1','10','100','1000','10000','100000','1000000'),  key='k_inf_c_itmx_mirror_tf_rq_8', default_value='1000000', enable_events=True),sg.Text('resonance frequency 08'), sg.Combo(('0.1','1','10','100','1000','10000','100000','1000000'),  key='k_inf_c_itmx_mirror_tf_rf_8',  default_value='0.1', enable_events=True), sg.Text('pole or zero'), sg.Combo(('p','z'),  key='k_inf_c_itmx_mirror_tf_zp_8',  default_value='p', enable_events=True, readonly=True)]]
    k_inf_c_itmx_mirror_tf_re_box_9  = [[sg.Text('      quality factor value 09'), sg.Combo(('0.1','1','10','100','1000','10000','100000','1000000'),  key='k_inf_c_itmx_mirror_tf_rq_9', default_value='1000000', enable_events=True),sg.Text('resonance frequency 09'), sg.Combo(('0.1','1','10','100','1000','10000','100000','1000000'),  key='k_inf_c_itmx_mirror_tf_rf_9',  default_value='0.1', enable_events=True), sg.Text('pole or zero'), sg.Combo(('p','z'),  key='k_inf_c_itmx_mirror_tf_zp_9',  default_value='p', enable_events=True, readonly=True)]]
    k_inf_c_itmx_mirror_tf_re_box_10 = [[sg.Text('      quality factor value 10'), sg.Combo(('0.1','1','10','100','1000','10000','100000','1000000'), key='k_inf_c_itmx_mirror_tf_rq_10', default_value='1000000', enable_events=True),sg.Text('resonance frequency 10'), sg.Combo(('0.1','1','10','100','1000','10000','100000','1000000'),  key='k_inf_c_itmx_mirror_tf_rf_10', default_value='0.1', enable_events=True), sg.Text('pole or zero'), sg.Combo(('p','z'),  key='k_inf_c_itmx_mirror_tf_zp_10', default_value='p', enable_events=True, readonly=True)]]

    ##################################################################################################################################################################################################
    k_inf_c_itmy_mirror_tf_re_box_1  = [[sg.Text('      quality factor value 01'), sg.Combo(('0.1','1','10','100','1000','10000','100000','1000000'),  key='k_inf_c_itmy_mirror_tf_rq_1', default_value='1000000', enable_events=True),sg.Text('resonance frequency 01'), sg.Combo(('0.1','1','10','100','1000','10000','100000','1000000'),  key='k_inf_c_itmy_mirror_tf_rf_1',  default_value='0.1', enable_events=True), sg.Text('pole or zero'), sg.Combo(('p','z'),  key='k_inf_c_itmy_mirror_tf_zp_1',  default_value='p', enable_events=True, readonly=True)]]
    k_inf_c_itmy_mirror_tf_re_box_2  = [[sg.Text('      quality factor value 02'), sg.Combo(('0.1','1','10','100','1000','10000','100000','1000000'),  key='k_inf_c_itmy_mirror_tf_rq_2', default_value='1000000', enable_events=True),sg.Text('resonance frequency 02'), sg.Combo(('0.1','1','10','100','1000','10000','100000','1000000'),  key='k_inf_c_itmy_mirror_tf_rf_2',  default_value='0.1', enable_events=True), sg.Text('pole or zero'), sg.Combo(('p','z'),  key='k_inf_c_itmy_mirror_tf_zp_2',  default_value='p', enable_events=True, readonly=True)]]
    k_inf_c_itmy_mirror_tf_re_box_3  = [[sg.Text('      quality factor value 03'), sg.Combo(('0.1','1','10','100','1000','10000','100000','1000000'),  key='k_inf_c_itmy_mirror_tf_rq_3', default_value='1000000', enable_events=True),sg.Text('resonance frequency 03'), sg.Combo(('0.1','1','10','100','1000','10000','100000','1000000'),  key='k_inf_c_itmy_mirror_tf_rf_3',  default_value='0.1', enable_events=True), sg.Text('pole or zero'), sg.Combo(('p','z'),  key='k_inf_c_itmy_mirror_tf_zp_3',  default_value='p', enable_events=True, readonly=True)]]
    k_inf_c_itmy_mirror_tf_re_box_4  = [[sg.Text('      quality factor value 04'), sg.Combo(('0.1','1','10','100','1000','10000','100000','1000000'),  key='k_inf_c_itmy_mirror_tf_rq_4', default_value='1000000', enable_events=True),sg.Text('resonance frequency 04'), sg.Combo(('0.1','1','10','100','1000','10000','100000','1000000'),  key='k_inf_c_itmy_mirror_tf_rf_4',  default_value='0.1', enable_events=True), sg.Text('pole or zero'), sg.Combo(('p','z'),  key='k_inf_c_itmy_mirror_tf_zp_4',  default_value='p', enable_events=True, readonly=True)]]
    k_inf_c_itmy_mirror_tf_re_box_5  = [[sg.Text('      quality factor value 05'), sg.Combo(('0.1','1','10','100','1000','10000','100000','1000000'),  key='k_inf_c_itmy_mirror_tf_rq_5', default_value='1000000', enable_events=True),sg.Text('resonance frequency 05'), sg.Combo(('0.1','1','10','100','1000','10000','100000','1000000'),  key='k_inf_c_itmy_mirror_tf_rf_5',  default_value='0.1', enable_events=True), sg.Text('pole or zero'), sg.Combo(('p','z'),  key='k_inf_c_itmy_mirror_tf_zp_5',  default_value='p', enable_events=True, readonly=True)]]
    k_inf_c_itmy_mirror_tf_re_box_6  = [[sg.Text('      quality factor value 06'), sg.Combo(('0.1','1','10','100','1000','10000','100000','1000000'),  key='k_inf_c_itmy_mirror_tf_rq_6', default_value='1000000', enable_events=True),sg.Text('resonance frequency 06'), sg.Combo(('0.1','1','10','100','1000','10000','100000','1000000'),  key='k_inf_c_itmy_mirror_tf_rf_6',  default_value='0.1', enable_events=True), sg.Text('pole or zero'), sg.Combo(('p','z'),  key='k_inf_c_itmy_mirror_tf_zp_6',  default_value='p', enable_events=True, readonly=True)]]
    k_inf_c_itmy_mirror_tf_re_box_7  = [[sg.Text('      quality factor value 07'), sg.Combo(('0.1','1','10','100','1000','10000','100000','1000000'),  key='k_inf_c_itmy_mirror_tf_rq_7', default_value='1000000', enable_events=True),sg.Text('resonance frequency 07'), sg.Combo(('0.1','1','10','100','1000','10000','100000','1000000'),  key='k_inf_c_itmy_mirror_tf_rf_7',  default_value='0.1', enable_events=True), sg.Text('pole or zero'), sg.Combo(('p','z'),  key='k_inf_c_itmy_mirror_tf_zp_7',  default_value='p', enable_events=True, readonly=True)]]
    k_inf_c_itmy_mirror_tf_re_box_8  = [[sg.Text('      quality factor value 08'), sg.Combo(('0.1','1','10','100','1000','10000','100000','1000000'),  key='k_inf_c_itmy_mirror_tf_rq_8', default_value='1000000', enable_events=True),sg.Text('resonance frequency 08'), sg.Combo(('0.1','1','10','100','1000','10000','100000','1000000'),  key='k_inf_c_itmy_mirror_tf_rf_8',  default_value='0.1', enable_events=True), sg.Text('pole or zero'), sg.Combo(('p','z'),  key='k_inf_c_itmy_mirror_tf_zp_8',  default_value='p', enable_events=True, readonly=True)]]
    k_inf_c_itmy_mirror_tf_re_box_9  = [[sg.Text('      quality factor value 09'), sg.Combo(('0.1','1','10','100','1000','10000','100000','1000000'),  key='k_inf_c_itmy_mirror_tf_rq_9', default_value='1000000', enable_events=True),sg.Text('resonance frequency 09'), sg.Combo(('0.1','1','10','100','1000','10000','100000','1000000'),  key='k_inf_c_itmy_mirror_tf_rf_9',  default_value='0.1', enable_events=True), sg.Text('pole or zero'), sg.Combo(('p','z'),  key='k_inf_c_itmy_mirror_tf_zp_9',  default_value='p', enable_events=True, readonly=True)]]
    k_inf_c_itmy_mirror_tf_re_box_10 = [[sg.Text('      quality factor value 10'), sg.Combo(('0.1','1','10','100','1000','10000','100000','1000000'), key='k_inf_c_itmy_mirror_tf_rq_10', default_value='1000000', enable_events=True),sg.Text('resonance frequency 10'), sg.Combo(('0.1','1','10','100','1000','10000','100000','1000000'),  key='k_inf_c_itmy_mirror_tf_rf_10', default_value='0.1', enable_events=True), sg.Text('pole or zero'), sg.Combo(('p','z'),  key='k_inf_c_itmy_mirror_tf_zp_10', default_value='p', enable_events=True, readonly=True)]]

    ##################################################################################################################################################################################################
    k_inf_c_etmx_mirror_tf_re_box_1  = [[sg.Text('      quality factor value 01'), sg.Combo(('0.1','1','10','100','1000','10000','100000','1000000'),  key='k_inf_c_etmx_mirror_tf_rq_1', default_value='1000000', enable_events=True),sg.Text('resonance frequency 01'), sg.Combo(('0.1','1','10','100','1000','10000','100000','1000000'),  key='k_inf_c_etmx_mirror_tf_rf_1',  default_value='0.1', enable_events=True), sg.Text('pole or zero'), sg.Combo(('p','z'),  key='k_inf_c_etmx_mirror_tf_zp_1',  default_value='p', enable_events=True, readonly=True)]]
    k_inf_c_etmx_mirror_tf_re_box_2  = [[sg.Text('      quality factor value 02'), sg.Combo(('0.1','1','10','100','1000','10000','100000','1000000'),  key='k_inf_c_etmx_mirror_tf_rq_2', default_value='1000000', enable_events=True),sg.Text('resonance frequency 02'), sg.Combo(('0.1','1','10','100','1000','10000','100000','1000000'),  key='k_inf_c_etmx_mirror_tf_rf_2',  default_value='0.1', enable_events=True), sg.Text('pole or zero'), sg.Combo(('p','z'),  key='k_inf_c_etmx_mirror_tf_zp_2',  default_value='p', enable_events=True, readonly=True)]]
    k_inf_c_etmx_mirror_tf_re_box_3  = [[sg.Text('      quality factor value 03'), sg.Combo(('0.1','1','10','100','1000','10000','100000','1000000'),  key='k_inf_c_etmx_mirror_tf_rq_3', default_value='1000000', enable_events=True),sg.Text('resonance frequency 03'), sg.Combo(('0.1','1','10','100','1000','10000','100000','1000000'),  key='k_inf_c_etmx_mirror_tf_rf_3',  default_value='0.1', enable_events=True), sg.Text('pole or zero'), sg.Combo(('p','z'),  key='k_inf_c_etmx_mirror_tf_zp_3',  default_value='p', enable_events=True, readonly=True)]]
    k_inf_c_etmx_mirror_tf_re_box_4  = [[sg.Text('      quality factor value 04'), sg.Combo(('0.1','1','10','100','1000','10000','100000','1000000'),  key='k_inf_c_etmx_mirror_tf_rq_4', default_value='1000000', enable_events=True),sg.Text('resonance frequency 04'), sg.Combo(('0.1','1','10','100','1000','10000','100000','1000000'),  key='k_inf_c_etmx_mirror_tf_rf_4',  default_value='0.1', enable_events=True), sg.Text('pole or zero'), sg.Combo(('p','z'),  key='k_inf_c_etmx_mirror_tf_zp_4',  default_value='p', enable_events=True, readonly=True)]]
    k_inf_c_etmx_mirror_tf_re_box_5  = [[sg.Text('      quality factor value 05'), sg.Combo(('0.1','1','10','100','1000','10000','100000','1000000'),  key='k_inf_c_etmx_mirror_tf_rq_5', default_value='1000000', enable_events=True),sg.Text('resonance frequency 05'), sg.Combo(('0.1','1','10','100','1000','10000','100000','1000000'),  key='k_inf_c_etmx_mirror_tf_rf_5',  default_value='0.1', enable_events=True), sg.Text('pole or zero'), sg.Combo(('p','z'),  key='k_inf_c_etmx_mirror_tf_zp_5',  default_value='p', enable_events=True, readonly=True)]]
    k_inf_c_etmx_mirror_tf_re_box_6  = [[sg.Text('      quality factor value 06'), sg.Combo(('0.1','1','10','100','1000','10000','100000','1000000'),  key='k_inf_c_etmx_mirror_tf_rq_6', default_value='1000000', enable_events=True),sg.Text('resonance frequency 06'), sg.Combo(('0.1','1','10','100','1000','10000','100000','1000000'),  key='k_inf_c_etmx_mirror_tf_rf_6',  default_value='0.1', enable_events=True), sg.Text('pole or zero'), sg.Combo(('p','z'),  key='k_inf_c_etmx_mirror_tf_zp_6',  default_value='p', enable_events=True, readonly=True)]]
    k_inf_c_etmx_mirror_tf_re_box_7  = [[sg.Text('      quality factor value 07'), sg.Combo(('0.1','1','10','100','1000','10000','100000','1000000'),  key='k_inf_c_etmx_mirror_tf_rq_7', default_value='1000000', enable_events=True),sg.Text('resonance frequency 07'), sg.Combo(('0.1','1','10','100','1000','10000','100000','1000000'),  key='k_inf_c_etmx_mirror_tf_rf_7',  default_value='0.1', enable_events=True), sg.Text('pole or zero'), sg.Combo(('p','z'),  key='k_inf_c_etmx_mirror_tf_zp_7',  default_value='p', enable_events=True, readonly=True)]]
    k_inf_c_etmx_mirror_tf_re_box_8  = [[sg.Text('      quality factor value 08'), sg.Combo(('0.1','1','10','100','1000','10000','100000','1000000'),  key='k_inf_c_etmx_mirror_tf_rq_8', default_value='1000000', enable_events=True),sg.Text('resonance frequency 08'), sg.Combo(('0.1','1','10','100','1000','10000','100000','1000000'),  key='k_inf_c_etmx_mirror_tf_rf_8',  default_value='0.1', enable_events=True), sg.Text('pole or zero'), sg.Combo(('p','z'),  key='k_inf_c_etmx_mirror_tf_zp_8',  default_value='p', enable_events=True, readonly=True)]]
    k_inf_c_etmx_mirror_tf_re_box_9  = [[sg.Text('      quality factor value 09'), sg.Combo(('0.1','1','10','100','1000','10000','100000','1000000'),  key='k_inf_c_etmx_mirror_tf_rq_9', default_value='1000000', enable_events=True),sg.Text('resonance frequency 09'), sg.Combo(('0.1','1','10','100','1000','10000','100000','1000000'),  key='k_inf_c_etmx_mirror_tf_rf_9',  default_value='0.1', enable_events=True), sg.Text('pole or zero'), sg.Combo(('p','z'),  key='k_inf_c_etmx_mirror_tf_zp_9',  default_value='p', enable_events=True, readonly=True)]]
    k_inf_c_etmx_mirror_tf_re_box_10 = [[sg.Text('      quality factor value 10'), sg.Combo(('0.1','1','10','100','1000','10000','100000','1000000'), key='k_inf_c_etmx_mirror_tf_rq_10', default_value='1000000', enable_events=True),sg.Text('resonance frequency 10'), sg.Combo(('0.1','1','10','100','1000','10000','100000','1000000'),  key='k_inf_c_etmx_mirror_tf_rf_10', default_value='0.1', enable_events=True), sg.Text('pole or zero'), sg.Combo(('p','z'),  key='k_inf_c_etmx_mirror_tf_zp_10', default_value='p', enable_events=True, readonly=True)]]

    ##################################################################################################################################################################################################
    k_inf_c_etmy_mirror_tf_re_box_1  = [[sg.Text('      quality factor value 01'), sg.Combo(('0.1','1','10','100','1000','10000','100000','1000000'),  key='k_inf_c_etmy_mirror_tf_rq_1', default_value='1000000', enable_events=True),sg.Text('resonance frequency 01'), sg.Combo(('0.1','1','10','100','1000','10000','100000','1000000'),  key='k_inf_c_etmy_mirror_tf_rf_1',  default_value='0.1', enable_events=True), sg.Text('pole or zero'), sg.Combo(('p','z'),  key='k_inf_c_etmy_mirror_tf_zp_1',  default_value='p', enable_events=True, readonly=True)]]
    k_inf_c_etmy_mirror_tf_re_box_2  = [[sg.Text('      quality factor value 02'), sg.Combo(('0.1','1','10','100','1000','10000','100000','1000000'),  key='k_inf_c_etmy_mirror_tf_rq_2', default_value='1000000', enable_events=True),sg.Text('resonance frequency 02'), sg.Combo(('0.1','1','10','100','1000','10000','100000','1000000'),  key='k_inf_c_etmy_mirror_tf_rf_2',  default_value='0.1', enable_events=True), sg.Text('pole or zero'), sg.Combo(('p','z'),  key='k_inf_c_etmy_mirror_tf_zp_2',  default_value='p', enable_events=True, readonly=True)]]
    k_inf_c_etmy_mirror_tf_re_box_3  = [[sg.Text('      quality factor value 03'), sg.Combo(('0.1','1','10','100','1000','10000','100000','1000000'),  key='k_inf_c_etmy_mirror_tf_rq_3', default_value='1000000', enable_events=True),sg.Text('resonance frequency 03'), sg.Combo(('0.1','1','10','100','1000','10000','100000','1000000'),  key='k_inf_c_etmy_mirror_tf_rf_3',  default_value='0.1', enable_events=True), sg.Text('pole or zero'), sg.Combo(('p','z'),  key='k_inf_c_etmy_mirror_tf_zp_3',  default_value='p', enable_events=True, readonly=True)]]
    k_inf_c_etmy_mirror_tf_re_box_4  = [[sg.Text('      quality factor value 04'), sg.Combo(('0.1','1','10','100','1000','10000','100000','1000000'),  key='k_inf_c_etmy_mirror_tf_rq_4', default_value='1000000', enable_events=True),sg.Text('resonance frequency 04'), sg.Combo(('0.1','1','10','100','1000','10000','100000','1000000'),  key='k_inf_c_etmy_mirror_tf_rf_4',  default_value='0.1', enable_events=True), sg.Text('pole or zero'), sg.Combo(('p','z'),  key='k_inf_c_etmy_mirror_tf_zp_4',  default_value='p', enable_events=True, readonly=True)]]
    k_inf_c_etmy_mirror_tf_re_box_5  = [[sg.Text('      quality factor value 05'), sg.Combo(('0.1','1','10','100','1000','10000','100000','1000000'),  key='k_inf_c_etmy_mirror_tf_rq_5', default_value='1000000', enable_events=True),sg.Text('resonance frequency 05'), sg.Combo(('0.1','1','10','100','1000','10000','100000','1000000'),  key='k_inf_c_etmy_mirror_tf_rf_5',  default_value='0.1', enable_events=True), sg.Text('pole or zero'), sg.Combo(('p','z'),  key='k_inf_c_etmy_mirror_tf_zp_5',  default_value='p', enable_events=True, readonly=True)]]
    k_inf_c_etmy_mirror_tf_re_box_6  = [[sg.Text('      quality factor value 06'), sg.Combo(('0.1','1','10','100','1000','10000','100000','1000000'),  key='k_inf_c_etmy_mirror_tf_rq_6', default_value='1000000', enable_events=True),sg.Text('resonance frequency 06'), sg.Combo(('0.1','1','10','100','1000','10000','100000','1000000'),  key='k_inf_c_etmy_mirror_tf_rf_6',  default_value='0.1', enable_events=True), sg.Text('pole or zero'), sg.Combo(('p','z'),  key='k_inf_c_etmy_mirror_tf_zp_6',  default_value='p', enable_events=True, readonly=True)]]
    k_inf_c_etmy_mirror_tf_re_box_7  = [[sg.Text('      quality factor value 07'), sg.Combo(('0.1','1','10','100','1000','10000','100000','1000000'),  key='k_inf_c_etmy_mirror_tf_rq_7', default_value='1000000', enable_events=True),sg.Text('resonance frequency 07'), sg.Combo(('0.1','1','10','100','1000','10000','100000','1000000'),  key='k_inf_c_etmy_mirror_tf_rf_7',  default_value='0.1', enable_events=True), sg.Text('pole or zero'), sg.Combo(('p','z'),  key='k_inf_c_etmy_mirror_tf_zp_7',  default_value='p', enable_events=True, readonly=True)]]
    k_inf_c_etmy_mirror_tf_re_box_8  = [[sg.Text('      quality factor value 08'), sg.Combo(('0.1','1','10','100','1000','10000','100000','1000000'),  key='k_inf_c_etmy_mirror_tf_rq_8', default_value='1000000', enable_events=True),sg.Text('resonance frequency 08'), sg.Combo(('0.1','1','10','100','1000','10000','100000','1000000'),  key='k_inf_c_etmy_mirror_tf_rf_8',  default_value='0.1', enable_events=True), sg.Text('pole or zero'), sg.Combo(('p','z'),  key='k_inf_c_etmy_mirror_tf_zp_8',  default_value='p', enable_events=True, readonly=True)]]
    k_inf_c_etmy_mirror_tf_re_box_9  = [[sg.Text('      quality factor value 09'), sg.Combo(('0.1','1','10','100','1000','10000','100000','1000000'),  key='k_inf_c_etmy_mirror_tf_rq_9', default_value='1000000', enable_events=True),sg.Text('resonance frequency 09'), sg.Combo(('0.1','1','10','100','1000','10000','100000','1000000'),  key='k_inf_c_etmy_mirror_tf_rf_9',  default_value='0.1', enable_events=True), sg.Text('pole or zero'), sg.Combo(('p','z'),  key='k_inf_c_etmy_mirror_tf_zp_9',  default_value='p', enable_events=True, readonly=True)]]
    k_inf_c_etmy_mirror_tf_re_box_10 = [[sg.Text('      quality factor value 10'), sg.Combo(('0.1','1','10','100','1000','10000','100000','1000000'), key='k_inf_c_etmy_mirror_tf_rq_10', default_value='1000000', enable_events=True),sg.Text('resonance frequency 10'), sg.Combo(('0.1','1','10','100','1000','10000','100000','1000000'),  key='k_inf_c_etmy_mirror_tf_rf_10', default_value='0.1', enable_events=True), sg.Text('pole or zero'), sg.Combo(('p','z'),  key='k_inf_c_etmy_mirror_tf_zp_10', default_value='p', enable_events=True, readonly=True)]]

    ##################################################################################################################################################################################################
    k_inf_c_prm_mirror_tf_re_box_1  = [[sg.Text('      quality factor value 01'), sg.Combo(('0.1','1','10','100','1000','10000','100000','1000000'),  key='k_inf_c_prm_mirror_tf_rq_1', default_value='1000000', enable_events=True)  ,sg.Text('resonance frequency 01'), sg.Combo(('0.1','1','10','100','1000','10000','100000','1000000'),  key='k_inf_c_prm_mirror_tf_rf_1',  default_value='0.1', enable_events=True), sg.Text('pole or zero'), sg.Combo(('p','z'),  key='k_inf_c_prm_mirror_tf_zp_1',  default_value='p', enable_events=True, readonly=True)]]
    k_inf_c_prm_mirror_tf_re_box_2  = [[sg.Text('      quality factor value 02'), sg.Combo(('0.1','1','10','100','1000','10000','100000','1000000'),  key='k_inf_c_prm_mirror_tf_rq_2', default_value='1000000', enable_events=True)  ,sg.Text('resonance frequency 02'), sg.Combo(('0.1','1','10','100','1000','10000','100000','1000000'),  key='k_inf_c_prm_mirror_tf_rf_2',  default_value='0.1', enable_events=True), sg.Text('pole or zero'), sg.Combo(('p','z'),  key='k_inf_c_prm_mirror_tf_zp_2',  default_value='p', enable_events=True, readonly=True)]]
    k_inf_c_prm_mirror_tf_re_box_3  = [[sg.Text('      quality factor value 03'), sg.Combo(('0.1','1','10','100','1000','10000','100000','1000000'),  key='k_inf_c_prm_mirror_tf_rq_3', default_value='1000000', enable_events=True)  ,sg.Text('resonance frequency 03'), sg.Combo(('0.1','1','10','100','1000','10000','100000','1000000'),  key='k_inf_c_prm_mirror_tf_rf_3',  default_value='0.1', enable_events=True), sg.Text('pole or zero'), sg.Combo(('p','z'),  key='k_inf_c_prm_mirror_tf_zp_3',  default_value='p', enable_events=True, readonly=True)]]
    k_inf_c_prm_mirror_tf_re_box_4  = [[sg.Text('      quality factor value 04'), sg.Combo(('0.1','1','10','100','1000','10000','100000','1000000'),  key='k_inf_c_prm_mirror_tf_rq_4', default_value='1000000', enable_events=True)  ,sg.Text('resonance frequency 04'), sg.Combo(('0.1','1','10','100','1000','10000','100000','1000000'),  key='k_inf_c_prm_mirror_tf_rf_4',  default_value='0.1', enable_events=True), sg.Text('pole or zero'), sg.Combo(('p','z'),  key='k_inf_c_prm_mirror_tf_zp_4',  default_value='p', enable_events=True, readonly=True)]]
    k_inf_c_prm_mirror_tf_re_box_5  = [[sg.Text('      quality factor value 05'), sg.Combo(('0.1','1','10','100','1000','10000','100000','1000000'),  key='k_inf_c_prm_mirror_tf_rq_5', default_value='1000000', enable_events=True)  ,sg.Text('resonance frequency 05'), sg.Combo(('0.1','1','10','100','1000','10000','100000','1000000'),  key='k_inf_c_prm_mirror_tf_rf_5',  default_value='0.1', enable_events=True), sg.Text('pole or zero'), sg.Combo(('p','z'),  key='k_inf_c_prm_mirror_tf_zp_5',  default_value='p', enable_events=True, readonly=True)]]
    k_inf_c_prm_mirror_tf_re_box_6  = [[sg.Text('      quality factor value 06'), sg.Combo(('0.1','1','10','100','1000','10000','100000','1000000'),  key='k_inf_c_prm_mirror_tf_rq_6', default_value='1000000', enable_events=True)  ,sg.Text('resonance frequency 06'), sg.Combo(('0.1','1','10','100','1000','10000','100000','1000000'),  key='k_inf_c_prm_mirror_tf_rf_6',  default_value='0.1', enable_events=True), sg.Text('pole or zero'), sg.Combo(('p','z'),  key='k_inf_c_prm_mirror_tf_zp_6',  default_value='p', enable_events=True, readonly=True)]]
    k_inf_c_prm_mirror_tf_re_box_7  = [[sg.Text('      quality factor value 07'), sg.Combo(('0.1','1','10','100','1000','10000','100000','1000000'),  key='k_inf_c_prm_mirror_tf_rq_7', default_value='1000000', enable_events=True)  ,sg.Text('resonance frequency 07'), sg.Combo(('0.1','1','10','100','1000','10000','100000','1000000'),  key='k_inf_c_prm_mirror_tf_rf_7',  default_value='0.1', enable_events=True), sg.Text('pole or zero'), sg.Combo(('p','z'),  key='k_inf_c_prm_mirror_tf_zp_7',  default_value='p', enable_events=True, readonly=True)]]
    k_inf_c_prm_mirror_tf_re_box_8  = [[sg.Text('      quality factor value 08'), sg.Combo(('0.1','1','10','100','1000','10000','100000','1000000'),  key='k_inf_c_prm_mirror_tf_rq_8', default_value='1000000', enable_events=True)  ,sg.Text('resonance frequency 08'), sg.Combo(('0.1','1','10','100','1000','10000','100000','1000000'),  key='k_inf_c_prm_mirror_tf_rf_8',  default_value='0.1', enable_events=True), sg.Text('pole or zero'), sg.Combo(('p','z'),  key='k_inf_c_prm_mirror_tf_zp_8',  default_value='p', enable_events=True, readonly=True)]]
    k_inf_c_prm_mirror_tf_re_box_9  = [[sg.Text('      quality factor value 09'), sg.Combo(('0.1','1','10','100','1000','10000','100000','1000000'),  key='k_inf_c_prm_mirror_tf_rq_9', default_value='1000000', enable_events=True)  ,sg.Text('resonance frequency 09'), sg.Combo(('0.1','1','10','100','1000','10000','100000','1000000'),  key='k_inf_c_prm_mirror_tf_rf_9',  default_value='0.1', enable_events=True), sg.Text('pole or zero'), sg.Combo(('p','z'),  key='k_inf_c_prm_mirror_tf_zp_9',  default_value='p', enable_events=True, readonly=True)]]
    k_inf_c_prm_mirror_tf_re_box_10 = [[sg.Text('      quality factor value 10'), sg.Combo(('0.1','1','10','100','1000','10000','100000','1000000'), key='k_inf_c_prm_mirror_tf_rq_10', default_value='1000000', enable_events=True)  ,sg.Text('resonance frequency 10'), sg.Combo(('0.1','1','10','100','1000','10000','100000','1000000'),  key='k_inf_c_prm_mirror_tf_rf_10', default_value='0.1', enable_events=True), sg.Text('pole or zero'), sg.Combo(('p','z'),  key='k_inf_c_prm_mirror_tf_zp_10', default_value='p', enable_events=True, readonly=True)]]

    ##################################################################################################################################################################################################
    k_inf_c_pr2_mirror_tf_re_box_1  = [[sg.Text('      quality factor value 01'), sg.Combo(('0.1','1','10','100','1000','10000','100000','1000000'),  key='k_inf_c_pr2_mirror_tf_rq_1', default_value='1000000', enable_events=True)  ,sg.Text('resonance frequency 01'), sg.Combo(('0.1','1','10','100','1000','10000','100000','1000000'),  key='k_inf_c_pr2_mirror_tf_rf_1',  default_value='0.1', enable_events=True), sg.Text('pole or zero'), sg.Combo(('p','z'),  key='k_inf_c_pr2_mirror_tf_zp_1',  default_value='p', enable_events=True, readonly=True)]]
    k_inf_c_pr2_mirror_tf_re_box_2  = [[sg.Text('      quality factor value 02'), sg.Combo(('0.1','1','10','100','1000','10000','100000','1000000'),  key='k_inf_c_pr2_mirror_tf_rq_2', default_value='1000000', enable_events=True)  ,sg.Text('resonance frequency 02'), sg.Combo(('0.1','1','10','100','1000','10000','100000','1000000'),  key='k_inf_c_pr2_mirror_tf_rf_2',  default_value='0.1', enable_events=True), sg.Text('pole or zero'), sg.Combo(('p','z'),  key='k_inf_c_pr2_mirror_tf_zp_2',  default_value='p', enable_events=True, readonly=True)]]
    k_inf_c_pr2_mirror_tf_re_box_3  = [[sg.Text('      quality factor value 03'), sg.Combo(('0.1','1','10','100','1000','10000','100000','1000000'),  key='k_inf_c_pr2_mirror_tf_rq_3', default_value='1000000', enable_events=True)  ,sg.Text('resonance frequency 03'), sg.Combo(('0.1','1','10','100','1000','10000','100000','1000000'),  key='k_inf_c_pr2_mirror_tf_rf_3',  default_value='0.1', enable_events=True), sg.Text('pole or zero'), sg.Combo(('p','z'),  key='k_inf_c_pr2_mirror_tf_zp_3',  default_value='p', enable_events=True, readonly=True)]]
    k_inf_c_pr2_mirror_tf_re_box_4  = [[sg.Text('      quality factor value 04'), sg.Combo(('0.1','1','10','100','1000','10000','100000','1000000'),  key='k_inf_c_pr2_mirror_tf_rq_4', default_value='1000000', enable_events=True)  ,sg.Text('resonance frequency 04'), sg.Combo(('0.1','1','10','100','1000','10000','100000','1000000'),  key='k_inf_c_pr2_mirror_tf_rf_4',  default_value='0.1', enable_events=True), sg.Text('pole or zero'), sg.Combo(('p','z'),  key='k_inf_c_pr2_mirror_tf_zp_4',  default_value='p', enable_events=True, readonly=True)]]
    k_inf_c_pr2_mirror_tf_re_box_5  = [[sg.Text('      quality factor value 05'), sg.Combo(('0.1','1','10','100','1000','10000','100000','1000000'),  key='k_inf_c_pr2_mirror_tf_rq_5', default_value='1000000', enable_events=True)  ,sg.Text('resonance frequency 05'), sg.Combo(('0.1','1','10','100','1000','10000','100000','1000000'),  key='k_inf_c_pr2_mirror_tf_rf_5',  default_value='0.1', enable_events=True), sg.Text('pole or zero'), sg.Combo(('p','z'),  key='k_inf_c_pr2_mirror_tf_zp_5',  default_value='p', enable_events=True, readonly=True)]]
    k_inf_c_pr2_mirror_tf_re_box_6  = [[sg.Text('      quality factor value 06'), sg.Combo(('0.1','1','10','100','1000','10000','100000','1000000'),  key='k_inf_c_pr2_mirror_tf_rq_6', default_value='1000000', enable_events=True)  ,sg.Text('resonance frequency 06'), sg.Combo(('0.1','1','10','100','1000','10000','100000','1000000'),  key='k_inf_c_pr2_mirror_tf_rf_6',  default_value='0.1', enable_events=True), sg.Text('pole or zero'), sg.Combo(('p','z'),  key='k_inf_c_pr2_mirror_tf_zp_6',  default_value='p', enable_events=True, readonly=True)]]
    k_inf_c_pr2_mirror_tf_re_box_7  = [[sg.Text('      quality factor value 07'), sg.Combo(('0.1','1','10','100','1000','10000','100000','1000000'),  key='k_inf_c_pr2_mirror_tf_rq_7', default_value='1000000', enable_events=True)  ,sg.Text('resonance frequency 07'), sg.Combo(('0.1','1','10','100','1000','10000','100000','1000000'),  key='k_inf_c_pr2_mirror_tf_rf_7',  default_value='0.1', enable_events=True), sg.Text('pole or zero'), sg.Combo(('p','z'),  key='k_inf_c_pr2_mirror_tf_zp_7',  default_value='p', enable_events=True, readonly=True)]]
    k_inf_c_pr2_mirror_tf_re_box_8  = [[sg.Text('      quality factor value 08'), sg.Combo(('0.1','1','10','100','1000','10000','100000','1000000'),  key='k_inf_c_pr2_mirror_tf_rq_8', default_value='1000000', enable_events=True)  ,sg.Text('resonance frequency 08'), sg.Combo(('0.1','1','10','100','1000','10000','100000','1000000'),  key='k_inf_c_pr2_mirror_tf_rf_8',  default_value='0.1', enable_events=True), sg.Text('pole or zero'), sg.Combo(('p','z'),  key='k_inf_c_pr2_mirror_tf_zp_8',  default_value='p', enable_events=True, readonly=True)]]
    k_inf_c_pr2_mirror_tf_re_box_9  = [[sg.Text('      quality factor value 09'), sg.Combo(('0.1','1','10','100','1000','10000','100000','1000000'),  key='k_inf_c_pr2_mirror_tf_rq_9', default_value='1000000', enable_events=True)  ,sg.Text('resonance frequency 09'), sg.Combo(('0.1','1','10','100','1000','10000','100000','1000000'),  key='k_inf_c_pr2_mirror_tf_rf_9',  default_value='0.1', enable_events=True), sg.Text('pole or zero'), sg.Combo(('p','z'),  key='k_inf_c_pr2_mirror_tf_zp_9',  default_value='p', enable_events=True, readonly=True)]]
    k_inf_c_pr2_mirror_tf_re_box_10 = [[sg.Text('      quality factor value 10'), sg.Combo(('0.1','1','10','100','1000','10000','100000','1000000'), key='k_inf_c_pr2_mirror_tf_rq_10', default_value='1000000', enable_events=True)  ,sg.Text('resonance frequency 10'), sg.Combo(('0.1','1','10','100','1000','10000','100000','1000000'),  key='k_inf_c_pr2_mirror_tf_rf_10', default_value='0.1', enable_events=True), sg.Text('pole or zero'), sg.Combo(('p','z'),  key='k_inf_c_pr2_mirror_tf_zp_10', default_value='p', enable_events=True, readonly=True)]]

    ##################################################################################################################################################################################################
    k_inf_c_pr3_mirror_tf_re_box_1  = [[sg.Text('      quality factor value 01'), sg.Combo(('0.1','1','10','100','1000','10000','100000','1000000'),  key='k_inf_c_pr3_mirror_tf_rq_1', default_value='1000000', enable_events=True)  ,sg.Text('resonance frequency 01'), sg.Combo(('0.1','1','10','100','1000','10000','100000','1000000'),  key='k_inf_c_pr3_mirror_tf_rf_1',  default_value='0.1', enable_events=True), sg.Text('pole or zero'), sg.Combo(('p','z'),  key='k_inf_c_pr3_mirror_tf_zp_1',  default_value='p', enable_events=True, readonly=True)]]
    k_inf_c_pr3_mirror_tf_re_box_2  = [[sg.Text('      quality factor value 02'), sg.Combo(('0.1','1','10','100','1000','10000','100000','1000000'),  key='k_inf_c_pr3_mirror_tf_rq_2', default_value='1000000', enable_events=True)  ,sg.Text('resonance frequency 02'), sg.Combo(('0.1','1','10','100','1000','10000','100000','1000000'),  key='k_inf_c_pr3_mirror_tf_rf_2',  default_value='0.1', enable_events=True), sg.Text('pole or zero'), sg.Combo(('p','z'),  key='k_inf_c_pr3_mirror_tf_zp_2',  default_value='p', enable_events=True, readonly=True)]]
    k_inf_c_pr3_mirror_tf_re_box_3  = [[sg.Text('      quality factor value 03'), sg.Combo(('0.1','1','10','100','1000','10000','100000','1000000'),  key='k_inf_c_pr3_mirror_tf_rq_3', default_value='1000000', enable_events=True)  ,sg.Text('resonance frequency 03'), sg.Combo(('0.1','1','10','100','1000','10000','100000','1000000'),  key='k_inf_c_pr3_mirror_tf_rf_3',  default_value='0.1', enable_events=True), sg.Text('pole or zero'), sg.Combo(('p','z'),  key='k_inf_c_pr3_mirror_tf_zp_3',  default_value='p', enable_events=True, readonly=True)]]
    k_inf_c_pr3_mirror_tf_re_box_4  = [[sg.Text('      quality factor value 04'), sg.Combo(('0.1','1','10','100','1000','10000','100000','1000000'),  key='k_inf_c_pr3_mirror_tf_rq_4', default_value='1000000', enable_events=True)  ,sg.Text('resonance frequency 04'), sg.Combo(('0.1','1','10','100','1000','10000','100000','1000000'),  key='k_inf_c_pr3_mirror_tf_rf_4',  default_value='0.1', enable_events=True), sg.Text('pole or zero'), sg.Combo(('p','z'),  key='k_inf_c_pr3_mirror_tf_zp_4',  default_value='p', enable_events=True, readonly=True)]]
    k_inf_c_pr3_mirror_tf_re_box_5  = [[sg.Text('      quality factor value 05'), sg.Combo(('0.1','1','10','100','1000','10000','100000','1000000'),  key='k_inf_c_pr3_mirror_tf_rq_5', default_value='1000000', enable_events=True)  ,sg.Text('resonance frequency 05'), sg.Combo(('0.1','1','10','100','1000','10000','100000','1000000'),  key='k_inf_c_pr3_mirror_tf_rf_5',  default_value='0.1', enable_events=True), sg.Text('pole or zero'), sg.Combo(('p','z'),  key='k_inf_c_pr3_mirror_tf_zp_5',  default_value='p', enable_events=True, readonly=True)]]
    k_inf_c_pr3_mirror_tf_re_box_6  = [[sg.Text('      quality factor value 06'), sg.Combo(('0.1','1','10','100','1000','10000','100000','1000000'),  key='k_inf_c_pr3_mirror_tf_rq_6', default_value='1000000', enable_events=True)  ,sg.Text('resonance frequency 06'), sg.Combo(('0.1','1','10','100','1000','10000','100000','1000000'),  key='k_inf_c_pr3_mirror_tf_rf_6',  default_value='0.1', enable_events=True), sg.Text('pole or zero'), sg.Combo(('p','z'),  key='k_inf_c_pr3_mirror_tf_zp_6',  default_value='p', enable_events=True, readonly=True)]]
    k_inf_c_pr3_mirror_tf_re_box_7  = [[sg.Text('      quality factor value 07'), sg.Combo(('0.1','1','10','100','1000','10000','100000','1000000'),  key='k_inf_c_pr3_mirror_tf_rq_7', default_value='1000000', enable_events=True)  ,sg.Text('resonance frequency 07'), sg.Combo(('0.1','1','10','100','1000','10000','100000','1000000'),  key='k_inf_c_pr3_mirror_tf_rf_7',  default_value='0.1', enable_events=True), sg.Text('pole or zero'), sg.Combo(('p','z'),  key='k_inf_c_pr3_mirror_tf_zp_7',  default_value='p', enable_events=True, readonly=True)]]
    k_inf_c_pr3_mirror_tf_re_box_8  = [[sg.Text('      quality factor value 08'), sg.Combo(('0.1','1','10','100','1000','10000','100000','1000000'),  key='k_inf_c_pr3_mirror_tf_rq_8', default_value='1000000', enable_events=True)  ,sg.Text('resonance frequency 08'), sg.Combo(('0.1','1','10','100','1000','10000','100000','1000000'),  key='k_inf_c_pr3_mirror_tf_rf_8',  default_value='0.1', enable_events=True), sg.Text('pole or zero'), sg.Combo(('p','z'),  key='k_inf_c_pr3_mirror_tf_zp_8',  default_value='p', enable_events=True, readonly=True)]]
    k_inf_c_pr3_mirror_tf_re_box_9  = [[sg.Text('      quality factor value 09'), sg.Combo(('0.1','1','10','100','1000','10000','100000','1000000'),  key='k_inf_c_pr3_mirror_tf_rq_9', default_value='1000000', enable_events=True)  ,sg.Text('resonance frequency 09'), sg.Combo(('0.1','1','10','100','1000','10000','100000','1000000'),  key='k_inf_c_pr3_mirror_tf_rf_9',  default_value='0.1', enable_events=True), sg.Text('pole or zero'), sg.Combo(('p','z'),  key='k_inf_c_pr3_mirror_tf_zp_9',  default_value='p', enable_events=True, readonly=True)]]
    k_inf_c_pr3_mirror_tf_re_box_10 = [[sg.Text('      quality factor value 10'), sg.Combo(('0.1','1','10','100','1000','10000','100000','1000000'), key='k_inf_c_pr3_mirror_tf_rq_10', default_value='1000000', enable_events=True)  ,sg.Text('resonance frequency 10'), sg.Combo(('0.1','1','10','100','1000','10000','100000','1000000'),  key='k_inf_c_pr3_mirror_tf_rf_10', default_value='0.1', enable_events=True), sg.Text('pole or zero'), sg.Combo(('p','z'),  key='k_inf_c_pr3_mirror_tf_zp_10', default_value='p', enable_events=True, readonly=True)]]

    ##################################################################################################################################################################################################
    k_inf_c_srm_mirror_tf_re_box_1  = [[sg.Text('      quality factor value 01'), sg.Combo(('0.1','1','10','100','1000','10000','100000','1000000'),  key='k_inf_c_srm_mirror_tf_rq_1', default_value='1000000', enable_events=True)  ,sg.Text('resonance frequency 01'), sg.Combo(('0.1','1','10','100','1000','10000','100000','1000000'),  key='k_inf_c_srm_mirror_tf_rf_1',  default_value='0.1', enable_events=True), sg.Text('pole or zero'), sg.Combo(('p','z'),  key='k_inf_c_srm_mirror_tf_zp_1',  default_value='p', enable_events=True, readonly=True)]]
    k_inf_c_srm_mirror_tf_re_box_2  = [[sg.Text('      quality factor value 02'), sg.Combo(('0.1','1','10','100','1000','10000','100000','1000000'),  key='k_inf_c_srm_mirror_tf_rq_2', default_value='1000000', enable_events=True)  ,sg.Text('resonance frequency 02'), sg.Combo(('0.1','1','10','100','1000','10000','100000','1000000'),  key='k_inf_c_srm_mirror_tf_rf_2',  default_value='0.1', enable_events=True), sg.Text('pole or zero'), sg.Combo(('p','z'),  key='k_inf_c_srm_mirror_tf_zp_2',  default_value='p', enable_events=True, readonly=True)]]
    k_inf_c_srm_mirror_tf_re_box_3  = [[sg.Text('      quality factor value 03'), sg.Combo(('0.1','1','10','100','1000','10000','100000','1000000'),  key='k_inf_c_srm_mirror_tf_rq_3', default_value='1000000', enable_events=True)  ,sg.Text('resonance frequency 03'), sg.Combo(('0.1','1','10','100','1000','10000','100000','1000000'),  key='k_inf_c_srm_mirror_tf_rf_3',  default_value='0.1', enable_events=True), sg.Text('pole or zero'), sg.Combo(('p','z'),  key='k_inf_c_srm_mirror_tf_zp_3',  default_value='p', enable_events=True, readonly=True)]]
    k_inf_c_srm_mirror_tf_re_box_4  = [[sg.Text('      quality factor value 04'), sg.Combo(('0.1','1','10','100','1000','10000','100000','1000000'),  key='k_inf_c_srm_mirror_tf_rq_4', default_value='1000000', enable_events=True)  ,sg.Text('resonance frequency 04'), sg.Combo(('0.1','1','10','100','1000','10000','100000','1000000'),  key='k_inf_c_srm_mirror_tf_rf_4',  default_value='0.1', enable_events=True), sg.Text('pole or zero'), sg.Combo(('p','z'),  key='k_inf_c_srm_mirror_tf_zp_4',  default_value='p', enable_events=True, readonly=True)]]
    k_inf_c_srm_mirror_tf_re_box_5  = [[sg.Text('      quality factor value 05'), sg.Combo(('0.1','1','10','100','1000','10000','100000','1000000'),  key='k_inf_c_srm_mirror_tf_rq_5', default_value='1000000', enable_events=True)  ,sg.Text('resonance frequency 05'), sg.Combo(('0.1','1','10','100','1000','10000','100000','1000000'),  key='k_inf_c_srm_mirror_tf_rf_5',  default_value='0.1', enable_events=True), sg.Text('pole or zero'), sg.Combo(('p','z'),  key='k_inf_c_srm_mirror_tf_zp_5',  default_value='p', enable_events=True, readonly=True)]]
    k_inf_c_srm_mirror_tf_re_box_6  = [[sg.Text('      quality factor value 06'), sg.Combo(('0.1','1','10','100','1000','10000','100000','1000000'),  key='k_inf_c_srm_mirror_tf_rq_6', default_value='1000000', enable_events=True)  ,sg.Text('resonance frequency 06'), sg.Combo(('0.1','1','10','100','1000','10000','100000','1000000'),  key='k_inf_c_srm_mirror_tf_rf_6',  default_value='0.1', enable_events=True), sg.Text('pole or zero'), sg.Combo(('p','z'),  key='k_inf_c_srm_mirror_tf_zp_6',  default_value='p', enable_events=True, readonly=True)]]
    k_inf_c_srm_mirror_tf_re_box_7  = [[sg.Text('      quality factor value 07'), sg.Combo(('0.1','1','10','100','1000','10000','100000','1000000'),  key='k_inf_c_srm_mirror_tf_rq_7', default_value='1000000', enable_events=True)  ,sg.Text('resonance frequency 07'), sg.Combo(('0.1','1','10','100','1000','10000','100000','1000000'),  key='k_inf_c_srm_mirror_tf_rf_7',  default_value='0.1', enable_events=True), sg.Text('pole or zero'), sg.Combo(('p','z'),  key='k_inf_c_srm_mirror_tf_zp_7',  default_value='p', enable_events=True, readonly=True)]]
    k_inf_c_srm_mirror_tf_re_box_8  = [[sg.Text('      quality factor value 08'), sg.Combo(('0.1','1','10','100','1000','10000','100000','1000000'),  key='k_inf_c_srm_mirror_tf_rq_8', default_value='1000000', enable_events=True)  ,sg.Text('resonance frequency 08'), sg.Combo(('0.1','1','10','100','1000','10000','100000','1000000'),  key='k_inf_c_srm_mirror_tf_rf_8',  default_value='0.1', enable_events=True), sg.Text('pole or zero'), sg.Combo(('p','z'),  key='k_inf_c_srm_mirror_tf_zp_8',  default_value='p', enable_events=True, readonly=True)]]
    k_inf_c_srm_mirror_tf_re_box_9  = [[sg.Text('      quality factor value 09'), sg.Combo(('0.1','1','10','100','1000','10000','100000','1000000'),  key='k_inf_c_srm_mirror_tf_rq_9', default_value='1000000', enable_events=True)  ,sg.Text('resonance frequency 09'), sg.Combo(('0.1','1','10','100','1000','10000','100000','1000000'),  key='k_inf_c_srm_mirror_tf_rf_9',  default_value='0.1', enable_events=True), sg.Text('pole or zero'), sg.Combo(('p','z'),  key='k_inf_c_srm_mirror_tf_zp_9',  default_value='p', enable_events=True, readonly=True)]]
    k_inf_c_srm_mirror_tf_re_box_10 = [[sg.Text('      quality factor value 10'), sg.Combo(('0.1','1','10','100','1000','10000','100000','1000000'), key='k_inf_c_srm_mirror_tf_rq_10', default_value='1000000', enable_events=True)  ,sg.Text('resonance frequency 10'), sg.Combo(('0.1','1','10','100','1000','10000','100000','1000000'),  key='k_inf_c_srm_mirror_tf_rf_10', default_value='0.1', enable_events=True), sg.Text('pole or zero'), sg.Combo(('p','z'),  key='k_inf_c_srm_mirror_tf_zp_10', default_value='p', enable_events=True, readonly=True)]]

    ##################################################################################################################################################################################################
    k_inf_c_sr2_mirror_tf_re_box_1  = [[sg.Text('      quality factor value 01'), sg.Combo(('0.1','1','10','100','1000','10000','100000','1000000'),  key='k_inf_c_sr2_mirror_tf_rq_1', default_value='1000000', enable_events=True)  ,sg.Text('resonance frequency 01'), sg.Combo(('0.1','1','10','100','1000','10000','100000','1000000'),  key='k_inf_c_sr2_mirror_tf_rf_1',  default_value='0.1', enable_events=True), sg.Text('pole or zero'), sg.Combo(('p','z'),  key='k_inf_c_sr2_mirror_tf_zp_1',  default_value='p', enable_events=True, readonly=True)]]
    k_inf_c_sr2_mirror_tf_re_box_2  = [[sg.Text('      quality factor value 02'), sg.Combo(('0.1','1','10','100','1000','10000','100000','1000000'),  key='k_inf_c_sr2_mirror_tf_rq_2', default_value='1000000', enable_events=True)  ,sg.Text('resonance frequency 02'), sg.Combo(('0.1','1','10','100','1000','10000','100000','1000000'),  key='k_inf_c_sr2_mirror_tf_rf_2',  default_value='0.1', enable_events=True), sg.Text('pole or zero'), sg.Combo(('p','z'),  key='k_inf_c_sr2_mirror_tf_zp_2',  default_value='p', enable_events=True, readonly=True)]]
    k_inf_c_sr2_mirror_tf_re_box_3  = [[sg.Text('      quality factor value 03'), sg.Combo(('0.1','1','10','100','1000','10000','100000','1000000'),  key='k_inf_c_sr2_mirror_tf_rq_3', default_value='1000000', enable_events=True)  ,sg.Text('resonance frequency 03'), sg.Combo(('0.1','1','10','100','1000','10000','100000','1000000'),  key='k_inf_c_sr2_mirror_tf_rf_3',  default_value='0.1', enable_events=True), sg.Text('pole or zero'), sg.Combo(('p','z'),  key='k_inf_c_sr2_mirror_tf_zp_3',  default_value='p', enable_events=True, readonly=True)]]
    k_inf_c_sr2_mirror_tf_re_box_4  = [[sg.Text('      quality factor value 04'), sg.Combo(('0.1','1','10','100','1000','10000','100000','1000000'),  key='k_inf_c_sr2_mirror_tf_rq_4', default_value='1000000', enable_events=True)  ,sg.Text('resonance frequency 04'), sg.Combo(('0.1','1','10','100','1000','10000','100000','1000000'),  key='k_inf_c_sr2_mirror_tf_rf_4',  default_value='0.1', enable_events=True), sg.Text('pole or zero'), sg.Combo(('p','z'),  key='k_inf_c_sr2_mirror_tf_zp_4',  default_value='p', enable_events=True, readonly=True)]]
    k_inf_c_sr2_mirror_tf_re_box_5  = [[sg.Text('      quality factor value 05'), sg.Combo(('0.1','1','10','100','1000','10000','100000','1000000'),  key='k_inf_c_sr2_mirror_tf_rq_5', default_value='1000000', enable_events=True)  ,sg.Text('resonance frequency 05'), sg.Combo(('0.1','1','10','100','1000','10000','100000','1000000'),  key='k_inf_c_sr2_mirror_tf_rf_5',  default_value='0.1', enable_events=True), sg.Text('pole or zero'), sg.Combo(('p','z'),  key='k_inf_c_sr2_mirror_tf_zp_5',  default_value='p', enable_events=True, readonly=True)]]
    k_inf_c_sr2_mirror_tf_re_box_6  = [[sg.Text('      quality factor value 06'), sg.Combo(('0.1','1','10','100','1000','10000','100000','1000000'),  key='k_inf_c_sr2_mirror_tf_rq_6', default_value='1000000', enable_events=True)  ,sg.Text('resonance frequency 06'), sg.Combo(('0.1','1','10','100','1000','10000','100000','1000000'),  key='k_inf_c_sr2_mirror_tf_rf_6',  default_value='0.1', enable_events=True), sg.Text('pole or zero'), sg.Combo(('p','z'),  key='k_inf_c_sr2_mirror_tf_zp_6',  default_value='p', enable_events=True, readonly=True)]]
    k_inf_c_sr2_mirror_tf_re_box_7  = [[sg.Text('      quality factor value 07'), sg.Combo(('0.1','1','10','100','1000','10000','100000','1000000'),  key='k_inf_c_sr2_mirror_tf_rq_7', default_value='1000000', enable_events=True)  ,sg.Text('resonance frequency 07'), sg.Combo(('0.1','1','10','100','1000','10000','100000','1000000'),  key='k_inf_c_sr2_mirror_tf_rf_7',  default_value='0.1', enable_events=True), sg.Text('pole or zero'), sg.Combo(('p','z'),  key='k_inf_c_sr2_mirror_tf_zp_7',  default_value='p', enable_events=True, readonly=True)]]
    k_inf_c_sr2_mirror_tf_re_box_8  = [[sg.Text('      quality factor value 08'), sg.Combo(('0.1','1','10','100','1000','10000','100000','1000000'),  key='k_inf_c_sr2_mirror_tf_rq_8', default_value='1000000', enable_events=True)  ,sg.Text('resonance frequency 08'), sg.Combo(('0.1','1','10','100','1000','10000','100000','1000000'),  key='k_inf_c_sr2_mirror_tf_rf_8',  default_value='0.1', enable_events=True), sg.Text('pole or zero'), sg.Combo(('p','z'),  key='k_inf_c_sr2_mirror_tf_zp_8',  default_value='p', enable_events=True, readonly=True)]]
    k_inf_c_sr2_mirror_tf_re_box_9  = [[sg.Text('      quality factor value 09'), sg.Combo(('0.1','1','10','100','1000','10000','100000','1000000'),  key='k_inf_c_sr2_mirror_tf_rq_9', default_value='1000000', enable_events=True)  ,sg.Text('resonance frequency 09'), sg.Combo(('0.1','1','10','100','1000','10000','100000','1000000'),  key='k_inf_c_sr2_mirror_tf_rf_9',  default_value='0.1', enable_events=True), sg.Text('pole or zero'), sg.Combo(('p','z'),  key='k_inf_c_sr2_mirror_tf_zp_9',  default_value='p', enable_events=True, readonly=True)]]
    k_inf_c_sr2_mirror_tf_re_box_10 = [[sg.Text('      quality factor value 10'), sg.Combo(('0.1','1','10','100','1000','10000','100000','1000000'), key='k_inf_c_sr2_mirror_tf_rq_10', default_value='1000000', enable_events=True)  ,sg.Text('resonance frequency 10'), sg.Combo(('0.1','1','10','100','1000','10000','100000','1000000'),  key='k_inf_c_sr2_mirror_tf_rf_10', default_value='0.1', enable_events=True), sg.Text('pole or zero'), sg.Combo(('p','z'),  key='k_inf_c_sr2_mirror_tf_zp_10', default_value='p', enable_events=True, readonly=True)]]

    ##################################################################################################################################################################################################
    k_inf_c_sr3_mirror_tf_re_box_1  = [[sg.Text('      quality factor value 01'), sg.Combo(('0.1','1','10','100','1000','10000','100000','1000000'),  key='k_inf_c_sr3_mirror_tf_rq_1', default_value='1000000', enable_events=True)  ,sg.Text('resonance frequency 01'), sg.Combo(('0.1','1','10','100','1000','10000','100000','1000000'),  key='k_inf_c_sr3_mirror_tf_rf_1',  default_value='0.1', enable_events=True), sg.Text('pole or zero'), sg.Combo(('p','z'),  key='k_inf_c_sr3_mirror_tf_zp_1',  default_value='p', enable_events=True, readonly=True)]]
    k_inf_c_sr3_mirror_tf_re_box_2  = [[sg.Text('      quality factor value 02'), sg.Combo(('0.1','1','10','100','1000','10000','100000','1000000'),  key='k_inf_c_sr3_mirror_tf_rq_2', default_value='1000000', enable_events=True)  ,sg.Text('resonance frequency 02'), sg.Combo(('0.1','1','10','100','1000','10000','100000','1000000'),  key='k_inf_c_sr3_mirror_tf_rf_2',  default_value='0.1', enable_events=True), sg.Text('pole or zero'), sg.Combo(('p','z'),  key='k_inf_c_sr3_mirror_tf_zp_2',  default_value='p', enable_events=True, readonly=True)]]
    k_inf_c_sr3_mirror_tf_re_box_3  = [[sg.Text('      quality factor value 03'), sg.Combo(('0.1','1','10','100','1000','10000','100000','1000000'),  key='k_inf_c_sr3_mirror_tf_rq_3', default_value='1000000', enable_events=True)  ,sg.Text('resonance frequency 03'), sg.Combo(('0.1','1','10','100','1000','10000','100000','1000000'),  key='k_inf_c_sr3_mirror_tf_rf_3',  default_value='0.1', enable_events=True), sg.Text('pole or zero'), sg.Combo(('p','z'),  key='k_inf_c_sr3_mirror_tf_zp_3',  default_value='p', enable_events=True, readonly=True)]]
    k_inf_c_sr3_mirror_tf_re_box_4  = [[sg.Text('      quality factor value 04'), sg.Combo(('0.1','1','10','100','1000','10000','100000','1000000'),  key='k_inf_c_sr3_mirror_tf_rq_4', default_value='1000000', enable_events=True)  ,sg.Text('resonance frequency 04'), sg.Combo(('0.1','1','10','100','1000','10000','100000','1000000'),  key='k_inf_c_sr3_mirror_tf_rf_4',  default_value='0.1', enable_events=True), sg.Text('pole or zero'), sg.Combo(('p','z'),  key='k_inf_c_sr3_mirror_tf_zp_4',  default_value='p', enable_events=True, readonly=True)]]
    k_inf_c_sr3_mirror_tf_re_box_5  = [[sg.Text('      quality factor value 05'), sg.Combo(('0.1','1','10','100','1000','10000','100000','1000000'),  key='k_inf_c_sr3_mirror_tf_rq_5', default_value='1000000', enable_events=True)  ,sg.Text('resonance frequency 05'), sg.Combo(('0.1','1','10','100','1000','10000','100000','1000000'),  key='k_inf_c_sr3_mirror_tf_rf_5',  default_value='0.1', enable_events=True), sg.Text('pole or zero'), sg.Combo(('p','z'),  key='k_inf_c_sr3_mirror_tf_zp_5',  default_value='p', enable_events=True, readonly=True)]]
    k_inf_c_sr3_mirror_tf_re_box_6  = [[sg.Text('      quality factor value 06'), sg.Combo(('0.1','1','10','100','1000','10000','100000','1000000'),  key='k_inf_c_sr3_mirror_tf_rq_6', default_value='1000000', enable_events=True)  ,sg.Text('resonance frequency 06'), sg.Combo(('0.1','1','10','100','1000','10000','100000','1000000'),  key='k_inf_c_sr3_mirror_tf_rf_6',  default_value='0.1', enable_events=True), sg.Text('pole or zero'), sg.Combo(('p','z'),  key='k_inf_c_sr3_mirror_tf_zp_6',  default_value='p', enable_events=True, readonly=True)]]
    k_inf_c_sr3_mirror_tf_re_box_7  = [[sg.Text('      quality factor value 07'), sg.Combo(('0.1','1','10','100','1000','10000','100000','1000000'),  key='k_inf_c_sr3_mirror_tf_rq_7', default_value='1000000', enable_events=True)  ,sg.Text('resonance frequency 07'), sg.Combo(('0.1','1','10','100','1000','10000','100000','1000000'),  key='k_inf_c_sr3_mirror_tf_rf_7',  default_value='0.1', enable_events=True), sg.Text('pole or zero'), sg.Combo(('p','z'),  key='k_inf_c_sr3_mirror_tf_zp_7',  default_value='p', enable_events=True, readonly=True)]]
    k_inf_c_sr3_mirror_tf_re_box_8  = [[sg.Text('      quality factor value 08'), sg.Combo(('0.1','1','10','100','1000','10000','100000','1000000'),  key='k_inf_c_sr3_mirror_tf_rq_8', default_value='1000000', enable_events=True)  ,sg.Text('resonance frequency 08'), sg.Combo(('0.1','1','10','100','1000','10000','100000','1000000'),  key='k_inf_c_sr3_mirror_tf_rf_8',  default_value='0.1', enable_events=True), sg.Text('pole or zero'), sg.Combo(('p','z'),  key='k_inf_c_sr3_mirror_tf_zp_8',  default_value='p', enable_events=True, readonly=True)]]
    k_inf_c_sr3_mirror_tf_re_box_9  = [[sg.Text('      quality factor value 09'), sg.Combo(('0.1','1','10','100','1000','10000','100000','1000000'),  key='k_inf_c_sr3_mirror_tf_rq_9', default_value='1000000', enable_events=True)  ,sg.Text('resonance frequency 09'), sg.Combo(('0.1','1','10','100','1000','10000','100000','1000000'),  key='k_inf_c_sr3_mirror_tf_rf_9',  default_value='0.1', enable_events=True), sg.Text('pole or zero'), sg.Combo(('p','z'),  key='k_inf_c_sr3_mirror_tf_zp_9',  default_value='p', enable_events=True, readonly=True)]]
    k_inf_c_sr3_mirror_tf_re_box_10 = [[sg.Text('      quality factor value 10'), sg.Combo(('0.1','1','10','100','1000','10000','100000','1000000'), key='k_inf_c_sr3_mirror_tf_rq_10', default_value='1000000', enable_events=True)  ,sg.Text('resonance frequency 10'), sg.Combo(('0.1','1','10','100','1000','10000','100000','1000000'),  key='k_inf_c_sr3_mirror_tf_rf_10', default_value='0.1', enable_events=True), sg.Text('pole or zero'), sg.Combo(('p','z'),  key='k_inf_c_sr3_mirror_tf_zp_10', default_value='p', enable_events=True, readonly=True)]]



    k_inf_c_mibs_mirror_tf_pzk_box   = [
            [sg.Text('  ・please set the transfer function gain.'), sg.Combo(('1', '2', '3', '4', '5', '6', '7', '8', '9', '10'), key='k_inf_c_mibs_mirror_tf_gn', default_value='1', enable_events=True)],
            
            [sg.Text('  ・please set the available number of poles.'), sg.Combo(('1', '2', '3', '4', '5', '6', '7', '8', '9', '10'), key='k_inf_c_mibs_mirror_tf_pn', default_value='1', enable_events=True)],

            [collapse(k_inf_c_mibs_mirror_tf_pf_box_1, 'k_inf_c_mibs_mirror_tf_pf_box_1')],
            [collapse(k_inf_c_mibs_mirror_tf_pf_box_2, 'k_inf_c_mibs_mirror_tf_pf_box_2')],
            [collapse(k_inf_c_mibs_mirror_tf_pf_box_3, 'k_inf_c_mibs_mirror_tf_pf_box_3')],
            [collapse(k_inf_c_mibs_mirror_tf_pf_box_4, 'k_inf_c_mibs_mirror_tf_pf_box_4')],
            [collapse(k_inf_c_mibs_mirror_tf_pf_box_5, 'k_inf_c_mibs_mirror_tf_pf_box_5')],
            [collapse(k_inf_c_mibs_mirror_tf_pf_box_6, 'k_inf_c_mibs_mirror_tf_pf_box_6')],
            [collapse(k_inf_c_mibs_mirror_tf_pf_box_7, 'k_inf_c_mibs_mirror_tf_pf_box_7')],
            [collapse(k_inf_c_mibs_mirror_tf_pf_box_8, 'k_inf_c_mibs_mirror_tf_pf_box_8')],
            [collapse(k_inf_c_mibs_mirror_tf_pf_box_9, 'k_inf_c_mibs_mirror_tf_pf_box_9')],
            [collapse(k_inf_c_mibs_mirror_tf_pf_box_10,'k_inf_c_mibs_mirror_tf_pf_box_10')],

            [sg.Text('  ・please set the available number of zeros.'), sg.Combo(('1', '2', '3', '4', '5', '6', '7', '8', '9', '10'), key='k_inf_c_mibs_mirror_tf_zn', default_value='1', enable_events=True)],
            [collapse(k_inf_c_mibs_mirror_tf_zf_box_1, 'k_inf_c_mibs_mirror_tf_zf_box_1')],
            [collapse(k_inf_c_mibs_mirror_tf_zf_box_2, 'k_inf_c_mibs_mirror_tf_zf_box_2')],
            [collapse(k_inf_c_mibs_mirror_tf_zf_box_3, 'k_inf_c_mibs_mirror_tf_zf_box_3')],
            [collapse(k_inf_c_mibs_mirror_tf_zf_box_4, 'k_inf_c_mibs_mirror_tf_zf_box_4')],
            [collapse(k_inf_c_mibs_mirror_tf_zf_box_5, 'k_inf_c_mibs_mirror_tf_zf_box_5')],
            [collapse(k_inf_c_mibs_mirror_tf_zf_box_6, 'k_inf_c_mibs_mirror_tf_zf_box_6')],
            [collapse(k_inf_c_mibs_mirror_tf_zf_box_7, 'k_inf_c_mibs_mirror_tf_zf_box_7')],
            [collapse(k_inf_c_mibs_mirror_tf_zf_box_8, 'k_inf_c_mibs_mirror_tf_zf_box_8')],
            [collapse(k_inf_c_mibs_mirror_tf_zf_box_9, 'k_inf_c_mibs_mirror_tf_zf_box_9')],
            [collapse(k_inf_c_mibs_mirror_tf_zf_box_10,'k_inf_c_mibs_mirror_tf_zf_box_10')]
            ]

    k_inf_c_itmx_mirror_tf_pzk_box   = [
            [sg.Text('  ・please set the transfer function gain.'), sg.Combo(('1', '2', '3', '4', '5', '6', '7', '8', '9', '10'), key='k_inf_c_itmx_mirror_tf_gn', default_value='1', enable_events=True)],
            [sg.Text('  ・please set the available number of poles.'), sg.Combo(('1', '2', '3', '4', '5', '6', '7', '8', '9', '10'), key='k_inf_c_itmx_mirror_tf_pn', default_value='1', enable_events=True)],
            [collapse(k_inf_c_itmx_mirror_tf_pf_box_1, 'k_inf_c_itmx_mirror_tf_pf_box_1')],
            [collapse(k_inf_c_itmx_mirror_tf_pf_box_2, 'k_inf_c_itmx_mirror_tf_pf_box_2')],
            [collapse(k_inf_c_itmx_mirror_tf_pf_box_3, 'k_inf_c_itmx_mirror_tf_pf_box_3')],
            [collapse(k_inf_c_itmx_mirror_tf_pf_box_4, 'k_inf_c_itmx_mirror_tf_pf_box_4')],
            [collapse(k_inf_c_itmx_mirror_tf_pf_box_5, 'k_inf_c_itmx_mirror_tf_pf_box_5')],
            [collapse(k_inf_c_itmx_mirror_tf_pf_box_6, 'k_inf_c_itmx_mirror_tf_pf_box_6')],
            [collapse(k_inf_c_itmx_mirror_tf_pf_box_7, 'k_inf_c_itmx_mirror_tf_pf_box_7')],
            [collapse(k_inf_c_itmx_mirror_tf_pf_box_8, 'k_inf_c_itmx_mirror_tf_pf_box_8')],
            [collapse(k_inf_c_itmx_mirror_tf_pf_box_9, 'k_inf_c_itmx_mirror_tf_pf_box_9')],
            [collapse(k_inf_c_itmx_mirror_tf_pf_box_10,'k_inf_c_itmx_mirror_tf_pf_box_10')],

            [sg.Text('  ・please set the available number of zeros.'), sg.Combo(('1', '2', '3', '4', '5', '6', '7', '8', '9', '10'), key='k_inf_c_itmx_mirror_tf_zn', default_value='1', enable_events=True)],
            [collapse(k_inf_c_itmx_mirror_tf_zf_box_1, 'k_inf_c_itmx_mirror_tf_zf_box_1')],
            [collapse(k_inf_c_itmx_mirror_tf_zf_box_2, 'k_inf_c_itmx_mirror_tf_zf_box_2')],
            [collapse(k_inf_c_itmx_mirror_tf_zf_box_3, 'k_inf_c_itmx_mirror_tf_zf_box_3')],
            [collapse(k_inf_c_itmx_mirror_tf_zf_box_4, 'k_inf_c_itmx_mirror_tf_zf_box_4')],
            [collapse(k_inf_c_itmx_mirror_tf_zf_box_5, 'k_inf_c_itmx_mirror_tf_zf_box_5')],
            [collapse(k_inf_c_itmx_mirror_tf_zf_box_6, 'k_inf_c_itmx_mirror_tf_zf_box_6')],
            [collapse(k_inf_c_itmx_mirror_tf_zf_box_7, 'k_inf_c_itmx_mirror_tf_zf_box_7')],
            [collapse(k_inf_c_itmx_mirror_tf_zf_box_8, 'k_inf_c_itmx_mirror_tf_zf_box_8')],
            [collapse(k_inf_c_itmx_mirror_tf_zf_box_9, 'k_inf_c_itmx_mirror_tf_zf_box_9')],
            [collapse(k_inf_c_itmx_mirror_tf_zf_box_10,'k_inf_c_itmx_mirror_tf_zf_box_10')]
            ]
    k_inf_c_itmy_mirror_tf_pzk_box   = [
            [sg.Text('  ・please set the transfer function gain.'), sg.Combo(('1', '2', '3', '4', '5', '6', '7', '8', '9', '10'), key='k_inf_c_itmy_mirror_tf_gn', default_value='1', enable_events=True)],
            [sg.Text('  ・please set the available number of poles.'), sg.Combo(('1', '2', '3', '4', '5', '6', '7', '8', '9', '10'), key='k_inf_c_itmy_mirror_tf_pn', default_value='1', enable_events=True)],
            [collapse(k_inf_c_itmy_mirror_tf_pf_box_1, 'k_inf_c_itmy_mirror_tf_pf_box_1')],
            [collapse(k_inf_c_itmy_mirror_tf_pf_box_2, 'k_inf_c_itmy_mirror_tf_pf_box_2')],
            [collapse(k_inf_c_itmy_mirror_tf_pf_box_3, 'k_inf_c_itmy_mirror_tf_pf_box_3')],
            [collapse(k_inf_c_itmy_mirror_tf_pf_box_4, 'k_inf_c_itmy_mirror_tf_pf_box_4')],
            [collapse(k_inf_c_itmy_mirror_tf_pf_box_5, 'k_inf_c_itmy_mirror_tf_pf_box_5')],
            [collapse(k_inf_c_itmy_mirror_tf_pf_box_6, 'k_inf_c_itmy_mirror_tf_pf_box_6')],
            [collapse(k_inf_c_itmy_mirror_tf_pf_box_7, 'k_inf_c_itmy_mirror_tf_pf_box_7')],
            [collapse(k_inf_c_itmy_mirror_tf_pf_box_8, 'k_inf_c_itmy_mirror_tf_pf_box_8')],
            [collapse(k_inf_c_itmy_mirror_tf_pf_box_9, 'k_inf_c_itmy_mirror_tf_pf_box_9')],
            [collapse(k_inf_c_itmy_mirror_tf_pf_box_10,'k_inf_c_itmy_mirror_tf_pf_box_10')],

            [sg.Text('  ・please set the available number of zeros.'), sg.Combo(('1', '2', '3', '4', '5', '6', '7', '8', '9', '10'), key='k_inf_c_itmy_mirror_tf_zn', default_value='1', enable_events=True)],
            [collapse(k_inf_c_itmy_mirror_tf_zf_box_1, 'k_inf_c_itmy_mirror_tf_zf_box_1')],
            [collapse(k_inf_c_itmy_mirror_tf_zf_box_2, 'k_inf_c_itmy_mirror_tf_zf_box_2')],
            [collapse(k_inf_c_itmy_mirror_tf_zf_box_3, 'k_inf_c_itmy_mirror_tf_zf_box_3')],
            [collapse(k_inf_c_itmy_mirror_tf_zf_box_4, 'k_inf_c_itmy_mirror_tf_zf_box_4')],
            [collapse(k_inf_c_itmy_mirror_tf_zf_box_5, 'k_inf_c_itmy_mirror_tf_zf_box_5')],
            [collapse(k_inf_c_itmy_mirror_tf_zf_box_6, 'k_inf_c_itmy_mirror_tf_zf_box_6')],
            [collapse(k_inf_c_itmy_mirror_tf_zf_box_7, 'k_inf_c_itmy_mirror_tf_zf_box_7')],
            [collapse(k_inf_c_itmy_mirror_tf_zf_box_8, 'k_inf_c_itmy_mirror_tf_zf_box_8')],
            [collapse(k_inf_c_itmy_mirror_tf_zf_box_9, 'k_inf_c_itmy_mirror_tf_zf_box_9')],
            [collapse(k_inf_c_itmy_mirror_tf_zf_box_10,'k_inf_c_itmy_mirror_tf_zf_box_10')]
            ]
    k_inf_c_etmx_mirror_tf_pzk_box   = [
            [sg.Text('  ・please set the transfer function gain.'), sg.Combo(('1', '2', '3', '4', '5', '6', '7', '8', '9', '10'), key='k_inf_c_etmx_mirror_tf_gn', default_value='1', enable_events=True)],
            [sg.Text('  ・please set the available number of poles.'), sg.Combo(('1', '2', '3', '4', '5', '6', '7', '8', '9', '10'), key='k_inf_c_etmx_mirror_tf_pn', default_value='1', enable_events=True)],
            [collapse(k_inf_c_etmx_mirror_tf_pf_box_1, 'k_inf_c_etmx_mirror_tf_pf_box_1')],
            [collapse(k_inf_c_etmx_mirror_tf_pf_box_2, 'k_inf_c_etmx_mirror_tf_pf_box_2')],
            [collapse(k_inf_c_etmx_mirror_tf_pf_box_3, 'k_inf_c_etmx_mirror_tf_pf_box_3')],
            [collapse(k_inf_c_etmx_mirror_tf_pf_box_4, 'k_inf_c_etmx_mirror_tf_pf_box_4')],
            [collapse(k_inf_c_etmx_mirror_tf_pf_box_5, 'k_inf_c_etmx_mirror_tf_pf_box_5')],
            [collapse(k_inf_c_etmx_mirror_tf_pf_box_6, 'k_inf_c_etmx_mirror_tf_pf_box_6')],
            [collapse(k_inf_c_etmx_mirror_tf_pf_box_7, 'k_inf_c_etmx_mirror_tf_pf_box_7')],
            [collapse(k_inf_c_etmx_mirror_tf_pf_box_8, 'k_inf_c_etmx_mirror_tf_pf_box_8')],
            [collapse(k_inf_c_etmx_mirror_tf_pf_box_9, 'k_inf_c_etmx_mirror_tf_pf_box_9')],
            [collapse(k_inf_c_etmx_mirror_tf_pf_box_10,'k_inf_c_etmx_mirror_tf_pf_box_10')],

            [sg.Text('  ・please set the available number of zeros.'), sg.Combo(('1', '2', '3', '4', '5', '6', '7', '8', '9', '10'), key='k_inf_c_etmx_mirror_tf_zn', default_value='1', enable_events=True)],
            [collapse(k_inf_c_etmx_mirror_tf_zf_box_1, 'k_inf_c_etmx_mirror_tf_zf_box_1')],
            [collapse(k_inf_c_etmx_mirror_tf_zf_box_2, 'k_inf_c_etmx_mirror_tf_zf_box_2')],
            [collapse(k_inf_c_etmx_mirror_tf_zf_box_3, 'k_inf_c_etmx_mirror_tf_zf_box_3')],
            [collapse(k_inf_c_etmx_mirror_tf_zf_box_4, 'k_inf_c_etmx_mirror_tf_zf_box_4')],
            [collapse(k_inf_c_etmx_mirror_tf_zf_box_5, 'k_inf_c_etmx_mirror_tf_zf_box_5')],
            [collapse(k_inf_c_etmx_mirror_tf_zf_box_6, 'k_inf_c_etmx_mirror_tf_zf_box_6')],
            [collapse(k_inf_c_etmx_mirror_tf_zf_box_7, 'k_inf_c_etmx_mirror_tf_zf_box_7')],
            [collapse(k_inf_c_etmx_mirror_tf_zf_box_8, 'k_inf_c_etmx_mirror_tf_zf_box_8')],
            [collapse(k_inf_c_etmx_mirror_tf_zf_box_9, 'k_inf_c_etmx_mirror_tf_zf_box_9')],
            [collapse(k_inf_c_etmx_mirror_tf_zf_box_10,'k_inf_c_etmx_mirror_tf_zf_box_10')]
            ]
    k_inf_c_etmy_mirror_tf_pzk_box   = [
            [sg.Text('  ・please set the transfer function gain.'), sg.Combo(('1', '2', '3', '4', '5', '6', '7', '8', '9', '10'), key='k_inf_c_etmy_mirror_tf_gn', default_value='1', enable_events=True)],
            [sg.Text('  ・please set the available number of poles.'), sg.Combo(('1', '2', '3', '4', '5', '6', '7', '8', '9', '10'), key='k_inf_c_etmy_mirror_tf_pn', default_value='1', enable_events=True)],
            [collapse(k_inf_c_etmy_mirror_tf_pf_box_1, 'k_inf_c_etmy_mirror_tf_pf_box_1')],
            [collapse(k_inf_c_etmy_mirror_tf_pf_box_2, 'k_inf_c_etmy_mirror_tf_pf_box_2')],
            [collapse(k_inf_c_etmy_mirror_tf_pf_box_3, 'k_inf_c_etmy_mirror_tf_pf_box_3')],
            [collapse(k_inf_c_etmy_mirror_tf_pf_box_4, 'k_inf_c_etmy_mirror_tf_pf_box_4')],
            [collapse(k_inf_c_etmy_mirror_tf_pf_box_5, 'k_inf_c_etmy_mirror_tf_pf_box_5')],
            [collapse(k_inf_c_etmy_mirror_tf_pf_box_6, 'k_inf_c_etmy_mirror_tf_pf_box_6')],
            [collapse(k_inf_c_etmy_mirror_tf_pf_box_7, 'k_inf_c_etmy_mirror_tf_pf_box_7')],
            [collapse(k_inf_c_etmy_mirror_tf_pf_box_8, 'k_inf_c_etmy_mirror_tf_pf_box_8')],
            [collapse(k_inf_c_etmy_mirror_tf_pf_box_9, 'k_inf_c_etmy_mirror_tf_pf_box_9')],
            [collapse(k_inf_c_etmy_mirror_tf_pf_box_10,'k_inf_c_etmy_mirror_tf_pf_box_10')],

            [sg.Text('  ・please set the available number of zeros.'), sg.Combo(('1', '2', '3', '4', '5', '6', '7', '8', '9', '10'), key='k_inf_c_etmy_mirror_tf_zn', default_value='1', enable_events=True)],
            [collapse(k_inf_c_etmy_mirror_tf_zf_box_1, 'k_inf_c_etmy_mirror_tf_zf_box_1')],
            [collapse(k_inf_c_etmy_mirror_tf_zf_box_2, 'k_inf_c_etmy_mirror_tf_zf_box_2')],
            [collapse(k_inf_c_etmy_mirror_tf_zf_box_3, 'k_inf_c_etmy_mirror_tf_zf_box_3')],
            [collapse(k_inf_c_etmy_mirror_tf_zf_box_4, 'k_inf_c_etmy_mirror_tf_zf_box_4')],
            [collapse(k_inf_c_etmy_mirror_tf_zf_box_5, 'k_inf_c_etmy_mirror_tf_zf_box_5')],
            [collapse(k_inf_c_etmy_mirror_tf_zf_box_6, 'k_inf_c_etmy_mirror_tf_zf_box_6')],
            [collapse(k_inf_c_etmy_mirror_tf_zf_box_7, 'k_inf_c_etmy_mirror_tf_zf_box_7')],
            [collapse(k_inf_c_etmy_mirror_tf_zf_box_8, 'k_inf_c_etmy_mirror_tf_zf_box_8')],
            [collapse(k_inf_c_etmy_mirror_tf_zf_box_9, 'k_inf_c_etmy_mirror_tf_zf_box_9')],
            [collapse(k_inf_c_etmy_mirror_tf_zf_box_10,'k_inf_c_etmy_mirror_tf_zf_box_10')]
            ]
    k_inf_c_prm_mirror_tf_pzk_box   = [
            [sg.Text('  ・please set the transfer function gain.'), sg.Combo(('1', '2', '3', '4', '5', '6', '7', '8', '9', '10'), key='k_inf_c_prm_mirror_tf_gn', default_value='1', enable_events=True)],
            [sg.Text('  ・please set the available number of poles.'), sg.Combo(('1', '2', '3', '4', '5', '6', '7', '8', '9', '10'), key='k_inf_c_prm_mirror_tf_pn', default_value='1', enable_events=True)],
            [collapse(k_inf_c_prm_mirror_tf_pf_box_1, 'k_inf_c_prm_mirror_tf_pf_box_1')],
            [collapse(k_inf_c_prm_mirror_tf_pf_box_2, 'k_inf_c_prm_mirror_tf_pf_box_2')],
            [collapse(k_inf_c_prm_mirror_tf_pf_box_3, 'k_inf_c_prm_mirror_tf_pf_box_3')],
            [collapse(k_inf_c_prm_mirror_tf_pf_box_4, 'k_inf_c_prm_mirror_tf_pf_box_4')],
            [collapse(k_inf_c_prm_mirror_tf_pf_box_5, 'k_inf_c_prm_mirror_tf_pf_box_5')],
            [collapse(k_inf_c_prm_mirror_tf_pf_box_6, 'k_inf_c_prm_mirror_tf_pf_box_6')],
            [collapse(k_inf_c_prm_mirror_tf_pf_box_7, 'k_inf_c_prm_mirror_tf_pf_box_7')],
            [collapse(k_inf_c_prm_mirror_tf_pf_box_8, 'k_inf_c_prm_mirror_tf_pf_box_8')],
            [collapse(k_inf_c_prm_mirror_tf_pf_box_9, 'k_inf_c_prm_mirror_tf_pf_box_9')],
            [collapse(k_inf_c_prm_mirror_tf_pf_box_10,'k_inf_c_prm_mirror_tf_pf_box_10')],

            [sg.Text('  ・please set the available number of zeros.'), sg.Combo(('1', '2', '3', '4', '5', '6', '7', '8', '9', '10'), key='k_inf_c_prm_mirror_tf_zn', default_value='1', enable_events=True)],
            [collapse(k_inf_c_prm_mirror_tf_zf_box_1, 'k_inf_c_prm_mirror_tf_zf_box_1')],
            [collapse(k_inf_c_prm_mirror_tf_zf_box_2, 'k_inf_c_prm_mirror_tf_zf_box_2')],
            [collapse(k_inf_c_prm_mirror_tf_zf_box_3, 'k_inf_c_prm_mirror_tf_zf_box_3')],
            [collapse(k_inf_c_prm_mirror_tf_zf_box_4, 'k_inf_c_prm_mirror_tf_zf_box_4')],
            [collapse(k_inf_c_prm_mirror_tf_zf_box_5, 'k_inf_c_prm_mirror_tf_zf_box_5')],
            [collapse(k_inf_c_prm_mirror_tf_zf_box_6, 'k_inf_c_prm_mirror_tf_zf_box_6')],
            [collapse(k_inf_c_prm_mirror_tf_zf_box_7, 'k_inf_c_prm_mirror_tf_zf_box_7')],
            [collapse(k_inf_c_prm_mirror_tf_zf_box_8, 'k_inf_c_prm_mirror_tf_zf_box_8')],
            [collapse(k_inf_c_prm_mirror_tf_zf_box_9, 'k_inf_c_prm_mirror_tf_zf_box_9')],
            [collapse(k_inf_c_prm_mirror_tf_zf_box_10,'k_inf_c_prm_mirror_tf_zf_box_10')]
            ]
    k_inf_c_pr2_mirror_tf_pzk_box   = [
            [sg.Text('  ・please set the transfer function gain.'), sg.Combo(('1', '2', '3', '4', '5', '6', '7', '8', '9', '10'), key='k_inf_c_pr2_mirror_tf_gn', default_value='1', enable_events=True)],
            [sg.Text('  ・please set the available number of poles.'), sg.Combo(('1', '2', '3', '4', '5', '6', '7', '8', '9', '10'), key='k_inf_c_pr2_mirror_tf_pn', default_value='1', enable_events=True)],
            [collapse(k_inf_c_pr2_mirror_tf_pf_box_1, 'k_inf_c_pr2_mirror_tf_pf_box_1')],
            [collapse(k_inf_c_pr2_mirror_tf_pf_box_2, 'k_inf_c_pr2_mirror_tf_pf_box_2')],
            [collapse(k_inf_c_pr2_mirror_tf_pf_box_3, 'k_inf_c_pr2_mirror_tf_pf_box_3')],
            [collapse(k_inf_c_pr2_mirror_tf_pf_box_4, 'k_inf_c_pr2_mirror_tf_pf_box_4')],
            [collapse(k_inf_c_pr2_mirror_tf_pf_box_5, 'k_inf_c_pr2_mirror_tf_pf_box_5')],
            [collapse(k_inf_c_pr2_mirror_tf_pf_box_6, 'k_inf_c_pr2_mirror_tf_pf_box_6')],
            [collapse(k_inf_c_pr2_mirror_tf_pf_box_7, 'k_inf_c_pr2_mirror_tf_pf_box_7')],
            [collapse(k_inf_c_pr2_mirror_tf_pf_box_8, 'k_inf_c_pr2_mirror_tf_pf_box_8')],
            [collapse(k_inf_c_pr2_mirror_tf_pf_box_9, 'k_inf_c_pr2_mirror_tf_pf_box_9')],
            [collapse(k_inf_c_pr2_mirror_tf_pf_box_10,'k_inf_c_pr2_mirror_tf_pf_box_10')],

            [sg.Text('  ・please set the available number of zeros.'), sg.Combo(('1', '2', '3', '4', '5', '6', '7', '8', '9', '10'), key='k_inf_c_pr2_mirror_tf_zn', default_value='1', enable_events=True)],
            [collapse(k_inf_c_pr2_mirror_tf_zf_box_1, 'k_inf_c_pr2_mirror_tf_zf_box_1')],
            [collapse(k_inf_c_pr2_mirror_tf_zf_box_2, 'k_inf_c_pr2_mirror_tf_zf_box_2')],
            [collapse(k_inf_c_pr2_mirror_tf_zf_box_3, 'k_inf_c_pr2_mirror_tf_zf_box_3')],
            [collapse(k_inf_c_pr2_mirror_tf_zf_box_4, 'k_inf_c_pr2_mirror_tf_zf_box_4')],
            [collapse(k_inf_c_pr2_mirror_tf_zf_box_5, 'k_inf_c_pr2_mirror_tf_zf_box_5')],
            [collapse(k_inf_c_pr2_mirror_tf_zf_box_6, 'k_inf_c_pr2_mirror_tf_zf_box_6')],
            [collapse(k_inf_c_pr2_mirror_tf_zf_box_7, 'k_inf_c_pr2_mirror_tf_zf_box_7')],
            [collapse(k_inf_c_pr2_mirror_tf_zf_box_8, 'k_inf_c_pr2_mirror_tf_zf_box_8')],
            [collapse(k_inf_c_pr2_mirror_tf_zf_box_9, 'k_inf_c_pr2_mirror_tf_zf_box_9')],
            [collapse(k_inf_c_pr2_mirror_tf_zf_box_10,'k_inf_c_pr2_mirror_tf_zf_box_10')]
            ]
    k_inf_c_pr3_mirror_tf_pzk_box   = [
            [sg.Text('  ・please set the transfer function gain.'), sg.Combo(('1', '2', '3', '4', '5', '6', '7', '8', '9', '10'), key='k_inf_c_pr3_mirror_tf_gn', default_value='1', enable_events=True)],
            [sg.Text('  ・please set the available number of poles.'), sg.Combo(('1', '2', '3', '4', '5', '6', '7', '8', '9', '10'), key='k_inf_c_pr3_mirror_tf_pn', default_value='1', enable_events=True)],
            [collapse(k_inf_c_pr3_mirror_tf_pf_box_1, 'k_inf_c_pr3_mirror_tf_pf_box_1')],
            [collapse(k_inf_c_pr3_mirror_tf_pf_box_2, 'k_inf_c_pr3_mirror_tf_pf_box_2')],
            [collapse(k_inf_c_pr3_mirror_tf_pf_box_3, 'k_inf_c_pr3_mirror_tf_pf_box_3')],
            [collapse(k_inf_c_pr3_mirror_tf_pf_box_4, 'k_inf_c_pr3_mirror_tf_pf_box_4')],
            [collapse(k_inf_c_pr3_mirror_tf_pf_box_5, 'k_inf_c_pr3_mirror_tf_pf_box_5')],
            [collapse(k_inf_c_pr3_mirror_tf_pf_box_6, 'k_inf_c_pr3_mirror_tf_pf_box_6')],
            [collapse(k_inf_c_pr3_mirror_tf_pf_box_7, 'k_inf_c_pr3_mirror_tf_pf_box_7')],
            [collapse(k_inf_c_pr3_mirror_tf_pf_box_8, 'k_inf_c_pr3_mirror_tf_pf_box_8')],
            [collapse(k_inf_c_pr3_mirror_tf_pf_box_9, 'k_inf_c_pr3_mirror_tf_pf_box_9')],
            [collapse(k_inf_c_pr3_mirror_tf_pf_box_10,'k_inf_c_pr3_mirror_tf_pf_box_10')],

            [sg.Text('  ・please set the available number of zeros.'), sg.Combo(('1', '2', '3', '4', '5', '6', '7', '8', '9', '10'), key='k_inf_c_pr3_mirror_tf_zn', default_value='1', enable_events=True)],
            [collapse(k_inf_c_pr3_mirror_tf_zf_box_1, 'k_inf_c_pr3_mirror_tf_zf_box_1')],
            [collapse(k_inf_c_pr3_mirror_tf_zf_box_2, 'k_inf_c_pr3_mirror_tf_zf_box_2')],
            [collapse(k_inf_c_pr3_mirror_tf_zf_box_3, 'k_inf_c_pr3_mirror_tf_zf_box_3')],
            [collapse(k_inf_c_pr3_mirror_tf_zf_box_4, 'k_inf_c_pr3_mirror_tf_zf_box_4')],
            [collapse(k_inf_c_pr3_mirror_tf_zf_box_5, 'k_inf_c_pr3_mirror_tf_zf_box_5')],
            [collapse(k_inf_c_pr3_mirror_tf_zf_box_6, 'k_inf_c_pr3_mirror_tf_zf_box_6')],
            [collapse(k_inf_c_pr3_mirror_tf_zf_box_7, 'k_inf_c_pr3_mirror_tf_zf_box_7')],
            [collapse(k_inf_c_pr3_mirror_tf_zf_box_8, 'k_inf_c_pr3_mirror_tf_zf_box_8')],
            [collapse(k_inf_c_pr3_mirror_tf_zf_box_9, 'k_inf_c_pr3_mirror_tf_zf_box_9')],
            [collapse(k_inf_c_pr3_mirror_tf_zf_box_10,'k_inf_c_pr3_mirror_tf_zf_box_10')]
            ]
    k_inf_c_srm_mirror_tf_pzk_box   = [
            [sg.Text('  ・please set the transfer function gain.'), sg.Combo(('1', '2', '3', '4', '5', '6', '7', '8', '9', '10'), key='k_inf_c_srm_mirror_tf_gn', default_value='1', enable_events=True)],
            [sg.Text('  ・please set the available number of poles.'), sg.Combo(('1', '2', '3', '4', '5', '6', '7', '8', '9', '10'), key='k_inf_c_srm_mirror_tf_pn', default_value='1', enable_events=True)],
            [collapse(k_inf_c_srm_mirror_tf_pf_box_1, 'k_inf_c_srm_mirror_tf_pf_box_1')],
            [collapse(k_inf_c_srm_mirror_tf_pf_box_2, 'k_inf_c_srm_mirror_tf_pf_box_2')],
            [collapse(k_inf_c_srm_mirror_tf_pf_box_3, 'k_inf_c_srm_mirror_tf_pf_box_3')],
            [collapse(k_inf_c_srm_mirror_tf_pf_box_4, 'k_inf_c_srm_mirror_tf_pf_box_4')],
            [collapse(k_inf_c_srm_mirror_tf_pf_box_5, 'k_inf_c_srm_mirror_tf_pf_box_5')],
            [collapse(k_inf_c_srm_mirror_tf_pf_box_6, 'k_inf_c_srm_mirror_tf_pf_box_6')],
            [collapse(k_inf_c_srm_mirror_tf_pf_box_7, 'k_inf_c_srm_mirror_tf_pf_box_7')],
            [collapse(k_inf_c_srm_mirror_tf_pf_box_8, 'k_inf_c_srm_mirror_tf_pf_box_8')],
            [collapse(k_inf_c_srm_mirror_tf_pf_box_9, 'k_inf_c_srm_mirror_tf_pf_box_9')],
            [collapse(k_inf_c_srm_mirror_tf_pf_box_10,'k_inf_c_srm_mirror_tf_pf_box_10')],

            [sg.Text('  ・please set the available number of zeros.'), sg.Combo(('1', '2', '3', '4', '5', '6', '7', '8', '9', '10'), key='k_inf_c_srm_mirror_tf_zn', default_value='1', enable_events=True)],
            [collapse(k_inf_c_srm_mirror_tf_zf_box_1, 'k_inf_c_srm_mirror_tf_zf_box_1')],
            [collapse(k_inf_c_srm_mirror_tf_zf_box_2, 'k_inf_c_srm_mirror_tf_zf_box_2')],
            [collapse(k_inf_c_srm_mirror_tf_zf_box_3, 'k_inf_c_srm_mirror_tf_zf_box_3')],
            [collapse(k_inf_c_srm_mirror_tf_zf_box_4, 'k_inf_c_srm_mirror_tf_zf_box_4')],
            [collapse(k_inf_c_srm_mirror_tf_zf_box_5, 'k_inf_c_srm_mirror_tf_zf_box_5')],
            [collapse(k_inf_c_srm_mirror_tf_zf_box_6, 'k_inf_c_srm_mirror_tf_zf_box_6')],
            [collapse(k_inf_c_srm_mirror_tf_zf_box_7, 'k_inf_c_srm_mirror_tf_zf_box_7')],
            [collapse(k_inf_c_srm_mirror_tf_zf_box_8, 'k_inf_c_srm_mirror_tf_zf_box_8')],
            [collapse(k_inf_c_srm_mirror_tf_zf_box_9, 'k_inf_c_srm_mirror_tf_zf_box_9')],
            [collapse(k_inf_c_srm_mirror_tf_zf_box_10,'k_inf_c_srm_mirror_tf_zf_box_10')]
            ]
    k_inf_c_sr2_mirror_tf_pzk_box   = [
            [sg.Text('  ・please set the transfer function gain.'), sg.Combo(('1', '2', '3', '4', '5', '6', '7', '8', '9', '10'), key='k_inf_c_sr2_mirror_tf_gn', default_value='1', enable_events=True)],
            [sg.Text('  ・please set the available number of poles.'), sg.Combo(('1', '2', '3', '4', '5', '6', '7', '8', '9', '10'), key='k_inf_c_sr2_mirror_tf_pn', default_value='1', enable_events=True)],
            [collapse(k_inf_c_sr2_mirror_tf_pf_box_1, 'k_inf_c_sr2_mirror_tf_pf_box_1')],
            [collapse(k_inf_c_sr2_mirror_tf_pf_box_2, 'k_inf_c_sr2_mirror_tf_pf_box_2')],
            [collapse(k_inf_c_sr2_mirror_tf_pf_box_3, 'k_inf_c_sr2_mirror_tf_pf_box_3')],
            [collapse(k_inf_c_sr2_mirror_tf_pf_box_4, 'k_inf_c_sr2_mirror_tf_pf_box_4')],
            [collapse(k_inf_c_sr2_mirror_tf_pf_box_5, 'k_inf_c_sr2_mirror_tf_pf_box_5')],
            [collapse(k_inf_c_sr2_mirror_tf_pf_box_6, 'k_inf_c_sr2_mirror_tf_pf_box_6')],
            [collapse(k_inf_c_sr2_mirror_tf_pf_box_7, 'k_inf_c_sr2_mirror_tf_pf_box_7')],
            [collapse(k_inf_c_sr2_mirror_tf_pf_box_8, 'k_inf_c_sr2_mirror_tf_pf_box_8')],
            [collapse(k_inf_c_sr2_mirror_tf_pf_box_9, 'k_inf_c_sr2_mirror_tf_pf_box_9')],
            [collapse(k_inf_c_sr2_mirror_tf_pf_box_10,'k_inf_c_sr2_mirror_tf_pf_box_10')],

            [sg.Text('  ・please set the available number of zeros.'), sg.Combo(('1', '2', '3', '4', '5', '6', '7', '8', '9', '10'), key='k_inf_c_sr2_mirror_tf_zn', default_value='1', enable_events=True)],
            [collapse(k_inf_c_sr2_mirror_tf_zf_box_1, 'k_inf_c_sr2_mirror_tf_zf_box_1')],
            [collapse(k_inf_c_sr2_mirror_tf_zf_box_2, 'k_inf_c_sr2_mirror_tf_zf_box_2')],
            [collapse(k_inf_c_sr2_mirror_tf_zf_box_3, 'k_inf_c_sr2_mirror_tf_zf_box_3')],
            [collapse(k_inf_c_sr2_mirror_tf_zf_box_4, 'k_inf_c_sr2_mirror_tf_zf_box_4')],
            [collapse(k_inf_c_sr2_mirror_tf_zf_box_5, 'k_inf_c_sr2_mirror_tf_zf_box_5')],
            [collapse(k_inf_c_sr2_mirror_tf_zf_box_6, 'k_inf_c_sr2_mirror_tf_zf_box_6')],
            [collapse(k_inf_c_sr2_mirror_tf_zf_box_7, 'k_inf_c_sr2_mirror_tf_zf_box_7')],
            [collapse(k_inf_c_sr2_mirror_tf_zf_box_8, 'k_inf_c_sr2_mirror_tf_zf_box_8')],
            [collapse(k_inf_c_sr2_mirror_tf_zf_box_9, 'k_inf_c_sr2_mirror_tf_zf_box_9')],
            [collapse(k_inf_c_sr2_mirror_tf_zf_box_10,'k_inf_c_sr2_mirror_tf_zf_box_10')]
            ]
    k_inf_c_sr3_mirror_tf_pzk_box   = [
            [sg.Text('  ・please set the transfer function gain.'), sg.Combo(('1', '2', '3', '4', '5', '6', '7', '8', '9', '10'), key='k_inf_c_sr3_mirror_tf_gn', default_value='1', enable_events=True)],
            [sg.Text('  ・please set the available number of poles.'), sg.Combo(('1', '2', '3', '4', '5', '6', '7', '8', '9', '10'), key='k_inf_c_sr3_mirror_tf_pn', default_value='1', enable_events=True)],
            [collapse(k_inf_c_sr3_mirror_tf_pf_box_1, 'k_inf_c_sr3_mirror_tf_pf_box_1')],
            [collapse(k_inf_c_sr3_mirror_tf_pf_box_2, 'k_inf_c_sr3_mirror_tf_pf_box_2')],
            [collapse(k_inf_c_sr3_mirror_tf_pf_box_3, 'k_inf_c_sr3_mirror_tf_pf_box_3')],
            [collapse(k_inf_c_sr3_mirror_tf_pf_box_4, 'k_inf_c_sr3_mirror_tf_pf_box_4')],
            [collapse(k_inf_c_sr3_mirror_tf_pf_box_5, 'k_inf_c_sr3_mirror_tf_pf_box_5')],
            [collapse(k_inf_c_sr3_mirror_tf_pf_box_6, 'k_inf_c_sr3_mirror_tf_pf_box_6')],
            [collapse(k_inf_c_sr3_mirror_tf_pf_box_7, 'k_inf_c_sr3_mirror_tf_pf_box_7')],
            [collapse(k_inf_c_sr3_mirror_tf_pf_box_8, 'k_inf_c_sr3_mirror_tf_pf_box_8')],
            [collapse(k_inf_c_sr3_mirror_tf_pf_box_9, 'k_inf_c_sr3_mirror_tf_pf_box_9')],
            [collapse(k_inf_c_sr3_mirror_tf_pf_box_10,'k_inf_c_sr3_mirror_tf_pf_box_10')],

            [sg.Text('  ・please set the available number of zeros.'), sg.Combo(('1', '2', '3', '4', '5', '6', '7', '8', '9', '10'), key='k_inf_c_sr3_mirror_tf_zn', default_value='1', enable_events=True)],
            [collapse(k_inf_c_sr3_mirror_tf_zf_box_1, 'k_inf_c_sr3_mirror_tf_zf_box_1')],
            [collapse(k_inf_c_sr3_mirror_tf_zf_box_2, 'k_inf_c_sr3_mirror_tf_zf_box_2')],
            [collapse(k_inf_c_sr3_mirror_tf_zf_box_3, 'k_inf_c_sr3_mirror_tf_zf_box_3')],
            [collapse(k_inf_c_sr3_mirror_tf_zf_box_4, 'k_inf_c_sr3_mirror_tf_zf_box_4')],
            [collapse(k_inf_c_sr3_mirror_tf_zf_box_5, 'k_inf_c_sr3_mirror_tf_zf_box_5')],
            [collapse(k_inf_c_sr3_mirror_tf_zf_box_6, 'k_inf_c_sr3_mirror_tf_zf_box_6')],
            [collapse(k_inf_c_sr3_mirror_tf_zf_box_7, 'k_inf_c_sr3_mirror_tf_zf_box_7')],
            [collapse(k_inf_c_sr3_mirror_tf_zf_box_8, 'k_inf_c_sr3_mirror_tf_zf_box_8')],
            [collapse(k_inf_c_sr3_mirror_tf_zf_box_9, 'k_inf_c_sr3_mirror_tf_zf_box_9')],
            [collapse(k_inf_c_sr3_mirror_tf_zf_box_10,'k_inf_c_sr3_mirror_tf_zf_box_10')]
            ]


    
    k_inf_c_mibs_mirror_tf_quality_factor_box   = [
            [sg.Text('  ・please set the available number of resonance.'), sg.Combo(('1', '2', '3', '4', '5', '6', '7', '8', '9', '10'), key='k_inf_c_mibs_mirror_tf_rn', default_value='1', enable_events=True)],

            [collapse(k_inf_c_mibs_mirror_tf_re_box_1, 'k_inf_c_mibs_mirror_tf_re_box_1')],
            [collapse(k_inf_c_mibs_mirror_tf_re_box_2, 'k_inf_c_mibs_mirror_tf_re_box_2')],
            [collapse(k_inf_c_mibs_mirror_tf_re_box_3, 'k_inf_c_mibs_mirror_tf_re_box_3')],
            [collapse(k_inf_c_mibs_mirror_tf_re_box_4, 'k_inf_c_mibs_mirror_tf_re_box_4')],
            [collapse(k_inf_c_mibs_mirror_tf_re_box_5, 'k_inf_c_mibs_mirror_tf_re_box_5')],
            [collapse(k_inf_c_mibs_mirror_tf_re_box_6, 'k_inf_c_mibs_mirror_tf_re_box_6')],
            [collapse(k_inf_c_mibs_mirror_tf_re_box_7, 'k_inf_c_mibs_mirror_tf_re_box_7')],
            [collapse(k_inf_c_mibs_mirror_tf_re_box_8, 'k_inf_c_mibs_mirror_tf_re_box_8')],
            [collapse(k_inf_c_mibs_mirror_tf_re_box_9, 'k_inf_c_mibs_mirror_tf_re_box_9')],
            [collapse(k_inf_c_mibs_mirror_tf_re_box_10,'k_inf_c_mibs_mirror_tf_re_box_10')]
            ]
    k_inf_c_itmx_mirror_tf_quality_factor_box   = [
            [sg.Text('  ・please set the available number of resonance.'), sg.Combo(('1', '2', '3', '4', '5', '6', '7', '8', '9', '10'), key='k_inf_c_itmx_mirror_tf_rn', default_value='1', enable_events=True)],
            [collapse(k_inf_c_itmx_mirror_tf_re_box_1, 'k_inf_c_itmx_mirror_tf_re_box_1')],
            [collapse(k_inf_c_itmx_mirror_tf_re_box_2, 'k_inf_c_itmx_mirror_tf_re_box_2')],
            [collapse(k_inf_c_itmx_mirror_tf_re_box_3, 'k_inf_c_itmx_mirror_tf_re_box_3')],
            [collapse(k_inf_c_itmx_mirror_tf_re_box_4, 'k_inf_c_itmx_mirror_tf_re_box_4')],
            [collapse(k_inf_c_itmx_mirror_tf_re_box_5, 'k_inf_c_itmx_mirror_tf_re_box_5')],
            [collapse(k_inf_c_itmx_mirror_tf_re_box_6, 'k_inf_c_itmx_mirror_tf_re_box_6')],
            [collapse(k_inf_c_itmx_mirror_tf_re_box_7, 'k_inf_c_itmx_mirror_tf_re_box_7')],
            [collapse(k_inf_c_itmx_mirror_tf_re_box_8, 'k_inf_c_itmx_mirror_tf_re_box_8')],
            [collapse(k_inf_c_itmx_mirror_tf_re_box_9, 'k_inf_c_itmx_mirror_tf_re_box_9')],
            [collapse(k_inf_c_itmx_mirror_tf_re_box_10,'k_inf_c_itmx_mirror_tf_re_box_10')]
            ]
    k_inf_c_itmy_mirror_tf_quality_factor_box   = [
            [sg.Text('  ・please set the available number of resonance.'), sg.Combo(('1', '2', '3', '4', '5', '6', '7', '8', '9', '10'), key='k_inf_c_itmy_mirror_tf_rn', default_value='1', enable_events=True)],
            [collapse(k_inf_c_itmy_mirror_tf_re_box_1, 'k_inf_c_itmy_mirror_tf_re_box_1')],
            [collapse(k_inf_c_itmy_mirror_tf_re_box_2, 'k_inf_c_itmy_mirror_tf_re_box_2')],
            [collapse(k_inf_c_itmy_mirror_tf_re_box_3, 'k_inf_c_itmy_mirror_tf_re_box_3')],
            [collapse(k_inf_c_itmy_mirror_tf_re_box_4, 'k_inf_c_itmy_mirror_tf_re_box_4')],
            [collapse(k_inf_c_itmy_mirror_tf_re_box_5, 'k_inf_c_itmy_mirror_tf_re_box_5')],
            [collapse(k_inf_c_itmy_mirror_tf_re_box_6, 'k_inf_c_itmy_mirror_tf_re_box_6')],
            [collapse(k_inf_c_itmy_mirror_tf_re_box_7, 'k_inf_c_itmy_mirror_tf_re_box_7')],
            [collapse(k_inf_c_itmy_mirror_tf_re_box_8, 'k_inf_c_itmy_mirror_tf_re_box_8')],
            [collapse(k_inf_c_itmy_mirror_tf_re_box_9, 'k_inf_c_itmy_mirror_tf_re_box_9')],
            [collapse(k_inf_c_itmy_mirror_tf_re_box_10,'k_inf_c_itmy_mirror_tf_re_box_10')]
            ]
    k_inf_c_etmx_mirror_tf_quality_factor_box   = [
            [sg.Text('  ・please set the available number of resonance.'), sg.Combo(('1', '2', '3', '4', '5', '6', '7', '8', '9', '10'), key='k_inf_c_etmx_mirror_tf_rn', default_value='1', enable_events=True)],
            [collapse(k_inf_c_etmx_mirror_tf_re_box_1, 'k_inf_c_etmx_mirror_tf_re_box_1')],
            [collapse(k_inf_c_etmx_mirror_tf_re_box_2, 'k_inf_c_etmx_mirror_tf_re_box_2')],
            [collapse(k_inf_c_etmx_mirror_tf_re_box_3, 'k_inf_c_etmx_mirror_tf_re_box_3')],
            [collapse(k_inf_c_etmx_mirror_tf_re_box_4, 'k_inf_c_etmx_mirror_tf_re_box_4')],
            [collapse(k_inf_c_etmx_mirror_tf_re_box_5, 'k_inf_c_etmx_mirror_tf_re_box_5')],
            [collapse(k_inf_c_etmx_mirror_tf_re_box_6, 'k_inf_c_etmx_mirror_tf_re_box_6')],
            [collapse(k_inf_c_etmx_mirror_tf_re_box_7, 'k_inf_c_etmx_mirror_tf_re_box_7')],
            [collapse(k_inf_c_etmx_mirror_tf_re_box_8, 'k_inf_c_etmx_mirror_tf_re_box_8')],
            [collapse(k_inf_c_etmx_mirror_tf_re_box_9, 'k_inf_c_etmx_mirror_tf_re_box_9')],
            [collapse(k_inf_c_etmx_mirror_tf_re_box_10,'k_inf_c_etmx_mirror_tf_re_box_10')]
            ]
    k_inf_c_etmy_mirror_tf_quality_factor_box   = [
            [sg.Text('  ・please set the available number of resonance.'), sg.Combo(('1', '2', '3', '4', '5', '6', '7', '8', '9', '10'), key='k_inf_c_etmy_mirror_tf_rn', default_value='1', enable_events=True)],
            [collapse(k_inf_c_etmy_mirror_tf_re_box_1, 'k_inf_c_etmy_mirror_tf_re_box_1')],
            [collapse(k_inf_c_etmy_mirror_tf_re_box_2, 'k_inf_c_etmy_mirror_tf_re_box_2')],
            [collapse(k_inf_c_etmy_mirror_tf_re_box_3, 'k_inf_c_etmy_mirror_tf_re_box_3')],
            [collapse(k_inf_c_etmy_mirror_tf_re_box_4, 'k_inf_c_etmy_mirror_tf_re_box_4')],
            [collapse(k_inf_c_etmy_mirror_tf_re_box_5, 'k_inf_c_etmy_mirror_tf_re_box_5')],
            [collapse(k_inf_c_etmy_mirror_tf_re_box_6, 'k_inf_c_etmy_mirror_tf_re_box_6')],
            [collapse(k_inf_c_etmy_mirror_tf_re_box_7, 'k_inf_c_etmy_mirror_tf_re_box_7')],
            [collapse(k_inf_c_etmy_mirror_tf_re_box_8, 'k_inf_c_etmy_mirror_tf_re_box_8')],
            [collapse(k_inf_c_etmy_mirror_tf_re_box_9, 'k_inf_c_etmy_mirror_tf_re_box_9')],
            [collapse(k_inf_c_etmy_mirror_tf_re_box_10,'k_inf_c_etmy_mirror_tf_re_box_10')]
            ]
    k_inf_c_prm_mirror_tf_quality_factor_box   = [
            [sg.Text('  ・please set the available number of resonance.'), sg.Combo(('1', '2', '3', '4', '5', '6', '7', '8', '9', '10'), key='k_inf_c_prm_mirror_tf_rn', default_value='1', enable_events=True)],
            [collapse(k_inf_c_prm_mirror_tf_re_box_1, 'k_inf_c_prm_mirror_tf_re_box_1')],
            [collapse(k_inf_c_prm_mirror_tf_re_box_2, 'k_inf_c_prm_mirror_tf_re_box_2')],
            [collapse(k_inf_c_prm_mirror_tf_re_box_3, 'k_inf_c_prm_mirror_tf_re_box_3')],
            [collapse(k_inf_c_prm_mirror_tf_re_box_4, 'k_inf_c_prm_mirror_tf_re_box_4')],
            [collapse(k_inf_c_prm_mirror_tf_re_box_5, 'k_inf_c_prm_mirror_tf_re_box_5')],
            [collapse(k_inf_c_prm_mirror_tf_re_box_6, 'k_inf_c_prm_mirror_tf_re_box_6')],
            [collapse(k_inf_c_prm_mirror_tf_re_box_7, 'k_inf_c_prm_mirror_tf_re_box_7')],
            [collapse(k_inf_c_prm_mirror_tf_re_box_8, 'k_inf_c_prm_mirror_tf_re_box_8')],
            [collapse(k_inf_c_prm_mirror_tf_re_box_9, 'k_inf_c_prm_mirror_tf_re_box_9')],
            [collapse(k_inf_c_prm_mirror_tf_re_box_10,'k_inf_c_prm_mirror_tf_re_box_10')]
            ]
    k_inf_c_pr2_mirror_tf_quality_factor_box   = [
            [sg.Text('  ・please set the available number of resonance.'), sg.Combo(('1', '2', '3', '4', '5', '6', '7', '8', '9', '10'), key='k_inf_c_pr2_mirror_tf_rn', default_value='1', enable_events=True)],
            [collapse(k_inf_c_pr2_mirror_tf_re_box_1, 'k_inf_c_pr2_mirror_tf_re_box_1')],
            [collapse(k_inf_c_pr2_mirror_tf_re_box_2, 'k_inf_c_pr2_mirror_tf_re_box_2')],
            [collapse(k_inf_c_pr2_mirror_tf_re_box_3, 'k_inf_c_pr2_mirror_tf_re_box_3')],
            [collapse(k_inf_c_pr2_mirror_tf_re_box_4, 'k_inf_c_pr2_mirror_tf_re_box_4')],
            [collapse(k_inf_c_pr2_mirror_tf_re_box_5, 'k_inf_c_pr2_mirror_tf_re_box_5')],
            [collapse(k_inf_c_pr2_mirror_tf_re_box_6, 'k_inf_c_pr2_mirror_tf_re_box_6')],
            [collapse(k_inf_c_pr2_mirror_tf_re_box_7, 'k_inf_c_pr2_mirror_tf_re_box_7')],
            [collapse(k_inf_c_pr2_mirror_tf_re_box_8, 'k_inf_c_pr2_mirror_tf_re_box_8')],
            [collapse(k_inf_c_pr2_mirror_tf_re_box_9, 'k_inf_c_pr2_mirror_tf_re_box_9')],
            [collapse(k_inf_c_pr2_mirror_tf_re_box_10,'k_inf_c_pr2_mirror_tf_re_box_10')]
            ]
    k_inf_c_pr3_mirror_tf_quality_factor_box   = [
            [sg.Text('  ・please set the available number of resonance.'), sg.Combo(('1', '2', '3', '4', '5', '6', '7', '8', '9', '10'), key='k_inf_c_pr3_mirror_tf_rn', default_value='1', enable_events=True)],

            [collapse(k_inf_c_pr3_mirror_tf_re_box_1, 'k_inf_c_pr3_mirror_tf_re_box_1')],
            [collapse(k_inf_c_pr3_mirror_tf_re_box_2, 'k_inf_c_pr3_mirror_tf_re_box_2')],
            [collapse(k_inf_c_pr3_mirror_tf_re_box_3, 'k_inf_c_pr3_mirror_tf_re_box_3')],
            [collapse(k_inf_c_pr3_mirror_tf_re_box_4, 'k_inf_c_pr3_mirror_tf_re_box_4')],
            [collapse(k_inf_c_pr3_mirror_tf_re_box_5, 'k_inf_c_pr3_mirror_tf_re_box_5')],
            [collapse(k_inf_c_pr3_mirror_tf_re_box_6, 'k_inf_c_pr3_mirror_tf_re_box_6')],
            [collapse(k_inf_c_pr3_mirror_tf_re_box_7, 'k_inf_c_pr3_mirror_tf_re_box_7')],
            [collapse(k_inf_c_pr3_mirror_tf_re_box_8, 'k_inf_c_pr3_mirror_tf_re_box_8')],
            [collapse(k_inf_c_pr3_mirror_tf_re_box_9, 'k_inf_c_pr3_mirror_tf_re_box_9')],
            [collapse(k_inf_c_pr3_mirror_tf_re_box_10,'k_inf_c_pr3_mirror_tf_re_box_10')]
            ]
    k_inf_c_srm_mirror_tf_quality_factor_box   = [
            [sg.Text('  ・please set the available number of resonance.'), sg.Combo(('1', '2', '3', '4', '5', '6', '7', '8', '9', '10'), key='k_inf_c_srm_mirror_tf_rn', default_value='1', enable_events=True)],

            [collapse(k_inf_c_srm_mirror_tf_re_box_1, 'k_inf_c_srm_mirror_tf_re_box_1')],
            [collapse(k_inf_c_srm_mirror_tf_re_box_2, 'k_inf_c_srm_mirror_tf_re_box_2')],
            [collapse(k_inf_c_srm_mirror_tf_re_box_3, 'k_inf_c_srm_mirror_tf_re_box_3')],
            [collapse(k_inf_c_srm_mirror_tf_re_box_4, 'k_inf_c_srm_mirror_tf_re_box_4')],
            [collapse(k_inf_c_srm_mirror_tf_re_box_5, 'k_inf_c_srm_mirror_tf_re_box_5')],
            [collapse(k_inf_c_srm_mirror_tf_re_box_6, 'k_inf_c_srm_mirror_tf_re_box_6')],
            [collapse(k_inf_c_srm_mirror_tf_re_box_7, 'k_inf_c_srm_mirror_tf_re_box_7')],
            [collapse(k_inf_c_srm_mirror_tf_re_box_8, 'k_inf_c_srm_mirror_tf_re_box_8')],
            [collapse(k_inf_c_srm_mirror_tf_re_box_9, 'k_inf_c_srm_mirror_tf_re_box_9')],
            [collapse(k_inf_c_srm_mirror_tf_re_box_10,'k_inf_c_srm_mirror_tf_re_box_10')]
            ]
    k_inf_c_sr2_mirror_tf_quality_factor_box   = [
            [sg.Text('  ・please set the available number of resonance.'), sg.Combo(('1', '2', '3', '4', '5', '6', '7', '8', '9', '10'), key='k_inf_c_sr2_mirror_tf_rn', default_value='1', enable_events=True)],

            [collapse(k_inf_c_sr2_mirror_tf_re_box_1, 'k_inf_c_sr2_mirror_tf_re_box_1')],
            [collapse(k_inf_c_sr2_mirror_tf_re_box_2, 'k_inf_c_sr2_mirror_tf_re_box_2')],
            [collapse(k_inf_c_sr2_mirror_tf_re_box_3, 'k_inf_c_sr2_mirror_tf_re_box_3')],
            [collapse(k_inf_c_sr2_mirror_tf_re_box_4, 'k_inf_c_sr2_mirror_tf_re_box_4')],
            [collapse(k_inf_c_sr2_mirror_tf_re_box_5, 'k_inf_c_sr2_mirror_tf_re_box_5')],
            [collapse(k_inf_c_sr2_mirror_tf_re_box_6, 'k_inf_c_sr2_mirror_tf_re_box_6')],
            [collapse(k_inf_c_sr2_mirror_tf_re_box_7, 'k_inf_c_sr2_mirror_tf_re_box_7')],
            [collapse(k_inf_c_sr2_mirror_tf_re_box_8, 'k_inf_c_sr2_mirror_tf_re_box_8')],
            [collapse(k_inf_c_sr2_mirror_tf_re_box_9, 'k_inf_c_sr2_mirror_tf_re_box_9')],
            [collapse(k_inf_c_sr2_mirror_tf_re_box_10,'k_inf_c_sr2_mirror_tf_re_box_10')]
            ]
    k_inf_c_sr3_mirror_tf_quality_factor_box   = [
            [sg.Text('  ・please set the available number of resonance.'), sg.Combo(('1', '2', '3', '4', '5', '6', '7', '8', '9', '10'), key='k_inf_c_sr3_mirror_tf_rn', default_value='1', enable_events=True)],

            [collapse(k_inf_c_sr3_mirror_tf_re_box_1, 'k_inf_c_sr3_mirror_tf_re_box_1')],
            [collapse(k_inf_c_sr3_mirror_tf_re_box_2, 'k_inf_c_sr3_mirror_tf_re_box_2')],
            [collapse(k_inf_c_sr3_mirror_tf_re_box_3, 'k_inf_c_sr3_mirror_tf_re_box_3')],
            [collapse(k_inf_c_sr3_mirror_tf_re_box_4, 'k_inf_c_sr3_mirror_tf_re_box_4')],
            [collapse(k_inf_c_sr3_mirror_tf_re_box_5, 'k_inf_c_sr3_mirror_tf_re_box_5')],
            [collapse(k_inf_c_sr3_mirror_tf_re_box_6, 'k_inf_c_sr3_mirror_tf_re_box_6')],
            [collapse(k_inf_c_sr3_mirror_tf_re_box_7, 'k_inf_c_sr3_mirror_tf_re_box_7')],
            [collapse(k_inf_c_sr3_mirror_tf_re_box_8, 'k_inf_c_sr3_mirror_tf_re_box_8')],
            [collapse(k_inf_c_sr3_mirror_tf_re_box_9, 'k_inf_c_sr3_mirror_tf_re_box_9')],
            [collapse(k_inf_c_sr3_mirror_tf_re_box_10,'k_inf_c_sr3_mirror_tf_re_box_10')]
            ]

    mirror_sec_mibs = [
            [sg.Text('BS   mirror power transmittance'), sg.Input(key='k_inf_c_mibs_mirror_transmittance', default_text='0.5'     , enable_events=True)],
            [sg.Text('BS   mirror power loss'),          sg.Input(key='k_inf_c_mibs_mirror_loss'         , default_text='0'       , enable_events=True)],
            [sg.Text('BS   mirror mass'),                sg.Input(key='k_inf_c_mibs_mirror_mass'         , default_text='40'      , enable_events=True)],
            [sg.Checkbox("using transfer function('force' to 'motion')",key="mibs_using_tf_f_to_m")],
            [sg.Text('transfer function (force to motion)')
            ,sg.Radio('PZK', 'mibs_transfer_function_selecter', default=False,  key='k_inf_c_mibs_mirror_tf_style_pzk', enable_events=True)
            ,sg.Radio('Quality factor and frequency', 'mibs_transfer_function_selecter', default=True,  key='k_inf_c_mibs_mirror_tf_style_qf', enable_events=True)],
            [collapse(k_inf_c_mibs_mirror_tf_pzk_box, 'k_inf_c_mibs_mirror_tf_pzk_box')],
            [collapse(k_inf_c_mibs_mirror_tf_quality_factor_box, 'k_inf_c_mibs_mirror_tf_quality_factor_box')],
            
            [sg.Checkbox('load mirror map file?'                , key='k_inf_c_mirror_sec_isload_mirror_map_bs', enable_events=True)],
            ]
    
    mirror_sec_itmx = [
            [sg.Text('ITMX mirror power transmittance'), sg.Input(key='k_inf_c_itmx_mirror_transmittance', default_text='0.004'   , enable_events=True)],
            [sg.Text('ITMX mirror power loss'),          sg.Input(key='k_inf_c_itmx_mirror_loss'         , default_text='0'       , enable_events=True)],
            [sg.Text('ITMX mirror mass'),                sg.Input(key='k_inf_c_itmx_mirror_mass'         , default_text='40'      , enable_events=True)],
            [sg.Checkbox("using transfer function('force' to 'motion')",key="itmx_using_tf_f_to_m")],
            [sg.Text('transfer function (force to motion)')
            ,sg.Radio('PZK', 'itmx_transfer_function_selecter', default=False,  key='k_inf_c_itmx_mirror_tf_style_pzk', enable_events=True)
            ,sg.Radio('Quality factor and frequency', 'itmx_transfer_function_selecter', default=True,  key='k_inf_c_itmx_mirror_tf_style_qf', enable_events=True)
            ],
            [collapse(k_inf_c_itmx_mirror_tf_pzk_box, 'k_inf_c_itmx_mirror_tf_pzk_box')],
            [collapse(k_inf_c_itmx_mirror_tf_quality_factor_box, 'k_inf_c_itmx_mirror_tf_quality_factor_box')],

            
            [sg.Checkbox('load mirror map file?',                 key='k_inf_c_mirror_sec_isload_mirror_map_itmx', enable_events=True)],
            ]
    mirror_sec_itmy = [
            [sg.Text('ITMY mirror power transmittance'), sg.Input(key='k_inf_c_itmy_mirror_transmittance', default_text='0.004'   , enable_events=True)],
            [sg.Text('ITMY mirror power loss'),          sg.Input(key='k_inf_c_itmy_mirror_loss'         , default_text='45e-6'       , enable_events=True)],
            [sg.Text('ITMY mirror mass'),                sg.Input(key='k_inf_c_itmy_mirror_mass'         , default_text='40'      , enable_events=True)],
            [sg.Checkbox("using transfer function('force' to 'motion')",key="itmy_using_tf_f_to_m")],
            [sg.Text('transfer function (force to motion)')
            ,sg.Radio('PZK', 'itmy_transfer_function_selecter', default=False,  key='k_inf_c_itmy_mirror_tf_style_pzk', enable_events=True)
            ,sg.Radio('Quality factor and frequency', 'itmy_transfer_function_selecter', default=True,  key='k_inf_c_itmy_mirror_tf_style_qf', enable_events=True)
            ],
            [collapse(k_inf_c_itmy_mirror_tf_pzk_box, 'k_inf_c_itmy_mirror_tf_pzk_box')],
            [collapse(k_inf_c_itmy_mirror_tf_quality_factor_box, 'k_inf_c_itmy_mirror_tf_quality_factor_box')],
            ]
    mirror_sec_etmx = [
            [sg.Text('ETMX mirror power transmittance'), sg.Input(key='k_inf_c_etmx_mirror_transmittance', default_text='5e-06'   , enable_events=True)],
            [sg.Text('ETMX mirror power loss'),          sg.Input(key='k_inf_c_etmx_mirror_loss'         , default_text='0'       , enable_events=True)],
            [sg.Text('ETMX mirror mass'),                sg.Input(key='k_inf_c_etmx_mirror_mass'         , default_text='40'      , enable_events=True)],
            [sg.Checkbox("using transfer function('force' to 'motion')",key="etmx_using_tf_f_to_m")],
            [sg.Text('transfer function (force to motion)')
            ,sg.Radio('PZK', 'etmx_transfer_function_selecter', default=False,  key='k_inf_c_etmx_mirror_tf_style_pzk', enable_events=True)
            ,sg.Radio('Quality factor and frequency', 'etmx_transfer_function_selecter', default=True,  key='k_inf_c_etmx_mirror_tf_style_qf', enable_events=True)
            ],
            [collapse(k_inf_c_etmx_mirror_tf_pzk_box, 'k_inf_c_etmx_mirror_tf_pzk_box')],
            [collapse(k_inf_c_etmx_mirror_tf_quality_factor_box, 'k_inf_c_etmx_mirror_tf_quality_factor_box')],

            
            ]
    mirror_sec_etmy = [
            [sg.Text('ETMY mirror power transmittance'), sg.Input(key='k_inf_c_etmy_mirror_transmittance', default_text='5e-06'   , enable_events=True)],
            [sg.Text('ETMY mirror power loss'),          sg.Input(key='k_inf_c_etmy_mirror_loss'         , default_text='0'       , enable_events=True)],
            [sg.Text('ETMY mirror mass'),                sg.Input(key='k_inf_c_etmy_mirror_mass'         , default_text='40'      , enable_events=True)],

            [sg.Checkbox("using transfer function('force' to 'motion')",key="etmy_using_tf_f_to_m")],
            [sg.Text('transfer function (force to motion)')
            ,sg.Radio('PZK', 'etmy_transfer_function_selecter', default=False,  key='k_inf_c_etmy_mirror_tf_style_pzk', enable_events=True)
            ,sg.Radio('Quality factor and frequency', 'etmy_transfer_function_selecter', default=True,  key='k_inf_c_etmy_mirror_tf_style_qf', enable_events=True)
            ],
            [collapse(k_inf_c_etmy_mirror_tf_pzk_box, 'k_inf_c_etmy_mirror_tf_pzk_box')],
            [collapse(k_inf_c_etmy_mirror_tf_quality_factor_box, 'k_inf_c_etmy_mirror_tf_quality_factor_box')],

            
            ]
    
    mirror_sec_prm = [
            [sg.Text('PRM  mirror power transmittance'), sg.Input(key='k_inf_c_prm_mirror_transmittance' , default_text='0.1'     , enable_events=True)],
            [sg.Text('PRM  mirror power loss'),          sg.Input(key='k_inf_c_prm_mirror_loss'          , default_text='45e-6'   , enable_events=True)],
            [sg.Text('PRM  mirror mass'),                sg.Input(key='k_inf_c_prm_mirror_mass'         , default_text='40'      , enable_events=True)],

            [sg.Checkbox("using transfer function('force' to 'motion')",key="prm_using_tf_f_to_m")],
            [sg.Text('transfer function (force to motion)')
            ,sg.Radio('PZK', 'prm_transfer_function_selecter', default=False,  key='k_inf_c_prm_mirror_tf_style_pzk', enable_events=True)
            ,sg.Radio('Quality factor and frequency', 'prm_transfer_function_selecter', default=True,  key='k_inf_c_prm_mirror_tf_style_qf', enable_events=True)
            ],
            [collapse(k_inf_c_prm_mirror_tf_pzk_box, 'k_inf_c_prm_mirror_tf_pzk_box')],
            [collapse(k_inf_c_prm_mirror_tf_quality_factor_box, 'k_inf_c_prm_mirror_tf_quality_factor_box')],

            
            ]
    mirror_sec_pr2 = [
            [sg.Text('PR2  mirror power transmittance'), sg.Input(key='k_inf_c_pr2_mirror_transmittance' , default_text='500e-6'  , enable_events=True)],
            [sg.Text('PR2  mirror power loss'),          sg.Input(key='k_inf_c_pr2_mirror_loss'          , default_text='45e-6'  , enable_events=True)],
            [sg.Text('PR2  mirror mass'),                sg.Input(key='k_inf_c_pr2_mirror_mass'          , default_text='40'      , enable_events=True)],

            [sg.Checkbox("using transfer function('force' to 'motion')",key="pr2_using_tf_f_to_m")],
            [sg.Text('transfer function (force to motion)')
            ,sg.Radio('PZK', 'pr2_transfer_function_selecter', default=False,  key='k_inf_c_pr2_mirror_tf_style_pzk', enable_events=True)
            ,sg.Radio('Quality factor and frequency', 'pr2_transfer_function_selecter', default=True,  key='k_inf_c_pr2_mirror_tf_style_qf', enable_events=True)
            ],
            [collapse(k_inf_c_pr2_mirror_tf_pzk_box, 'k_inf_c_pr2_mirror_tf_pzk_box')],
            [collapse(k_inf_c_pr2_mirror_tf_quality_factor_box, 'k_inf_c_pr2_mirror_tf_quality_factor_box')],

            
            ]
    mirror_sec_pr3 = [
            [sg.Text('PR3  mirror power transmittance'), sg.Input(key='k_inf_c_pr3_mirror_transmittance' , default_text='50e-6'   , enable_events=True)],
            [sg.Text('PR3  mirror power loss'),          sg.Input(key='k_inf_c_pr3_mirror_loss'          , default_text='45e-6'   , enable_events=True)],
            [sg.Text('PR3  mirror mass'),                sg.Input(key='k_inf_c_pr3_mirror_mass'          , default_text='40'      , enable_events=True)],

            [sg.Checkbox("using transfer function('force' to 'motion')",key="pr3_using_tf_f_to_m")],
            [sg.Text('transfer function (force to motion)')
            ,sg.Radio('PZK', 'pr3_transfer_function_selecter', default=False,  key='k_inf_c_pr3_mirror_tf_style_pzk', enable_events=True)
            ,sg.Radio('Quality factor and frequency', 'pr3_transfer_function_selecter', default=True,  key='k_inf_c_pr3_mirror_tf_style_qf', enable_events=True)
            ],
            [collapse(k_inf_c_pr3_mirror_tf_pzk_box, 'k_inf_c_pr3_mirror_tf_pzk_box')],
            [collapse(k_inf_c_pr3_mirror_tf_quality_factor_box, 'k_inf_c_pr3_mirror_tf_quality_factor_box')],

            
            ]

    mirror_sec_srm = [
            [sg.Text('SRM  mirror power transmittance'), sg.Input(key='k_inf_c_srm_mirror_transmittance' , default_text='0.3'     , enable_events=True)],
            [sg.Text('SRM  mirror power loss'),          sg.Input(key='k_inf_c_srm_mirror_loss'          , default_text='45e-6'   , enable_events=True)],
            [sg.Text('SRM  mirror mass'),                sg.Input(key='k_inf_c_srm_mirror_mass'          , default_text='40'      , enable_events=True)],

            [sg.Checkbox("using transfer function('force' to 'motion')",key="srm_using_tf_f_to_m")],
            [sg.Text('transfer function (force to motion)')
            ,sg.Radio('PZK', 'srm_transfer_function_selecter', default=False,  key='k_inf_c_srm_mirror_tf_style_pzk', enable_events=True)
            ,sg.Radio('Quality factor and frequency', 'srm_transfer_function_selecter', default=True,  key='k_inf_c_srm_mirror_tf_style_qf', enable_events=True)
            ],
            [collapse(k_inf_c_srm_mirror_tf_pzk_box, 'k_inf_c_srm_mirror_tf_pzk_box')],
            [collapse(k_inf_c_srm_mirror_tf_quality_factor_box, 'k_inf_c_srm_mirror_tf_quality_factor_box')],

            
            ]
    mirror_sec_sr2 = [
            [sg.Text('SR2  mirror power transmittance'), sg.Input(key='k_inf_c_sr2_mirror_transmittance' , default_text='500e-6'  , enable_events=True)],
            [sg.Text('SR2  mirror power loss'),          sg.Input(key='k_inf_c_sr2_mirror_loss'          , default_text='45e-6'  , enable_events=True)],
            [sg.Text('SR2  mirror mass'),                sg.Input(key='k_inf_c_sr2_mirror_mass'          , default_text='40'      , enable_events=True)],

            [sg.Checkbox("using transfer function('force' to 'motion')",key="sr2_using_tf_f_to_m")],
            [sg.Text('transfer function (force to motion)')
            ,sg.Radio('PZK', 'sr2_transfer_function_selecter', default=False,  key='k_inf_c_sr2_mirror_tf_style_pzk', enable_events=True)
            ,sg.Radio('Quality factor and frequency', 'sr2_transfer_function_selecter', default=True,  key='k_inf_c_sr2_mirror_tf_style_qf', enable_events=True)
            ],
            [collapse(k_inf_c_sr2_mirror_tf_pzk_box, 'k_inf_c_sr2_mirror_tf_pzk_box')],
            [collapse(k_inf_c_sr2_mirror_tf_quality_factor_box, 'k_inf_c_sr2_mirror_tf_quality_factor_box')],

            
            ]
    mirror_sec_sr3 = [
            [sg.Text('SR3  mirror power transmittance'), sg.Input(key='k_inf_c_sr3_mirror_transmittance' , default_text='50e-6'   , enable_events=True)],
            [sg.Text('SR3  mirror power loss'),          sg.Input(key='k_inf_c_sr3_mirror_loss'          , default_text='45e-6'   , enable_events=True)],
            [sg.Text('SR3  mirror mass'),                sg.Input(key='k_inf_c_sr3_mirror_mass'          , default_text='40'      , enable_events=True)],

            [sg.Checkbox("using transfer function('force' to 'motion')",key="sr3_using_tf_f_to_m")],
            [sg.Text('transfer function (force to motion)')
            ,sg.Radio('PZK', 'sr3_transfer_function_selecter', default=False,  key='k_inf_c_sr3_mirror_tf_style_pzk', enable_events=True)
            ,sg.Radio('Quality factor and frequency', 'sr3_transfer_function_selecter', default=True,  key='k_inf_c_sr3_mirror_tf_style_qf', enable_events=True)
            ],
            [collapse(k_inf_c_sr3_mirror_tf_pzk_box, 'k_inf_c_sr3_mirror_tf_pzk_box')],
            [collapse(k_inf_c_sr3_mirror_tf_quality_factor_box, 'k_inf_c_sr3_mirror_tf_quality_factor_box')],

            
            ]

    mirror_param_sec = [
        [sg.Text('note: Reflectance and transmittance are values between 0 and 1.')],
        [sg.Checkbox('BS'  , key='k_inf_c_open_mirror_param_sec_mibs'  , enable_events=True)],
        [collapse(mirror_sec_mibs,   'mirror_param_sec_mibs')],
        [sg.Checkbox('ITMX', key='k_inf_c_open_mirror_param_sec_itmx', enable_events=True)],
        [collapse(mirror_sec_itmx, 'mirror_param_sec_itmx')],
        [sg.Checkbox('ITMY', key='k_inf_c_open_mirror_param_sec_itmy', enable_events=True)],
        [collapse(mirror_sec_itmy, 'mirror_param_sec_itmy')],
        [sg.Checkbox('ETMX', key='k_inf_c_open_mirror_param_sec_etmx', enable_events=True)],
        [collapse(mirror_sec_etmx, 'mirror_param_sec_etmx')],
        [sg.Checkbox('ETMY', key='k_inf_c_open_mirror_param_sec_etmy', enable_events=True)],
        [collapse(mirror_sec_etmy, 'mirror_param_sec_etmy')],
        [sg.Checkbox('PRM', key='k_inf_c_open_mirror_param_sec_prm'  , enable_events=True)],
        [collapse(mirror_sec_prm,  'mirror_param_sec_prm')],
        [sg.Checkbox('SRM', key='k_inf_c_open_mirror_param_sec_srm'  , enable_events=True)],
        [collapse(mirror_sec_srm,  'mirror_param_sec_srm')],
        [sg.Checkbox('PR2', key='k_inf_c_open_mirror_param_sec_pr2'  , enable_events=True)],
        [collapse(mirror_sec_pr2,  'mirror_param_sec_pr2')],
        [sg.Checkbox('PR3', key='k_inf_c_open_mirror_param_sec_pr3'  , enable_events=True)],
        [collapse(mirror_sec_pr3,  'mirror_param_sec_pr3')],
        [sg.Checkbox('SR2', key='k_inf_c_open_mirror_param_sec_sr2'  , enable_events=True)],
        [collapse(mirror_sec_sr2,  'mirror_param_sec_sr2')],
        [sg.Checkbox('SR3', key='k_inf_c_open_mirror_param_sec_sr3'  , enable_events=True)],
        [collapse(mirror_sec_sr3,  'mirror_param_sec_sr3')]
        ]
    hom_combo = []
    for i in range(100):
        hom_combo.append("%d"%i)


    hom_param_sec = [
        [sg.Checkbox('use high order mode beam?', key='k_inf_c_laser_use_hom' , default=False, enable_events=True)],
        [sg.Text('number of higher mode order'), sg.Combo((hom_combo), default_value='1', readonly=True, disabled=True, key='k_inf_c_num_of_hom_order', enable_events=True)]
    ]
    laser_param_sec = [    
        [sg.Text('laser_power [W]'), sg.Input(key='k_inf_c_laser_power' ,default_text='1' ,enable_events=True)]
    ]
    
    eom_param_sec = [
        [sg.Text('modulation f1 frequency'),                 sg.Input(key='k_inf_c_f1_mod_frequency'        , default_text='16.881M' , enable_events=True)],
        [sg.Text('f1 modulation index', visible=False),      sg.Input(key='k_inf_c_f1_mod_index'            , default_text='0.3'     , enable_events=True, visible=False)],# 必要なさそうなので表示していません
        [sg.Text('modulation f2 frequency'),                 sg.Input(key='k_inf_c_f2_mod_frequency'        , default_text='45.0159M', enable_events=True)],
        [sg.Text('f2 modulation index', visible=False),      sg.Input(key='k_inf_c_f2_mod_index'            , default_text='0.3'     , enable_events=True, visible=False)],# 必要なさそうなので表示していません
        [sg.Text('number of produced modulator sidebands'), sg.Combo(('1', '2', '3', '4', '5', '6', '7', '8', '9', '10'), default_value='3', readonly=True, key='k_inf_c_num_of_sidebands', enable_events=True)]
                
    ]
    # kifo
    kifo_kagra_drawing_para_mi_normalsize = [
                    [sg.Image('../fig/DRFPMI_picture_normal.png', key='kifo_imageContainer_para1',  size=(400,300))]#size=(800,600)
    ]
    kifo_kagra_drawing_para_mi_largesize = [
                    [sg.Image('../fig/DRFPMI_picture_large.png',  key='kifo_imageContainer_para2', size=(800,600))]#size=(800,600)
    ]
    kifo_kagra_drawing_para_fpmi_normalsize = [
                    [sg.Image('../fig/DRFPMI_picture_normal.png', key='kifo_imageContainer_para3',  size=(400,300))]#size=(800,600)
    ]
    kifo_kagra_drawing_para_fpmi_largesize = [
                    [sg.Image('../fig/DRFPMI_picture_large.png',  key='kifo_imageContainer_para4', size=(800,600))]#size=(800,600)
    ]
    kifo_kagra_drawing_para_prfpmi_normalsize = [
                    [sg.Image('../fig/DRFPMI_picture_normal.png', key='kifo_imageContainer_para5',  size=(400,300))]#size=(800,600)
    ]
    kifo_kagra_drawing_para_prfpmi_largesize = [
                    [sg.Image('../fig/DRFPMI_picture_large.png',  key='kifo_imageContainer_para6', size=(800,600))]#size=(800,600)
    ]
    kifo_kagra_drawing_para_drfpmi_normalsize = [
                    [sg.Image('../fig/DRFPMI_picture_normal.png', key='kifo_imageContainer_para7',  size=(400,300))]#size=(800,600)
    ]
    kifo_kagra_drawing_para_drfpmi_largesize = [
                    [sg.Image('../fig/DRFPMI_picture_large.png',  key='kifo_imageContainer_para8', size=(800,600))]#size=(800,600)
    ]

    para_drawing = make_layout_drawing('../fig/DRFPMI_picture_normal.png', "para_drawing_key")

    length_param_sec = [
        #[collapse(kifo_kagra_drawing_para_mi_normalsize,     'kifo_kagra_drawing_para_mi_normalsize')],
        #[collapse(kifo_kagra_drawing_para_mi_largesize,      'kifo_kagra_drawing_para_mi_largesize')],
        #[collapse(kifo_kagra_drawing_para_fpmi_normalsize,   'kifo_kagra_drawing_para_fpmi_normalsize')],
        #[collapse(kifo_kagra_drawing_para_fpmi_largesize,    'kifo_kagra_drawing_para_fpmi_largesize')],
        #[collapse(kifo_kagra_drawing_para_prfpmi_normalsize, 'kifo_kagra_drawing_para_prfpmi_normalsize')],
        #[collapse(kifo_kagra_drawing_para_prfpmi_largesize,  'kifo_kagra_drawing_para_prfpmi_largesize')],
        #[collapse(kifo_kagra_drawing_para_drfpmi_normalsize, 'kifo_kagra_drawing_para_drfpmi_normalsize')],
        #[collapse(kifo_kagra_drawing_para_drfpmi_largesize,  'kifo_kagra_drawing_para_drfpmi_largesize')],
        [sg.Text('length PRM-PR2'),sg.Input(key='k_inf_c_length_prm_pr2' ,default_text='1' ,enable_events=True)],
        [sg.Text('length PR2-PR3'),sg.Input(key='k_inf_c_length_pr2_pr3' ,default_text='1' ,enable_events=True)],
        [sg.Text('length PR3-BS'),sg.Input(key='k_inf_c_length_pr3_bs' ,default_text='1' ,enable_events=True)],

        [sg.Text('length BS-ITMX'),sg.Input(key='k_inf_c_length_bs_itmx' ,default_text='1' ,enable_events=True)],
        [sg.Text('length ITMX-ETMX'),sg.Input(key='k_inf_c_length_itmx_etmx' ,default_text='1' ,enable_events=True)],
        [sg.Text('length BS-ITMY'),sg.Input(key='k_inf_c_length_bs_itmy' ,default_text='1' ,enable_events=True)],
        [sg.Text('length ITMY-ETMY'),sg.Input(key='k_inf_c_length_itmy_etmy' ,default_text='1' ,enable_events=True)],


        [sg.Text('length SRM-SR2'),sg.Input(key='k_inf_c_length_srm_sr2' ,default_text='1' ,enable_events=True)],
        [sg.Text('length SR2-SR3'),sg.Input(key='k_inf_c_length_sr2_sr3' ,default_text='1' ,enable_events=True)],
        [sg.Text('length SR3-BS'),sg.Input(key='k_inf_c_length_sr3_bs' ,default_text='1' ,enable_events=True)],
    ]
    extra_ifo_param_tab =  [
                    # 新しい選択ボックス
                    [sg.Combo(('KAGRA','LIGO','VIRGO'), size=(20,1), default_value='KAGRA', readonly=True, key='default_base_interferometer', enable_events=True)
                    ,sg.Combo(('Michelson', 'FPMI', 'PRFPMI', 'PRMI', 'DRFPMI'), size=(20,1), default_value='DRFPMI', readonly=True, key='kifo_interferometrer', enable_events=True)
                    ],
                    # 画像
                    [collapse(para_drawing, 'para_drawing')],
                    # default button
                    #[sg.Button('KAGRA', button_color=('white', 'black'), key='default_setting_kagra')],
                    # plot button
                    #[sg.Button('Michelson', button_color=('white', 'black'), key='kifo_interferometrer_mi')
                    #,sg.Button('FPMI'     , button_color=('white', 'black'), key='kifo_interferometrer_fpmi')
                    #,sg.Button('PRFPMI'   , button_color=('white', 'black'), key='kifo_interferometrer_prfpmi')
                    #,sg.Button('DRFPMI'   , button_color=('white', 'black'), key='kifo_interferometrer_drfpmi')],
                    
                    # laser settings
                    [sg.Button('▽', size=(2,1),button_color=('white', 'black'), key='k_inf_c_open_laser_param_sec'),sg.Text('laser parameter')],
                    [collapse(laser_param_sec,  'laser_param_sec')],
                    # mirror param
                    [sg.Button('▽', size=(2,1),button_color=('white', 'black'), key='k_inf_c_open_mirror_param_sec'),sg.Text('mirror parameter')],
                    [collapse(mirror_param_sec,  'mirror_param_sec')],
                    # length param
                    [sg.Button('▽', size=(2,1),button_color=('white', 'black'), key='k_inf_c_open_length_param_sec'),sg.Text('length parameter')],
                    [collapse(length_param_sec,  'length_param_sec')],
                    # EOM param
                    [sg.Button('▽', size=(2,1),button_color=('white', 'black'), key='k_inf_c_open_eom_param_sec'),sg.Text('eom parameter')],
                    [collapse(eom_param_sec,  'eom_param_sec')],
                    # HOM param
                    [sg.Button('▽', size=(2,1),button_color=('white', 'black'), key='k_inf_c_open_hom_param_sec'),sg.Text('hom parameter')],
                    [collapse(hom_param_sec, 'hom_param_sec')],
                    # future task
                    ]
    return extra_ifo_param_tab


# make_layout

#
kifo_layout         = make_kifo_layout()#[[sg.Column(make_kifo_layout(),scrollable=True)]]
extra_ifo_param_tab = make_extra_ifo_param_tab()
#
#additional_option_tab = [
#                    # output
#                    [sg.Text('Which data you output?')],
#                    [sg.Checkbox('kat file',  key='k_inf_c_output_kat'),
#                     sg.Checkbox('plot data', key='k_inf_c_output_plotdata')]
#]
#
finesse_input_layout_child_3 = [
    [sg.Frame("",[
            [sg.Button('reflesh fig'    ,key='k_inf_reflesh_matplot_fig')
            #,sg.Button('Figure', key='k_inf_c_tmp_script_fig_button')
            #,sg.Button('used last time' ,key='k_inf_latest_kat_script')
            ,sg.Button('run this kat'   ,key='k_inf_run_kat_script')
            ,sg.Button('save temporary' ,key='k_inf_save_tmp_script')
            #,sg.Button('Plot'           ,key='kifo_event')
            ,sg.Checkbox("using toolbar",key='k_inf_use_matplotlib_toolbar',default=False)
            ],
            [sg.Input(key='k_inf_c_finesse_kat_savetitle_box' ,size=(80,1),default_text='test_title' ,enable_events=True)],
            [sg.Multiline(key='k_inf_c_finesse_kat_savememo_box' ,size=(80,1),default_text='test_memo' ,enable_events=True)],
            [sg.Multiline(key='k_inf_c_finesse_kat_box', default_text='メモ', size=(77,16), enable_events=True)],
            [sg.Text('Which data do you export?'),sg.Checkbox('kat file',  key='k_inf_c_output_kat'),sg.Checkbox('plot data', key='k_inf_c_output_plotdata'),sg.Button('export',key="save_script")],
        ], key="finesse_input_layout_child_3_1")
    ,sg.Frame("",[

            # output
            [sg.Button('load temp script',key="load_script")],
            #[sg.Radio('●', 'box1', default=True, key='box1', enable_events=True)],
            #[sg.Radio('●', 'box1', default=False, key='box2', enable_events=True)],
            [sg.Listbox(values=[], size=(30, 20), key="k_inf_c_tmp_script")]
            #,[sg.Sizer(0,50)]
        ], key="finesse_input_layout_child_3_2")
    ]
]

finesse_input_layout_child_2_1 = [
    [sg.Column([[sg.Canvas(key='finesse_input_matplotlib_canvas')],[sg.Canvas(key='controls_cv')]], key="finesse_input_layout_child_2_1_canvas_key",scrollable=True,size=(450,400))
    ]
]

finesse_input_layout_child_2 = [
    [sg.Frame("",finesse_input_layout_child_2_1, key="finesse_input_layout_child_2_1_key")]#作成したGUIを枠線で囲む
]
finesse_input_layout_child_1_1 = [
    # output
    #[sg.Text('Text will be directly input to finesse.')],
    #[sg.Text('Plot test')],
    [sg.Column(finesse_input_layout_child_2, key="finesse_input_layout_child_2_key")],#プロットを表示するところ
    [sg.Column(finesse_input_layout_child_3, key="finesse_input_layout_child_3_key"),],#スクリプトを書くところ
    [sg.Sizer(0,1200)]
]

finesse_input_layout_child_4_1_1 = [
    [sg.Frame("",[
        [sg.Text('Send to Box')
        ,sg.Button('1', key='k_inf_c_tmp_script_send_to_box1')
        ,sg.Button('2', key='k_inf_c_tmp_script_send_to_box2')
        ,sg.Button('3', key='k_inf_c_tmp_script_send_to_box3')
        ,sg.Button('4', key='k_inf_c_tmp_script_send_to_box4')],
        #[sg.Button('send to compare list', key='k_inf_c_tmp_script_send_to_compare_button',size=(13,1))],
        [sg.Input(key='k_inf_c_tmp_script_figure_title' ,size=(25,1),default_text='figure_title' ,enable_events=True)],
        [sg.Listbox(values=[], size=(25, 18), key="k_inf_c_tmp_script_pdlist",enable_events=True)]
    ], key="finesse_input_layout_child_1_2_1_frame_1")]
]

finesse_input_layout_child_4_1_2_1 = [
    [sg.Frame("",[
        [sg.Button('compare', key='k_inf_c_tmp_script_compare_button',size=(7,1)), sg.Button('reset', key='k_inf_c_tmp_script_compare_reset_button',size=(7,1))],
        [sg.Input(key='k_inf_c_tmp_script_compare_axex_title1' ,size=(40,1),default_text='axes_title1' ,enable_events=True)],
        [sg.Listbox(values=[], size=(40, 17), key="k_inf_c_tmp_script_comparelist",enable_events=False)]
        ], key="finesse_input_layout_child_1_2_1_frame_2")]
]
finesse_input_layout_child_4_1_2_2 = [
    [sg.Frame("",[
        [sg.Button('compare', key='k_inf_c_tmp_script_compare_button2',size=(7,1)), sg.Button('reset', key='k_inf_c_tmp_script_compare_reset_button2',size=(7,1))],
        [sg.Input(key='k_inf_c_tmp_script_compare_axex_title2' ,size=(40,1),default_text='axes_title2' ,enable_events=True)],
        [sg.Listbox(values=[], size=(40, 17), key="k_inf_c_tmp_script_comparelist2",enable_events=False)]
        ], key="finesse_input_layout_child_1_2_1_frame_2_2")]
]
finesse_input_layout_child_4_1_2_3 = [
    [sg.Frame("",[
        [sg.Button('compare', key='k_inf_c_tmp_script_compare_button3',size=(7,1)), sg.Button('reset', key='k_inf_c_tmp_script_compare_reset_button3',size=(7,1))],
        [sg.Input(key='k_inf_c_tmp_script_compare_axex_title3' ,size=(40,1),default_text='axes_title3' ,enable_events=True)],
        [sg.Listbox(values=[], size=(40, 17), key="k_inf_c_tmp_script_comparelist3",enable_events=False)]
        ], key="finesse_input_layout_child_1_2_1_frame_2_3")]
]
finesse_input_layout_child_4_1_2_4 = [
    [sg.Frame("",[
        [sg.Button('compare', key='k_inf_c_tmp_script_compare_button4',size=(7,1)), sg.Button('reset', key='k_inf_c_tmp_script_compare_reset_button4',size=(7,1))],
        [sg.Input(key='k_inf_c_tmp_script_compare_axex_title4' ,size=(40,1),default_text='axes_title4' ,enable_events=True)],
        [sg.Listbox(values=[], size=(40, 17), key="k_inf_c_tmp_script_comparelist4",enable_events=False)]
        ], key="finesse_input_layout_child_1_2_1_frame_2_4")]
]

finesse_input_layout_child_4_1_2 = [

    [sg.TabGroup([

            [
                sg.Tab('         1         ', finesse_input_layout_child_4_1_2_1),
                sg.Tab('         2         ', finesse_input_layout_child_4_1_2_2),
                sg.Tab('         3         ', finesse_input_layout_child_4_1_2_3),
                sg.Tab('         4         ', finesse_input_layout_child_4_1_2_4)

            ]
        ], enable_events=True, key="finesse_input_layout_child_4_1_2_tab"
        )
    ],

    
]

finesse_input_layout_child_4_1 = [
    [sg.Column(finesse_input_layout_child_4_1_1, key="finesse_input_layout_child_4_1_1_key")
    ,sg.Column(finesse_input_layout_child_4_1_2, key="finesse_input_layout_child_4_1_2_key")
    ],
    [sg.Sizer(0,0)]
]

treedata = sg.TreeData()
default_path = "../"
util_func_pysimplegui.add_files_in_folder("", default_path,treedata)



finesse_input_layout_child_4_2 = [
    [sg.TabGroup([

            [
                sg.Tab('browser_tab', [
                    [sg.Frame("",[
                        [sg.Text('File and folder browser Test')],
                        [sg.Input(key='browser_path' ,size=(50,1),default_text=default_path, readonly=False,enable_events=True),sg.Button('open floder',key="open_folder"),sg.Button('browser',key="browser")],
                        [sg.Tree(data=treedata,
                            headings=['Size', ],
                            auto_size_columns=True,
                            num_rows=10,
                            col0_width=50,
                            key='browser_key',
                            show_expanded=False,
                            enable_events=True),
                        ],
                    [sg.Button('import .kat file',key="select_file"), sg.Button('Cancel')]
                    ])]]),
                sg.Tab('matplotlib code', [
                    [sg.Multiline(size=(70, 35),key="matplotlib_code", enter_submits=True),
                    sg.Button('SEND',key="run_matplotlib_code")]
                    ])
            ]
        ], enable_events=True)
        ]
]

finesse_input_layout_child_4 = [
    [sg.Column(finesse_input_layout_child_4_1, key="finesse_input_layout_child_4_1_key")],
    [sg.Column(finesse_input_layout_child_4_2, key="finesse_input_layout_child_4_2_key")]
]
finesse_input_layout_child_1 = [
    [sg.Column(finesse_input_layout_child_1_1, key="finesse_input_layout_child_1_1_key")
    ,sg.Column(finesse_input_layout_child_4, key="finesse_input_layout_child_4_key")]
    
]

finesse_input_layout_child_0 = [
    [sg.Column(finesse_input_layout_child_1, key="finesse_input_layout_child_1_key",scrollable=True)]
]

finesse_input_layout = [
    [sg.Frame("",finesse_input_layout_child_0, key="finesse_input_layout_child_0")]#作成したGUIを枠線で囲む
] 

memo_option_tab = [
                    # output
                    [sg.Text('Memo')],
                    [sg.Multiline(key='k_inf_c_memo_box', default_text='メモ', size=(100,100), enable_events=True)]
]
layout_child_0 = [
    # Tab Group
    [sg.TabGroup([

            [
                sg.Tab('DRFPMI', kifo_layout,key="DRFPMI_tab"),
                sg.Tab('IFO_param', extra_ifo_param_tab),
                #sg.Tab('OPTION', additional_option_tab),
                sg.Tab('MEMO', memo_option_tab)
            ]
        ], enable_events=True, key="kselected_tab"
        )
    ],

]
layout_child_2 = [
    [sg.Column(layout_child_0, key="layout_child_0_key",size=(750,2000))],
    [sg.Sizer(750,2000)]
]
layout_child_1 = [

    [sg.Column(layout_child_2, key="layout_child_2_key",scrollable=True)]
]

#layout_child_1 = [[sg.Sizer(0,2000), sg.Column([[sg.Sizer(700,0)]] + layout_child_1, element_justification='c', pad=(0,0))]]
#layout = [
#    [sg.Frame("",L1, key="layoutkey",size=(2000,800))]#作成したGUIを枠線で囲む
#] 
layout = [
    [sg.Frame("",layout_child_1, key="layoutkey",size=(600,1400))]#作成したGUIを枠線で囲む
]

"""
    [sg.Frame("",[
        [sg.Button('GUI window'),sg.Button('Plot window'),sg.Button('Kat window')],
            #[sg.Button('GUI window')],
            #[sg.Button('Plot window')],
            #[sg.Button('Kat window')],
        ], key="window_selector",background_color="White")],

    [sg.Column(layout, key="colkey")
    ,sg.Column(finesse_input_layout, key="finesse_input_layout_key")
    ],#枠戦で囲んだGUIをスクロールできるようにする]#枠戦で囲んだGUIをスクロールできるようにする]
"""
col = [

]

t1 =  [[sg.Column(layout, key="colkey")]]
t2 =  [[sg.Column(finesse_input_layout, key="finesse_input_layout_key")]]
Debug_tab = Debug_window_layout.make_window()#get_debug_window()
#all_layout_child= [[sg.Column(col, key="all_layout_child_key")]]
Transparent_tab = [[sg.Text("",key="transparent_text")]]
all_layout_child= [[sg.TabGroup([

            [
                sg.Tab('Base model setting',t1),
                sg.Tab('Plotting',t2),
                sg.Tab('Debug tab',Debug_tab),
                #sg.Tab('Transparent tab',Transparent_tab,background_color="#ff1493")
            ]
        ], enable_events=True, key="kselected_window_tab"
        )
    ]
]
all_layout = [
    [sg.Frame("",all_layout_child, key="all_layout_key",size=(1400,500))]#作成したGUIを枠線で囲む
]

def make_window():
    window = sg.Window('finesse GUI', all_layout, default_element_size=(15,1), finalize=True, size=(1280,720),transparent_color="#814721",resizable=True, auto_size_text=True,margins=(0,0))
    #window = sg.Window('finesse GUI', all_layout, default_element_size=(15,1), finalize=True, size=(1400,1400), auto_size_text=True)
    return window

