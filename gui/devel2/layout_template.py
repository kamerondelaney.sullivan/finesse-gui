import PySimpleGUI as sg

def Return_right_layout():

    Mysize = (50,100)

    layout_right_1_1 = [
        [sg.Text("PD")],
        [sg.Multiline(size=(80,20),expand_x=True,expand_y=True)],
    ]
    layout_right_1 = [
        [sg.Column(layout_right_1_1,size=Mysize,scrollable=True,expand_x=True,expand_y=True)]
    ]
    layout_right_2_1 = [
        [sg.Text("Xaxis")],
        [sg.Multiline(size=(80,20),expand_x=True,expand_y=True)],
    ]
    layout_right_2 = [
        [sg.Column(layout_right_2_1,size=Mysize,scrollable=True,expand_x=True,expand_y=True)]
    ]
    layout_right_3_1 = [
        [sg.Text("Plot setting")],
        [sg.Multiline(size=(80,20),expand_x=True,expand_y=True)],
    ]
    layout_right_3= [
        [sg.Column(layout_right_3_1,size=Mysize,scrollable=True,expand_x=True,expand_y=True)]
    ]
    layout_right_4 = [
        [sg.Column([
            [sg.Frame("",layout_right_1,size=(640,120),expand_x=True,expand_y=True)],
            [sg.Frame("",layout_right_2,size=(640,240),expand_x=True,expand_y=True)],
            [sg.Frame("",layout_right_3,size=(640,240),expand_x=True,expand_y=True)]
        ],expand_x=True,expand_y=True)]
    ]

    return layout_right_4
