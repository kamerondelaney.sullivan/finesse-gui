import PySimpleGUI as sg

def Get_DRFPMI_base_model():
    base_model   = (
    """
    # ======== Constants ========================
    const fsb1 16.881M
    const fsb2 45.0159M
    const mfsb1 -16.881M
    const mfsb2 -45.0159M
    const a 0.686
    const pi 3.1415

    # おそらく間違っていると思われるもの
    attr REFL_gouy_tuner g 0.0
    attr POP_gouy_tuner g 0.0
    attr POP2_gouy_tuner g 0.0


    # ======== Input optics =====================
    l i1 1 0 n0
    s s_eo0 0 n0 n_eo1
    mod eom1 $fsb1 0.3 1 pm n_eo1 n_eo2
    s s_eo1 0 n_eo2 n_eo3
    mod eom2 $fsb2 0.3 1 pm n_eo3 n_eo4
    s s_eo2 0 n_eo4 n_eo5
    bs REFL_dummy_mirror 0.001 0.009 0 0 n_eo5 dump REFL_dummy_mirror_out REFL
    s REFL_gouy_tuner 0 REFL_dummy_mirror_out REFL_gouy_tuner_out

    ## ======= PRC each mirror loss 45ppm =======
    # PRC

    m1 PRM 0.1 45e-6 0 REFL_gouy_tuner_out npr1
    s sLpr1 14.7615 npr1 npr2
    bs1 PR2 500e-6 45e-6 0 $a npr3 npr2 POP_gouy_tuner_in POP2_gouy_tuner_in
    s sLpr2 11.0661 npr3 npr4
    bs1 PR3 50e-6 45e-6 0 $a dump dump npr4 npr5
    s sLpr3 15.7638 npr5 npr6
    s POP_gouy_tuner 0 POP_gouy_tuner_in POP
    s POP2_gouy_tuner 0 POP2_gouy_tuner_in POP2

    # Michelson
    bs MIBS 0.5 0.5 0 45 npr6 n2 n3 n4
    s lx 26.4018 n3 nitx1 %26.6649-thickness*1.754
    s ly 23.072 n2 nity1  %23.3351-thickness*1.754

    # ======== Thick ITMs ======================
    m IXAR 0     1     0 nitx1 nitx2
    s thick_IX 0.15 1.754 nitx2 nx1

    m IYAR 0     1     0 nity1 nity2
    s thick_IY 0.15 1.754 nity2 ny1

    # X arm
    m ITMX 0.996 0.004 0 nx1 nx2
    s sx1 3000 nx2 nx3
    m ETMX 0.999995 5e-06 0 nx3 netx1

    # Y arm
    m ITMY 0.996 0.004 90 ny1 ny2
    s sy1 3000 ny2 ny3
    m ETMY 0.999995 5e-06 90 ny3 nety1

    # ======== Thick ETMs ======================
    s thick_EX 0.15 1.754 netx1 netx2
    m EXAR 0     1     0  netx2 TMSX_gouy_tuner_in
    s TMSX_gouy_tuner 0 TMSX_gouy_tuner_in TMSX
    attr TMSX_gouy_tuner g 0.0

    s thick_EY 0.15 1.754 nety1 nety2
    m EYAR 0     1     0  nety2 TMSY_gouy_tuner_in
    s TMSY_gouy_tuner 0 TMSY_gouy_tuner_in TMSY
    attr TMSY_gouy_tuner g 0.0

    # ========= SRC each mirror loss 45ppm =======
    s sLsr3 15.7386 n4 nsr5
    bs1 SR3 50e-6 45e-6 0 $a nsr5 nsr4 dump dump
    s sLsr2 11.1115 nsr4 nsr3
    bs1 SR2 500e-6 45e-6 0 $a nsr2 nsr3 POS_gouy_tuner_in dump
    s POS_gouy_tuner 0 POS_gouy_tuner_in POS
    attr POS_gouy_tuner g 0.0
    s sLsr1 14.7412 nsr2 nsr1
    m1 SRM 0.3 0e-6 0 nsr1 AS_gouy_tuner_in
    s AS_gouy_tuner 0 AS_gouy_tuner_in AS
    attr AS_gouy_tuner g 0.0
    maxtem 1
    yaxis abs:deg
    """
            )
    return base_model

def Get_PRMI_base_model():
        text = """
l i1 10.0 0.0 0.0 n0
s s_eo0 0.0 n0 n_eo1
mod eom1 16881000.0 0.3 3 pm 0.0 n_eo1 n_eo2
s s_eo1 0.0 n_eo2 n_eo3
mod eom2 45015900.0 0.3 3 pm 0.0 n_eo3 n_eo4
s s_eo2 0.0 n_eo4 n_eo5

bs REFL_dummy_mirror 0.0001 0.9999 0 0.0 n_eo5 dump REFL_dummy_space_in REFL_gouy_tuner_in #pickoffのために適当に反射率を設定した
s REFL_dummy_space 0 REFL_dummy_space_in REFL_dummy_space_out
s REFL_gouy_tuner 0.0 REFL_gouy_tuner_in REFL

# IFI to IMMT1 5.386?
## IMMT
bs IMMT1 0.9922 0.0078 0.0 3.6 REFL_dummy_space_out node2 dump dump
attr IMMT1 Rcx -8.91
s simmt1 3.105 node2 node3
bs IMMT2 0.9922 0.0078 0.0 -3.6 node3 node4 dump dump
attr IMMT2 Rcy 14.005
s simmt2 4.8416 node4 PRMDUMMY_in #5.017-0.1*100mm

##
m PRMDUMMY 0.0 1.0 0.0 PRMDUMMY_in PRMDUMMY_1
attr PRMDUMMY Rcx 0.0
attr PRMDUMMY Rcy 0.0
s thick_PRM 0.1 1.45 PRMDUMMY_1 PRMDUMMY_out

m PRM 0.8999550000000001 0.1 0.0 PRMDUMMY_out npr1
attr PRM Rcx -458.1285
attr PRM Rcy -458.1285
s sLpr1 14.7615 npr1 npr2
bs PR2 0.9994550000000001 0.0005 0.0 0.686 npr3 npr2 POP_gouy_tuner_in POP2_gouy_tuner_in
attr PR2 Rcx -3.0764
attr PR2 Rcy -3.0764
s sLpr2 11.0661 npr3 npr4
bs PR3 0.999905 5e-05 0.0 0.686 dump dump npr4 npr5
attr PR3 Rcx -24.9165
attr PR3 Rcy -24.9165
s sLpr3 15.7638 npr5 npr6

s POP_gouy_tuner 0.0 POP_gouy_tuner_in POP

s POP2_gouy_tuner 0.0 POP2_gouy_tuner_in POP2

bs MIBS 0.5 0.5 0.0 45.0 npr6 n2 n3 n4
attr MIBS Rcx 0.0
attr MIBS Rcy 0.0
s lx 26.4018 n3 nitx1
s ly 23.072 n2 nity1

m IXAR 0.0 1.0 0.0 nitx1 nitx2
attr IXAR Rcx 0.0
attr IXAR Rcy 0.0
s thick_IX 0.15 1.754 nitx2 nx1

m IYAR 0.0 1.0 0.0 nity1 nity2
attr IYAR Rcx 0.0
attr IYAR Rcy 0.0
s thick_IY 0.15 1.754 nity2 ny1

m ITMX 0.995955 0.004 0.0 nx1 TMSX_gouy_tuner_in
attr ITMX Rcx -1900.0
attr ITMX Rcy -1900.0

m ITMY 0.995955 0.004 90.0 ny1 TMSY_gouy_tuner_in
attr ITMY Rcx -1900.0
attr ITMY Rcy -1900.0
#attr ETMX Rc 1900.0 #ETMはなくした
#attr ETMY Rc 1900.0

s TMSX_gouy_tuner 0.0 TMSX_gouy_tuner_in TMSX

s TMSY_gouy_tuner 0.0 TMSY_gouy_tuner_in TMSY

s sLsr3 15.7386 n4 nsr5
bs SR3 1.0 0.0 0.0 0.686 nsr5 nsr4 dump dump
attr SR3 Rcx 24.9165
attr SR3 Rcy 24.9165
s sLsr2 11.1115 nsr4 nsr3
bs SR2 1.0 0.0 0.0 0.686 nsr2 nsr3 POS_gouy_tuner_in dump
attr SR2 Rcx -2.9872
attr SR2 Rcy -2.9872
s POS_gouy_tuner 0.0 POS_gouy_tuner_in POS
s sLsr1 14.7412 nsr2 nsr1
m SRM 0.0 1.0 0.0 nsr1 SRMDUMMY_1
attr SRM Rcx 458.1285
attr SRM Rcy 458.1285
# 厚さなど
s thick_SRM 0.1 1.45 SRMDUMMY_1 SRMDUMMY_out
attr SRMDUMMY Rcx 0.0
attr SRMDUMMY Rcy 0.0
m SRMDUMMY 0.0 1.0 0.0 SRMDUMMY_out AS_gouy_tuner_in



s AS_gouy_tuner 0.0 AS_gouy_tuner_in AS

cav PRX PRM npr1 ITMX nx1
cav PRY PRM npr1 ITMY ny1

#cp PRX x z
#cp PRY x z
maxtem 1
yaxis abs:deg
"""

        return text

def Get_FPMI_base_model():
        text = """
l i1 2.5 0.0 0.0 n0
s s_eo0 0.0 n0 n_eo1
mod eom1 16881000.0 0.3 3 pm 0.0 n_eo1 n_eo2
s s_eo1 0.0 n_eo2 n_eo3
mod eom2 45015900.0 0.3 3 pm 0.0 n_eo3 n_eo4
s s_eo2 0.0 n_eo4 n_eo5

bs REFL_dummy_mirror 0.0001 0.9999 0 0.0 n_eo5 dump REFL_dummy_space_in REFL_gouy_tuner_in #pickoffのために適当に反射率を設定した
s REFL_dummy_space 0 REFL_dummy_space_in REFL_dummy_space_out
s REFL_gouy_tuner 0.0 REFL_gouy_tuner_in REFL

# IFI to IMMT1 5.386?
## IMMT
bs IMMT1 0.9922 0.0078 0.0 3.6 REFL_dummy_space_out node2 dump dump
attr IMMT1 Rcx -8.91
s simmt1 3.105 node2 node3
bs IMMT2 0.9922 0.0078 0.0 -3.6 node3 node4 dump dump
attr IMMT2 Rcy 14.005
s simmt2 4.8416 node4 PRMDUMMY_in #5.017-0.1*100mm

# 厚さなど
m PRMDUMMY 0.0 1.0 0.0 PRMDUMMY_in PRMDUMMY_1
attr PRMDUMMY Rcx 0.0
attr PRMDUMMY Rcy 0.0
s thick_PRM 0.1 1.45 PRMDUMMY_1 PRMDUMMY_out

m PRM 0 1 0.0 PRMDUMMY_out npr1
attr PRM Rcx -458.1285
attr PRM Rcy -458.1285
s sLpr1 14.7615 npr1 npr2
bs PR2 0.9994550000000001 0.0005 0.0 0.686 npr3 npr2 POP_gouy_tuner_in POP2_gouy_tuner_in
attr PR2 Rcx -3.0764
attr PR2 Rcy -3.0764
s sLpr2 11.0661 npr3 npr4
bs PR3 0.999905 5e-05 0.0 0.686 dump dump npr4 npr5
attr PR3 Rcx -24.9165
attr PR3 Rcy -24.9165
s sLpr3 15.7638 npr5 npr6

s POP_gouy_tuner 0.0 POP_gouy_tuner_in POP

s POP2_gouy_tuner 0.0 POP2_gouy_tuner_in POP2

bs MIBS 0.5 0.5 0.0 45.0 npr6 n2 n3 n4
attr MIBS Rcx 0.0
attr MIBS Rcy 0.0
s lx 26.4018 n3 nitx1
s ly 23.072 n2 nity1

m IXAR 0.0 1.0 0.0 nitx1 nitx2
attr IXAR Rcx 0.0
attr IXAR Rcy 0.0
s thick_IX 0.15 1.754 nitx2 nx1

m IYAR 0.0 1.0 0.0 nity1 nity2
attr IYAR Rcx 0.0
attr IYAR Rcy 0.0
s thick_IY 0.15 1.754 nity2 ny1

m ITMX 0.995955 0.004 0.0 nx1 nx2
attr ITMX Rcx -1900.0
attr ITMX Rcy -1900.0
s sx1 3000 nx2 nx3
m ETMX 0.999995 5e-06 0 nx3 netx1
s thick_EX 0.15 1.754 netx1 netx2
m EXAR 0     1     0  netx2 TMSX_gouy_tuner_in
s TMSX_gouy_tuner 0 TMSX_gouy_tuner_in TMSX
attr ETMX Rc 1900.0

m ITMY 0.995955 0.004 90.0 ny1 ny2
attr ITMY Rcx -1900.0
attr ITMY Rcy -1900.0
s sy1 3000 ny2 ny3
m ETMY 0.999995 5e-06 90 ny3 nety1
attr ETMY Rc 1900.0

s thick_EY 0.15 1.754 nety1 nety2
m EYAR 0     1     0  nety2 TMSY_gouy_tuner_in
s TMSY_gouy_tuner 0 TMSY_gouy_tuner_in TMSY


s sLsr3 15.7386 n4 nsr5
bs SR3 1.0 0.0 0.0 0.686 nsr5 nsr4 dump dump
attr SR3 Rcx 24.9165
attr SR3 Rcy 24.9165
s sLsr2 11.1115 nsr4 nsr3
bs SR2 1.0 0.0 0.0 0.686 nsr2 nsr3 POS_gouy_tuner_in dump
attr SR2 Rcx -2.9872
attr SR2 Rcy -2.9872
s POS_gouy_tuner 0.0 POS_gouy_tuner_in POS
s sLsr1 14.7412 nsr2 nsr1
m SRM 0.0 1.0 0.0 nsr1 SRMDUMMY_1
attr SRM Rcx 458.1285
attr SRM Rcy 458.1285
# 厚さなど
s thick_SRM 0.1 1.45 SRMDUMMY_1 SRMDUMMY_out
attr SRMDUMMY Rcx 0.0
attr SRMDUMMY Rcy 0.0
m SRMDUMMY 0.0 1.0 0.0 SRMDUMMY_out AS_gouy_tuner_in



s AS_gouy_tuner 0.0 AS_gouy_tuner_in AS

cav XARM ITMX nx2 ETMX nx3
cav YARM ITMY ny2 ETMY ny3

#cav PRX PRM npr1 ITMX nx1
#cav PRY PRM npr1 ITMY ny1

#cp PRX x z
#cp PRY x z
maxtem 1
yaxis abs:deg
"""

        return text

def Get_PRFPMI_base_model():
        text = """
l i1 2.5 0.0 0.0 n0
s s_eo0 0.0 n0 n_eo1
mod eom1 16881000.0 0.3 3 pm 0.0 n_eo1 n_eo2
s s_eo1 0.0 n_eo2 n_eo3
mod eom2 45015900.0 0.3 3 pm 0.0 n_eo3 n_eo4
s s_eo2 0.0 n_eo4 n_eo5

bs REFL_dummy_mirror 0.0001 0.9999 0 0.0 n_eo5 dump REFL_dummy_space_in REFL_gouy_tuner_in #pickoffのために適当に反射率を設定した
s REFL_dummy_space 0 REFL_dummy_space_in REFL_dummy_space_out
s REFL_gouy_tuner 0.0 REFL_gouy_tuner_in REFL

# IFI to IMMT1 5.386?
## IMMT
bs IMMT1 0.9922 0.0078 0.0 3.6 REFL_dummy_space_out node2 dump dump
attr IMMT1 Rcx -8.91
s simmt1 3.105 node2 node3
bs IMMT2 0.9922 0.0078 0.0 -3.6 node3 node4 dump dump
attr IMMT2 Rcy 14.005
s simmt2 4.8416 node4 PRMDUMMY_in #5.017-0.1*100mm

# 厚さなど

##
m PRMDUMMY 0.0 1.0 0.0 PRMDUMMY_in PRMDUMMY_1
attr PRMDUMMY Rcx 0.0
attr PRMDUMMY Rcy 0.0
s thick_PRM 0.1 1.45 PRMDUMMY_1 PRMDUMMY_out

m PRM 0.8999550000000001 0.1 0.0 PRMDUMMY_out npr1
attr PRM Rcx -458.1285
attr PRM Rcy -458.1285
s sLpr1 14.7615 npr1 npr2
bs PR2 0.9994550000000001 0.0005 0.0 0.686 npr3 npr2 POP_gouy_tuner_in POP2_gouy_tuner_in
attr PR2 Rcx -3.0764
attr PR2 Rcy -3.0764
s sLpr2 11.0661 npr3 npr4
bs PR3 0.999905 5e-05 0.0 0.686 dump dump npr4 npr5
attr PR3 Rcx -24.9165
attr PR3 Rcy -24.9165
s sLpr3 15.7638 npr5 npr6

s POP_gouy_tuner 0.0 POP_gouy_tuner_in POP

s POP2_gouy_tuner 0.0 POP2_gouy_tuner_in POP2

bs MIBS 0.5 0.5 0.0 45.0 npr6 n2 n3 n4
attr MIBS Rcx 0.0
attr MIBS Rcy 0.0
s lx 26.4018 n3 nitx1
s ly 23.072 n2 nity1

m IXAR 0.0 1.0 0.0 nitx1 nitx2
attr IXAR Rcx 0.0
attr IXAR Rcy 0.0
s thick_IX 0.15 1.754 nitx2 nx1

m IYAR 0.0 1.0 0.0 nity1 nity2
attr IYAR Rcx 0.0
attr IYAR Rcy 0.0
s thick_IY 0.15 1.754 nity2 ny1

m ITMX 0.995955 0.004 0.0 nx1 nx2
attr ITMX Rcx -1900.0
attr ITMX Rcy -1900.0
s sx1 3000 nx2 nx3
m ETMX 0.999995 5e-06 0 nx3 netx1
s thick_EX 0.15 1.754 netx1 netx2
m EXAR 0     1     0  netx2 TMSX_gouy_tuner_in
s TMSX_gouy_tuner 0 TMSX_gouy_tuner_in TMSX
attr ETMX Rc 1900.0

m ITMY 0.995955 0.004 90.0 ny1 ny2
attr ITMY Rcx -1900.0
attr ITMY Rcy -1900.0
s sy1 3000 ny2 ny3
m ETMY 0.999995 5e-06 90 ny3 nety1
attr ETMY Rc 1900.0

s thick_EY 0.15 1.754 nety1 nety2
m EYAR 0     1     0  nety2 TMSY_gouy_tuner_in
s TMSY_gouy_tuner 0 TMSY_gouy_tuner_in TMSY


s sLsr3 15.7386 n4 nsr5
bs SR3 1.0 0.0 0.0 0.686 nsr5 nsr4 dump dump
attr SR3 Rcx 24.9165
attr SR3 Rcy 24.9165
s sLsr2 11.1115 nsr4 nsr3
bs SR2 1.0 0.0 0.0 0.686 nsr2 nsr3 POS_gouy_tuner_in dump
attr SR2 Rcx -2.9872
attr SR2 Rcy -2.9872
s POS_gouy_tuner 0.0 POS_gouy_tuner_in POS
s sLsr1 14.7412 nsr2 nsr1
m SRM 0.0 1.0 0.0 nsr1 SRMDUMMY_1
attr SRM Rcx 458.1285
attr SRM Rcy 458.1285
# 厚さなど
s thick_SRM 0.1 1.45 SRMDUMMY_1 SRMDUMMY_out
attr SRMDUMMY Rcx 0.0
attr SRMDUMMY Rcy 0.0
m SRMDUMMY 0.0 1.0 0.0 SRMDUMMY_out AS_gouy_tuner_in



s AS_gouy_tuner 0.0 AS_gouy_tuner_in AS

cav XARM ITMX nx2 ETMX nx3
cav YARM ITMY ny2 ETMY ny3

cav PRX PRM npr1 ITMX nx1
cav PRY PRM npr1 ITMY ny1

#cp PRX x z
#cp PRY x z
maxtem 1
yaxis abs:deg
"""
        return text
