import sys
import PySimpleGUI as sg
sys.path.append('../')
from Utility import Asc_base_model

def Return_left_layout_0():
    Mysize = (50,720)
    Components_list = ["mirror","space","beam splitter","laser","modulator","commands"]
    Hermite_gauss_extension = ["cav","maxtem"]

    #Mysize = (1,240)
    Base_models = ["FPMI","PRMI","PRFPMI"]
    Base_detectors = ["KAGRA"]
    #TEM_list = ["OFF","0","1","2"]
    #Mirror_Beam_splitter_list = ["ITMX","ITMY"]
    #Mirror_parameter_list = ["R"]
    #Category_dict = {
    #    Categories[0]:[""],
    #    Categories[1]:Mirror_Beam_splitter_list
    #}
    layout_left_1_1 = [sg.Tab("base",[[sg.Image('../fig/DRFPMI_picture_normal.png', key='kifo_imageContainer1',  size=(400,300),background_color="White")]])]
    
    layout_left_1_2 = [sg.Tab("matplot0",[[sg.Image('../fig/DRFPMI_picture_normal.png', key='-IMAGE-',  size=(400,300),background_color="White")]])]
    
    layout_left_1 = [
        [sg.Text("~~ Base Settings ~~",background_color="#444E2B",text_color="white",expand_x=True,justification="center")],
        [sg.Text("・Base Detector"),sg.Combo(Base_detectors,default_value=Base_detectors[0],key="base_setting_base_detector",readonly=True),sg.Text("Base Model"),sg.Combo(Base_models,default_value=Base_models[0],key="base_setting_base_model",readonly=True),sg.Button("update",button_color="silver",key="base_setting_base_update")],
        [sg.TabGroup([layout_left_1_1,layout_left_1_2])],
        #[[sg.Image('../fig/DRFPMI_picture_normal.png', key='kifo_imageContainer1',  size=(400,300),background_color="White")]]
    ]

    layout_left_0 = [
        [sg.Column(layout_left_1,size=Mysize,scrollable=True,expand_x=True,expand_y=True)]
    ]
    return layout_left_0

def Return_left_layout_1():
    layout_left_1 = [
        [sg.Text("~~ Extra Settings ~~",background_color="#444E2B",text_color="white",expand_x=True,justification="center")],
        [sg.Text("・Components")],
        [sg.Combo([""],size=(30,15),enable_events=True       ,key="layout_left_1_Extra_Settings_class_name",default_value=[""][0],expand_x=True,readonly=True)],
        [sg.Combo([""],size=(30,15),enable_events=True       ,key="layout_left_1_Extra_Settings_component_name",default_value=[""][0],expand_x=True,readonly=True)],
        [sg.Combo([""],size=(30,15),enable_events=True       ,key="layout_left_1_Extra_Settings_atrribute",default_value=[""][0],expand_x=True,readonly=True)],
        [sg.Multiline(size=(15,2)                           ,key="layout_left_1_Extra_Settings_parameter_multiline",expand_x=True)],
        [sg.Button("Modify",button_color="silver"           ,key="layout_left_1_Extra_Settings_parameter_modify",expand_x=True)],
        [sg.Text("・Max TEM"),sg.Combo(["off","1","2","3","4","5","6","7","8","9","10"],key="layout_left_1_Extra_Settings_maxtem",default_value="1",readonly=True),sg.Button("update",key="layout_left_1_Extra_Settings_maxtem_modify")],
    ]
    return layout_left_1

def Return_left_layout_2():

    layout_left_2_1 = [sg.Tab("added line",[[sg.Multiline(size=(10,10),expand_x=True,key="bp_check_added_pd_multiline",expand_y=True)]])]
    
    layout_left_2_2 = [sg.Tab("matplot",
    [
        [sg.Image('../fig/DRFPMI_picture_normal.png', key='-IMAGE2-',  size=(500,500),background_color="White")],
        [sg.Text("Figure title"),sg.Combo([],default_value="test figure title",size = (20,1),key="bp_check_figure_title")],
        [sg.Text("xlabels")     ,sg.Combo(["[m]"],default_value="[m]"    ,size = (20,1),key="bp_check_xlabels",readonly=True)
        ,sg.Text("ylabels")     ,sg.Combo([],default_value="gouy phase[deg]"    ,size = (20,1),key="bp_check_ylabels")],
        [sg.Text("Plot scale")  ,sg.Combo(["linear-linear","linear-log","log-linear","log-log"],default_value="linear-linear",size = (20,1),key="bp_check_plot_scale",readonly=True)],
        [sg.Button("Open interactive window",button_color="silver",expand_x=True,size=(None,1),key="bp_check_open_interactive_window",enable_events=True)]
    ])]

    layout_left_2_3 = [sg.Tab("Result infomation",[[sg.Multiline(size=(10,10),expand_x=True,key="bp_check_result_information",expand_y=True)]])]
    
    layout_left_2_4 = [sg.Tab("BP model",[[sg.Multiline(size=(10,10),expand_x=True,key="bp_check_bp_model_multiline",expand_y=True)]])]
    
    layout_left_2 = [
        [sg.Text("~~ Beam Parameter Check ~~",background_color="#444E2B",text_color="white",expand_x=True,justification="center")],
        [sg.Text("Length to check"),sg.Combo(["lx","ly","Lx","Ly"],size=(15,1),key="bp_check_select_length",default_value="lx",expand_x=True)
        ,sg.Text("Parameters to check"),sg.Combo(["w","w0","z","zr","g","r","q"],size=(5,1),key="bp_check_select_parameter",default_value="g",enable_events=True)
        ,sg.Text("direction"),sg.Combo(["x","y"],size=(5,1),key="bp_check_select_direction",default_value="x")],

        [sg.Button("Check Beam Parameter",button_color="silver",key="bp_check_check_beam_parameter",expand_x=True)],
        [sg.TabGroup([layout_left_2_1,layout_left_2_2,layout_left_2_3,layout_left_2_4],size=(580,None))],
    ]
    return layout_left_2

def Return_left_layout_3():
    layout_left_3 = [
        #[sg.Image('../fig/DRFPMI_picture_normal.png', key='-IMAGE3-',  size=(400,300),background_color="White")],
    ]
    return layout_left_3

def Return_left_layout_4():
    Categories = ["Mirror","Beam splitter","space","laser","modulator","commands"]
    layout_left_4 = [
        [sg.Text("Filter"),sg.Button("All",button_color="silver",key="base_model_filter_all",size=(10,1)),sg.Combo(Categories,default_value=Categories[0],key="base_model_filter_categories",enable_events=True,readonly=True),sg.Combo(Categories,default_value=Categories[0],key="base_model_filter_items",size=(20,1),enable_events=True,readonly=True,expand_x=True),sg.Button("Others",button_color="silver",key="base_model_filter_others",size=(10,1))],
        [sg.Text("Currently displayed"),sg.Multiline(size=(20,1),default_text="All kat script",key="base_model_filter_current_displayed")],
        [sg.Multiline(size=(50,50),key="base_model_multiline",expand_x=True,expand_y=True,default_text=Asc_base_model.Get_PRMI_base_model())],
        [sg.Button("Update Base Model",button_color="silver",key="base_model_update_base_model",expand_x=True,expand_y=False)],
        [sg.Button("Default Base Model",button_color="silver",key="base_model_default_base_model",expand_x=True,expand_y=False)],
    ]
    return layout_left_4

def Return_left_layout_5():
    layout_left_5 = [
        [sg.Multiline(size=(50,50),key="layout_left_5_stdout_multiline",expand_x=True,expand_y=True,echo_stdout_stderr=True)]
    ]
    return layout_left_5