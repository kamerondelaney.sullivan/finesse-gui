#!/bin/bash
#
# This code is called by medm command.
# See this page as example : http://gwwiki.icrr.u-tokyo.ac.jp/JGWwiki/KAGRA/Commissioning/NoiseBudgetter
#
# cd /users/MIF/gw-finesse/gui/bin
# conda activate mifsim37
# ./run_finesse.sh
#

source /kagra/apps/etc/conda3-user-env_deb10.sh
conda activate finessegui
#cd /users/MIF/gw-finesse/gui/devel2/
cd /users/MIF/finesse-gui/gui/devel2/
python run_gui.py
conda deactivate finessegui

